import argparse

import numpy as np
#from tfDeepNLP.utils import vocab_utils

SPECIAL_TOKENS = ["$UNK$"]
#SPECIAL_TOKENS = ["$UNK$", "$NUM$"]
#SPECIAL_TOKENS = [vocab_utils.UNK, vocab_utils.SOS, vocab_utils.EOS ]

def get_glove_vocab(filename):
  vocab_emb = dict()

  # is_a_number_pat = re.compile(r'[-./,\s]')
  #
  # def is_a_number(w):
  #   return re.sub(is_a_number_pat, '', w).isdigit()

  with open(filename, encoding='utf-8') as f:
    for _line in f:
      line = _line.strip().split(' ')
      token = line[0]
      #if not is_a_number(token):

      if (len(line[1:]) % 50) != 0 or not token:
        print("The following embedding line did not parse: ", _line)
        continue

      vector = np.asarray([float(x) for x in line[1:]])
      vocab_emb[token] = vector

  return vocab_emb

def extract_glove_to_npy_data_and_text_vocab(glove_file_name, output_directory, output_suffix, filter_vocab_file_name=None):
  print("Loading glove...")
  glove_vocab = get_glove_vocab(glove_file_name)
  filtered_glove_vocab = {}

  glove_dim = len(glove_vocab["and"])
  print("glove_dim: ", glove_dim)

  glove_embeddings = []
  glove_words = []

  print("Filtering oov words")

  if filter_vocab_file_name is not None:
    with open(filter_vocab_file_name, 'r', encoding="utf-8") as file:
      filter_vocab = file.readlines()

      filter_vocab_size = len(filter_vocab)
      for token in filter_vocab:
        token = token[:-1]
        if len(token) > 0 and token in glove_vocab:
          filtered_glove_vocab[token] = glove_vocab[token]

    percent_overlap = ((len(filtered_glove_vocab) / filter_vocab_size) * 100)
    print("Vocab overlap is: %f %%" % percent_overlap)

  else:
    filtered_glove_vocab = glove_vocab

  for special_token in enumerate(SPECIAL_TOKENS):
    if special_token in filtered_glove_vocab:
      filtered_glove_vocab.remove(special_token)

  glove_words += filtered_glove_vocab.keys()
  glove_embeddings += filtered_glove_vocab.values()

  print("Adding extra tokens...")

  def create_one_hot_vector(index):
    vector = np.zeros([glove_dim]) # [0] * model.vector_size+len(SPECIAL_TOKENS)
    vector[index] = 1
    return vector

  special_token_vectors = [create_one_hot_vector(token_number + (glove_dim-len(SPECIAL_TOKENS))) for token_number, _ in
                           enumerate(SPECIAL_TOKENS)]

  glove_embeddings = special_token_vectors + glove_embeddings
  glove_words = SPECIAL_TOKENS + glove_words

  print("Writing refined embeddings")

  print("length: ", len(glove_embeddings))

  out_prefix = (output_directory + glove_file_name.split("/")[-1]).replace(".txt", "")

  #print([ for embedding in [:10]])

  # for idx, embedding in enumerate(glove_embeddings):
  #   if embedding.shape[0] != glove_dim:
  #     print("Error embedding %d is non-compliant, with shape" % idx, embedding.shape[0])

  embeddings = np.asarray(glove_embeddings)

  print("shape: ", embeddings.shape)

  assert len(embeddings.shape) == 2, "Shape appears to be out of order?"

  np.savez_compressed(out_prefix + "_refined_" + output_suffix, embeddings=embeddings)

  print("Writing embeddings vocab")

  with open(out_prefix + "_glove_vocab_%s.txt" % output_suffix, 'w', encoding="utf-8") as file:
    file.writelines(["%s\n" % item for item in glove_words])


def extract_glove_to_npy_data_for_init(glove_file_name, output_directory, output_suffix, filter_vocab_file_name,
                                       sample_stat_size=200):
  """convert glove input to numpy file for given vocab"""
  print("Loading glove...")
  glove_vocab = get_glove_vocab(glove_file_name)
  glove_embeddings = []

  dim = len(list(glove_vocab.values())[0])
  print('pretrained dim: ', dim)
  sample_mean = np.mean(list(map(lambda x: np.mean(x), list(glove_vocab.values())[:sample_stat_size])))
  sample_std = np.mean(list(map(lambda x: np.std(x), list(glove_vocab.values())[:sample_stat_size])))

  n0, n1= 0, 0
  if filter_vocab_file_name is not None:
    with open(filter_vocab_file_name, 'r', encoding="utf-8") as file:
      for line in file:
        n0 += 1
        try:
          glove_embeddings += [glove_vocab[line.strip()]]
          n1 += 1
        except:
          glove_embeddings += [np.random.normal(loc=sample_mean, scale=sample_std, size=dim)]

  print("%d out of %d has pretrained embeddings" % (n1, n0))
  out_prefix = (output_directory + glove_file_name.split("/")[-1]).replace(".txt", "")
  np.savez_compressed(out_prefix + "_refined_" + output_suffix, embeddings=np.asarray(glove_embeddings))


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Glove embeddings transformation')

  parser.add_argument('-g', '--glove', default="./../../models/metadata_tagging/data/glove.6B.100d.txt",
                      help='location of glove file')

  parser.add_argument('-o', '--out_dir', default="./../../models/metadata_tagging/data/",
                      help='output directory')

  parser.add_argument('-f', '--filter_vocab', default="./../../models/metadata_tagging/data/train_token_vocab.txt",
                      help='output directory')

  ARGS = parser.parse_args()

  #extract_glove_to_npy_data_and_text_vocab(ARGS.glove, ARGS.out_dir, "train_dev", ARGS.filter_vocab)
  #extract_glove_to_npy_data_and_text_vocab(ARGS.glove, ARGS.out_dir, "inference")
  extract_glove_to_npy_data_for_init(ARGS.glove, ARGS.out_dir, "train_dev", ARGS.filter_vocab)