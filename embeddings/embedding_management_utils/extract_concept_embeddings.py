# Copyright (c) 2017-present, Unsilo A/S.
# All rights reserved.

from __future__ import print_function

import argparse
import sys

import numpy as np
from gensim.models import Word2Vec

#SPECIAL_TOKENS = ["<EOS>", "<SKIP>"]
SPECIAL_TOKENS = []

def load_gensim_w2v(model_path, external_vocabulary=None):
  model = Word2Vec.load(model_path)
  model.init_sims(replace=True)

  # Some progress status
  print("Model Loaded!")
  print("Model vocab size: ", len(model.wv.vocab))

  display_labels, concept_keys = external_vocabulary
  original_vocab = [vocab_entry for vocab_entry in model.wv.vocab.keys()]

  concept_keys_set = set(concept_keys)
  original_vocab_set = set(original_vocab)

  print("concept_keys: " + str(concept_keys[:100]))
  print("original_vocab: " + str(original_vocab[:100]))

  print("Concept metrics vocab size: ", len(concept_keys))

  intersection_set = original_vocab_set.intersection(concept_keys_set)

  print("Concept key intersections: ", len(intersection_set))

  return model, intersection_set


def load_vocabulary_from_concept_store(concept_store_path):
  display_labels = []
  concept_keys = []
  concept_frequencies = []

  with open(concept_store_path, 'r', encoding="utf-8") as f:
    # Skip header
    headerline = next(f)
    for i, line in enumerate(f):
      split_line = line.split("\t")

      display_label = split_line[0]
      concept_key = split_line[2]
      concept_frequency = split_line[3]

      display_labels.append(display_label)
      concept_keys.append(concept_key)
      concept_frequencies.append(concept_frequency)

  return display_labels, concept_keys, concept_frequencies

def main():
  parser = argparse.ArgumentParser(description='Make a concept vector k nearest neighbor query.')

  parser.add_argument('-m', '--gensim_w2v_model_path', type=str, default='w2v-output.5',
                      help='Path to the Gensim w2v model file to load for embedding creation')
  parser.add_argument('-c', '--concept_metrics_path', type=str, default='ncbi-pmc-v9-concept-metrics-indexing.csv',
                      help='Path to a concept metrics file')

  args = parser.parse_args()
  gensim_w2v_model_path = args.gensim_w2v_model_path
  concept_store_path = args.concept_metrics_path

  print("Reading concepts / creating embeddings!")

  display_labels, concept_keys, concept_frequencies = load_vocabulary_from_concept_store(concept_store_path)
  custom_vocab = (display_labels, concept_keys)

  # Load gensim w2v model:
  model, intersection_set = load_gensim_w2v(gensim_w2v_model_path, custom_vocab)

  print("Vector size: ", model.vector_size)

  concepts_vocab = list(intersection_set)

  embeddings_vectors = []
  for concept_key in concepts_vocab:
    try:
        vector = model[concept_key]
        embedding_vector = np.concatenate((vector, np.zeros([len(SPECIAL_TOKENS)])))
        embeddings_vectors.append(embedding_vector)
    except KeyError:
      # print("Failed with concept: ", display_labels[i])
      raise KeyError("Concept key not found")

  concepts_vocab = SPECIAL_TOKENS + concepts_vocab

  with open('concept_key_vocab.txt', 'w', encoding="utf-8") as f:
    f.write('\n'.join(concepts_vocab))

  def create_one_hot_vector(index):
    vector = np.zeros([model.vector_size+len(SPECIAL_TOKENS)]) # [0] * model.vector_size+len(SPECIAL_TOKENS)
    vector[index] = 1
    return vector

  special_token_vectors = [create_one_hot_vector(token_number + model.vector_size) for token_number, _ in enumerate(SPECIAL_TOKENS)]

  embeddings_vectors = special_token_vectors + embeddings_vectors

  print(embeddings_vectors[:10])

  embeddings = np.asarray(embeddings_vectors)

  print("shape: ", embeddings.shape)

  np.savez_compressed("concept_embedding", embeddings=embeddings)

if __name__ == "__main__":
  main()