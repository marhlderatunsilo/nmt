"""
Definitions model: Preprocesses springer link text and saves to TFRecords.
"""
import numpy as np
import pandas as pd
import tensorflow as tf
import time
from sklearn.utils import shuffle as sk_shuffle

from models.multilabel_classification.code.text_preprocessor import MultilabelTextPreprocessor, print_and_time
from tfDeepNLP.input_pipes.text_preprocessor import TextPreprocessor, int64_feature, int64_array_feature, \
  bytes_feature, parallelize_dataframe


class SpringerLinkMultilabelTextPreprocessor(MultilabelTextPreprocessor):
  def preprocess(self, num_cores=8, parallelize=False, **kwargs):

    print("Start preprocessing")
    timer_start = time.time()

    # Remove duplicate rows - this takes care of repeated headers
    df_unique_rows = self.remove_duplicate_rows(self.data_input, copy=False)
    timer_unique = print_and_time("Removed duplicate rows", timer_start, timer_start)

    # Convert cols to numerical columns. Because they were originally saved as strings.
    df_numerical = self.convert_to_numericals(df_unique_rows, copy=False)
    timer_numerical = print_and_time("Converted to numeric column", timer_unique, timer_start)

    # Replace the element separator with a whitespace in source
    # It is very slow, so we (potentially) run in parallel.
    df_spaced_source = parallelize_dataframe(df_numerical, func=self.space_source,
                                             num_cores=num_cores, group_col=None,
                                             parallelize=parallelize)
    timer_spaced = print_and_time("Spaced source", timer_numerical, timer_start)

    df_known_concepts = self.keep_only_specific_source_concepts(df_spaced_source, self.srcs_to_keep,
                                                                split_sep=" ", copy=False)
    timer_kept_concepts = print_and_time("Kept known concepts only", timer_spaced, timer_start)

    # Remove concepts in source that are the same as the previous concept - to avoid repetition.
    # It is very slow, so we (potentially) run in parallel.

    txt_col_name = self.txt_col_name

    def remove_source_consecutive_duplicates(data):
      return self.remove_consequtive_duplicates(data,
                                                txt_col_name=txt_col_name,
                                                split_sep=" ", copy=False)

    df_removed_source_consecutive_duplicates = parallelize_dataframe(df_known_concepts,
                                                                     func=remove_source_consecutive_duplicates,
                                                                     num_cores=num_cores, group_col=None,
                                                                     parallelize=parallelize)

    timer_src_duplicates = print_and_time("Removed consecutive duplicates from source", timer_kept_concepts,
                                          timer_start)

    data_updated_n_concepts_col = self.update_n_concepts_col(df_removed_source_consecutive_duplicates, split_sep=" ")

    timer_updated_n_concepts = print_and_time("Updated number of concepts per sentence", timer_src_duplicates,
                                              timer_start)

    # Filter sentences by the squared sum of their 5 best concept scores
    # It is very slow, so we (potentially) run in parallel.
    # Note that it is very important that the splitting for parallelization
    # keeps all articles in the same partition.
    final_n_concepts_per_article = self.final_n_concepts_per_article

    def filter_sentences(data):
      return self.filter_by_squared_sum_of_scores(data,
                                                  n_concepts_threshold=final_n_concepts_per_article,
                                                  copy=False)

    df_filtered = parallelize_dataframe(data_updated_n_concepts_col,
                                        func=filter_sentences,
                                        num_cores=num_cores, group_col=self.article_id_col_name,
                                        parallelize=parallelize)

    timer_filtered = print_and_time("Filtered by score", timer_updated_n_concepts, timer_start)

    # Split target string into tokens.
    df_targets_split = self.split_targets(df_filtered)
    timer_tgt_split = print_and_time("Split target", timer_filtered, timer_start)

    # Find the sentences where the previous sentence was skipped.
    # Note: Not useful in this model, as every sentence got a tag.
    # df_skipped = self.mark_skipped_sentences(df_rows_with_targets, copy=False)

    # Insert tags in source, e.g. <s> fdfhk dhfskj <s> dfsh ...
    df_tagged = self.insert_tags(df_targets_split, insert_pre=True, insert_post=False, insert_skip=False,
                                 copy=False)
    timer_tagged = print_and_time("Added tags to source", timer_tgt_split, timer_start)

    data_updated_n_concepts_col_2 = self.update_n_concepts_col(df_tagged, split_sep=" ")

    timer_updated_n_concepts_2 = print_and_time("Updated number of concepts per sentence", timer_tagged,
                                                timer_start)

    # Join sentences to articles in source.
    df_source_joined = self.join_concepts(data_updated_n_concepts_col_2, txt_col=self.txt_col_name, pre_sort=True,
                                          copy=False)
    timer_joined_source = print_and_time("Joined source", timer_updated_n_concepts_2, timer_start)

    # Replace the collapsed target concepts in collapsing_map.
    df_targets_collapsed = self.replace_collapsed_target_concepts(df_source_joined, copy=False)
    timer_collapsed_targets = print_and_time("Collapsed targets", timer_joined_source, timer_start)

    # Remove specific targets. This also gets rid of duplicate targets, as we use set operations.
    df_targets_cleaned = self.remove_specific_targets(df_targets_collapsed, self.tgts_to_remove,
                                                      copy=False)
    timer_tgt_cleaned = print_and_time("Removed specific targets", timer_collapsed_targets, timer_start)

    # Remove rows with no targets left.
    df_rows_with_targets = self.remove_targetless_rows(df_targets_cleaned, copy=False)
    timer_no_empty_targets = print_and_time("Removed rows without targets.", timer_tgt_cleaned, timer_start)

    # Cut the number of targets to a maximum.
    df_targets_cut = self.cut_target_concepts(df_rows_with_targets, n=self.max_n_targets, copy=False)
    timer_cut_targets = print_and_time("Cut to max number of target.", timer_no_empty_targets, timer_start)

    # Extract sequence lengths of source and then target.
    df_targets_cut["src_seq_lengths"] = self.extract_sequence_lengths(df_targets_cut[self.txt_col_name])
    df_targets_cut["tgt_seq_lengths"] = [len(cpts) for cpts in df_targets_cut[self.tgt_col_name]]
    timer_seq_lengths = print_and_time("Extracted sequence lengths.", timer_cut_targets, timer_start)

    # Join targets to string with separator.
    df_targets_joined = self.join_target_concepts(df_targets_cut, copy=False)
    timer_targets_joined = print_and_time("Joined targets.", timer_seq_lengths, timer_start)

    # Shuffle dataframe.
    self.data_preprocessed = sk_shuffle(df_targets_joined).reset_index(drop=True)
    timer_shuffled = print_and_time("Shuffled dataframe.", timer_targets_joined, timer_start)

    # Pad source to the size of the longest article.
    self.data_preprocessed[self.txt_col_name] = self.pad_sentences(self.data_preprocessed[self.txt_col_name],
                                                                   seq_lengths=self.data_preprocessed[
                                                                     "src_seq_lengths"])
    timer_padding_src = print_and_time("Padded source.", timer_shuffled, timer_start)

    # Pad target to the max number of targets in an article.
    self.data_preprocessed[self.tgt_col_name] = self.pad_sentences(self.data_preprocessed[self.tgt_col_name],
                                                                   seq_lengths=self.data_preprocessed[
                                                                     "tgt_seq_lengths"], sep=self.element_sep)
    timer_padding_tgt = print_and_time("Padded targets.", timer_padding_src, timer_start)

  def create_features(self, data=None):
    # TODO This is a bad bad bad temporary hack because we need to do partitioning
    """

    :param data: pd.DataFrame or None.
      If None, uses self.data_input.
    """
    if not data:
      self.section_tag_features = self.create_section_features(self.data_input, tags=["pre_sentence"])
    else:
      self.section_tag_features = self.create_section_features(data, tags=["pre_sentence"])

  def write_to_tfr(self, f_name='text_data.tfrecords', out_dir=None):
    if self.data_preprocessed is None:
      self.data_preprocessed = self.data_input
    writer = self.create_tfr_writer(f_name, out_dir=out_dir)
    if self.data_preprocessed is None:
      raise ValueError("Data has not been correctly preprocessed.")
    text = self.data_preprocessed[self.txt_col_name]
    target = self.data_preprocessed[self.tgt_col_name]
    src_seq_len = self.data_preprocessed["src_seq_lengths"]
    tgt_seq_len = self.data_preprocessed["tgt_seq_lengths"]
    article_id = self.data_preprocessed[self.article_id_col_name]

    num_examples = len(text)
    for i in range(num_examples):
      example = tf.train.Example(features=tf.train.Features(feature={
        'source': bytes_feature(text[i]),
        'target': bytes_feature(target[i]),
        'src_seq_len': int64_feature(src_seq_len[i]),
        'tgt_seq_len': int64_feature(tgt_seq_len[i]),
        'article_id': bytes_feature(article_id[i]),
        # 'is_skipped_tag': int64_array_feature(self.section_tag_features[0][i])
        'is_pre_sentence_tag': int64_array_feature(self.section_tag_features[0][i]),
        # 'is_post_sentence_tag': int64_array_feature(self.section_tag_features[2][i])
      }))
      writer.write(example.SerializeToString())
    writer.close()
