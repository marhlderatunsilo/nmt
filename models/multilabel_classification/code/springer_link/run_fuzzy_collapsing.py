"""
Collapses very similar strings in vocab using fuzzy matching and a few heuristics.
"""

from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.cleaning_utils import find_letterless, remove_strings
from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.dict_utils import get_keys
from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.file_handling import open_vocab, save_obj, save_strings
from tfDeepNLP.input_pipes.fuzzy_collapsing.windowed_fuzzy_collapser import WindowedFuzzyCollapser

main_path = "/Users/ludvigolsen/unsilo-code/tfDeepNLP/models/multilabel_classification/data/springer_link/"
name_of_editor_keywords_vocab = "editor_keywords_vocab.txt"
name_of_editor_keywords_vocab_output = "editor_keywords_vocab_collapsed.txt"
name_of_editor_keywords_most_frequent_vocab_output = "editor_keywords_vocab_collapsed_40k.txt"
name_of_filtered_out_strings_output = "removed_letterless.txt"
name_of_collapsing_map_output = "collapsing_map_by_longest_and_last_alphabetically"  # Automatically have .pkl appended

vocab = open_vocab(name_of_editor_keywords_vocab)

# Find and remove letterless
letterless_strings = find_letterless(vocab)
vocab = remove_strings(vocab, letterless_strings)

fuzzy_collapser = WindowedFuzzyCollapser(
  vocab,
  lev_thresholds=None,  # Uses default thresholds, see WindowedFuzzyCollapser.default_thresholds
  w_size=20,
  exclude=["africa", "adams", "adrs", "adsc", "antibodies to s-100b protein"],
  match_without_end_abbreviations=True)

fuzzy_collapser.collapse_strings()

print("Found {} potential collapses to {} concepts.".format(fuzzy_collapser.n_mappings, fuzzy_collapser.n_replacements))

# Rename the collapsed to the longest of them
fuzzy_collapser.pick_common_name(by="longest_last")

vocab_collapsed = remove_strings(vocab, get_keys(fuzzy_collapser.mappings))

assert len(vocab) - len(vocab_collapsed) == fuzzy_collapser.n_mappings


def partition_vocab(vocab, n):
  to_keep = vocab[:n]
  to_remove = vocab[n:]

  assert len(vocab) == len(to_keep) + len(to_remove)

  return to_keep, to_remove


vocab_most_frequent, vocab_least_frequent = partition_vocab(vocab_collapsed, 40000)

""" Save objects """

# Save collapsing map
save_obj(fuzzy_collapser.mappings, main_path, name_of_collapsing_map_output)

# Save collapsed vocab
save_strings(vocab_collapsed, name_of_editor_keywords_vocab_output)

# Save most frequent collapsed vocab
# Save collapsed vocab
save_strings(vocab_collapsed, name_of_editor_keywords_most_frequent_vocab_output)

# Save removed letterless strings
save_strings(letterless_strings, name_of_filtered_out_strings_output)
