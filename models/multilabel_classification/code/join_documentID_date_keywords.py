import csv

EDITOR_KEYWORDS_INDEX = 4

def get_document_id_to_date():
  document_id_to_date = {}
  with open('documentID_date.csv', encoding="utf-8", newline='') as date_csvfile:
    date_reader = csv.reader(date_csvfile, delimiter=',')

    for row in date_reader:
      article_id, date_string = row[0], row[1]
      document_id_to_date[article_id] = date_string

  return document_id_to_date

def join_with_keywords(document_id_to_date):
  last_article_id = None

  with open('concepts_and_editor_keywords.csv', encoding="utf-8", newline='') as keywords_csvfile, open(
      "joined_id_date_keywords.csv", encoding="utf-8", mode='w',
      newline='') as joined_csv:
    keywords_reader = csv.reader(keywords_csvfile, delimiter=',')
    joined_writer = csv.writer(joined_csv, delimiter=',')

    # Write header
    joined_writer.writerow(['ArticleID', 'Date', "EditorKeywords"])

    for row in keywords_reader:
      article_id = row[0]
      if article_id != last_article_id:
        keywords = row[EDITOR_KEYWORDS_INDEX].split(";")
        date_string = document_id_to_date[article_id]
        joined_writer.writerow([article_id, date_string, ";".join(keywords)])

        last_article_id = article_id

def main():
  document_id_to_date = get_document_id_to_date()
  join_with_keywords(document_id_to_date)

if __name__ == '__main__':
  main()
