import csv
import pickle

def extract_statistics():
  statistics_map = {}
  with open('joined_id_date_keywords.csv', encoding="utf-8", newline='') as date_csvfile:
    date_reader = csv.reader(date_csvfile, delimiter=',')

    for row in date_reader:
      article_id, date_string, editor_keywords = row[0], row[1], row[2]

      for editor_keyword in editor_keywords.split(";"):
        if editor_keyword not in statistics_map:
          statistics_map[editor_keyword] = {}
        if date_string not in statistics_map[editor_keyword]:
          statistics_map[editor_keyword][date_string] = 1
        else:
          statistics_map[editor_keyword][date_string] += 1

    with open('statistics.pickle', 'wb') as handle:
      pickle.dump(statistics_map, handle, protocol=pickle.HIGHEST_PROTOCOL)

  return


def main():
  extract_statistics()

if __name__ == '__main__':
  main()
