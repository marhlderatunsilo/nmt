"""
Running preprocessing and writing to csv.
This is the first run. Then, the
"""

# from pprint import pprint

import pickle

import pandas as pd

from models.multilabel_classification.code.oxford.text_preprocessor import OxfordMultilabelTextPreprocessor
from tfDeepNLP.input_pipes.text_preprocessor import append_slash

path_to_data_folder = "../../data/oxford/"
data_path = path_to_data_folder + "joined_id_keywords_labels.csv"
csv_name = "preprocessed_data_labelled.csv"
targets_to_keep_path = "used_labels.txt"

out_dir = "../../.../"

source_vocab_path = ""  # concept metrics vocab

dev_set_size = 1500

"""
Load data
"""


# Pickle read
def load_obj(path, name):
  path = append_slash(path)
  with open(path + name + '.pkl', 'rb') as f:
    return pickle.load(f)


def open_line_separated_strings(path):
  with open(path) as f:
    strings = f.readlines()
    print(len(strings))
    return [sv[:-1] for sv in strings]


# Load targets_to_remove
targets_to_keep = open_line_separated_strings(targets_to_keep_path)

# Load source vocab
source_vocab = open_line_separated_strings(source_vocab_path)

# Load data
data = pd.read_csv(data_path, engine="python")
# data = data.head(10).tail(5)  # for quick dev.
# pprint(data)

print("Finished loading data")

"""
Run preprocessing
"""
# Be aware of targets being one-hot encoded alphabetically (for definitions, target array has later been inversed)
preprocessor = OxfordMultilabelTextPreprocessor(data_input=data, article_id_col_name="ArticleID",
                                                sentence_id_col_name="SentenceID",
                                                txt_col_name="SentenceConcepts",
                                                n_concepts_col_name="NConcepts",
                                                tgt_col_name="Targets",
                                                score_col_names=["Score1", "Score2", "Score3", "Score4", "Score5"],
                                                final_n_concepts_per_article=300,
                                                max_n_targets=None,
                                                collapsing_map=None,
                                                tgts_to_keep=targets_to_keep,
                                                srcs_to_keep=source_vocab,
                                                skipped_sentence_tag="<skipped>",
                                                pre_sentence_tag="<s>",
                                                post_sentence_tag="</s>",
                                                col_sep=",", element_sep=";",
                                                copy=False)

preprocessor.preprocess(num_cores=70,
                        parallelize=False)  # TODO Get parallelization to work. Currently slower than without.
print("Finished preprocessing.")

"""
Write to csv
"""

preprocessor.to_csv(f_path=out_dir + csv_name)

"""
Partitioning
"""

preprocessed_dataframe = pd.read_csv(out_dir + csv_name, sep=",", engine="python")

train_set_size = len(preprocessed_dataframe) - dev_set_size

train = preprocessed_dataframe[:train_set_size].copy().reset_index(drop=True)
dev = preprocessed_dataframe[train_set_size:train_set_size + dev_set_size].copy().reset_index(drop=True)
full = preprocessed_dataframe.copy().reset_index(drop=True)

for dset, dset_name in zip([train, dev, full], ["train", "dev", "full"]):
  tmp_processor = OxfordMultilabelTextPreprocessor(data_input=dset, article_id_col_name="ArticleID",
                                                   sentence_id_col_name=None,
                                                   txt_col_name="SentenceConcepts",
                                                   n_concepts_col_name=None,
                                                   tgt_col_name="Targets",
                                                   score_col_names=None,
                                                   final_n_concepts_per_article=None,
                                                   max_n_targets=None,
                                                   collapsing_map=None,
                                                   tgts_to_remove=None,
                                                   skipped_sentence_tag="<skipped>",
                                                   pre_sentence_tag="<s>",
                                                   post_sentence_tag="</s>",
                                                   col_sep=",", element_sep=";",
                                                   copy=False)

  tmp_processor.create_features()
  tmp_processor.write_to_tfr(f_name="multilabel_data_{}.tfrecords".format(dset_name), out_dir=out_dir)
