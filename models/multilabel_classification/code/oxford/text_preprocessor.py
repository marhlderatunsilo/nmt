"""
Definitions model: Preprocesses springer link text and saves to TFRecords.
"""
import time

import tensorflow as tf
from sklearn.utils import shuffle as sk_shuffle

from models.multilabel_classification.code.text_preprocessor import MultilabelTextPreprocessor, print_and_time
from tfDeepNLP.input_pipes.text_preprocessor import int64_feature, int64_array_feature, \
  bytes_feature, parallelize_dataframe


class OxfordMultilabelTextPreprocessor(MultilabelTextPreprocessor):
  def preprocess(self, num_cores=8, parallelize=False, labelled=True):

    print("Start preprocessing")
    timer_start = time.time()

    # Remove duplicate rows - this takes care of repeated headers
    df = self.remove_duplicate_rows(self.data_input, copy=False)
    timer_step = print_and_time("Removed duplicate rows", timer_start, timer_start)

    # Convert cols to numerical columns. Because they were originally saved as strings.
    df = self.convert_to_numericals(df, copy=False)
    timer_step = print_and_time("Converted to numeric column", timer_step, timer_start)

    # Replace the element separator with a whitespace in source
    # It is very slow, so we (potentially) run in parallel.
    df = parallelize_dataframe(df, func=self.space_source,
                               num_cores=num_cores, group_col=None,
                               parallelize=parallelize)
    timer_step = print_and_time("Spaced source", timer_step, timer_start)

    # Only keep the source concepts we know
    df = self.keep_only_specific_source_concepts(df, self.srcs_to_keep,
                                                 split_sep=" ", copy=False)
    timer_step = print_and_time("Kept known source concepts only", timer_step, timer_start)

    # Remove concepts in source that are the same as the previous concept - to avoid repetition.
    # It is very slow, so we (potentially) run in parallel.

    txt_col_name = self.txt_col_name

    def remove_source_consecutive_duplicates(data):
      return self.remove_consequtive_duplicates(data,
                                                txt_col_name=txt_col_name,
                                                split_sep=" ", copy=False)

    df = parallelize_dataframe(df,
                               func=remove_source_consecutive_duplicates,
                               num_cores=num_cores, group_col=None,
                               parallelize=parallelize)

    timer_step = print_and_time("Removed consecutive duplicates from source", timer_step,
                                timer_start)

    # Remove empty rows
    df = df[(df[self.txt_col_name] != "") & (df[self.txt_col_name] != " ")]

    timer_step = print_and_time("Removed rows with no source concepts", timer_step, timer_start)

    df = self.update_n_concepts_col(df, split_sep=" ")

    timer_step = print_and_time("Updated number of concepts per sentence", timer_step,
                                timer_start)

    # Filter sentences by the squared sum of their 5 best concept scores
    # It is very slow, so we (potentially) run in parallel.
    # Note that it is very important that the splitting for parallelization
    # keeps all articles in the same partition.
    final_n_concepts_per_article = self.final_n_concepts_per_article

    def filter_sentences(data):
      return self.filter_by_squared_sum_of_scores(data,
                                                  n_concepts_threshold=final_n_concepts_per_article,
                                                  copy=False)

    df = parallelize_dataframe(df,
                               func=filter_sentences,
                               num_cores=num_cores, group_col=self.article_id_col_name,
                               parallelize=parallelize)

    timer_step = print_and_time("Filtered by score", timer_step, timer_start)

    if labelled:
      # Split target string into tokens.
      df = self.split_targets(df)
      timer_step = print_and_time("Split target", timer_step, timer_start)

      # Deduplicate targets
      df = self.deduplicate_targets(df, copy=False)
      timer_step = print_and_time("Deduplicated targets", timer_step, timer_start)

      # Only keep the target concepts we know
      df = self.keep_only_specific_target_concepts(df,
                                                   self.tgts_to_keep,
                                                   split_sep=None, copy=False)
      timer_step = print_and_time("Kept known target concepts only", timer_step, timer_start)

    # Insert tags in source, e.g. <s> fdfhk dhfskj <s> dfsh ...
    df = self.insert_tags(df, insert_pre=True, insert_post=False, insert_skip=False,
                          copy=False)
    timer_step = print_and_time("Added tags to source", timer_step, timer_start)

    df = self.update_n_concepts_col(df, split_sep=" ")

    timer_step = print_and_time("Updated number of concepts per sentence", timer_step, timer_start)

    # Join sentences to articles in source.
    df = self.join_concepts(df, txt_col=self.txt_col_name, pre_sort=True, labelled=labelled,
                            copy=False)
    timer_step = print_and_time("Joined source", timer_step, timer_start)

    if labelled:
      # Remove rows with no targets left.
      df = self.remove_targetless_rows(df, copy=False)
      timer_step = print_and_time("Removed rows without targets.", timer_step, timer_start)

    # Extract sequence lengths of source and then target.
    df["src_seq_lengths"] = self.extract_sequence_lengths(df[self.txt_col_name])
    if labelled:
      df["tgt_seq_lengths"] = [len(cpts) for cpts in df[self.tgt_col_name]]
    timer_step = print_and_time("Extracted sequence lengths.", timer_step, timer_start)

    if labelled:
      # Join targets to string with separator.
      df = self.join_target_concepts(df, copy=False)
      timer_step = print_and_time("Joined targets.", timer_step, timer_start)

    # Shuffle dataframe.
    self.data_preprocessed = sk_shuffle(df).reset_index(drop=True)
    timer_step = print_and_time("Shuffled dataframe.", timer_step, timer_start)

    # Pad source to the size of the longest article.
    self.data_preprocessed[self.txt_col_name] = self.pad_sentences(self.data_preprocessed[self.txt_col_name],
                                                                   seq_lengths=self.data_preprocessed[
                                                                     "src_seq_lengths"])
    timer_step = print_and_time("Padded source.", timer_step, timer_start)

    if labelled:
      # Pad target to the max number of targets in an article.
      self.data_preprocessed[self.tgt_col_name] = self.pad_sentences(self.data_preprocessed[self.tgt_col_name],
                                                                     seq_lengths=self.data_preprocessed[
                                                                       "tgt_seq_lengths"], sep=self.element_sep)
      timer_step = print_and_time("Padded targets.", timer_step, timer_start)

  def create_features(self, data=None):
    # TODO This is a bad bad bad temporary hack because we need to do partitioning
    """

    :param data: pd.DataFrame or None.
      If None, uses self.data_input.
    """
    if not data:
      self.section_tag_features = self.create_section_features(self.data_input, tags=["pre_sentence"])
    else:
      self.section_tag_features = self.create_section_features(data, tags=["pre_sentence"])

  def write_to_tfr(self, f_name='text_data.tfrecords', out_dir=None, labelled=True):
    if self.data_preprocessed is None:
      self.data_preprocessed = self.data_input
    writer = self.create_tfr_writer(f_name, out_dir=out_dir)
    if self.data_preprocessed is None:
      raise ValueError("Data has not been correctly preprocessed.")
    text = self.data_preprocessed[self.txt_col_name]
    src_seq_len = self.data_preprocessed["src_seq_lengths"]
    article_id = self.data_preprocessed[self.article_id_col_name]

    if labelled:
      target = self.data_preprocessed[self.tgt_col_name]
      tgt_seq_len = self.data_preprocessed["tgt_seq_lengths"]
    else:
      target = ["NA"] * len(article_id)
      tgt_seq_len = [0] * len(article_id)

    num_examples = len(text)
    for i in range(num_examples):
      example = tf.train.Example(features=tf.train.Features(feature={
        'source': bytes_feature(text[i]),
        'target': bytes_feature(target[i]),
        'src_seq_len': int64_feature(src_seq_len[i]),
        'tgt_seq_len': int64_feature(tgt_seq_len[i]),
        'article_id': bytes_feature(article_id[i]),
        'is_pre_sentence_tag': int64_array_feature(self.section_tag_features[0][i]),
      }))
      writer.write(example.SerializeToString())
    writer.close()
