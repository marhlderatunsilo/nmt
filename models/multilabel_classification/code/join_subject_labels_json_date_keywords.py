import csv
import json

EDITOR_KEYWORDS_INDEX = 4

def get_ids_to_subject_labels():
  with open("D:/Users/marhl/Downloads/Classifications_all.json", encoding="utf-8", newline='') as labels_json:
    labels_map = json.load(labels_json)
  return labels_map

def join_with_keywords(labels_map):

  with open("C:/Users/marhl/unsilo/unsilo-content-processing/concepts_and_editor_keywords.csv", encoding="utf-8", newline='') as keywords_csvfile, open(
      "C:/Users/marhl/unsilo/unsilo-content-processing/joined_id_keywords_labels.csv", encoding="utf-8", mode='w',
      newline='') as joined_with_labels_csv, open(
      "C:/Users/marhl/unsilo/unsilo-content-processing/joined_id_keywords_no_labels.csv", encoding="utf-8", mode='w',
      newline='') as joined_without_labels_csv:

    # Skip header line
    next(keywords_csvfile)

    keywords_reader = csv.reader(keywords_csvfile, delimiter=',')
    joined_labels_writer = csv.writer(joined_with_labels_csv, delimiter=',')
    joined_no_labels_writer = csv.writer(joined_without_labels_csv, delimiter=',')

    # Write headers
    joined_labels_writer.writerow([
            "ArticleID",
            "SentenceID",
            "SentenceConcepts",
            "NConcepts",
            "Targets",
            "Score1",
            "Score2",
            "Score3",
            "Score4",
            "Score5"])

    joined_no_labels_writer.writerow([
      "ArticleID",
      "SentenceID",
      "SentenceConcepts",
      "NConcepts",
      "Targets",
      "Score1",
      "Score2",
      "Score3",
      "Score4",
      "Score5"])

    for row in keywords_reader:
      article_id = row[0]
      article_id = article_id + ".txt"
      #sentence_id = row[1]
      #sentence_concepts = row[2]
      #n_concepts = row[3]

      if article_id in labels_map:
        targets = labels_map[article_id]
        joined_labels_writer.writerow(row[:4] + [";".join(targets)] + row[5:])
      else:
        joined_no_labels_writer.writerow(row)

def main():
  labels_map = get_ids_to_subject_labels()
  join_with_keywords(labels_map)

if __name__ == '__main__':
  main()
