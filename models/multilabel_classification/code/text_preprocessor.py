"""
Definitions model: Preprocesses text and saves to TFRecords.
"""
import abc
import time

import numpy as np
import pandas as pd
import tensorflow as tf

from tfDeepNLP.input_pipes.text_preprocessor import TextPreprocessor, int64_feature, int64_array_feature, \
  bytes_feature


class MultilabelTextPreprocessor(TextPreprocessor):
  """
  Methods for preprocessing text and converting data to TFRecords.

  Filters out sentences with too few useful concepts.

  Inserts tags and joins texts, grouped by articles.
  """

  def __init__(self, data_input, article_id_col_name, sentence_id_col_name,
               txt_col_name, n_concepts_col_name, tgt_col_name, score_col_names,
               final_n_concepts_per_article, max_n_targets=10, collapsing_map=None,
               tgts_to_remove=None, tgts_to_keep=None,
               srcs_to_keep=None,
               skipped_sentence_tag="<skipped>",
               pre_sentence_tag="<s>", post_sentence_tag="</s>", copy=True, col_sep=",", element_sep=";"):
    """
    :param data_input: pandas data frame
      +--------------------------------------------------------------------------------------------------------+
      | ArticleID | Sentence ID | Sentence Concepts | n concepts | 1. score | 2. score | ... | Target Concepts |
      +--------------------------------------------------------------------------------------------------------+
      | Int       | Int         | Str               | Int        | Float    | Float    | ... | Str             |
      +--------------------------------------------------------------------------------------------------------+

    :param article_id_col_name: Name of article ID column.
    :param txt_col_name: Name of text column.
    :param tgt_col_name: Name of target column.
    :param score_col_names: List of names of score columns in the order of best score to worst score.
    :param collapsing_map: Dictionary where *keys* have to be replaced with *value* in target concepts.
    :param tgts_to_remove: A list of strings to remove from target concepts.
    :param srcs_to_keep: A list of strings to keep in source, e.g. vocab.
    :param copy: Whether to make copy of data_input when initializing class.
    """
    super().__init__()

    if copy:
      self.data_input = data_input.copy()
    else:
      self.data_input = data_input

    self.article_id_col_name = article_id_col_name
    self.sentence_id_col_name = sentence_id_col_name
    self.txt_col_name = txt_col_name
    self.n_concepts_col_name = n_concepts_col_name
    self.tgt_col_name = tgt_col_name
    self.score_col_names = score_col_names
    self.final_n_concepts_per_article = int(final_n_concepts_per_article) if final_n_concepts_per_article else None
    self.max_n_targets = max_n_targets
    self.collapsing_map = collapsing_map
    self.tgts_to_remove = tgts_to_remove
    self.tgts_to_keep = tgts_to_keep
    self.srcs_to_keep = srcs_to_keep
    self.skipped_sentence_tag = skipped_sentence_tag
    self.pre_sentence_tag = pre_sentence_tag
    self.post_sentence_tag = post_sentence_tag
    self.data_preprocessed = None
    self.element_sep = element_sep
    self.col_sep = col_sep
    self.section_tag_features = None

  def remove_duplicate_rows(self, data, copy=True):
    """
    If the headers have been inserted at the beginning of every article,
    we would like to remove them.
    :return:
    """
    if copy:
      data = data.copy()

    return data.reset_index(drop=True).drop_duplicates(subset=None, keep=False, inplace=False)

  def remove_targetless_rows(self, data, copy=True):

    if copy:
      data = data.copy()

    return data[data[self.tgt_col_name].map(len) != 0].reset_index(drop=True)

  def convert_to_numericals(self, data, copy=True):

    if copy:
      data = data.copy()

    for score in self.score_col_names:
      data[score] = data[score].astype(float)

    data[self.sentence_id_col_name] = data[self.sentence_id_col_name].astype(int)
    data[self.n_concepts_col_name] = data[self.n_concepts_col_name].astype(int)

    return data

  def space_source(self, data):
    data[self.txt_col_name] = data.apply(
      lambda row: " ".join(str(row[self.txt_col_name]).split(self.element_sep)), axis=1)
    return data

  @staticmethod
  def remove_consequtive_duplicate_tokens(string, split_sep=None):

    """
    :param split_sep: If not None, split by it, remove duplicates, join by it.
    """

    if split_sep:
      string = string.split(split_sep)

    string_shifted = ["NA"] + string[:-1]
    is_previous = [True if s1 == s2 else False for s1, s2 in zip(string, string_shifted)]
    non_duplicated = [s for s, ip in zip(string, is_previous) if not ip]

    if split_sep:
      non_duplicated = split_sep.join(non_duplicated)

    return non_duplicated

  def remove_consequtive_duplicates(self, data, txt_col_name, split_sep=None, copy=True):

    if copy:
      data = data.copy()

    data[txt_col_name] = data.apply(
      lambda row: self.remove_consequtive_duplicate_tokens(row[txt_col_name], split_sep=split_sep), axis=1)
    return data

  def split_targets(self, data):
    data[self.tgt_col_name] = data.apply(
      lambda row: str(row[self.tgt_col_name]).split(self.element_sep), axis=1)
    return data

  def update_n_concepts_col(self, data, split_sep=None):

    strings = data[self.txt_col_name]

    def get_length(string, split_sep=None):
      if split_sep:
        string = string.split(split_sep)
      return len(string)

    data[self.n_concepts_col_name] = [get_length(s, split_sep=split_sep) for s in strings]

    return data

  def filter_by_squared_sum_of_scores(self, data, n_concepts_threshold, copy=True):
    """
    Filter sentences by their sum of squared scores.

    :param data: pandas DataFrame.
    :param n_concepts_threshold: How many concepts we want in the final article. Maximum.
    :return: Filtered DataFrame.
    """

    if copy:
      data = data.copy()

    def square_sum(row, col_names):
      squares = [float(row[score]) ** 2 for score in col_names]
      return sum(squares)

    # Find the sum of squared scores
    # By squaring, we favor the good concepts, so a sentence with one really good concept is still
    # better than a sentence with more concepts, but medium ones.
    data["squared_score_sum"] = data.apply(lambda row: square_sum(row, self.score_col_names), axis=1)

    def filter_article_sentences(subset, n_concepts_col, n_concepts_threshold):
      # Sort by this sum of squared scores
      subset = subset.sort_values("squared_score_sum", ascending=False)

      # Make cumsum of number of concepts
      # This allows us to have a somewhat even number of concepts per article in the end.
      subset["score_cumsum"] = np.cumsum(subset[n_concepts_col].astype(int))
      subset = subset.sort_values(self.sentence_id_col_name)
      subset = subset[subset["score_cumsum"] <= n_concepts_threshold]
      subset = subset.drop(["score_cumsum", "squared_score_sum"], axis=1)
      return subset

    # Apply filtering to each article and reset index.
    return data.groupby([self.article_id_col_name]).apply(
      filter_article_sentences, n_concepts_col=self.n_concepts_col_name,
      n_concepts_threshold=n_concepts_threshold).reset_index(drop=True)

  def filter_by_scores_hard_threshold(self, data, min_n_good_concepts, min_score_threshold, copy=True):

    if copy:
      data = data.copy()

    # Start by removing rows with to few concepts
    # This will make it faster to calculate the scores
    data = data[data[self.n_concepts_col_name] >= min_n_good_concepts]

    def count_good_concepts(row, threshold):
      counter = 0
      for score in self.score_col_names:
        try:
          if float(row[score]) >= threshold:
            counter += 1
        except:
          pass
        return counter

    data["n_good_concepts"] = data.apply(lambda row: count_good_concepts(row, min_score_threshold), axis=1)

    return data[data["n_good_concepts"] >= min_n_good_concepts]

  def mark_skipped_sentences(self, data, copy=True):

    if copy:
      data = data.copy()

    # Add new index within each article
    data["article_idx"] = data.groupby([self.article_id_col_name], sort=False).cumcount()  # TODO Drop before returning

    # Copy sentence ID and shift one down
    all_but_last = data[self.sentence_id_col_name][:-1]
    data["previous_sentence_id"] = np.concatenate([[0], all_but_last])

    data["is_post_skip"] = data.apply(lambda row: int(
      (row["article_idx"] != 0 and
       row[self.sentence_id_col_name] - row["previous_sentence_id"] > 1)), axis=1)

    return data.drop(["article_idx", "previous_sentence_id"], axis=1)

  def insert_tag(self, data, txt_col, tag_flag_col=None,
                 tag_str="<s>", insert_at="beginning",
                 overwrite=True, copy=True):
    """
    :param data: pandas DataFrame.
    :param txt_col: Name of text column to insert tag in.
    :param tag_flag_col: Name of column deciding whether the tag should be inserted.
      If None, all sentences are tagged.
    :param tag_str: The string to insert.
    :param insert_at: "beginning" or "end".
    :param overwrite: Whether to write tag to txt_col or a copy suffixed _tagged
    :param copy: Whether to make a copy of data.
    :return:
    """

    if copy:
      data = data.copy()

    def tag_insertion(txt, tag, insert_at):
      if insert_at == "beginning":
        return tag + " " + txt
      elif insert_at == "end":
        return txt + " " + tag
      else:
        raise ValueError("insert_at must be either 'beginning' or 'end'.")

    if tag_flag_col:
      new_text_col = data.apply(
        lambda row: tag_insertion(row[txt_col], tag_str, insert_at) if row[tag_flag_col] == 1 else row[txt_col], axis=1)
      if overwrite:
        data[txt_col] = new_text_col
      else:
        data[txt_col + "_tagged"] = new_text_col
    else:
      new_text_col = data.apply(lambda row: tag_insertion(row[txt_col], tag_str, insert_at), axis=1)
      if overwrite:
        data[txt_col] = new_text_col
      else:
        data[txt_col + "_tagged"] = new_text_col
    return data

  def insert_tags(self, data, insert_pre=True, insert_post=True, insert_skip=True, copy=True):

    if copy:
      data = data.copy()

    if insert_pre:
      # Insert pre-sentence tag
      data = self.insert_tag(data, txt_col=self.txt_col_name, tag_flag_col=None,
                             tag_str=self.pre_sentence_tag, insert_at="beginning", overwrite=True, copy=False)

    if insert_post:
      # Insert post-sentence tag
      data = self.insert_tag(data, txt_col=self.txt_col_name, tag_flag_col=None,
                             tag_str=self.post_sentence_tag, insert_at="end", overwrite=True, copy=False)

    if insert_skip:
      # Insert skipped sentence tag
      data = self.insert_tag(data, txt_col=self.txt_col_name, tag_flag_col="is_post_skip",
                             tag_str=self.skipped_sentence_tag, insert_at="beginning", overwrite=True, copy=False)

    return data

  def join_concepts(self, data, txt_col, pre_sort=True, labelled=True, copy=True):
    """Sentences to articles"""

    if copy:
      data = data.copy()

    if pre_sort:
      data = data.sort_values([self.article_id_col_name, self.sentence_id_col_name])

    if labelled:
      # Get article targets
      article_targets = data.copy().groupby(
        self.article_id_col_name).first().reset_index()[[self.article_id_col_name, self.tgt_col_name]]

    def joiner(data, col_name):
      return pd.Series({self.txt_col_name: "%s" % ' '.join(data[col_name])})

    joined_txt = data.groupby(self.article_id_col_name, sort=False).apply(joiner, col_name=txt_col).reset_index()
    if labelled:
      return pd.merge(joined_txt, article_targets, how='inner')
    else:
      return joined_txt

  def cut_target_concepts(self, data, n, copy=True):

    if copy:
      data = data.copy()

    def as_1dlist(s):
      if not isinstance(s, list):
        return list(s)
      else:
        return s

    data[self.tgt_col_name] = data.apply(lambda row: as_1dlist(row[self.tgt_col_name])[:n], axis=1)

    return data

  def join_target_concepts(self, data, copy=True):

    if copy:
      data = data.copy()

    data[self.tgt_col_name] = data.apply(lambda row: self.element_sep.join(row[self.tgt_col_name]), axis=1)

    return data

  def replace_collapsed_target_concepts(self, data, copy=True):

    if copy:
      data = data.copy()

    data[self.tgt_col_name] = [[self.collapsing_map[c] if c in self.collapsing_map else c for c in concepts] for
                               concepts in data[self.tgt_col_name]]
    return data

  def remove_specific_targets(self, data, strings_to_remove, copy=True):

    if copy:
      data = data.copy()

    strings_to_remove = set(strings_to_remove)  # We only want to do the hashing once
    data[self.tgt_col_name] = [list(set(targets) - strings_to_remove) for targets in data[self.tgt_col_name]]

    return data

  def deduplicate_targets(self, data, copy=True):

    if copy:
      data = data.copy()

    data[self.tgt_col_name] = [list(set(targets)) for targets in data[self.tgt_col_name]]

    return data

  def keep_if_in(self, string, strings_to_keep, split_sep=None):

    if split_sep:
      string = string.split(split_sep)

    to_keep = list(filter(lambda x: x in strings_to_keep, string))

    if split_sep:
      to_keep = split_sep.join(to_keep)

    return to_keep

  def keep_only_specific_source_concepts(self, data, strings_to_keep, split_sep=None, copy=True):

    if copy:
      data = data.copy()

    strings_to_keep = set(strings_to_keep)  # We only want to do the hashing once
    data[self.txt_col_name] = [self.keep_if_in(srcs, strings_to_keep, split_sep=split_sep)
                               for srcs in data[self.txt_col_name]]

    return data

  def keep_only_specific_target_concepts(self, data, strings_to_keep, split_sep=None, copy=True):

    if copy:
      data = data.copy()

    strings_to_keep = set(strings_to_keep)  # We only want to do the hashing once
    data[self.tgt_col_name] = [self.keep_if_in(tgts, strings_to_keep, split_sep=split_sep)
                               for tgts in data[self.tgt_col_name]]

    return data

  def create_section_features(self, data, tags=None):

    text_col = data[self.txt_col_name].copy()

    if not tags:
      tags = ["skip", "pre_sentence", "post_sentence"]

    def _is_skipped_tag(word, skipped_tag):
      return [1 if w == skipped_tag else 0 for w in word.split(" ")]

    def _is_pre_sentence_tag(word, pre_sentence_tag):
      return [1 if w == pre_sentence_tag else 0 for w in word.split(" ")]

    def _is_post_sentence_tag(word, post_sentence_tag):
      return [1 if w == post_sentence_tag else 0 for w in word.split(" ")]

    def convert_to_ndarray(a_list, transpose=True):
      an_array = np.asarray(a_list)
      if transpose:
        an_array = an_array.transpose()
      return an_array

    extracted_features = []
    if "skip" in tags:
      extracted_features.append(convert_to_ndarray([_is_skipped_tag(t, self.skipped_sentence_tag) for t in text_col],
                                                   transpose=False))
    if "pre_sentence" in tags:
      extracted_features.append(convert_to_ndarray([_is_pre_sentence_tag(t, self.pre_sentence_tag) for t in text_col],
                                                   transpose=False))
    if "post_sentence" in tags:
      extracted_features.append(convert_to_ndarray([_is_post_sentence_tag(t, self.post_sentence_tag) for t in text_col],
                                                   transpose=False))
    return extracted_features

  def create_features(self, data=None):
    # TODO This is a bad bad bad temporary hack because we need to do partitioning
    """

    :param data: pd.DataFrame or None.
      If None, uses self.data_input.
    """
    if not data:
      self.section_tag_features = self.create_section_features(self.data_input, tags=["pre_sentence"])
    else:
      self.section_tag_features = self.create_section_features(data, tags=["pre_sentence"])

  def to_csv(self, f_path="preprocessed.csv"):
    self.data_preprocessed.to_csv(f_path, sep=self.col_sep, encoding="utf-8")

  def write_to_tfr(self, f_name='text_data.tfrecords', out_dir=None):
    if self.data_preprocessed is None:
      self.data_preprocessed = self.data_input
    writer = self.create_tfr_writer(f_name, out_dir=out_dir)
    if self.data_preprocessed is None:
      raise ValueError("Data has not been correctly preprocessed.")
    text = self.data_preprocessed[self.txt_col_name]
    target = self.data_preprocessed[self.tgt_col_name]
    src_seq_len = self.data_preprocessed["src_seq_lengths"]
    tgt_seq_len = self.data_preprocessed["tgt_seq_lengths"]
    article_id = self.data_preprocessed[self.article_id_col_name]

    num_examples = len(text)
    for i in range(num_examples):
      example = tf.train.Example(features=tf.train.Features(feature={
        'source': bytes_feature(text[i]),
        'target': bytes_feature(target[i]),
        'src_seq_len': int64_feature(src_seq_len[i]),
        'tgt_seq_len': int64_feature(tgt_seq_len[i]),
        'article_id': bytes_feature(article_id[i]),
        # 'is_skipped_tag': int64_array_feature(self.section_tag_features[0][i])
        'is_pre_sentence_tag': int64_array_feature(self.section_tag_features[0][i]),
        # 'is_post_sentence_tag': int64_array_feature(self.section_tag_features[2][i])
      }))
      writer.write(example.SerializeToString())
    writer.close()

  @abc.abstractmethod
  def preprocess(self, num_cores=8, parallelize=False, **kwargs):
    """
    Subclass must implement this
    """
    pass


def print_and_time(msg, prev_timer, orig_timer):
  new_timer = time.time()
  print(msg, ", time: ", new_timer - prev_timer, ", total time: ", new_timer - orig_timer)
  return new_timer
