"""
Running preprocessing and conversion to TFRecords on dataset.
"""

# from pprint import pprint

import pandas as pd

from models.multilabel_classification.code.text_preprocessor import MultilabelTextPreprocessor

dset = "DEV"
data_path = "../../..._{}.csv".format(dset)
out_dir = "../../.../"

# data_path = "/Users/ludvigolsen/unsilo-code/tfDeepNLP/models/definitions/data/wiki_defs_data/en_wiki_science_definitions_DEV.csv"
# out_dir = "/Users/ludvigolsen/unsilo-code/tfDeepNLP/models/definitions/data/wiki_defs_data/"

"""
Load data
"""

data = pd.read_csv(data_path, engine="python")
# data = data.head(10).tail(5)  # for quick dev.
# pprint(data)

"""
Run preprocessing
"""
# Be aware of targets being one-hot encoded alphabetically (for definitions, target array has later been inversed)
preprocessor = MultilabelTextPreprocessor(data_input=data, article_id_col_name="ArticleID",
                                          sentence_id_col_name="SentenceID",
                                          txt_col_name="Concepts",
                                          n_concepts_col_name="NConcepts",
                                          tgt_col_name="Target",
                                          score_col_names=["Score1", "Score2", "Score3", "Score4", "Score5"],
                                          final_n_concepts_per_article=200,
                                          skipped_sentence_tag="<skipped>",
                                          pre_sentence_tag="<s>",
                                          post_sentence_tag="</s>",
                                          copy=False)

preprocessor.preprocess()
print("Finished preprocessing.")

"""
Write to TFRecords
"""

preprocessor.write_to_tfr(f_name="multilabel_data_{}.tfrecords".format(dset), out_dir=out_dir)
