import argparse
import csv
import gzip
import json
from pathlib import Path
from random import randrange

import boto3
import xlrd
from fuzzywuzzy import fuzz

parser = argparse.ArgumentParser()
parser.add_argument("--xls_dir", default='D:/SME feedback sheets')
parser.add_argument("--output_file_name", default="SME_feedback_sentences.txt")
parser.add_argument("--mode", default="XML", choices=["XML", "JSON"])

args = parser.parse_args()

NEWEST_OBJECT_VERSION = "12"


def convert_excel_file_to_tuples(file_name):
  wb = xlrd.open_workbook(file_name)
  sh = wb.sheet_by_index(0)

  # We skip the first rows as it includes an example and not actual data
  for rownum in range(5, sh.nrows):
    yield sh.row_values(rownum)


# def create_bucket_top_level_dictionary(objects):
#   dict = {}
#   for object_summary in objects:
#     document_key = object_summary.key.split("/")[0]
#     if document_key in dict:
#       dict[document_key].append(object_summary)
#     else:
#       dict[document_key] = [object_summary]
#
#   return dict


def create_n_non_key_sentences(bucket, document_id, key_sentences, n):
  # determine path to the newest version of document json

  # def get_newest_version_object():
  #   objects = bucket_dict[document_id]
  #
  #   def get_version_number(o):
  #
  #     try:
  #       return int(o.key.split("/")[-2].replace("v", ""))
  #     except:
  #       return -1
  #
  #   newest_object = max(objects, key=get_version_number)
  #
  #   "document-concepts-pre.json.gz"
  #
  #   return newest_object

  def reconstruct_sentence_from_tokens(tokens_dicts):
    token_text_elements = []
    for token_dict in tokens_dicts:
      token_text = token_dict["text"]
      token_text_elements.append(token_text)

    return " ".join(token_text_elements)

  def find_sentence_candidate(sentences_dict, sentence_index):
    token_dicts = sentences_dict[sentence_index]["tokens"]
    return reconstruct_sentence_from_tokens(token_dicts)

  def select_random_non_key_sentence(document_sentences):

    sentence_count = len(document_sentences)
    while (sentence_count > 0):
      random_sentence_index = randrange(0, sentence_count)
      document_sentence = find_sentence_candidate(document_sentences, random_sentence_index)

      document_sentences.pop(random_sentence_index)
      sentence_count = len(document_sentences)

      if not any([fuzz.ratio(document_sentence, key_sentence) > 90 for key_sentence in key_sentences]):
        return document_sentence

  compressed_json_object_key = document_id + "/7001.01.xml.gz/springer/link/v" + NEWEST_OBJECT_VERSION + "/document-concepts-pre.json.gz"

  try:
    newest_object_summary = bucket.Object(compressed_json_object_key)
    newest_object = newest_object_summary.get()
  except:
    return

  object_data = newest_object['Body'].read()
  uncompressed_object_data = gzip.decompress(object_data)
  document_json = json.loads(uncompressed_object_data)
  document_sentences = document_json["sentences"]

  for _ in range(0, n):
    selected_non_key_sentence = select_random_non_key_sentence(document_sentences)
    yield selected_non_key_sentence


def main():
  excel_files_dir = Path(args.xls_dir)
  excel_files_list = set([f for f in excel_files_dir.glob('**/**/*.xls*') if f.is_file()])

  s3_resource = boto3.resource('s3')
  bucket = s3_resource.Bucket("springer-link-prod-unsilo")

  print("Loading bucket keys...")

  # all_objects = bucket.objects.all()

  # check_object_key_re = re.compile(r)

  # def keep_only_json(o):
  #   return check_object_key_re.match(o.key)

  # Filter irrelevant paths / objects
  # json_objects = list(
  #   filter(keep_only_json, all_objects))

  # bucket_dict = create_bucket_top_level_dictionary(json_objects)

  print("Done loading bucket keys...")

  with open(args.output_file_name, 'w', encoding="utf8", newline="\n") as output_csv_file:
    csv_wr = csv.writer(output_csv_file, quoting=csv.QUOTE_ALL)
    for file_name in excel_files_list:
      for row in convert_excel_file_to_tuples(file_name):

        doi = row[1]
        s3_doi_name = (''.join(reversed(doi))).split("/")[0]
        key_sentences = row[12:17]

        key_sentence_counter = 0
        for key_sentence in key_sentences:
          if key_sentence:
            key_sentence = key_sentence.replace("\n", "")
            csv_wr.writerow((key_sentence, "True"))
            key_sentence_counter = key_sentence_counter + 1

        for non_key_sentence in create_n_non_key_sentences(bucket, s3_doi_name, key_sentences,
                                                           key_sentence_counter * 3):
          non_key_sentence = non_key_sentence.replace("\n", "")
          csv_wr.writerow((non_key_sentence, "False"))


if __name__ == '__main__':
  main()
