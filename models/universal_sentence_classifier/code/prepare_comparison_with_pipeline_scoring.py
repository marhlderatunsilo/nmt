import csv
import gzip
import json
from pathlib import Path

import boto3
import tensorflow as tf
import xlrd
from tensorflow.contrib.distribute import OneDeviceStrategy

from tfDeepNLP.models.sen2class.universal_sentence_encoder_linear_model import \
  UnivervalsSentenceEncoderSentenceClassifier
from tfDeepNLP.utils import misc_utils as utils
from utils.vocab_utils import create_python_vocab_table

OLDEST_OBJECT_VERSION = 12
NEWEST_OBJECT_VERSION = 12

def convert_excel_file_to_tuples(file_name):
  wb = xlrd.open_workbook(file_name)
  sh = wb.sheet_by_index(0)

  # We skip the first rows as it includes heading and an example, i.e. not actual data
  for rownum in range(5, sh.nrows):
    yield sh.row_values(rownum)

class PipelineDocument:
  def __init__(self,
               document_key,
               sentence_components,
               reconstructed_sentences,
               key_sentence_indexes,
               publication_date,
               abstract, title):
    self.document_key = document_key
    self.sentence_components = sentence_components
    self.reconstructed_sentences = reconstructed_sentences
    self.key_sentence_indexes = key_sentence_indexes
    self.publication_date = publication_date
    self.abstract = abstract
    self.title = title

def reconstruct_sentence_from_tokens(tokens_dicts):
  token_text_elements = []
  for token_dict in tokens_dicts:
    token_text = token_dict["text"]
    token_text_elements.append(token_text)

  return " ".join(token_text_elements)

def get_n_documents_with_k_keysentences(bucket, training_document_ids, n, k):

  pipeline_documents = []

  if not NEWEST_OBJECT_VERSION - OLDEST_OBJECT_VERSION == 0:
    accepted_version_set = set(["v%d" % (OLDEST_OBJECT_VERSION + version_number_offset) for version_number_offset in
                                range(NEWEST_OBJECT_VERSION - OLDEST_OBJECT_VERSION)])
  else:
    accepted_version_set = {"v%d" % NEWEST_OBJECT_VERSION}

  already_seen_documents = set(training_document_ids)

  skip_counter = 0

  for s3_file_object in bucket.objects.all():

    if skip_counter > 0:
      skip_counter = skip_counter - 1
      continue

    file_key_components = s3_file_object.key.split("/")

    # Check if file is actually the compressed json we are looking for:
    if len(file_key_components) >= 5 and ".json.gz" in s3_file_object.key and file_key_components[
      4] in accepted_version_set and file_key_components[
      0] not in already_seen_documents:

      already_seen_documents.add(file_key_components[
                                   0])

      try:
        newest_object = s3_file_object.get()
      except:
        continue

      object_data = newest_object['Body'].read()
      uncompressed_object_data = gzip.decompress(object_data)
      document_json = json.loads(uncompressed_object_data)

      document_sentences = document_json["sentences"]

      if len(document_sentences) < 30 or "abstract" not in document_json or "title" not in document_json:
        continue

      publication_date = document_json["publicationDate"]
      title = document_json["title"]

      if not int(publication_date.split("-")[0]) in range(2009, 2019):
        continue

      abstract = document_json["abstract"]

      def get_sentence_centrality_in_document(scores):
        for score in scores:
          if score["label"] == "sentenceCentralityInDocument":
            return score["value"]

        return 0.0

      def get_sentence_fact_clue_score_in_document(scores):
        for score in scores:
          if score["label"] == "sentenceFactClueScoreInDocument":
            return score["value"]

        return 0.0

      sentence_centrality_scores = [
        get_sentence_centrality_in_document(document_sentence["scores"]) if "scores" in document_sentence else 0.0 for
        document_sentence in document_sentences]

      sentence_fact_clue_scores = [
        get_sentence_fact_clue_score_in_document(document_sentence["scores"]) if "scores" in document_sentence else 0.0 for
        document_sentence in document_sentences]

      key_sentence_scores = [sum(scores) for scores in zip(sentence_centrality_scores, sentence_fact_clue_scores)]

      key_sentence_indexes_and_scores = sorted(enumerate(key_sentence_scores), key=lambda x: x[1], reverse=True)[:k]
      key_sentence_indexes, key_sentence_scores = zip(*key_sentence_indexes_and_scores)

      pipeline_documents.append(
        PipelineDocument(s3_file_object.key,
                         sentence_components=[document["tokens"] for document in document_sentences],
                         reconstructed_sentences=[reconstruct_sentence_from_tokens(document["tokens"]) for document in
                                                  document_sentences],
                         key_sentence_indexes=key_sentence_indexes,
                         publication_date=publication_date,
                         abstract=abstract,
                         title=title))

      skip_counter = 800 # old = 500

      if len(pipeline_documents) >= n:
        return pipeline_documents

  return pipeline_documents


def create_evaluation_input_fn(pipeline_documents):
  def input_fn():
    def generator():
      for pipeline_document in pipeline_documents:
        sentences = [reconstruct_sentence for reconstruct_sentence in pipeline_document.reconstructed_sentences]
        yield sentences

    dataset = tf.data.Dataset.from_generator(generator=generator,
                                             output_types=(tf.string),
                                             output_shapes=(tf.TensorShape([None])),
                                             )

    def map_to_estimator_format(sentences):
      features = {
        "sentence": sentences,
      }

      return features

    dataset = dataset.map(map_to_estimator_format)

    return dataset

  return input_fn


def prepare_comparison_with_pipeline_scoring(hparams,
                                             scope=None,
                                             single_cell_fn=None):
  excel_files_dir = Path(hparams.xls_dir)
  excel_files_list = set([f for f in excel_files_dir.glob('**/**/*.xls*') if f.is_file()])

  s3_resource = boto3.resource('s3')
  bucket = s3_resource.Bucket("springer-link-prod-unsilo")

  document_dois = []

  tag_vocab_file = hparams.tag_vocab_file
  py_tags_vocab = create_python_vocab_table(tag_vocab_file)

  model_fn = UnivervalsSentenceEncoderSentenceClassifier(
    hparams,
    ntags=len(py_tags_vocab))

  config_proto = utils.get_config_proto(
    log_device_placement=hparams.log_device_placement)

  distribution_strategy = OneDeviceStrategy(tf.DeviceSpec(device_type="GPU", device_index=0))

  estimator_config = tf.estimator.RunConfig(
    save_checkpoints_secs=20 * 60,  # Save checkpoints every 20 minutes.
    keep_checkpoint_max=5,  # Retain the 10 most recent checkpoints.
    session_config=config_proto,
    model_dir=hparams.out_dir,
    save_summary_steps=hparams.every_n_step,  # points on tensorbord
    train_distribute=distribution_strategy
  )

  estimator = tf.estimator.Estimator(
    model_fn=model_fn,
    model_dir=hparams.out_dir,
    params=hparams,
    config=estimator_config)

  with open(hparams.output_file_name, 'w', encoding="utf8", newline="\n") as output_csv_file:
    csv_wr = csv.writer(output_csv_file, quoting=csv.QUOTE_ALL)
    csv_wr.writerow(["Document S3 ID", "Title", "Publication Date", "Abstract", "Model 1", "Model 2"])
    for file_name in excel_files_list:
      for row in convert_excel_file_to_tuples(file_name):
        doi = row[1]
        s3_doi_name = (''.join(reversed(doi))).split("/")[0]
        document_dois.append(s3_doi_name)

    pipeline_documents = get_n_documents_with_k_keysentences(bucket, document_dois, 10, 5)

    # Recreate sentences
    results_generator = estimator.predict(create_evaluation_input_fn(pipeline_documents),
                                          yield_single_examples=False)

    print("pipeline_documents:", pipeline_documents)
    print("results_generator:", results_generator)

    for document_idx, result in enumerate(results_generator):

      print("processing document: ", document_idx)
      pipeline_document = pipeline_documents[document_idx]

      labels_logits = result["labels_logits"]

      labels_true_logits = [logits[1] for logits in labels_logits]

      indexes_and_labels_true_logits = enumerate(labels_true_logits)

      # Filter short sentences:
      def short_sentence_filter(index_and_label_true_logit):
        index, logit = index_and_label_true_logit
        return len(pipeline_document.sentence_components[index]) > 10

      indexes_and_labels_true_logits = filter(short_sentence_filter, indexes_and_labels_true_logits)

      def remove_remove_duplicate_sentences(indexes_and_labels_true_logits):
        seen_sentences = set()
        for index, label_true_logit in indexes_and_labels_true_logits:
          if pipeline_document.reconstructed_sentences[index] not in seen_sentences:
            seen_sentences.add(pipeline_document.reconstructed_sentences[index])
            yield index, label_true_logit

      indexes_and_labels_true_logits = remove_remove_duplicate_sentences(indexes_and_labels_true_logits)

      labels_logits_and_indexes = list(sorted(indexes_and_labels_true_logits, key=lambda x: x[1], reverse=True)[:5])
      label_indexes, labels_true_logits = zip(*labels_logits_and_indexes)
      label_indexes = label_indexes

      print("labels_true_logits:", labels_true_logits)
      print("label_indexes: ", label_indexes)

      for row_index in range(0, max(len(label_indexes), len(pipeline_document.key_sentence_indexes))):

        s3_id_field = ""
        title_field = ""
        publication_date_field = ""
        abstract_field = ""

        if row_index == 0:
          s3_id_field = pipeline_document.document_key
          title_field = pipeline_document.title
          publication_date_field = pipeline_document.publication_date
          abstract_field = pipeline_document.abstract

        # AI model
        model_1_field = pipeline_document.reconstructed_sentences[label_indexes[row_index]] if row_index < len(
          label_indexes) else ""

        # Pipeline
        model_2_field = pipeline_document.reconstructed_sentences[
          pipeline_document.key_sentence_indexes[row_index]] if row_index < len(
          pipeline_document.key_sentence_indexes) else ""

        csv_wr.writerow((s3_id_field, title_field, publication_date_field, abstract_field, model_1_field, model_2_field))
      output_csv_file.write("\n")
