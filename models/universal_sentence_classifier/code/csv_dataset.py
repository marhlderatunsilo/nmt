import tensorflow as tf


def csv_dataset(file_names,
                column_names,
                feature_names,
                label_names,
                batch_size,
                shuffle=True):
  textDataset = tf.data.TextLineDataset(file_names)

  def map_to_csv_dict(csv_line_text):
    columns = tf.decode_csv(csv_line_text, record_defaults=[""] * len(column_names))

    features_dict = {}
    labels_dict = {}

    for index, column in enumerate(columns):

      column_name = column_names[index]

      if column_name in feature_names:
        features_dict[column_name] = column
      elif column_name in label_names:
        labels_dict[column_name] = column

    return features_dict, labels_dict

  csv_dataset = textDataset.map(map_to_csv_dict)

  if shuffle:
    csv_dataset = csv_dataset.shuffle(1000, reshuffle_each_iteration=True)

  return csv_dataset.batch(batch_size)
