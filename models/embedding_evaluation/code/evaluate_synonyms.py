"""
Evaluation of word embeddings: Do we find synonyms / spelling variants?

Currently, a prediction of a synonym entry (subject -> list of synonyms) is considered correct,
if *one* of the synonyms is found in the top n most similar keys in the vector space.
E.g. you make a look up of the subject and test your list of synonyms against the returned keys.
If there's just a single match, it's a 1, otherwise it's a 0. ;)

In order to run this, it should be adapted to use faiss for embedding lookups. Gensim seems very slow.
Because of this, the actual scoring part of the script (evaluate_synonyms()) has not been properly tested yet.
"""


import json
import time

import gensim

# Partly based on https://github.com/ltgoslo/norwegian-synonyms
from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.dict_utils import invert_dict_with_lists


class SynonymEvaluator(object):
  def __init__(self, embedding_path, synonym_map, collapsing_map, topn=10):
    """

    :param embedding_path:
    :param synonym_map: Map of subject -> synonyms. Should be display titles.
      Both subjects and synonyms will be converted to concept keys, using the collapsing_map.
    :param collapsing_map: Map from concept key -> display titles.
    :param topn: Use top n most similar words.
    """

    # Embeddings
    self.embedding_path = embedding_path
    self.embeddings = self.load_embeddings()
    self.embedding_vocab = list(self.embeddings.vocab.keys())  # Concept keys
    print("Size of embedding vocab (concept keys): {}".format(len(self.embedding_vocab)))

    # Collapsing map
    self.collapsing_map = collapsing_map
    self.orig_num_keys_collapsing_map = len(self.collapsing_map.keys())
    self.collapsing_vocab_concept_keys = set(self.collapsing_map.keys())  # Concept keys
    print("Size of collapsing vocab (concept keys): {}".format(len(self.collapsing_vocab_concept_keys)))

    collapsingKeysNotInVocab = self.collapsing_vocab_concept_keys - set(self.embedding_vocab)
    print("Keys in collapsing map but not in W2V vocabulary: ", len(collapsingKeysNotInVocab))
    print("10 of those keys: ", list(collapsingKeysNotInVocab)[:10])

    # Write collapsing keys, which are missing in vocab, to disk
    # with open("collapsingKeysNotInVocab.txt", "w") as output:
    #   for k in collapsingKeysNotInVocab:
    #     output.write(str(k)+"\n")

    # Remove the collapse keys not in vocab
    self.collapsing_vocab_concept_keys -= collapsingKeysNotInVocab
    for key in collapsingKeysNotInVocab:
      del self.collapsing_map[key]

    print("Removed {} keys from collapsing map, which were not in W2v vocabulary."
          .format(self.orig_num_keys_collapsing_map - len(self.collapsing_map.keys())))

    assert self.collapsing_vocab_concept_keys <= set(self.embedding_vocab), \
      "Concept keys in collapsing map is not a subset of the embeddings vocab."
    print("Passed: Concept keys in collapsing map is a subset of the embeddings vocab.")

    self.inverse_collapsing_map = invert_dict_with_lists(self.collapsing_map)
    self.collapsing_vocab_displays = set(self.inverse_collapsing_map.keys())  # Display titles
    print("Size of collapsing vocab (display titles): {}".format(len(self.collapsing_vocab_displays)))

    # Synonym map
    self.synonym_map_displays = synonym_map
    self.orig_num_entries_synonym_map = len(self.synonym_map_displays.keys())
    print("Num. entries in synonym map: {}  (Original)".format(self.orig_num_entries_synonym_map))
    self.synonym_map_concept_keys = self.replace_synonym_displays_with_concept_keys(self.synonym_map_displays)
    print("Num. entries in synonym map: {}  (Post replacement with collapsed keys)".format(
      len(self.synonym_map_concept_keys)))

    # Settings
    self.topn = topn

    # Num. entries where word or all of the synonyms is/are Out Of Vocabulary (misses concept key is collapsing_map).
    self.num_oov = self.orig_num_entries_synonym_map - len(self.synonym_map_concept_keys)

    # Initialization
    self.most_similar_map = None
    self.num_correct = None
    self.num_total_synonyms_found = None
    self.num_emb_lookup_errors = 0
    self.num_displays_without_concept_key = 0
    self.num_successful_lookups = 0
    self.precision = None
    self.recall = None
    self.found_synonyms_map = None
    print("Finished Initialization")

  def load_embeddings(self):
    return gensim.models.Word2Vec.load(self.embedding_path).wv

  def get_collapsed_key(self, word):
    """
    Get the key to the collapsed word
    :param word: Word to find collapsed key for
    :return: Key mapped to word in collapsing map.
    """
    return self.inverse_collapsing_map[word]

  def embedding_lookup(self, concept_key, topn, restrict_vocab):

    # TODO This is the part, which should be adapted to use faiss

    """
    :param concept_key: str
    :param topn: int
    :param restrict_vocab: Don't know, but you can pass it to gensim's similar_by_word().
    :return: Return the topn similar keys from the vector space.
    """

    concept_key = str(concept_key)

    if concept_key in self.embedding_vocab:
      try:
        similars = self.embeddings.similar_by_word(concept_key, topn=topn, restrict_vocab=restrict_vocab)
        self.num_successful_lookups += 1
        return similars
      except KeyError as e:
        self.num_emb_lookup_errors += 1
        return []
    else:
      raise KeyError("concept_key \'{}\' not in embeddings or collapsing map.".format(concept_key))

  def get_keys_for_displays(self, display_titles, check_lowercase=True):
    """
    Get the collapsed keys for a list of display titles, e.g. synonyms.
    Only returns unique keys. Not guaranteed to be in same order as input.
    :param display_titles: List of strings.
    :param check_lowercase: Whether to check the lowercase version if the original casing did not find a key.
    :return: List of *unique* keys.
    """

    assert isinstance(display_titles, list), "display_titles must be a list of strings."

    def get_key(outer, display, check_lowercase):
      """

      :param outer: the outer object ("self").
      :param display: word to get concept key for.
      :param check_lowercase: Whether to recursively check the lowercase version of display if
        the original casing did not have a concept key.
      :return: concept key or None-
      """
      try:
        return self.get_collapsed_key(display)
      except KeyError as e:
        if check_lowercase:
          return get_key(outer, display.lower(), check_lowercase=False)
        else:
          outer.num_synonym_lookup_errors += 1
          return None

    synonym_keys = set([get_key(outer=self, display=syn, check_lowercase=check_lowercase) for syn in display_titles])
    synonym_keys = [x for x in list(synonym_keys) if x is not None]  # Remove the None

    return synonym_keys

  def replace_synonym_displays_with_concept_keys(self, synonym_map, check_lowercase=True):
    """
    Replaces keys and values in the synonym map with their concept keys.

    """
    concept_key_synonym_map = dict()
    for word, synonyms in synonym_map.items():
      word_concept_key_list = self.get_keys_for_displays([word], check_lowercase=check_lowercase)
      if len(word_concept_key_list) > 0:
        continue
      synonym_keys = self.get_keys_for_displays(synonyms, check_lowercase=check_lowercase)
      if len(synonym_keys) > 0:
        concept_key_synonym_map[word_concept_key_list[0]] = synonym_keys
    return concept_key_synonym_map

  def extract_most_similar(self, restrict_vocab=None):
    """
    For each subject in the synonym map, get the topn most similar keys in the vector space.

    :param restrict_vocab:
    :return: None, but side effect is a map of {subject -> most similar keys}.
    """

    most_similar_mapping = {}

    for word_key, synonym_keys in self.synonym_map_concept_keys.items():
      # Find topn most similar words
      most_similar = self.embedding_lookup(word_key, topn=self.topn,
                                           restrict_vocab=restrict_vocab)
      if len(most_similar) > 0:
        most_similar_mapping[word_key] = most_similar

    self.most_similar_map = most_similar_mapping
    print("Size of most_similar_map: {}".format(len(self.most_similar_map.keys())))

  def evaluate_synonyms(self, topn=None):
    """
    Evaluate embeddings on ability to find synonyms in the first n most similar words.
    Asks the question: Do we find one or more correctly predicted synonyms for word_i?
    :return: Side effects are: map of found synonyms per subject, precision, recall, and other stats.
    """

    # Words with one or more correctly predicted synonym
    num_correct = 0

    # Total number of correctly predicted synonyms
    num_total_synonyms_found = 0

    # Words in vocabulary
    num_predicted = 0

    # Found synonyms mapping
    found_synonyms_mapping = {}

    topn = topn if topn is not None else self.topn

    for word_key, synonym_keys in self.synonym_map_concept_keys.items():

      # If one of the synonyms was collapsed to the same key as word
      if word_key in synonym_keys:
        num_predicted += 1
        num_correct += 1

      # Word for which we have predicted synonyms
      elif word_key in self.most_similar_map:
        num_predicted += 1
        # Synonyms in most similar words
        found_synonyms_mapping[word_key] = self.intersect_lists(self.most_similar_map[word_key][:topn], synonym_keys)
        # Do we find one or more correctly predicted synonyms?
        if len(found_synonyms_mapping[word_key]) > 0:
          num_correct += 1
          num_total_synonyms_found += len(found_synonyms_mapping[word_key])

    self.num_correct = num_correct
    self.num_total_synonyms_found = num_total_synonyms_found
    self.precision = num_correct / num_predicted
    self.recall = num_correct / self.orig_num_entries_synonym_map
    self.found_synonyms_map = found_synonyms_mapping

  @staticmethod
  def intersect_lists(a, b):
    return list(set(a).intersection(set(b)))

  def print_results(self):
    if self.precision is None:
      raise ValueError("Evaluate results before printing them, please.")
    print("Correct: {}".format(self.num_correct))
    print("Total number of synonyms found: {}".format(self.num_total_synonyms_found))
    print("Precision: %.4f" % float(self.precision))
    print("Recall: %.4f" % float(self.recall))
    print("Out-Of-Vocabulary: %.4f" % (self.num_oov / len(self.synonym_map_concept_keys) * 100))
    print("Num key lookup errors: {}".format(self.num_emb_lookup_errors))
    print("Num synonym lookup errors: {}".format(self.num_displays_without_concept_key))
    print("Num successful lookups: {}".format(self.num_successful_lookups))

  def write_found_synonyms_to_json(self, path):
    """
    :param path: file path ending in ".json"
    """

    def dump_json(data, filename):
      with open(filename, 'wt', encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=2, sort_keys=True)

    dump_json(self.found_synonyms_map, path)

  def test_embedding_lookup(self):
    """
    Makes lookup in embeddings for the first key in the embedding vocabulary.
    """
    print("-" * 25)
    print("Testing embedding lookup")
    t_test = time.time()
    # print(self.embedding_vocab)
    word = self.embedding_vocab[0]
    print("Word: ", word)
    similars = self.embedding_lookup(word, topn=10, restrict_vocab=None)
    t_test_done = time.time()
    print("Found:")
    print(similars)
    print("Took {}s".format(t_test_done - t_test))
    print("-" * 25)


def main():
  print("Welcome to the synonym evaluation!")
  t_start = time.time()
  synonyms_path = '../data/synonyms_wikidata_v4_pmc_v9.json'  # subject -> [synonyms]
  embedding_path = '../data/pmc_w2v_v9/w2v-output.5'
  collapsing_map_path = '../data/collapsing_map_pmc_v9.json'  # conceptKey -> [displays]
  restriction = None  # Int or None

  with open(synonyms_path, 'rt', encoding='utf-8') as s:
    synonym_map = json.load(s)

  with open(collapsing_map_path, 'rt', encoding='utf-8') as s:
    collapsing_map = json.load(s)

  syn_eva = SynonymEvaluator(embedding_path=embedding_path, synonym_map=synonym_map,
                             collapsing_map=collapsing_map, topn=50)
  print("Total Time: {}".format(time.time() - t_start))

  syn_eva.test_embedding_lookup()

  # raise ValueError(" stopping") # Used in dev.

  # We already removed synonyms with no embedding, so no need to check them again
  syn_eva.extract_most_similar(restrict_vocab=restriction)
  print("Extracted nearest neighbours.")
  print("Total Time: {}".format(time.time() - t_start))

  syn_eva.evaluate_synonyms()
  syn_eva.print_results()
  syn_eva.write_found_synonyms_to_json("../data/found_synonyms_pmc_v8.json")

  print("\nTotal Time: {}".format(time.time() - t_start))


if __name__ == '__main__':
  main()
