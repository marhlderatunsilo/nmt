Creating collapsing map from concept metrics
Creating synonym dataset


Order to R/python scripts in:

Note, you may have to set paths, etc. within scripts. 

1. creating_synonym_dataset.R 
 - Use when MESH or wikidata are *updated* (If you want to!)

2. create_collapsing_map.R
 - Creates collapsing map (key, display, alternative displays) as .csv 
 - and vocabulary (list of displays with frequency >= 50 (optional))

3. collapsing_map_to_json.ipynb
 - Reads the collapsing map from step 2 and makes a JSON
 - with conceptKey -> list of all displays

4. create_synonym_dataset_with_known_concepts.R
 - Uses vocabulary from step 2 to filter the synonym dataset from step 1
 - to only include entries where the subject and at least one of the synonyms have
 - conceptKeys (i.e. are in the vocabulary).

5. Synonyms_csv_to_json.ipynb
 - Create json version of synonyms dataset, which is what is used in evaluation
 - subject -> list of synonyms