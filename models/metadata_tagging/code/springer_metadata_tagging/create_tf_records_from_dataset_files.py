import operator

import numpy

from models.metadata_tagging.code.springer_metadata_tagging.metadata_dataset_utils import MetadataDataset
from tfDeepNLP.gazetteers.gazetteers_lookup import lookup_gazetteers
from tfDeepNLP.models.seq2tag.dataset_iterator import ner_iterator
from tfDeepNLP.utils.dataset_utils import DatasetPartitioner, DatasetCombiner
from tfDeepNLP.utils.tf_records_utils import *
from tfDeepNLP.utils.vocab_utils import create_python_vocab_table, create_vocab_table


def create_vocabs(train_dataset):
  token_vocab = {}
  char_vocab = {}

  print("dataset size: ", len(list(train_dataset)))

  for dataset in [train_dataset]:
    for tags, tokens in dataset:
      for token in tokens:
        if token in token_vocab:
          token_vocab[token] += 1
        else:
          token_vocab[token] = 1

        for char in token:
          if char in char_vocab:
            char_vocab[char] += 1
          else:
            char_vocab[char] = 1

  def filter_token_vocab_fn(key_value):
    _, value = key_value
    if value >= 10:
      return True
    else:
      return False

  def filter_char_vocab_fn(key_value):
    _, value = key_value
    if value >= 20:
      return True
    else:
      print("Not including: ", _)
      return False

  print("Filtering vocab...")
  char_vocab = filter(filter_char_vocab_fn, char_vocab.items())
  char_vocab = sorted(char_vocab, key=operator.itemgetter(1), reverse=True)

  token_vocab = filter(filter_token_vocab_fn, token_vocab.items())
  token_vocab = sorted(token_vocab, key=operator.itemgetter(1), reverse=True)

  # print(token_vocab)

  # print("Removing person/organistation names from vocab")
  # FEATURES_FROM_HPARAMS = [read_gazetteer_file_to_list(gazetteer_file) for gazetteer_file in
  #                          hparams.gazetteer_feature_files] if hparams.gazetteer_feature_files != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE else []
  #
  token_vocab = ["$UNK$"] + list(set([key for key, value in token_vocab]))
  char_vocab = ["$UNK$"] + list(set([key for key, value in char_vocab]))
  #
  # for names_list in FEATURES_FROM_HPARAMS:
  #   vocab.difference_update(names_list)

  print("Final char vocab size is: ", len(char_vocab))
  print("Final token vocab size is: ", len(token_vocab))

  with open("models/metadata_tagging/data/org_data/train_token_vocab.txt", mode="w",
            encoding="utf-8") as train_token_vocab_f:
    train_token_vocab_f.writelines(["%s\n" % item for item in token_vocab])

  with open("models/metadata_tagging/data/org_data/train_char_vocab.txt", mode="w",
            encoding="utf-8") as train_char_vocab_f:
    train_char_vocab_f.writelines(["%s\n" % item for item in char_vocab])

    # print("vocab: ", vocab)
  return token_vocab, char_vocab


def create_tf_records(hparams):
  # dev_filename = "%s.%s" % (hparams.dev_prefix, hparams.src)
  # eval_filename = "%s.%s" % (hparams.test_prefix, hparams.src)
  # train_filename =  #"%s.%s" % (hparams.train_prefix, hparams.src)

  token_vocab_train_dev_file = hparams.token_vocab_train_dev_file
  token_vocab_inference_file = hparams.token_vocab_inference_file
  character_level_vocab_file = hparams.char_vocab_file
  tag_vocab_file = hparams.tag_vocab_file

  py_train_dev_token_vocab = create_python_vocab_table(token_vocab_train_dev_file)
  py_inference_token_vocab = create_python_vocab_table(token_vocab_inference_file)
  py_character_level_vocab = create_python_vocab_table(character_level_vocab_file)
  py_tags_vocab = create_python_vocab_table(tag_vocab_file)

  metadata_dataset = MetadataDataset("models/metadata_tagging/data/org_data/springer_corpus_clean.txt",
                                     py_train_dev_token_vocab, py_tags_vocab)
  print("Done loading dataset")

  train_dataset_100k = DatasetPartitioner(metadata_dataset, 100, range(0, 98))
  train_dataset_org = MetadataDataset("models/metadata_tagging/data/org_data/springer_corpus.txt",
                                      py_train_dev_token_vocab, py_tags_vocab)

  train_dataset = DatasetCombiner([train_dataset_100k, train_dataset_org])

  dev_dataset = DatasetPartitioner(metadata_dataset, 100, [98])
  eval_dataset = DatasetPartitioner(metadata_dataset, 100, [99])

  # create_vocabs(train_dataset)
  # return

  train_dataset_tf = tf.data.Dataset.from_generator(train_dataset.__iter__,
                                                    output_types=(tf.int32, tf.string),
                                                    output_shapes=(
                                                      tf.TensorShape([None]),
                                                      tf.TensorShape([None])))

  dev_dataset_tf = tf.data.Dataset.from_generator(dev_dataset.__iter__,
                                                  output_types=(tf.int32, tf.string),
                                                  output_shapes=(
                                                    tf.TensorShape([None]),
                                                    tf.TensorShape([None])))

  eval_dataset_tf = tf.data.Dataset.from_generator(eval_dataset.__iter__,
                                                   output_types=(tf.int32, tf.string),
                                                   output_shapes=(
                                                     tf.TensorShape([None]),
                                                     tf.TensorShape([None])))

  # words_table, chars_table, tags_table, unk_word_idx, unk_char_idx = get_resources_for_graph(hparams, mapping_index)

  train_dev_token_vocab = create_vocab_table(token_vocab_train_dev_file)
  eval_token_vocab = create_vocab_table(token_vocab_inference_file)
  character_level_vocab = create_vocab_table(character_level_vocab_file)
  # tags_vocab = create_vocab_table(tag_vocab_file)

  train_iterator = ner_iterator.get_iterator(
    train_dataset_tf,
    144,
    train_dev_token_vocab,
    character_level_vocab,
    py_train_dev_token_vocab[hparams.unk],
    hparams.char_level_representation_max_len,
    num_parallel_calls=hparams.parallel_iterations,
    shuffle=False,
    label_dataset=True,
    num_splits=None,
    prefetch=False,
    perform_batching=True,
  )

  dev_iterator = ner_iterator.get_iterator(
    dev_dataset_tf,
    144,
    train_dev_token_vocab,
    character_level_vocab,
    py_train_dev_token_vocab[hparams.unk],
    hparams.char_level_representation_max_len,
    num_parallel_calls=hparams.parallel_iterations,
    shuffle=False,
    label_dataset=True,
    num_splits=None,
    prefetch=False,
    perform_batching=True,
  )

  eval_iterator = ner_iterator.get_iterator(
    eval_dataset_tf,
    144,
    eval_token_vocab,
    character_level_vocab,
    py_inference_token_vocab[hparams.unk],
    hparams.char_level_representation_max_len,
    num_parallel_calls=hparams.parallel_iterations,
    shuffle=False,
    label_dataset=True,
    num_splits=None,
    prefetch=False,
    perform_batching=True,
  )

  for prefix_dataset_iterator in [("training", train_iterator), ("development", dev_iterator),
                                  ("evaluation", eval_iterator)]:

    prefix, (initializer, dataset_iterator) = prefix_dataset_iterator

    num_written_examples = 0
    with tf.Session() as sess, tf.device('/cpu:0'):

      if hparams.gazetteers:
        gazetteers_features = lookup_gazetteers(hparams, dataset_iterator.words)
      else:
        gazetteers_features = None

      sess.run(tf.tables_initializer())
      sess.run(initializer)

      tf_records_destination_path = hparams.work_dir + prefix + '_seq2tag.tfrecords'
      writer = create_tfr_writer(tf_records_destination_path
                                 )  # out_dir=hparams.out_dir

      while (True):
        try:

          if hparams.gazetteers:
            (batch_words, batch_word_ids_len, batch_word_ids, batch_char_ids, batch_char_ids_len,
             batch_labels), batch_gazetteers_features_py = sess.run(
              (dataset_iterator, gazetteers_features))
          else:
            batch_gazetteers_features_py = None
            (batch_words, batch_word_ids_len, batch_word_ids, batch_char_ids, batch_char_ids_len,
             batch_labels) = sess.run(
              (dataset_iterator))

          # print("word_ids_len:", word_ids_len)
          # print("words:", words)

          batch_size = batch_words.shape[0]

          for batch_index_counter in range(0, batch_size):
            words = batch_words[batch_index_counter]
            word_ids_len = batch_word_ids_len[batch_index_counter]
            word_ids = batch_word_ids[batch_index_counter]
            char_ids = batch_char_ids[batch_index_counter]
            char_ids_len = batch_char_ids_len[batch_index_counter]
            labels = batch_labels[batch_index_counter]

            # Remove padding
            words = words[:word_ids_len]
            char_ids_len = char_ids_len[:word_ids_len]
            word_ids = word_ids[:word_ids_len]
            max_char_ids_len = numpy.amax(char_ids_len)
            char_ids = char_ids[:word_ids_len, :max_char_ids_len]
            labels = labels[:word_ids_len]

            char_ids_shape = list(char_ids.shape)

            feature_mapping = {
              'words': bytes_array_feature(words),
              'word_ids_len': int64_feature(word_ids_len),
              'word_ids': int64_array_feature(word_ids),
              'char_ids': int64_2darray_feature(char_ids),
              'char_ids_shape': int64_array_feature(char_ids_shape),
              'char_ids_len': int64_array_feature(char_ids_len),
              'labels': int64_array_feature(labels),
            }

            if hparams.gazetteers:
              gazetteers_features_py = batch_gazetteers_features_py[batch_index_counter]
              gazetteers_features_py = gazetteers_features_py[:word_ids_len]

              feature_mapping['gazetteers'] = float_2darray_feature(gazetteers_features_py)
              feature_mapping['gazetteers_shape'] = int64_array_feature(list(gazetteers_features_py.shape))

            # Write to records
            example = tf.train.Example(features=tf.train.Features(feature=feature_mapping))

            writer.write(example.SerializeToString())
            num_written_examples = num_written_examples + 1

        except tf.errors.OutOfRangeError:
          writer.close()
          print(prefix_dataset_iterator, " had ", num_written_examples, "examples written")
          break

  return
