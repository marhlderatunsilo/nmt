"""
Definitions model: Preprocesses text and saves to TFRecords.
"""
from tfDeepNLP.input_pipes.text_preprocessor import TextPreprocessor, shuffle, int64_feature, int64_array_feature, \
  bytes_feature

import tensorflow as tf
from fuzzywuzzy import fuzz  # requires python-Levenshtein (use pip install fuzzywuzzy[speedup])
import numpy as np


class DefsTextPreprocessor(TextPreprocessor):
  """
  Methods for preprocessing text and converting data to TFRecords.
  """

  def __init__(self, data_input, txt_col_name, tgt_col_name, concept_col_name):
    """
    :param data_input: pandas data frame with text column and target column
    :param txt_col_name: Name of text column for subsetting data frame
    :param tgt_col_name: Name of target column for subsetting data frame
    """
    super().__init__()

    self.data_input = data_input
    self.txt_col_name = txt_col_name
    self.tgt_col_name = tgt_col_name
    self.concept_col_name = concept_col_name

    self.text = list(self.data_input[self.txt_col_name])
    self.text_upper = []
    self.targets = list(self.data_input[self.tgt_col_name])
    self.concepts = list(self.data_input[self.concept_col_name])

    self.extracted_features = []

  def extract_word_features(self, txt=None,
                            features='all',
                            transpose=False):
    """

    :param txt: List of strings.
    :param features: List of feature names to extract, or simply 'all'.
      E.g. ['is_title', 'all_upper', 'all_lower', 'mixed_case', 'is_punctuation', 'is_numeric'].
    :param transpose: bool
    """
    assert features is not None, "features must be a list of strings or 'all'."

    if features == 'all':
      features = ['is_title', 'all_upper', 'all_lower', 'mixed_case', 'is_punctuation', 'is_numeric']

    return super().extract_word_features(txt, features=features, transpose=transpose)

  def one_hot_encoder(self, targets=None, sparse_output=False, invert_order=False):
    """
    :param invert_order: only works for binary classification atm.
    """
    one_hot_targets = super().one_hot_encoder(targets, sparse_output)
    if invert_order:
      return 1 - one_hot_targets
    else:
      return one_hot_targets

  def fuzzy_tagger(self, tokens=None, target=None, lev_threshold=0.8, ngram_lev_threshold=0.9):
    """
    Fuzzy tagging of concepts in string, using Levenshtein similarity.
    It first attempts to match the entire concept.
    If it is not found, it matches 1-grams to find the concept tokens distributed in the text.

    tokens: list of tokens in original order
    target: list of targets in original order
    lev_threshold: cutoff for Levenshtein similarity (0.0-1.0)
    ngram_lev_threshold: cutoff for Levenshtein similarity (0.0-1.0) in ngram matching
    """

    self._check_str_list(tokens)
    self._check_str_list(target)
    if not isinstance(lev_threshold, float) or not 0.0 < lev_threshold < 1.0:
      raise ValueError("lev_threshold must be a float between 0.0 and 1.0.")
    if not isinstance(ngram_lev_threshold, float) or not 0.0 < ngram_lev_threshold < 1.0:
      raise ValueError("ngram_lev_threshold must be a float between 0.0 and 1.0.")

    tags = []
    # N-gram matching
    # TODO: Perhaps checking on smaller ngrams and combining that result with the following 1 gram matching would make sense.
    n_gram_size = len(target)
    if n_gram_size > 1:
      n_grams = [tokens[i:i + n_gram_size] for i in range(len(tokens) - n_gram_size + 1)]
      lev_similarity = [fuzz.ratio(tok, target) / 100.0 for tok in n_grams]
      if lev_similarity:
        max_lev_similarity = max(lev_similarity)
        if max_lev_similarity > ngram_lev_threshold:
          max_lev_sim_idx = np.argmax(lev_similarity)
          tags += ["<no-in>"] * max_lev_sim_idx + ["<yes-begin>"] + ["<yes-in>"] * (n_gram_size - 1)
          tags += ["<no-in>"] * (len(tokens) - len(tags))
          return tags

    # 1-gram distributed search
    lev_similarity = [[fuzz.ratio(tok, tgt) / 100.0, tok, tgt] for tgt in target for tok in tokens]

    # tagging
    begin_target = target[0]
    matches = [ls for ls in lev_similarity if ls[0] > lev_threshold]

    seen = []
    for tok in tokens:
      tok_matches = [m for m in matches if m[1] == tok]
      if len(tok_matches) == 0:
        tags += ["<no-in>"]
      else:
        matched_targets = [ls[2] for ls in tok_matches]
        already_seen = any([mt in seen for mt in matched_targets])
        if already_seen:
          tags += ["<no-in>"]
        else:
          seen += matched_targets
          if begin_target in matched_targets:
            tags += ["<yes-begin>"]
          else:
            tags += ["<yes-in>"]

    return tags

  def tag_concepts(self, txt, concepts, lev_threshold=0.8, ngram_lev_threshold=0.9):
    def call_fuzzy_tagger(txt, concept, lev_threshold=0.8, ngram_lev_threshold=0.9):
      tokens = self.lowercase(str.split(txt, " "))
      concept = self.lowercase(str.split(concept, " "))
      tags = self.fuzzy_tagger(tokens, concept, lev_threshold=lev_threshold, ngram_lev_threshold=ngram_lev_threshold)
      return " ".join(tags)

    return [call_fuzzy_tagger(t, c, lev_threshold, ngram_lev_threshold) for t, c in zip(txt, concepts)]

  def shuffle(self, random_state=None):
    self.text_lower, self.text, self.targets, self.concepts, self.target_tags, self.seq_lengths, \
    self.extracted_features[0], self.extracted_features[1], \
    self.extracted_features[2], self.extracted_features[3], \
    self.extracted_features[4], self.extracted_features[5] = shuffle(self.text_lower, self.text,
                                                                     self.targets, self.concepts,
                                                                     self.target_tags,
                                                                     self.seq_lengths,
                                                                     self.extracted_features[0],
                                                                     self.extracted_features[1],
                                                                     self.extracted_features[2],
                                                                     self.extracted_features[3],
                                                                     self.extracted_features[4],
                                                                     self.extracted_features[5],
                                                                     random_state=random_state)

  def preprocess(self):
    self.text = self.space_punctuation(self.text)
    self.concepts = self.space_punctuation(self.concepts)
    self.target_tags = self.tag_concepts(self.text, self.concepts)
    self.seq_lengths = self.extract_sequence_lengths(self.text)
    self.text = self.pad_sentences(self.text, seq_lengths=self.seq_lengths)
    self.extracted_features = self.extract_word_features(self.text)
    self.text_lower = self.lowercase(self.text)
    self.concepts = self.lowercase(self.concepts)
    # As definition is before not-definition in the alphabet, we inverse the targets so definitions are 1
    self.targets = self.one_hot_encoder(self.targets, invert_order=True)
    self.shuffle()

  def write_to_tfr(self, f_name='text_data.tfrecords', out_dir=None):
    writer = self.create_tfr_writer(f_name, out_dir=out_dir)
    num_examples = len(self.text_lower)
    for i in range(num_examples):
      example = tf.train.Example(features=tf.train.Features(feature={
        'text_lower': bytes_feature(self.text_lower[i]),
        'text_upper': bytes_feature(self.text[i]),
        'target': int64_feature(self.targets[i]),
        'tags': bytes_feature(self.target_tags[i]),
        'concept': bytes_feature(self.concepts[i]),
        'sequence_length': int64_feature(self.seq_lengths[i]),
        'title_cased': int64_array_feature(self.extracted_features[0][i]),
        'all_upper_case': int64_array_feature(self.extracted_features[1][i]),
        'all_lower_case': int64_array_feature(self.extracted_features[2][i]),
        'mixed_case': int64_array_feature(self.extracted_features[3][i]),
        'punctuation': int64_array_feature(self.extracted_features[4][i]),
        'numeric': int64_array_feature(self.extracted_features[5][i])
      }))
      writer.write(example.SerializeToString())
    writer.close()
