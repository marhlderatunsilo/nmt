"""
Running preprocessing and conversion to TFRecords on dataset.
"""

# from pprint import pprint

import pandas as pd

from models.definitions.code.text_preprocessor import DefsTextPreprocessor

dset = "DEV"
data_path = "../../wiki_defs_data/en_wiki_science_definitions_{}.csv".format(dset)
out_dir = "../../wiki_defs_data/"
defs_only = True

# data_path = "/Users/ludvigolsen/unsilo-code/tfDeepNLP/models/definitions/data/wiki_defs_data/en_wiki_science_definitions_DEV.csv"
# out_dir = "/Users/ludvigolsen/unsilo-code/tfDeepNLP/models/definitions/data/wiki_defs_data/"

"""
Load data
"""

data = pd.read_csv(data_path, engine="python")
if defs_only:
  data = data.loc[data["class"] == "definition"]
# data = data.head(10).tail(5)  # for quick dev.
# pprint(data)

"""
Run preprocessing
"""
# Be aware of targets being one-hot encoded alphabetically (for definitions, target array has later been inversed)
preprocessor = DefsTextPreprocessor(data, txt_col_name="sentence", tgt_col_name="class", concept_col_name="title")
preprocessor.preprocess()
print("Finished preprocessing.")

# pprint(preprocessor.text)
# pprint(preprocessor.concepts)
# pprint(preprocessor.target_tags)
"""
Write to TFRecords
"""

preprocessor.write_to_tfr(f_name="s2c_wiki_defs_{}.tfrecords".format(dset), out_dir=out_dir)
