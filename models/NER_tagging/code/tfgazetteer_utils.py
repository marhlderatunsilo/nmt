import tensorflow as tf
from nltk.corpus import gazetteers

def create_binary_table_from_list(list):
    return tf.contrib.lookup.HashTable(
        tf.contrib.lookup.KeyValueTensorInitializer(keys=tf.constant(list),
                                                    values=tf.ones([len(list)], dtype=tf.float32),
                                                    key_dtype=tf.string,
                                                    value_dtype=tf.float32), default_value=0.0)


COUNTRIES = [country for filename in ('isocountries.txt', 'countries.txt')
             for country in gazetteers.words(filename)] + ['EU']

def create_gazetteer_generator(simple_gazetteer_lists=None):
    gazetteer_length = 2
    tf_gazetteer_dicts = []

    if simple_gazetteer_lists:

        gazetteer_length += len(simple_gazetteer_lists)

        for gazetteer_list in simple_gazetteer_lists:
            table = create_binary_table_from_list(gazetteer_list)
            tf_gazetteer_dicts.append(table)

    gazetteer_length = gazetteer_length * 2 # We provide both a feature and a counter feature

    # PERSON
    person_prefixes = ['mr', 'mrs', 'ms', 'miss', 'dr', 'rev', 'judge',
                       'justice', 'honorable', 'hon', 'rep', 'sen', 'sec',
                       'minister', 'chairman', 'succeeding', 'says', 'president']
    person_prefix_table = create_binary_table_from_list(person_prefixes)

    def _look_up_gazetteer(table, n_gram):
        result = table.lookup(n_gram)
        return [result, tf.abs(result - 1)]

    def is_person_prefix(prefix):
        split_prefix = tf.string_split([prefix], delimiter='.').values

        prefix = tf.cond(tf.shape(split_prefix)[0] > 0,
                         true_fn=lambda: split_prefix[0],
                         false_fn=lambda: prefix)

        return _look_up_gazetteer(person_prefix_table, prefix)

    # NATIONALITY
    nationalities = gazetteers.words('nationalities.txt')
    nationalities_table = create_binary_table_from_list(nationalities)

    def is_nationalities(nationality):
        return _look_up_gazetteer(nationalities_table, nationality)
        # nationality_minus1 = tf.string_join(tf.string_split([nationality], "").values[:-1], separator="")
        # nationality_minus2 = tf.string_join(tf.string_split([nationality], "").values[:-2], separator="")

        # return tf.sign(nationalities_table.lookup(nationality) + nationalities_table.lookup(
        #     nationality_minus1) + nationalities_table.lookup(nationality_minus2))

    # This implements a "counter" feature which is 1 when the n_gram is not in the gazetteer list

    def _look_up_gazetteers(n_gram):

        print("is_person_prefix(n_gram): ", is_person_prefix(n_gram))
        print("is_nationalities(n_gram): ", is_nationalities(n_gram))

        return tf.concat(([_look_up_gazetteer(tf_dict, n_gram) for tf_dict in tf_gazetteer_dicts] +
                          [is_person_prefix(n_gram), is_nationalities(n_gram)]
                          ), 0)

    def look_up_gazetteers(n_grams):

        print("n_grams: ", n_grams)

        original_shape = tf.shape(n_grams)
        n_grams_flat = tf.reshape(n_grams, [-1])

        print("n_grams_flat: ", n_grams_flat)

        flat_gazetteers = tf.map_fn(_look_up_gazetteers, n_grams_flat, dtype=tf.float32)

        print("flat_gazetteers: ", flat_gazetteers)
        print("original_shape + [gazetteer_length]", tf.concat([original_shape, [gazetteer_length]], axis=0))

        gazetteers = tf.reshape(flat_gazetteers, tf.concat([original_shape, [gazetteer_length]], axis=0))

        return gazetteers

    return look_up_gazetteers
