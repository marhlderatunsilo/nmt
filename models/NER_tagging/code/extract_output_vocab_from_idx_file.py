import json

def main():
  with open("../data/conll2k/idx.json", mode="r", encoding="utf-8") as idx_f:
    mapping_idx = json.load(idx_f)

    with open("../data/conll2k/tag_vocab.txt", mode="w", encoding="utf-8") as tag_vocab_f:
      python_tags_vocab = [i[0] for i in sorted(mapping_idx["IOB2"].items(), key=lambda x: x[1])]
      tag_vocab_f.writelines(["%s\n" % item for item in python_tags_vocab])

    with open("../data/conll2k/char_vocab.txt", mode="w", encoding="utf-8") as char_vocab_f:
      VOCAB_CHAR = "vocab_char"
      python_chars_vocab = [i[0] for i in sorted(mapping_idx[VOCAB_CHAR].items(), key=lambda x: x[1])]
      char_vocab_f.writelines(["%s\n" % item for item in python_chars_vocab])

    with open("../data/conll2k/train_token_vocab.txt", mode="w", encoding="utf-8") as train_token_vocab_f:
      python_tokens_train_vocab = [i[0] for i in sorted(mapping_idx["no_update_emb_train_dev"].items(), key=lambda x: x[1])]
      train_token_vocab_f.writelines(["%s\n" % item for item in python_tokens_train_vocab])

    with open("../data/conll2k/infer_token_vocab.txt", mode="w", encoding="utf-8") as infer_token_vocab_f:
      python_tokens_infer_vocab = set([i[0] for i in sorted(mapping_idx["no_update_emb_all"].items(), key=lambda x: x[1])])
      infer_token_vocab_f.writelines(["%s\n" % item for item in python_tokens_infer_vocab])

if __name__ == '__main__':
  main()
