from tfDeepNLP.gazetteers.gazetteers_lookup import lookup_gazetteers
from tfDeepNLP.models.seq2tag.dataset_iterator import ner_iterator
from tfDeepNLP.models.seq2tag.dataset_iterator.iob_dataset_utils import IOBDataset
from tfDeepNLP.utils.tf_records_utils import *
from tfDeepNLP.utils.vocab_utils import create_python_vocab_table, create_vocab_table

def create_tf_records(hparams):
  dev_filename = "%s.%s" % (hparams.dev_prefix, hparams.src)
  eval_filename = "%s.%s" % (hparams.test_prefix, hparams.src)
  train_filename = "%s.%s" % (hparams.train_prefix, hparams.src)

  token_vocab_train_dev_file = hparams.token_vocab_train_dev_file
  token_vocab_inference_file = hparams.token_vocab_inference_file
  character_level_vocab_file = hparams.char_vocab_file
  tag_vocab_file = hparams.tag_vocab_file

  py_train_dev_token_vocab = create_python_vocab_table(token_vocab_train_dev_file)
  py_inference_token_vocab = create_python_vocab_table(token_vocab_inference_file)
  py_character_level_vocab = create_python_vocab_table(character_level_vocab_file)
  py_tags_vocab = create_python_vocab_table(tag_vocab_file)

  train_dataset = IOBDataset(train_filename, py_train_dev_token_vocab, py_tags_vocab,
                             collapse_tag_vocab=hparams.collapse_tag_vocab)
  dev_dataset = IOBDataset(dev_filename, py_train_dev_token_vocab, py_tags_vocab,
                           collapse_tag_vocab=hparams.collapse_tag_vocab)
  eval_dataset = IOBDataset(eval_filename, py_inference_token_vocab, py_tags_vocab,
                            collapse_tag_vocab=hparams.collapse_tag_vocab)

  # vocab = set()
  # for dataset in [dev_dataset, train_dataset, eval_dataset]:
  #   for tags, tokens in dataset:
  #     vocab.update(tokens)

  # with open("models/NER_tagging/data/chemical_ner/chemd/train_token_vocab.txt", mode="w",
  #           encoding="utf-8") as train_token_vocab_f:
  #   train_token_vocab_f.writelines(["%s\n" % item for item in vocab])
  # return

  train_dataset_tf = tf.data.Dataset.from_generator(train_dataset.__iter__,
                                                    output_types=(tf.int32, tf.string),
                                                    output_shapes=(
                                                      tf.TensorShape([None]),
                                                      tf.TensorShape([None])))

  dev_dataset_tf = tf.data.Dataset.from_generator(dev_dataset.__iter__,
                                                  output_types=(tf.int32, tf.string),
                                                  output_shapes=(
                                                    tf.TensorShape([None]),
                                                    tf.TensorShape([None])))

  eval_dataset_tf = tf.data.Dataset.from_generator(eval_dataset.__iter__,
                                                   output_types=(tf.int32, tf.string),
                                                   output_shapes=(
                                                     tf.TensorShape([None]),
                                                     tf.TensorShape([None])))

  # words_table, chars_table, tags_table, unk_word_idx, unk_char_idx = get_resources_for_graph(hparams, mapping_index)

  train_dev_token_vocab = create_vocab_table(token_vocab_train_dev_file)
  eval_token_vocab = create_vocab_table(token_vocab_inference_file)
  character_level_vocab = create_vocab_table(character_level_vocab_file)
  # tags_vocab = create_vocab_table(tag_vocab_file)

  train_iterator = ner_iterator.get_iterator(
    train_dataset_tf,
    hparams.batch_size,
    train_dev_token_vocab,
    character_level_vocab,
    py_train_dev_token_vocab[hparams.unk],
    hparams.char_level_representation_max_len,
    shuffle=False,
    label_dataset=True,
    num_splits=None,
    prefetch=False,
    perform_batching=False,
  )

  dev_iterator = ner_iterator.get_iterator(
    dev_dataset_tf,
    hparams.batch_size,
    train_dev_token_vocab,
    character_level_vocab,
    py_train_dev_token_vocab[hparams.unk],
    hparams.char_level_representation_max_len,
    shuffle=False,
    label_dataset=True,
    num_splits=None,
    prefetch=False,
    perform_batching=False,
  )

  eval_iterator = ner_iterator.get_iterator(
    eval_dataset_tf,
    hparams.batch_size,
    eval_token_vocab,
    character_level_vocab,
    py_inference_token_vocab[hparams.unk],
    hparams.char_level_representation_max_len,
    shuffle=False,
    label_dataset=True,
    num_splits=None,
    prefetch=False,
    perform_batching=False,
  )

  for prefix_dataset_iterator in [("training", train_iterator), ("development", dev_iterator), ("evaluation", eval_iterator)]:

    prefix, (initializer, dataset_iterator) = prefix_dataset_iterator

    num_written_examples = 0
    with tf.Session() as sess:

      gazetteers_features = lookup_gazetteers(hparams, dataset_iterator.words)

      sess.run(tf.tables_initializer())
      sess.run(initializer)

      tf_records_destination_path = hparams.out_dir + "../" + prefix + ("_collapsed" if hparams.collapse_tag_vocab else "") + '_seq2tag.tfrecords'
      writer = create_tfr_writer(tf_records_destination_path
        )  # out_dir=hparams.out_dir

      while (True):
        try:

          (words, word_ids_len, word_ids, char_ids, char_ids_len, labels), gazetteers_features_py = sess.run(
            (dataset_iterator, gazetteers_features))

          char_ids_shape = list(char_ids.shape)

          # if 0 in word_ids:
          #  print("words: ", words)
          #  print("word_ids: ", word_ids)
          # print("labels:", labels)

          # Write to records
          example = tf.train.Example(features=tf.train.Features(feature={
            'words': bytes_array_feature(words),
            'word_ids_len': int64_feature(word_ids_len),
            'word_ids': int64_array_feature(word_ids),
            'char_ids': int64_2darray_feature(char_ids),
            'char_ids_shape': int64_array_feature(char_ids_shape),
            'char_ids_len': int64_array_feature(char_ids_len),
            'gazetteers': float_2darray_feature(gazetteers_features_py),
            'gazetteers_shape': int64_array_feature(list(gazetteers_features_py.shape)),
            'labels': int64_array_feature(labels),
          }))

          writer.write(example.SerializeToString())
          num_written_examples = num_written_examples + 1

        except tf.errors.OutOfRangeError:
          writer.close()
          print(prefix_dataset_iterator, " had ", num_written_examples, "examples written")
          break

  return
