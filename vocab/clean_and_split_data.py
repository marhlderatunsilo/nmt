# coding: utf-8

"""
Reads source and target data to pandas data frame.
Wraps tags in spaces.
Removes sources, and associated targets, containing more than 5000 spaces (rough measure for number of tokens).
Splits in train,dev and test set.
Writes sets to files.

Hardcoded: lines 5,15,25,35, ... will be test set
           lines 0,10,20,30, ... will be dev set
           everything else       will be train set
"""

import numpy as np
import pandas as pd
import re

source_path = "C:/Users/marhl/Desktop/text-sum-sources-merged.txt"
target_path = "C:/Users/marhl/Desktop/text-sum-targets-merged.txt"
output_path_prefix = r"C:/Users/marhl/Desktop/textsum_training_data/nmt_data_"
output_path_extension = r".txt"
line_lengths_path = "C:/Users/marhl/Desktop/textsum_training_data/line_lengths.txt"
line_stats_path = "C:/Users/marhl/Desktop/textsum_training_data/line_stats.txt"
n_tokens_cutoff = 5000

tags = [r"<p>", r"</p>", r"<s>", r"</s>"]

source = pd.read_csv(source_path, header=None, sep="\n")
target = pd.read_csv(target_path, header=None, sep="\n")

together = pd.concat([source, target], axis=1)
together.columns = ['source', 'target']


def wrap_in_spaces(string, patterns):
  for pattern in patterns:
    string = re.sub(r"[ \t\r\f]*" + pattern + r"[ \t\r\f]*", " " + pattern + " ", string)
  return string


def get_n_tokens(string):
  return len(string.split(" "))


together['source'] = together['source'].apply(lambda string: wrap_in_spaces(string, tags))
together['target'] = together['target'].apply(lambda string: wrap_in_spaces(string, tags))
together['source_length'] = together['source'].apply(lambda string: get_n_tokens(string))

together = together.loc[together['source_length'] < n_tokens_cutoff]

together['ind'] = range(len(together))
together['train_set'] = np.where(together['ind'] % 5 == 0, 0, 1)
together['dev_set'] = np.where((together['train_set'] == 0) & (together['ind'] % 10 == 0), 1, 0)
together['test_set'] = np.where((together['train_set'] == 0) & (together['ind'] % 10 == 5), 1, 0)

"""
For each wanted dataset:
  Subsets the (source or target) data
  Writes to file
  Deletes subset to free up memory
"""

# train
final_train_source = together['source'].loc[together['train_set'] == 1]
final_train_source.to_csv(output_path_prefix + r'train_source' + output_path_extension, header=None, index=None,
                          sep='\n', mode='a')
del final_train_source
final_train_target = together['target'].loc[together['train_set'] == 1]
final_train_target.to_csv(output_path_prefix + r'train_target' + output_path_extension, header=None, index=None,
                          sep='\n', mode='a')
del final_train_target

# dev
final_dev_source = together['source'].loc[together['dev_set'] == 1]
final_dev_source.to_csv(output_path_prefix + r'dev_source' + output_path_extension, header=None, index=None, sep='\n',
                        mode='a')
del final_dev_source
final_dev_target = together['target'].loc[together['dev_set'] == 1]
final_dev_target.to_csv(output_path_prefix + r'dev_target' + output_path_extension, header=None, index=None, sep='\n',
                        mode='a')
del final_dev_target

# test
final_test_source = together['source'].loc[together['test_set'] == 1]
final_test_source.to_csv(output_path_prefix + r'test_source' + output_path_extension, header=None, index=None, sep='\n',
                         mode='a')
del final_test_source
final_test_target = together['target'].loc[together['test_set'] == 1]
final_test_target.to_csv(output_path_prefix + r'test_target' + output_path_extension, header=None, index=None, sep='\n',
                         mode='a')
del final_test_target

"""
Statistics
"""

# Calculate line stats
line_stats = "Minimum: {}\nMax: {}\nMedian: {}\nMean: {}".format(min(together['source_length']),
                                                                 max(together['source_length']),
                                                                 np.median(together['source_length']),
                                                                 np.mean(together['source_length']))

# Print line stats
print("Line length statistics")
print("----------------------")
print(line_stats)
print("----------------------")

together['source_length'].to_csv(line_lengths_path, header=None, index=None, sep='\n',
                                 mode='a')
print("Wrote line lengths to {}".format(line_lengths_path))

with open(line_stats_path, 'w') as f3:
  f3.write(str(line_stats))
print("Wrote line stats to {}".format(line_stats_path))
print("----------------------")
