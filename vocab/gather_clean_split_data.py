# coding: utf-8

"""
Reads source and target data files to pandas data frame.
Concatenates the source files.
Concatenates the target files.
Removes sources, and associated targets, containing more than 2000 spaces (rough measure for number of tokens).
Splits in train,dev and test set.
Writes sets to files.

Hardcoded: lines 5,15,25,35, ... will be test set
           lines 0,10,20,30, ... will be dev set
           everything else       will be train set
"""

import numpy as np
import pandas as pd
import re

source_paths = ["processed_full_train_source.txt", "processed_full_dev_source.txt", "processed_full_test_source.txt"]
target_paths = ["processed_full_train_target.txt", "processed_full_dev_target.txt", "processed_full_test_target.txt"]
output_path_prefix = r"data_cut/processed_full_"
output_path_extension = r".txt"
line_lengths_path = "data_cut/line_lengths.txt"
line_stats_path = "data_cut/line_stats.txt"
n_tokens_cutoff = 2000

for i, (source_file, target_file) in enumerate(zip(source_paths, target_paths)):
  if i == 0:
    source = pd.read_csv(source_file, header=None, sep="\n", engine='python', quoting=3)
    target = pd.read_csv(target_file, header=None, sep="\n", engine='python', quoting=3)
  else:
    source = pd.concat([source, pd.read_csv(source_file, header=None, sep="\n", engine='python', quoting=3)], axis=0)
    target = pd.concat([target, pd.read_csv(target_file, header=None, sep="\n", engine='python', quoting=3)], axis=0)

together = pd.concat([source, target], axis=1)
together.columns = ['source', 'target']


def get_n_tokens(string):
  return len(string.split(" ")) - 1


together['source_length'] = together['source'].apply(lambda string: get_n_tokens(string))

together = together.loc[together['source_length'] < n_tokens_cutoff]

together['ind'] = range(len(together))
together['train_set'] = np.where(together['ind'] % 5 == 0, 0, 1)
together['dev_set'] = np.where((together['train_set'] == 0) & (together['ind'] % 10 == 0), 1, 0)
together['test_set'] = np.where((together['train_set'] == 0) & (together['ind'] % 10 == 5), 1, 0)

"""
For each wanted dataset:
  Subsets the (source or target) data
  Writes to file
  Deletes subset to free up memory
"""

# train
final_train_source = together['source'].loc[together['train_set'] == 1]
final_train_source.to_csv(output_path_prefix + r'train_source' + output_path_extension, header=None, index=None,
                          sep='\n', mode='a')
del final_train_source
final_train_target = together['target'].loc[together['train_set'] == 1]
final_train_target.to_csv(output_path_prefix + r'train_target' + output_path_extension, header=None, index=None,
                          sep='\n', mode='a')
del final_train_target

# dev
final_dev_source = together['source'].loc[together['dev_set'] == 1]
final_dev_source.to_csv(output_path_prefix + r'dev_source' + output_path_extension, header=None, index=None, sep='\n',
                        mode='a')
del final_dev_source
final_dev_target = together['target'].loc[together['dev_set'] == 1]
final_dev_target.to_csv(output_path_prefix + r'dev_target' + output_path_extension, header=None, index=None, sep='\n',
                        mode='a')
del final_dev_target

# test
final_test_source = together['source'].loc[together['test_set'] == 1]
final_test_source.to_csv(output_path_prefix + r'test_source' + output_path_extension, header=None, index=None, sep='\n',
                         mode='a')
del final_test_source
final_test_target = together['target'].loc[together['test_set'] == 1]
final_test_target.to_csv(output_path_prefix + r'test_target' + output_path_extension, header=None, index=None, sep='\n',
                         mode='a')
del final_test_target

"""
Statistics
"""

# Calculate line stats
line_stats = "Minimum: {}\nMax: {}\nMedian: {}\nMean: {}".format(min(together['source_length']),
                                                                 max(together['source_length']),
                                                                 np.median(together['source_length']),
                                                                 np.mean(together['source_length']))

# Print line stats
print("Line length statistics")
print("----------------------")
print(line_stats)
print("----------------------")

together['source_length'].to_csv(line_lengths_path, header=None, index=None, sep='\n',
                                 mode='a')
print("Wrote line lengths to {}".format(line_lengths_path))

with open(line_stats_path, 'w') as f3:
  f3.write(str(line_stats))
print("Wrote line stats to {}".format(line_stats_path))
print("----------------------")
