import io
import re
import operator
import functools

def compose(*functions):
  return functools.reduce(lambda f, g: lambda x: f(g(x)), functions, lambda x: x)

vocab_path = "C:/users/marhl/Desktop/ncbi-pmc-20171111-vocab.data"
vocab_clean_path = "C:/users/marhl/Desktop/clean_vocab.txt"

#tokens = set({})
token_order_mapping = {} # To preserve the order in the vocab

WEB_URL_REGEX = r"""(?i)\b((?:https?:(?:/{1,3}|[a-z0-9%])|[a-z0-9.\-]+[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)/)(?:[^\s()<>{}\[\]]+|\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\))+(?:\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\)|[^\s`!()\[\]{};:'".,<>?<<>>""''])|(?:(?<!@)[a-z0-9]+(?:[.\-][a-z0-9]+)*[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\b/?(?!@)))"""
MAX_TOKEN_LENGTH = 45
PUNCTUATION = [".",",","!", "?", ":", ";", "%", "\\", "\"", "'",
                            "$", "£", "#", "€", "(", ")", "/", "[", "]"]

def remove_odd_spaces(token):

  token_without_spaces = token.replace(" ", "").replace(" ", "").replace(" ", "").replace(" ", "")

  if len(token_without_spaces) == 0:
    return "."  # Should already be in set anyway
  else:
    return token_without_spaces

def filter_long_token(token):
  if len(token) > MAX_TOKEN_LENGTH:
    return "."  # Should already be in set anyway
  else:
    return token

def filter_number_token(token):
  if token.isdecimal():
    if int(token) > 10:
      return "."  # Should already be in set anyway
    else:
      return token
  elif token.replace('-','').replace(u"\u2212",'').replace('.','').replace(',','').isnumeric():
    return "."
  else:
    return token

def filter_web_url(token):
  if re.match(WEB_URL_REGEX, token):
    return "."
  else:
    return token

token_cleaner = compose(remove_odd_spaces, filter_long_token, filter_number_token, filter_web_url)

def clean_single_token(single_token):
  return token_cleaner(single_token)

with io.open(vocab_path, 'r', encoding='utf8') as inp:
  for i, line in enumerate(inp):
    clean_token = clean_single_token(line[:-1]) # ignore the newline

    print("clean_token: ", clean_token)
    #tokens.add(clean_token)
    if clean_token not in token_order_mapping:
      token_order_mapping[clean_token] = i

sorted_keys_tokens = sorted(token_order_mapping.items(), key=lambda x : x[1])

print(sorted_keys_tokens)

sorted_tokens, _ = zip(*sorted_keys_tokens)

print(type( sorted_tokens))

print(len(sorted_tokens))

with io.open(vocab_clean_path, 'w', encoding='utf8') as out:
  out.writelines("\n".join(sorted_tokens))

  #[out.write(item+"") for item in sorted_tokens]

#print(tokens)

