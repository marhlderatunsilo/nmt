import json
import sys
import operator
from nltk.tokenize import TweetTokenizer
import itertools

tknzr = TweetTokenizer()

vocab_dict = {}
vocab_dict["<s>"] = sys.maxsize


# vocab_dict["</s>"] = sys.maxsize

def main():
  work_on_file("ke20k_training.json", "train.src", "train.tgt")
  work_on_file("ke20k_validation.json", "dev.src", "dev.tgt", add_to_vocab=False)
  work_on_file("ke20k_testing.json", "test.src", "test.tgt", add_to_vocab=False)

  with open("vocab.src", "w+", encoding='utf-8') as f_vocab:
    sorted_vocab = sorted(vocab_dict.items(), key=operator.itemgetter(1), reverse=True)

    sorted_vocab = list(filter(lambda x: x[1] >= 10, sorted_vocab))

    print(len(sorted_vocab))

    words, counts = zip(*sorted_vocab)

    words = [word + "\n" for word in words]

    f_vocab.writelines(words)


def add_words_to_vocab(words):
  for word in words:
    if word in vocab_dict:
      vocab_dict[word] = vocab_dict[word] + 1
    else:
      vocab_dict[word] = 1


def work_on_file(input_file_name, src_file_name, tgt_file_name, add_to_vocab=True):
  with open(input_file_name, encoding="utf-8") as f, open(src_file_name, "w+", encoding="utf-8") as f_src, open(
      tgt_file_name, "w+", encoding="utf-8") as f_tgt:
    for line in f:
      sample = json.loads(line)

      abstract = sample["abstract"]
      title = sample["title"]
      keywords = sample["keyword"].split(";")
      keywords_split = [keyword.split(" ") for keyword in keywords]
      keywords_split = list(itertools.chain(*keywords_split))

      abstract_tkns = tknzr.tokenize(abstract)
      title_tkns = tknzr.tokenize(title)

      words = abstract_tkns + title_tkns + keywords_split

      if add_to_vocab:
        add_words_to_vocab(words)

      title_and_abstact = " ".join(title_tkns) + " . " + " ".join(abstract_tkns)
      title_and_abstact = title_and_abstact.replace("\n", "")

      for keyword in keywords:
        keyword = keyword.replace("\n", "")
        f_src.write(title_and_abstact + "\n")
        f_tgt.write(keyword + "\n")


if __name__ == '__main__':
  main()