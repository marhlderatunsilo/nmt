import tensorflow as tf


class DatasetIteratorMethods:

  def create_iterator(src_dataset,
                      tgt_dataset,
                      src_vocab_table,
                      tgt_vocab_table,
                      batch_size,
                      sos,
                      eos,
                      source_reverse,
                      random_seed,
                      num_buckets,
                      src_max_len=None,
                      tgt_max_len=None,
                      num_threads=32,
                      output_buffer_size=None,
                      skip_count=None,
                      num_splits=None,
                      num_shards=0,
                      shard_index=0):
    raise NotImplementedError('subclasses must override create_iterator()!')

  @staticmethod
  def is_zero_length_sequence(src, tgt, *unused):
    return tf.logical_and(tf.size(src) > 0, tf.size(tgt) > 0)
