"""Utility functions for dealing with RNN cells."""
from __future__ import print_function

import tensorflow as tf

from tfDeepNLP.single_cells.BN_LSTMCell import BN_LSTMCell
from tfDeepNLP.single_cells.ConvLSTMCellForDynamicRNN import ConvLSTMCellForDynamicRNN
from tfDeepNLP.single_cells.GFDropoutWrapper import GFDropoutWrapper
from tfDeepNLP.single_cells.GFResidualWrapper import GFResidualWrapper
from tfDeepNLP.single_cells.nlstm import LayerNormNLSTMCell
from tfDeepNLP.single_cells.zoneout_wrapper import ZoneoutWrapper
from .multi_cells.gated_feedback import GatedFeedbackMultiRNNCell
from .single_cells.BN_LSTMCell_with_gf import BN_LSTMCell as GF_BN_LSTMCell
from .utils import misc_utils as utils

__all__ = [
  "create_rnn_cell", "isLSTM"
]


def create_rnn_cell(unit_type, num_units, num_layers, num_residual_layers,
                    forget_bias, dropout, mode,
                    initial_layer_num_units=None,
                    initial_layer_wrapper=None,
                    single_cell_fn=None,
                    #model_device_fn=None,
                    multi_rnn_cell=None, use_peepholes=False,
                    zoneout=0.0, norm_gain=0.5,
                    chars_per_token=None,
                    conv_output_channels=None,
                    char_embedding_size=None,
                    conv_kernel_shape=None):
  """Create multi-layer RNN cell.

  Args:
    unit_type: string representing the unit type, i.e. "lstm".
    num_units: the depth of each unit.
    num_layers: number of cells.
    num_residual_layers: Number of residual layers from top to bottom. For
      example, if `num_layers=4` and `num_residual_layers=2`, the last 2 RNN
      cells in the returned list will be wrapped with `ResidualWrapper`.
    forget_bias: the initial forget bias of the RNNCell(s).
    dropout: floating point value between 0.0 and 1.0:
      the probability of dropout.  this is ignored if `mode != TRAIN`.
    mode: either tf.contrib.learn.TRAIN/EVAL/INFER
    single_cell_fn: single_cell_fn: allow for adding customized cell.
      When not specified, we default to model_helper._single_cell
    multi_rnn_cell: alternative to MultiRNNCell
    use_peepholes: whether to use peepholes
    chars_per_token: the fixed number of chars in a token, for conv_lstm
    conv_output_channels: number of channels in convolution, for conv_lstm
    char_embedding_size: the character embedding size, for conv_lstm
      (conv shape will be [chars_per_token, char_embedding_size])
    conv_kernel_shape: kernel shape, e.g. [5,5], in convolution, for conv_lstm
  Returns:
    An `RNNCell` instance.
  """
  cell_list = _cell_list(unit_type=unit_type,
                         num_units=num_units,
                         num_layers=num_layers,
                         num_residual_layers=num_residual_layers,
                         forget_bias=forget_bias,
                         dropout=dropout,
                         zoneout=zoneout,
                         norm_gain=norm_gain,
                         mode=mode,
                         single_cell_fn=single_cell_fn,
                         #model_device_fn=model_device_fn,
                         use_peepholes=use_peepholes,
                         initial_layer_num_units=initial_layer_num_units,
                         initial_layer_wrapper=initial_layer_wrapper,
                         chars_per_token=chars_per_token,
                         conv_output_channels=conv_output_channels,
                         char_embedding_size=char_embedding_size,
                         conv_kernel_shape=conv_kernel_shape)

  # if len(cell_list) == 1:  # Single layer.
  #   return cell_list[0]
  # else:  # Multi layers
  if multi_rnn_cell:
    if multi_rnn_cell == "gated_feedback":
      return GatedFeedbackMultiRNNCell(cell_list)
    elif multi_rnn_cell == "stacked":
      return tf.contrib.rnn.MultiRNNCell(cell_list)
    elif multi_rnn_cell == "None" or multi_rnn_cell == "custom":
      return cell_list # Just return the list of cells
    else:
      raise ValueError("Unknown Multi RNNCell type %s!" % multi_rnn_cell)
  else:
    return tf.contrib.rnn.MultiRNNCell(cell_list)


def _single_cell(unit_type, num_units, forget_bias, dropout,
                 mode, residual_connection=False, use_peepholes=False, model_device_fn=None, device_str=None,
                 zoneout=0.0, norm_gain=1.0, chars_per_token=None, conv_output_channels=None, char_embedding_size=None,
                 conv_kernel_shape=None):
  """Create an instance of a single RNN cell."""
  # dropout (= 1 - keep_prob) is set to 0 during eval and infer
  dropout = dropout if mode == tf.contrib.learn.ModeKeys.TRAIN else 0.0

  # Cell Type
  if unit_type == "lstm":

    # if mode in [tf.contrib.learn.ModeKeys.TRAIN, tf.contrib.learn.ModeKeys.EVAL]:
    #   utils.print_out("  LSTM Block Cell, forget_bias=%g" % forget_bias, new_line=False)
    #   single_cell = tf.contrib.rnn.LSTMBlockCell(  # tf.contrib.rnn.BasicLSTMCell
    #     num_units,
    #     forget_bias=forget_bias)
    # else:
    utils.print_out("  LSTM Cell, forget_bias=%g" % forget_bias, new_line=False)
    single_cell = tf.contrib.rnn.LSTMCell(  # tf.contrib.rnn.BasicLSTMCell
      num_units,
      forget_bias=forget_bias)
  elif unit_type == "lstm_with_peep_holes":

    if mode in [tf.contrib.learn.ModeKeys.TRAIN, tf.contrib.learn.ModeKeys.EVAL]:
      utils.print_out("  LSTM (with peep holes), forget_bias=%g" % forget_bias, new_line=False)
      single_cell = tf.contrib.rnn.LSTMBlockCell(  # tf.contrib.rnn.BasicLSTMCell
        num_units,
        use_peephole=True,
        forget_bias=forget_bias)
    else:
      utils.print_out("  LSTM Cell, forget_bias=%g" % forget_bias, new_line=False)
      single_cell = tf.contrib.rnn.LSTMCell(  # tf.contrib.rnn.BasicLSTMCell
        num_units,
        use_peepholes=True,
        forget_bias=forget_bias)

  elif unit_type == "gru":
    utils.print_out("  GRU", new_line=False)
    single_cell = tf.contrib.rnn.GRUCell(num_units)

  elif unit_type == "layer_norm_lstm":
    """ In this paper, we introduced layer normalization to speed-up the training of neural networks. We
        provided a theoretical analysis that compared the invariance properties of layer normalization with
        batch normalization and weight normalization. We showed that layer normalization is invariant to
        per training-case feature shifting and scaling.
        Empirically, we showed that recurrent neural networks benefit the most from the proposed method
        especially for long sequences and small mini-batches.
    """
    utils.print_out("  Layer Normalized LSTM, forget_bias=%g" % forget_bias,
                    new_line=False)
    single_cell = tf.contrib.rnn.LayerNormBasicLSTMCell(
      num_units,
      #activation=tf.identity,
      norm_gain=norm_gain,
      forget_bias=forget_bias,
      dropout_keep_prob=1.0 - dropout,
      layer_norm=True)
  elif unit_type == "nlstm":

    utils.print_out(" Layer Normalized Nested LSTM, forget_bias=%g" % forget_bias,
                    new_line=False)

    single_cell = LayerNormNLSTMCell(
      num_units,
      depth=2,
      dropout_keep_prob=1.0 - dropout,
      forget_bias=forget_bias,
      layer_norm=False)
  elif unit_type == "layer_norm_nlstm":
    utils.print_out(" Nested LSTM, forget_bias=%g" % forget_bias,
                    new_line=False)

    single_cell = LayerNormNLSTMCell(
      num_units,
      depth=2,
      dropout_keep_prob=1.0 - dropout,
      forget_bias=forget_bias,
      norm_gain=norm_gain,
      layer_norm=True)

  elif unit_type == "batch_norm_lstm":
    utils.print_out("  Batch Normalized LSTM, forget_bias=%g" % forget_bias,
                    new_line=False)
    single_cell = BN_LSTMCell(
      num_units=num_units,
      is_training=mode == tf.contrib.learn.ModeKeys.TRAIN,
      statistics_length=100,
      forget_bias=forget_bias,
      use_batch_norm_h=True,
      use_batch_norm_x=False,
      use_batch_norm_c=True,
      use_peepholes=use_peepholes)
  elif unit_type == "gf_batch_norm_lstm":
    utils.print_out("  Batch Normalized LSTM, forget_bias=%g" % forget_bias,
                    new_line=False)
    single_cell = GF_BN_LSTMCell(
      num_units=num_units,
      is_training=mode == tf.contrib.learn.ModeKeys.TRAIN,
      statistics_length=100,
      forget_bias=forget_bias,
      use_batch_norm_h=True,
      use_batch_norm_x=False,
      use_batch_norm_c=True,
      use_peepholes=use_peepholes)
  elif unit_type == "conv_lstm":
    utils.print_out("  convLSTM", new_line=False)

    assert chars_per_token, "chars_per_token must be specified when using convLSTM cell."
    assert conv_output_channels, "conv_output_channels must be specified when using convLSTM cell."
    assert char_embedding_size, "char_embedding_size must be specified when using convLSTM cell."
    assert conv_kernel_shape, "conv_kernel_shape must be specified when using convLSTM cell."
    assert zoneout == 0.0, "zoneout cannot be applied to convLSTM."
    assert dropout == 0.0, "dropout cannot be applied to convLSTM."

    single_cell = ConvLSTMCellForDynamicRNN(
      conv_ndims=2,
      input_shape=[chars_per_token, char_embedding_size, 1],  # Last dimension is channels
      channels=conv_output_channels,
      kernel_shape=conv_kernel_shape,  # Must be tuple
      use_bias=True,
      forget_bias=forget_bias,
      initializers=None,
      name='conv_lstm_cell'
    )
  else:
    raise ValueError("Unknown unit type %s!" % unit_type)

  # Dropout is (1 - keep_prob)
  if dropout > 0.0:

    if unit_type == "gf_batch_norm_lstm":
      single_cell = GFDropoutWrapper(
        cell=single_cell, input_keep_prob=(1.0 - dropout))
    else:
      single_cell = tf.contrib.rnn.DropoutWrapper(
        cell=single_cell, input_keep_prob=(1.0 - dropout))

    utils.print_out("  %s, dropout=%g " % (type(single_cell).__name__, dropout),
                    new_line=False)

  if zoneout > 0.0:
    single_cell = ZoneoutWrapper(
      cell=single_cell, state_zoneout_prob=zoneout, is_training=mode == tf.contrib.learn.ModeKeys.TRAIN)
    utils.print_out("  %s" % type(single_cell).__name__, new_line=False)

  # Residual
  if unit_type == "gf_batch_norm_lstm" and residual_connection:
    single_cell = GFResidualWrapper(single_cell)
    utils.print_out("  %s" % type(single_cell).__name__, new_line=False)
  elif residual_connection and unit_type != "conv_lstm":
    single_cell = tf.contrib.rnn.ResidualWrapper(single_cell)
    utils.print_out("  %s" % type(single_cell).__name__, new_line=False)

  # Device Wrapper
  if device_str:
    single_cell = tf.contrib.rnn.DeviceWrapper(single_cell, device_str)
  utils.print_out("  %s, device=%s" %
                  (type(single_cell).__name__, device_str), new_line=False)

  return single_cell


def _cell_list(unit_type, num_units, num_layers, num_residual_layers,
               forget_bias, dropout, zoneout, norm_gain, mode, initial_layer_num_units=None, initial_layer_wrapper=None,
               use_peepholes=False,
               single_cell_fn=None,
               #model_device_fn=None,
               chars_per_token=None,
               conv_output_channels=None, char_embedding_size=None, conv_kernel_shape=None):
  """Create a list of RNN cells."""
  if not single_cell_fn:
    single_cell_fn = _single_cell

  # Multi-GPU
  cell_list = []
  for i in range(num_layers):
    utils.print_out("  cell %d" % i, new_line=False)

    layer_num_units = num_units

    if i == 0 and initial_layer_num_units is not None:
      layer_num_units = initial_layer_num_units

    single_cell = single_cell_fn(
      unit_type=unit_type,
      num_units=layer_num_units,
      forget_bias=forget_bias,
      dropout=dropout,
      mode=mode,
      residual_connection=(i >= num_layers - num_residual_layers),
      use_peepholes=use_peepholes,
      #model_device_fn=model_device_fn,
      zoneout=zoneout,
      norm_gain=norm_gain,
      chars_per_token=chars_per_token,
      conv_output_channels=conv_output_channels,
      char_embedding_size=char_embedding_size,
      conv_kernel_shape=conv_kernel_shape
    )

    if i == 0 and initial_layer_wrapper is not None:
      single_cell = initial_layer_wrapper(single_cell)

    utils.print_out("")
    cell_list.append(single_cell)

    print("cell_list: ", cell_list)

  return cell_list


def unpack_encoder_state(encoder_state, cell_type, encoder_type="bi"):
  """
  The encoder state returned by dynamic_rnn is a tuple when using an lstm cell,
   and otherwise a tensor.

  :param encoder_state:
  :param cell_type:
  :return: Encoder state, h (output) if LSTM
  """

  if not isinstance(cell_type, str):
    raise TypeError("cell_type must be string.")

  if cell_type == "gf_batch_norm_lstm":
    num_tuples = len(encoder_state)
    if num_tuples == 1:
      return encoder_state[1]
    if encoder_type == "uni":
      return encoder_state[-1][1][0]  # return the last h state
    h_states = []
    for i in range(num_tuples - 2, num_tuples):
      h_states.append(encoder_state[i][1][0])
    print(h_states)
    return tf.concat(h_states, axis=1)
  elif cell_type == 'batch_norm_lstm':

    print("batch_norm_lstm encoder_state: ", encoder_state)

    num_tuples = len(encoder_state)
    if num_tuples == 1:
      return encoder_state[1]
    if encoder_type == "uni":
      return encoder_state[-1][1][0]  # return the last h state
    h_states = []
    for i in range(num_tuples - 2, num_tuples):
      h_states.append(encoder_state[i][0].h)
    print(h_states)
    return tf.concat(h_states, axis=1)
  elif cell_type in ["lstm", "layer_norm_lstm"]:

    num_tuples = len(encoder_state)
    if num_tuples == 1:
      return encoder_state[1]
    if encoder_type == "uni":
      return encoder_state[-1].h  # return the last h state
    h_states = []
    for i in range(num_tuples - 2, num_tuples):
      h_states.append(encoder_state[i].h)
    return tf.concat(h_states, axis=1)
  elif cell_type in ["nlstm", "layer_norm_nlstm"]:

    num_tuples = len(encoder_state)
    if num_tuples == 1:
      return encoder_state[1]
    if encoder_type == "uni":
      return encoder_state[-1].h  # return the last h state
    h_states = []
    for i in range(num_tuples - 2, num_tuples):
      h_states.append(encoder_state[i][0])
    return tf.concat(h_states, axis=1)
  elif cell_type == "conv_lstm":
    num_tuples = len(encoder_state)
    if num_tuples == 1:
      return tf.squeeze(encoder_state[1], axis=-1)
    if encoder_type == "uni":
      return tf.squeeze(encoder_state[-1][1], axis=-1)  # return the last h state
    h_states = []
    for i in range(num_tuples - 2, num_tuples):
      h_states.append(tf.squeeze(encoder_state[i][1], axis=-1))
    print(h_states)
    return tf.concat(h_states, axis=1)


  else:
    Warning("You might want to check the encoder state with this cell type.")
    return encoder_state


def isLSTM(unit_type):
  """
  Checks if the string "lstm" is in the lowercase version of the unit_type string.
  :param unit_type: String.
  :return: bool.
  """
  if "lstm" in unit_type.lower():
    return True
  else:
    return False
