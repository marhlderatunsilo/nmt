import tensorflow as tf

from tensorflow.python.framework import ops
from tensorflow.python.util.nest import map_structure


class IdentityRNN(tf.nn.rnn_cell.RNNCell):

  def __init__(self, zero_state, output_size):
    self._zero_state = zero_state
    self._output_size = output_size

  def __call__(self, inputs, state, scope=None):
    with tf.name_scope("identity_cell"):
      return tf.identity(inputs, name="identity_cell_output"), map_structure(tf.identity, state)

  @property
  def state_size(self):
    return map_structure(lambda atom: tf.size(atom), self._zero_state)

  @property
  def output_size(self):
    return self._output_size

  def zero_state(self, batch_size, dtype):
    print("self._zero_state: ", self._zero_state)
    with ops.name_scope(type(self).__name__ + "ZeroState", values=[batch_size]):
      return self._zero_state