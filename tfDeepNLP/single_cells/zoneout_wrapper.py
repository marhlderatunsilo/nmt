import tensorflow as tf

# Wrapper for the TF RNN cell
# For an LSTM, the 'cell' is a tuple containing state and cell
# We use TF's dropout to implement zoneout
from tensorflow.python.ops import nn_ops
import hashlib
from tensorflow.python.framework import tensor_util
from tensorflow.python.framework import ops
from tensorflow.python.ops.rnn_cell_impl import assert_like_rnncell, _default_dropout_state_filter_visitor, \
  _enumerated_map_structure_up_to
from tensorflow.python.util import nest

class ZoneoutWrapper(tf.nn.rnn_cell.RNNCell):
  """Operator adding dropout to inputs and outputs of the given cell."""

  def __init__(self, cell, state_zoneout_prob, is_training=True, seed=None, dropout_state_filter_visitor=None,
               **kwargs):

    super().__init__(**kwargs)
    if not assert_like_rnncell(cell):
      raise TypeError("The parameter cell is not a RNNCell.")
    if (dropout_state_filter_visitor is not None
        and not callable(dropout_state_filter_visitor)):
      raise TypeError("dropout_state_filter_visitor must be callable")
    self._dropout_state_filter = (
        dropout_state_filter_visitor or _default_dropout_state_filter_visitor)
    with ops.name_scope("DropoutWrapperInit"):
      def tensor_and_const_value(v):
        tensor_value = ops.convert_to_tensor(v)
        const_value = tensor_util.constant_value(tensor_value)
        return (tensor_value, const_value)

      tensor_prob, const_prob = tensor_and_const_value(state_zoneout_prob)
      if const_prob is not None:
        if const_prob < 0 or const_prob > 1:
          raise ValueError("Parameter %s must be between 0 and 1: %d"
                           % ("state_zoneout_prob", const_prob))
        setattr(self, "_%s" % "state_zoneout_prob", float(const_prob))
      else:
        setattr(self, "_%s" % "state_zoneout_prob", tensor_prob)

    self.state_zoneout_prob=state_zoneout_prob
    self._cell = cell
    self._seed = seed
    self.is_training = is_training

  def _gen_seed(self, salt_prefix, index):
    if self._seed is None:
      return None
    salt = "%s_%d" % (salt_prefix, index)
    string = (str(self._seed) + salt).encode("utf-8")
    return int(hashlib.md5(string).hexdigest()[:8], 16) & 0x7FFFFFFF

  @property
  def state_size(self):
    return self._cell.state_size

  @property
  def output_size(self):
    return self._cell.output_size

  def zero_state(self, batch_size, dtype):
    with ops.name_scope(type(self).__name__ + "ZeroState", values=[batch_size]):
      return self._cell.zero_state(batch_size, dtype)

  def _zoneout(self, new_values, old_values, salt_prefix, keep_prob,
               shallow_filtered_substructure=None):
    """Decides whether to perform standard dropout or recurrent dropout."""

    if shallow_filtered_substructure is None:
      # Put something so we traverse the entire structure; inside the
      # dropout function we check to see if leafs of this are bool or not.
      shallow_filtered_substructure = new_values

    def zoneout(i, do_zoneout, new_v, old_v):
      if not isinstance(do_zoneout, bool) or do_zoneout:

        # return nn_ops.dropout(
        #   v, keep_prob=keep_prob, seed=self._gen_seed(salt_prefix, i))
        if self.is_training:
            v = (1 - keep_prob) * nn_ops.dropout(
              new_v - old_v, (1 - keep_prob), seed=self._gen_seed(salt_prefix, i)) + old_v
        else:
            print("new_state: ", new_v)
            print("state: ", old_v)
            v =keep_prob * old_v + (1 - keep_prob) * new_v
        return v
      else:
        return new_v

    return _enumerated_map_structure_up_to(
      shallow_filtered_substructure, zoneout,
      *[shallow_filtered_substructure, new_values, old_values])


  def __call__(self, inputs, state, scope=None):
    """Run the cell with the declared dropouts."""

    def _should_zoneout(p):
      return (not isinstance(p, float)) or p < 1

    output, new_state = self._cell(inputs, state, scope)
    if _should_zoneout(self.state_zoneout_prob):
      # Identify which subsets of the state to perform dropout on and
      # which ones to keep.
      shallow_filtered_substructure = nest.get_traverse_shallow_structure(
        self._dropout_state_filter, new_state)

      new_state = self._zoneout(new_state, state, "state",
                                self.state_zoneout_prob,
                                shallow_filtered_substructure)

    return output, new_state
