import collections

import tensorflow as tf
from tensorflow.contrib.seq2seq import AttentionWrapperState
from tensorflow.python.framework import ops

# _PointerGeneratorProbabilityWrapperStateTuple = collections.namedtuple("PointerGeneratorProbabilityWrapperStateTuple",
#                                                                        ("cell_state", "p_gens"))
#
#
# class PointerGeneratorProbabilityWrapperStateTuple(_PointerGeneratorProbabilityWrapperStateTuple):
#
#   @property
#   def dtype(self):
#     (cell_state, p_gens) = self
#     if cell_state.dtype != p_gens.dtype:
#       raise TypeError("Inconsistent internal state: %s vs %s" %
#                       (str(cell_state.dtype), str(p_gens.dtype)))
#     return cell_state.dtype
#
#   def clone(self, **kwargs):
#     """Clone this object, overriding components provided by kwargs.
#     Example:
#     ```python
#     initial_state = attention_wrapper.zero_state(dtype=..., batch_size=...)
#     initial_state = initial_state.clone(cell_state=encoder_state)
#     ```
#     Args:
#       **kwargs: Any properties of the state object to replace in the returned
#         `AttentionWrapperState`.
#     Returns:
#       A new `AttentionWrapperState` whose properties are the same as
#       this one, except any overridden properties as provided in `kwargs`.
#     """
#     return super(PointerGeneratorProbabilityWrapperStateTuple, self)._replace(**kwargs)


class PointerGeneratorProbabilityWrapper(tf.nn.rnn_cell.RNNCell):
  """Operator adding dropout to inputs and outputs of the given cell."""

  def __init__(self, cell, output_layer, mode, extended_vsize, initializer=None, softmax_vocab_dist=True):
    super().__init__()
    self._cell = cell
    self.mode = mode
    self.initializer = initializer
    self.output_layer = output_layer
    self.extended_vsize = extended_vsize
    self.softmax_vocab_dist = softmax_vocab_dist

  @property
  def state_size(self):

    return self._cell.state_size
    #cell_state = self._cell.state_size  # list()
    # cell_state[0] = cell_state[0].clone(alignment_history=-1)
    # cell_state = tuple(cell_state)

    #return PointerGeneratorProbabilityWrapperStateTuple(cell_state=cell_state, p_gens=())

  @property
  def output_size(self):
    return self.extended_vsize
    #return self._cell.output_size

  def zero_state(self, batch_size, dtype):
    # with ops.name_scope(type(self).__name__ + "ZeroState", values=[batch_size]):
    #   zero_state_pgens = tf.TensorArray(dtype=tf.float32, size=0, dynamic_size=True)
    #   return PointerGeneratorProbabilityWrapperStateTuple(cell_state=self._cell.zero_state(batch_size, dtype),
    #                                                       p_gens=zero_state_pgens)
    return self._cell.zero_state(batch_size, dtype)

  def __call__(self, inputs, state, scope=None):
    """Run the cell with the declared dropouts."""

    #(state, p_gens) = state
    rnn_outputs, new_state = self._cell(inputs, state, scope)

    prev_context_vector = None
    if isinstance(state, AttentionWrapperState):
      prev_context_vector = state.attention
    else:
      for sub_state in state:
        if isinstance(sub_state, AttentionWrapperState):
          prev_context_vector = sub_state.attention
    current_context_vector = None
    if isinstance(new_state, AttentionWrapperState):
      current_context_vector = state.attention
    else:
      for sub_state in new_state:
        if isinstance(sub_state, AttentionWrapperState):
          current_context_vector = sub_state.attention

    assert prev_context_vector is not None, "There was no AttentionWrapperState"

    with tf.variable_scope('calculate_pgen', initializer=self.initializer):
      input_size = inputs.get_shape().with_rank(2)[1].value
      context_vector_size = prev_context_vector.get_shape().with_rank(2)[1].value

      print("input_size: ", input_size)
      print("context_vector_size: ", context_vector_size)
      print("new_state: ", new_state)

      #inputs_and_context_vector = tf.concat([inputs, prev_context_vector], 1)
      #x = tf.contrib.layers.fully_connected(inputs_and_context_vector, input_size, activation_fn=None)
      x = inputs

      # Select state of first and last layer
      if isinstance(new_state, tuple):
        first_layer_new_state = new_state[0].cell_state
        last_layer_new_state = new_state[-1]

        def grap_h(state):
          if isinstance(state, tf.contrib.rnn.LSTMStateTuple):
            return [state.h]
          else:
            state = list(state)
            return state[:1] if len(state) >= 2 else state

        def graph_remaining_state(state):
          if isinstance(state, tf.contrib.rnn.LSTMStateTuple):
            remaining_state = [tf.stop_gradient(state.c)]
          else:
            state = list(state)
            remaining_state = state[1:] if len(state) >= 2 else state
            remaining_state = list(state) if not isinstance(remaining_state, list) else state

          return tf.contrib.framework.nest.map_structure(lambda x: tf.stop_gradient(x), remaining_state)

        last_layer_new_state = grap_h(last_layer_new_state) + graph_remaining_state(last_layer_new_state)
        first_layer_new_state = grap_h(first_layer_new_state) + graph_remaining_state(first_layer_new_state)

        recurrent_state = last_layer_new_state + first_layer_new_state
      else:
        first_layer_new_state = new_state.cell_state

        recurrent_state = [first_layer_new_state.h]

      print("x: ", x)
      print("current_context_vector: ", current_context_vector)

      p_gen_input = tf.concat([current_context_vector, x] + recurrent_state, 1)  #
      first_layer_new_state_size = first_layer_new_state[0].get_shape()[-1].value

      # Initial tanh residual layer
      first_pgen_layer = tf.contrib.layers.fully_connected(
        p_gen_input, input_size,
        activation_fn=tf.nn.softsign,
        weights_initializer=tf.contrib.layers.xavier_initializer(),
        biases_initializer=tf.zeros_initializer(),
      )  # + p_gen_input

      first_pgen_layer = tf.layers.dropout(
       inputs=first_pgen_layer, rate=0.4, training=self.mode == tf.estimator.ModeKeys.TRAIN)

      second_pgen_layer = tf.contrib.layers.fully_connected(
        first_pgen_layer, input_size,
        activation_fn=tf.nn.softsign,
        weights_initializer=tf.contrib.layers.xavier_initializer(),
        biases_initializer=tf.zeros_initializer(),
      )  # + p_gen_input

      second_pgen_layer = tf.layers.dropout(
        inputs=second_pgen_layer, rate=0.4, training=self.mode == tf.estimator.ModeKeys.TRAIN)

      p_gen = tf.contrib.layers.fully_connected(second_pgen_layer, 1,
                                                activation_fn=tf.nn.sigmoid,
                                                weights_initializer=tf.contrib.layers.xavier_initializer(),
                                                biases_initializer=tf.zeros_initializer(),
                                                # weights_regularizer=self.regularizer,
                                                # biases_regularizer=self.regularizer
                                                )  # Tensor shape (batch_size, 1)
      #p_gen = tf.square(p_gen)

      #p_gens = p_gens.write(p_gens.size(), p_gen)

      #new_state = PointerGeneratorProbabilityWrapperStateTuple(cell_state=new_state, p_gens=p_gens)

      with tf.variable_scope("AttnOutputProjection"):
        rnn_outputs = tf.contrib.layers.fully_connected(tf.concat([rnn_outputs, current_context_vector], -1), self._cell.output_size,
                                                   activation_fn=None)

      alignments = state[0].alignments
      attn_dists = alignments[0]
      outputs = self.output_layer(rnn_outputs)

      if self.softmax_vocab_dist:
        vocab_dists = tf.nn.softmax(outputs)
      else:
        vocab_dists = outputs

      final_dists = self.calc_final_dist(vocab_dists, attn_dists, p_gen)

    return final_dists, new_state

  def calc_final_dist(self, vocab_dists, attn_dists, p_gens):
    """Calculate the final distribution, for the pointer-generator model
    Args:
      vocab_dists: The vocabulary distributions. List length max_dec_steps of (batch_size, vsize) arrays. The words are in the order they appear in the vocabulary file.
      attn_dists: The attention distributions. List length max_dec_steps of (batch_size, attn_len) arrays
    Returns:
      final_dists: The final distributions. List length max_dec_steps of (batch_size, extended_vsize) arrays.
    """
    with tf.variable_scope('final_distribution'):
      # Multiply vocab dists by p_gen and attention dists by (1-p_gen)
      org_attn_dists = attn_dists
      # p_gens = tf.maximum(p_gens * 0.9,0.1)
      vocab_dists = p_gens * vocab_dists  # [p_gen * dist for (p_gen, dist) in zip(self.p_gens, vocab_dists)]
      attn_dists = (1 - p_gens) * attn_dists  # [(1 - p_gen) * dist for (p_gen, dist) in zip(self.p_gens, attn_dists)]

      # # Concatenate some zeros to each vocabulary dist, to hold the probabilities for in-article OOV words
      # extended_vsize = self.hparams.projection_vocab_size + self.hparams.src_max_len  # the maximum (over the batch) size of the extended vocabulary
      # extra_zeros = tf.zeros((self.batch_size, self.hparams.src_max_len))
      #
      # vocab_dists_extended = tf.map_fn(lambda dist: tf.concat(axis=1, values=[dist, extra_zeros]), vocab_dists, swap_memory=True)
      # # vocab_dists_extended = [tf.concat(axis=1, values=[dist, extra_zeros]) for dist in
      # #                        vocab_dists]  # list length max_dec_steps of shape (batch_size, extended_vsize)
      #
      # # Project the values in the attention distributions onto the appropriate entries in the final distributions
      # # This means that if a_i = 0.1 and the ith encoder word is w, and w has index 500 in the vocabulary, then we add 0.1 onto the 500th entry of the final distribution
      # # This is done for each decoder timestep.
      # # This is fiddly; we use tf.scatter_nd to do the projection
      # batch_nums = tf.range(0, limit=self.batch_size)  # shape (batch_size)
      # batch_nums = tf.expand_dims(batch_nums, 1)  # shape (batch_size, 1)
      # attn_len = tf.shape(self.iterator.source)[1]  # number of states we attend over
      # batch_nums = tf.tile(batch_nums, [1, attn_len])  # shape (batch_size, attn_len)
      # indices = tf.stack((batch_nums, self.iterator.source), axis=2)  # shape (batch_size, enc_t, 2)
      # shape = [self.batch_size, extended_vsize]
      #
      # attn_dists_projected = tf.map_fn(lambda copy_dist: tf.scatter_nd(indices, copy_dist, shape), attn_dists, swap_memory=True, parallel_iterations=self.hparams.batch_size)

      # Add the vocab distributions and the copy distributions together to get the final distributions
      # final_dists is a list length max_dec_steps; each entry is a tensor shape (batch_size, extended_vsize) giving the final distribution for that decoder timestep
      # Note that for decoder timesteps and examples corresponding to a [PAD] token, this is junk - ignore.
      # final_dists = vocab_dists_extended + attn_dists_projected

      attn_dist_shape = tf.shape(attn_dists)

      final_dists = tf.concat([vocab_dists, attn_dists, tf.zeros([attn_dist_shape[0], self.extended_vsize - attn_dist_shape[1]])], axis=-1)

      # final_dists[0, 0, 8000:]
      # final_dists = tf.Print(final_dists, [tf.shape(final_dists), p_gens], message="final_dists: ", summarize=2000)

      return final_dists
