import tensorflow as tf
from tensorflow.contrib.rnn import ConvLSTMCell

from tensorflow.python.framework import tensor_shape
from tensorflow.python.ops import rnn_cell_impl, array_ops, math_ops
from tensorflow.contrib.rnn.python.ops.rnn_cell import _conv


class ConvLSTMCellForDynamicRNN(rnn_cell_impl.RNNCell):
  """Convolutional LSTM recurrent network cell with modifications.
  https://arxiv.org/pdf/1506.04214v1.pdf

  ConvLSTMCell does not seem to work well with dynamic_rnn, unless the input shape is also the output shape.
   This mod applies convolutional dimension reduction after the expected convolution step,
   so there's always only the same number of output channels as input channels.

  Convolutional dimension reduction: same channels as input, kernel shape = [1,1,..], i.e. all ones,
   depending on shape of kernel_shape. Does not use bias.
  """

  def __init__(self,
               conv_ndims,
               input_shape,
               channels,
               kernel_shape,
               use_bias=True,
               forget_bias=1.0,
               initializers=None,
               name="conv_lstm_cell_for_dynamic_rnn"):
    """Construct ConvLSTMCellForDynamicRNN.
    Args:
      conv_ndims: Convolution dimensionality (1, 2 or 3).
      input_shape: Shape of the input as int tuple, excluding the batch size.
      channels: int, number of channels of the internal convolution. Output will only contain 1 channel.
      kernel_shape: Shape of kernel as in tuple (of size 1,2 or 3).
      use_bias: Use bias in convolutions.
      forget_bias: Forget bias.
      name: Name of the module.
    Raises:
      ValueError: If `input_shape` is incompatible with `conv_ndims`.
    """
    super(ConvLSTMCellForDynamicRNN, self).__init__(name=name)

    if conv_ndims != len(input_shape) - 1:
      raise ValueError("Invalid input_shape {} for conv_ndims={}.".format(
        input_shape, conv_ndims))

    self._conv_ndims = conv_ndims
    self._input_shape = input_shape
    self._channels = channels
    self._kernel_shape = kernel_shape
    self._use_bias = use_bias
    self._forget_bias = forget_bias

    self._output_channels = self._input_shape[-1]

    state_size = tensor_shape.TensorShape(
      self._input_shape)
    self._state_size = rnn_cell_impl.LSTMStateTuple(state_size, state_size)
    self._output_size = tensor_shape.TensorShape(
      self._input_shape)

  @property
  def output_size(self):
    return self._output_size

  @property
  def state_size(self):
    return self._state_size

  def call(self, inputs, state, scope=None):
    cell, hidden = state

    """----------------------------The main modification----------------------------"""

    new_hidden = _conv([inputs, hidden], self._kernel_shape,
                       self._channels, self._use_bias)
    print("new hidden: ", new_hidden)
    with tf.variable_scope("dim_reduction"):
      restored_dimensions_hidden = _conv([new_hidden], [1 for s in self._kernel_shape],
                                         4 * self._output_channels, False)
    print("restored_dimensions_hidden: ", restored_dimensions_hidden)
    """-----------------------------------------------------------------------------"""

    gates = array_ops.split(
      value=restored_dimensions_hidden, num_or_size_splits=4, axis=self._conv_ndims + 1)

    input_gate, new_input, forget_gate, output_gate = gates
    new_cell = math_ops.sigmoid(forget_gate + self._forget_bias) * cell
    new_cell += math_ops.sigmoid(input_gate) * math_ops.tanh(new_input)
    output = math_ops.tanh(new_cell) * math_ops.sigmoid(output_gate)

    new_state = rnn_cell_impl.LSTMStateTuple(new_cell, output)
    return output, new_state
