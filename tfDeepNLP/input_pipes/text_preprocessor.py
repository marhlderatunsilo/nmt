"""
Base class for text preprocessing and conversion to TFRecords.
Tested with python 3.6.

"""

import numpy as np
import ntpath
import abc
from string import punctuation
import re
import pandas as pd
from pathos.multiprocessing import ProcessingPool as Pool  # pip install pathos

# Machine Learning
import tensorflow as tf
from sklearn.utils import shuffle as sk_shuffle
from sklearn import preprocessing

__all__ = [
  "TextPreprocessor",
  "shuffle",
  "int64_feature",
  "int64_array_feature",
  "floats_feature",
  "bytes_feature",
  "bytes_array_feature",
  "get_n_tokens"
]


class TextPreprocessor(object):
  """
  Methods for preprocessing text collections.

  Methods for:
    Text to lowercase
    Text to uppercase
    Add paddings (word level)
    Case features
      Is title, i.e. uppercase first letter
      All letters uppercase
      All letters lowercase
      Mixed upper and lowercase letters
    Other features
      Is punctuation
  Utils for converting to TFRecords

  """

  def __init__(self):
    pass

  """
  Feature extraction
  """

  def extract_word_features(self, txt=None,
                            features='all',
                            transpose=False):
    """
    Extract word features for each sentence in txt. Features are arrays with 0 or 1.

    :param txt: list of strings. Must be padded to have same number of words.
    :param features: List of feature names to extract, or simply 'all'.
      E.g. ['is_title', 'all_upper', 'all_lower', 'mixed_case', 'is_punctuation', 'is_numeric'].
    :param transpose: bool
    :return: Features with same order as features argument.
    """
    assert txt, "txt cannot be None."
    assert features is not None, "features must be a list of strings or 'all'."

    if features == 'all':
      features = ['is_title', 'all_upper', 'all_lower', 'mixed_case', 'is_punctuation', 'is_numeric']

    self._check_str_list(features)
    self._check_str_list(txt)

    extracted_features = []

    def convert_to_ndarray(a_list, transpose=True):
      an_array = np.asarray(a_list)
      if transpose:
        an_array = an_array.transpose()
      return an_array

    for feature in features:

      if feature == 'is_title':
        extracted_features.append(convert_to_ndarray([self._is_title(t) for t in txt], transpose=transpose))
      elif feature == 'all_upper':
        extracted_features.append(convert_to_ndarray([self._is_upper(t) for t in txt], transpose=transpose))
      elif feature == 'all_lower':
        extracted_features.append(convert_to_ndarray([self._is_lower(t) for t in txt], transpose=transpose))
      elif feature == 'mixed_case':
        extracted_features.append(convert_to_ndarray([self._is_mixed(t) for t in txt], transpose=transpose))
      elif feature == 'is_punctuation':
        extracted_features.append(convert_to_ndarray([self._is_punctuation(t) for t in txt], transpose=transpose))
      elif feature == 'is_numeric':
        extracted_features.append(convert_to_ndarray([self._is_numeric(t) for t in txt], transpose=transpose))
      else:
        raise ValueError("Don't recognize feature: {}".format(feature))

    return extracted_features

  def _is_title(self, word):
    return [1 if w.istitle() else 0 for w in word.split(" ")]

  def _is_upper(self, word):
    return [1 if w.isupper() else 0 for w in word.split(" ")]

  def _is_lower(self, word):
    return [1 if w.islower() else 0 for w in word.split(" ")]

  def _is_mixed(self, word):
    return [1 if not w.islower() and not w.isupper() else 0 for w in word.split(" ")]

  def _is_punctuation(self, word):
    return [1 if w in punctuation else 0 for w in word.split(" ")]

  def _is_numeric(self, word):
    return [1 if self._is_float(w) else 0 for w in word.split(" ")]

  @staticmethod
  def _is_float(string):
    try:
      float(string)
      return True
    except ValueError:
      return False

  def extract_sequence_lengths(self, txt=None, sep=" "):
    if isinstance(txt, (np.ndarray, pd.Series)):
      txt = txt.tolist()
    if txt is None:
      raise ValueError("txt cannot be None.")
    self._check_str_list(txt)
    return [get_n_tokens(t, sep=sep) for t in txt]

  """
  Text manipulations
  """

  def lowercase(self, txt=None):
    if txt is None:
      raise ValueError("txt cannot be None.")

    self._check_str_list(txt)
    return [t.lower() for t in txt]

  def uppercase(self, txt=None):
    if txt is None:
      raise ValueError("txt cannot be None.")

    self._check_str_list(txt)
    return [t.upper() for t in txt]

  def pad_sentences(self, txt=None, padding_symbol='[padding]', sep=" ", seq_lengths=None):
    if txt is None:
      raise ValueError("txt cannot be None.")

    self.padding_symbol = padding_symbol
    if seq_lengths is None:
      seq_lengths = self.extract_sequence_lengths(txt)
    self.max_seq_length = max(seq_lengths)
    padding_lengths = [self.max_seq_length - seql for seql in seq_lengths]
    # Add space before padding (not needed after)
    if not self.padding_symbol[0] == sep:
      self.padding_symbol = sep + self.padding_symbol
    txt_padded = [t + self.padding_symbol * pad_l for t, pad_l in zip(txt, padding_lengths)]
    return txt_padded

  def space_punctuation(self, txt=None):
    """
    Adds spaces around punctuation, so they are seen as single tokens.

    NEEDS TESTING!!

    :param txt: List of strings
    :return: txt with whitespaces around punctuation.
    """
    if not txt:
      raise ValueError("txt cannot be None.")

    def add_spaces(string):

      # TODO These were only shallowly tested. Create tests!!

      string = re.sub(r"([\w/'+$\s-]+|[^\w/'+$\s-]+)\s*", r"\1 ", string)
      string = re.sub(r'([!\\"#\\$%&\'\\(\\)\\*,\\./:;<=>\\?])', r' \1 ', string)
      string = re.sub(r'\s{2,}', ' ', string)
      if string[-1] == " ":
        string = string[:-1]
      return string

    return [add_spaces(string) for string in txt]

  def one_hot_encoder(self, targets=None, sparse_output=False):
    """
    One-hot encode an array with integers.
    Uses sklearn.preprocessing.LabelBinarizer.

    Note: Seems to sort string names alphabetically.

    :param targets: 1d array/list with ints specifying the class.
    :param sparse_output: Return array in sparse CSR format.
    :return: One-hot encoding, classes, and fitted LabelBinarizer for transforming test data.
    """
    if targets is None:
      raise ValueError("targets cannot be None.")

    self.lb = preprocessing.LabelBinarizer(sparse_output=sparse_output)
    self.lb.fit(targets)
    self.one_hots = self.lb.transform(targets)
    self.classes = self.lb.classes_
    return self.one_hots

  """
  Save as TFRecord
  """

  @abc.abstractmethod
  def write_to_tfr(self, **kwargs):
    """
    Subclass must implement this.

    Convert data to, and save as, TFRecords.
    """
    pass

  def create_tfr_writer(self, f_name, out_dir=None, options=None):
    """
    Creates TFRecordWriter.

    Note: Remember to close after use, with writer.close().

    Example
    -------

    writer = create_tfr_writer('my_data.tfrecords')
    for i in range(num_examples):
      example = tf.train.Example(features=tf.train.Features(feature={
        'feat_1': int64_array_feature(feature_1[i]),
        'label': int64_feature(labels[i])}))
      writer.write(example.SerializeToString())
    writer.close()

    :param f_name: Desired name of TFRecords output file. Is appended to self.out_dir.
                   Must have file extension ".tfrecords".
    :param out_dir: path to output directory. If None, file is written in working directory.
    :param options: A TFRecordOptions object (optional).
    :return: TFRecordWriter
    """
    assert f_name[-10:] == '.tfrecords', "file extension must be .tfrecords"
    if out_dir:
      assert isinstance(out_dir, str), "out_dir must be string or None."
      file_path = out_dir + f_name
    else:
      file_path = f_name
    return tf.python_io.TFRecordWriter(file_path, options=options)

  """
  Type check dataset_iterator
  """

  def _check_str_list(self, str_list):
    """
    Checks that input is a non-empty list, and that all elements are strings.
    Raises if a condition is not True.
    :param str_list: List of strings.
    """
    if not str_list:
      raise ValueError("input list is empty.")
    if not isinstance(str_list, list):
      raise TypeError("input list is not a list.")
    if not all(isinstance(t, str) for t in str_list):
      raise TypeError("not all elements are strings.")


"""
TF features
"""


def int64_feature(value):
  value = int(value)
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))


def int64_array_feature(array):
  array = list(np.int64(array))
  return tf.train.Feature(int64_list=tf.train.Int64List(value=array))


def floats_feature(array):
  array = np.float32(array)
  return tf.train.Feature(float_list=tf.train.FloatList(value=array))


def bytes_feature(value):
  value = tf.compat.as_bytes(value)
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))


def bytes_array_feature(array):
  array = tf.compat.as_bytes(array)
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=array))


"""
Shuffle
"""


def shuffle(*args, random_state=None):
  """
  Shuffle given arrays together.

  Uses sklearn.dataset_iterator.shuffle.

  :param args: Arrays to shuffle together.
  :return: Shuffled arrays.
  """
  return sk_shuffle(*args, random_state=random_state)


"""
  Path manipulations
"""


def extract_fname_from_path(path, rm_extension=True, extension=".txt"):
  """
  Extracts filename from path.

  :param path: File path
  :param rm_extension: Remove the file extension (bool).
  :param extension: Extension to remove (str). Removes the last n characters, where n is length of given extension.
  :return: File name.
  """
  f_name = _path_leaf(path)
  if rm_extension:
    f_name = f_name[:-len(extension)]
  return f_name


def _path_leaf(path):
  """
  Extracts the filename from a path string

  """
  head, tail = ntpath.split(path)
  return tail


def append_slash(path):
  """
  If the last character in path is not "/", add it.

  :param path: str
  :return: path with "/" at the end.
  """
  if not isinstance(path, str):
    raise ValueError("path must be string.")

  if path[-1] != "/":
    return path + "/"
  else:
    return path


"""
Statistics
"""


def get_n_tokens(string, sep=" "):
  return len(string.split(sep))


"""
Pandas parallelization

Note: Didn't run faster. Think it needs to be tweaked.
"""


def find_split_indices(data, group_col, num_partitions):
  """
  Finds indices of new groups for splitting data with numpy.array_split.
  :param data: DataFrame
  :param group_col: Name of group column.
  :param num_partitions: Number of partitions to find start indices for.
  :return: List with indices.
  """
  groups = np.unique(data[group_col])  # Sorts the unique values
  if len(groups) == 0:
    raise ValueError("No unique values where found.")
  elif len(groups) == 1:
    return [0]
  else:
    groups_split = np.array_split(groups, num_partitions)
    return [data[group_col].searchsorted(g[0], side='left')[0] for g in groups_split]


def parallelize_dataframe(data, func, num_cores, group_col=None, parallelize=True):
  """
  Run pandas DataFrame apply functions in parallel to speed up computation.

  :param data: DataFrame. If group_col is not None, it is sorted by group_col.
  :param func: Function with apply in it. Must have only "data" as input.
    TODO Add multiple arguments support - Pool.starmap?
  :param num_cores: Number of cores to utilize.
  :param group_col: Name of col containing the group ID of the element.
  :return:
  """
  if parallelize:
    if group_col:
      data = data.sort_values(by=group_col)
      split_indices = find_split_indices(data, group_col=group_col, num_partitions=min(len(data), num_cores))
      if len(split_indices) == 1:
        df_split = np.array_split(data, 1)
      else:
        df_split = np.array_split(data, split_indices)
    else:
      df_split = np.array_split(data, min(len(data), num_cores))
    pool = Pool(num_cores).restart(force=True)
    df = pd.concat(pool.map(func, df_split))
    pool.close()
    pool.join()
    return df
  else:
    return func(data)
