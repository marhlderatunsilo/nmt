import tensorflow as tf
import abc


class DatasetLoader(object):
  """Wrapper class around tf.data.Dataset pipeline.
  """

  def __init__(self, file_path, mode, batch_size, src_token_vocab_table=None, src_char_vocab_table=None,
               tgt_token_vocab_table=None, sos=None, eos=None, source_reverse=False, random_seed=None,
               num_buckets=None, fallback_bucket_width=None, num_splits=None,
               src_token_max_len=None, tgt_token_max_len=None, token_cnn_tokens_per_sentence=None,
               char_cnn_tokens_per_sentence=None, char_cnn_chars_per_token=None,
               shuffle=True,
               num_parallel_calls=32, buffer_size=10000, **kwargs):
    """
    Base class for dataset loaders.
    Handles initialization of input arguments and adds common properties.

    Use of properties in subclass:
    Getters:
      Subclasses should update the field variable in super before returning to user.
      E.g.:
        @property
        def property_to_get(self):
          super(Subclass,Subclass).property_to_get.__set__(self, self.__property_to_get)
          return super().property_to_get

    :param file_path: Path to file.
    :param mode: tf.contrib.learn.ModeKeys (TRAIN,EVAL,INFER)
    :param batch_size: int.
    :param src_token_vocab_table: Vocab table for src.
    :param src_char_vocab_table: Vocab table for src chars.
    :param tgt_token_vocab_table: Vocab table for tgt.
    :param sos: Start of Sentence symbol.
    :param eos: End of Sentence symbol.
    :param source_reverse: Reverse source.
    :param random_seed: Random seed to ensure reproducibility.
    :param num_buckets: Number of buckets.
    :param fallback_bucket_width: The bucket width used when src_token_max_len is None.
    :param num_splits: Number of splits, e.g. 1 split per GPU.
    :param src_token_max_len: Max length of source.
    :param tgt_token_max_len: Max length of target.
    :param token_cnn_tokens_per_sentence: Fixed number of tokens per sentence for token CNN.
    :param char_cnn_tokens_per_sentence: Fixed number of tokens per sentence for character CNN.
    :param char_cnn_chars_per_token: Fixed number of chars per token for character CNN.
    :param shuffle: bool.
    :param num_parallel_calls: Number of parallel processes used in input parsing.
    :param buffer_size: int.
    :param kwargs: Additional keyword arguments used in subclass.
    """

    self.file_path = file_path
    self.mode = mode
    self.batch_size = batch_size
    self.src_token_vocab_table = src_token_vocab_table
    self.src_char_vocab_table = src_char_vocab_table
    self.tgt_token_vocab_table = tgt_token_vocab_table
    self.sos = sos
    self.eos = eos
    self.source_reverse = source_reverse
    self.random_seed = random_seed
    self.num_buckets = num_buckets
    self.fallback_bucket_width = fallback_bucket_width
    self.num_splits = num_splits
    self.src_token_max_len = src_token_max_len
    self.tgt_token_max_len = tgt_token_max_len
    self.token_cnn_tokens_per_sentence = token_cnn_tokens_per_sentence
    self.char_cnn_tokens_per_sentence = char_cnn_tokens_per_sentence
    self.char_cnn_chars_per_token = char_cnn_chars_per_token
    self.shuffle = shuffle
    self.num_parallel_calls = num_parallel_calls
    self.buffer_size = buffer_size
    self.__split_batches = None
    self.__batched_data = None
    self.__iterator = None

    if self.src_token_vocab_table:
      self.src_token_sos_id = tf.cast(self.src_token_vocab_table.lookup(tf.constant(self.sos)), tf.int32)
      self.src_token_eos_id = tf.cast(self.src_token_vocab_table.lookup(tf.constant(self.eos)), tf.int32)
    if self.src_char_vocab_table:
      self.src_char_sos_id = tf.cast(self.src_char_vocab_table.lookup(tf.constant(self.sos)), tf.int32)
      self.src_char_eos_id = tf.cast(self.src_char_vocab_table.lookup(tf.constant(self.eos)), tf.int32)
    if self.tgt_token_vocab_table:
      self.tgt_sos_id = tf.cast(self.tgt_token_vocab_table.lookup(tf.constant(self.sos)), tf.int32)
      self.tgt_eos_id = tf.cast(self.tgt_token_vocab_table.lookup(tf.constant(self.eos)), tf.int32)

  @property
  def split_batches(self):
    """
    Gets the splitted batches.
    To make sure, that all GPUs run batches from the same bucket (so they don't have to wait for each other),
    one batch is split to smaller batches.
    :return: Split batches (BatchedInput tuple).
    """
    print("Getting split_batches: ", self.__split_batches)
    if not self.__split_batches:
      if self.mode == tf.contrib.learn.ModeKeys.INFER:
        raise ValueError("split_batches is empty. Use batched_data in inference mode.")
      else:
        raise ValueError("split_batches is empty. Something went wrong.")
    return self.__split_batches

  @split_batches.setter
  def split_batches(self, split_batches):
    self.__split_batches = split_batches

  @property
  def batched_data(self):
    """
    Gets the batched data before it is split to GPUs.
    :return: Batched data (BatchedInput tuple).
    """
    if not self.__batched_data:
      raise ValueError("batched_data is None.")
    return self.__batched_data

  @batched_data.setter
  def batched_data(self, batched_data):
    self.__batched_data = batched_data

  @property
  def iterator(self):
    """
    Gets the iterator, e.g. for initialization.
    :return: Iterator.
    """
    if not self.__iterator:
      raise ValueError("iterator is None.")
    return self.__iterator

  @iterator.setter
  def iterator(self, iterator):
    self.__iterator = iterator

  @abc.abstractmethod
  def _parse_function_train(self, content):
    """Input parser for samples of the training set.

    Subclass must implement this.
    """
    pass

  @abc.abstractmethod
  def _parse_function_eval(self, content):
    """Input parser for samples of the dev/test set.

    Subclass must implement this.
    """
    pass

  @abc.abstractmethod
  def _parse_function_inference(self, content):
    """Input parser for inference data with no true labels.

    Subclass must implement this.
    """
    pass

  @staticmethod
  def cut_to_character(token, fixed_n_chars, padding_value):
    """
    Split token into chars and make it fixed size by cutting and padding.

    Example
    -------
    # Tensor with strings
    ts = [["ab"],
          ["cedf"],
          ["g"]]

    # Cut each string to characters and make fixed size 3
    chars = tf.map_fn(lambda txt: self.cut_to_character(txt, 3, "padding"), ts)

    # Results in
    [["a", "b", "padding"],
     ["c", "e", "d"],
     ["g", "padding", "padding"]]


    :param token: tf.string tensor.
    :param fixed_n_chars: Number of chars to fix size to. Int.
    :param padding_value: String to pad with.
    :return: token padded or cut to fixed size.
    """
    chars = tf.string_split([token], delimiter='', skip_empty=True).values
    return DatasetLoader.make_fixed_length_1d(chars, fixed_n_chars, padding_value)

  @staticmethod
  def make_fixed_length_1d(text, to_length, padding_value):
    """
    Make all elements have a fixed size.
    Cuts and pads to achieve to_length size.

    :param text: tf.string tensor 1d
    :param to_length: fixed size as int
    :param padding_value: string scalar, e.g. "<eos>"
    :return: padded or cut input with fixed length.
    """
    if isinstance(to_length, (list, tuple)):
      if len(to_length) == 1:
        to_length = to_length[0]
      else:
        raise ValueError("to_length must be scalar or list with 1 element.")
    text = text[:to_length]
    size = tf.size(text)
    paddings = tf.tile([padding_value], [to_length - size])
    return tf.concat([text, paddings], axis=0)

  @staticmethod
  def make_fixed_length_2d(text, to_length, padding_value, fix_inner=True):
    """
    Make all elements have a fixed size. Different sizes can be passed for each dimension.
    Cuts and pads to achieve to_length size.

    :param text: tensor with string tensors.
      E.g. [["a","b"],["c","d","t","q"],["r"]]
    :param to_length: list of sizes to fix to.
      E.g. [2,3], which would result in [["a","b","padding"],["c","d","t"]]
      Must contain 2 sizes.
    :param padding_value: string scalar, e.g. "<eos>"
    :param fix_inner: Whether to call make_fixed_length_1d on existing inner tensors. Bool.
      Useful if the inner tensors already have a fixed size.
    :return: padded or cut input with fixed lengths on both dimensions.
    """
    if not (isinstance(to_length, (list, tuple)) and len(to_length) == 2):
      raise ValueError("to_length must be scalar or list with 1 element.")

    text = text[:to_length[0], :]
    if fix_inner:
      text = tf.map_fn(lambda t: DatasetLoader.make_fixed_length_1d(t, to_length[1], padding_value), text)
    size = tf.shape(text)[0]
    inner_paddings = tf.tile([padding_value], [to_length[1]])
    paddings = tf.tile([inner_paddings],
                       [to_length[0] - size, 1])
    return tf.concat([text, paddings], axis=0)
