"""
prints first 3 examples in a .tfrecord file

"""

import tensorflow as tf

counter = 0
for example in tf.python_io.tf_record_iterator("../../data/s2c_wiki_defs_TRAIN.tfrecords"):
    result = tf.train.Example.FromString(example)
    print(result)
    counter += 1
    if counter >= 3:
      break