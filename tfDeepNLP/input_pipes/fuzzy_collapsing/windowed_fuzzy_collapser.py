from collections import OrderedDict, defaultdict

import numpy as np

from tfDeepNLP.input_pipes.fuzzy_collapsing.sliding_collapse import sliding_collapse
from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.dict_utils import invert_dict_with_duplicate_values, \
  invert_dict_with_lists


class WindowedFuzzyCollapser(object):
  # Set default thresholds
  default_thresholds = {3: 1.0,
                        4: 1.0,
                        5: 0.85,
                        6: 0.87,
                        7: 0.89,
                        8: 0.90,
                        9: 0.91,
                        10: 0.93}

  def __init__(self, strings, lev_thresholds=None, w_size=20,
               exclude=None, match_without_end_abbreviations=True):
    """

    :param strings: List of strings to collapse.
    :param lev_thresholds: Dict that maps word size to threshold [0.0,1.0].
      If None, WindowedFuzzyCollapser.default_thresholds is used.
      Every word size below the smallest word size in the dict gets the threshold of the smallest word size in the dict.
      Every word size above the largest word size in the dict gets the threshold of the largest word size in the dict.
    :param w_size: Window size for sliding windows.
    :param exclude: List of strings to exclude from collapsing.
      Exclusion happens before collapsing.
    :param match_without_end_abbreviations: Whether to measure the distance without end abbreviations,
      where an end abbreviation is a pair of parantheses in the end of the string with text,
      e.g. "A Bold Bat (ABB)" (case-insensitive). Bool.
    """

    self.__strings = np.sort(strings)
    self.__levenshtein_thresholds = lev_thresholds if lev_thresholds is not None else \
      WindowedFuzzyCollapser.default_thresholds
    self.__w_size = w_size
    self.__exclude = exclude
    self.__match_without_end_abbreviations = match_without_end_abbreviations

    self.__exclude_strings()

    # We wish to remember the order of insertions
    self.__mappings = None
    self.__inverse_mappings = None

  def __exclude_strings(self):
    # remove elements in exclusion list
    if self.__exclude:
      if not isinstance(self.__exclude, list):
        raise TypeError("exclude must be a list of strings or None.")
      self.__strings = np.asarray([s for s in self.__strings if s not in self.__exclude])

  @property
  def strings(self):
    return self.__strings

  @property
  def w_size(self):
    return self.__w_size

  @property
  def exclude(self):
    return self.__exclude

  @property
  def mappings(self):
    """
    :return: An OrderedDict or DefaultDict with mappings,
        where *key* is the word to replace in vocab and *value* is the replacement.
    """
    if self.__mappings is None:
      raise ValueError("Mappings have not been computed yet.")
    return self.__mappings

  @property
  def inverse_mappings(self):
    """
    :return: An OrderedDict or DefaultDict with mappings,
        where *value* is a list of the words to replace in vocab and *key* is the replacement.
    """
    if self.__inverse_mappings is None:
      raise ValueError("Inverse mappings have not been computed yet.")
    return self.__inverse_mappings

  @property
  def n_mappings(self):
    """
    :return: Number of mappings.
    """
    if self.__mappings is None:
      raise ValueError("Mappings have not been computed yet.")
    return len(self.__mappings)

  @property
  def n_to_be_replaced(self):
    return self.n_mappings

  @property
  def n_replacements(self):
    if self.__inverse_mappings is None:
      raise ValueError("Inverse mappings have not been computed yet.")
    return len(self.__inverse_mappings)

  @property
  def thresholds(self):
    """
    :return: Dict with Levenshtein thresholds at different word sizes.
    """
    return self.__levenshtein_thresholds

  def collapse_strings(self, inplace=True):
    """
    Collapse strings in a sliding window.
    :param inplace: Whether to write directly to object's mappings or return mappings
    :return: None in inplace is True, else OrderedDict with mappings
      where key is the string to replace and value is the replacement.
    """
    self.__mappings = OrderedDict()

    # First collapsing run
    mappings, last_window = sliding_collapse(strings_sorted=self.__strings,
                                             mappings=self.__mappings,
                                             lev_thresholds=self.__levenshtein_thresholds,
                                             w_size=self.__w_size,
                                             overlap=self.__w_size - 1,
                                             match_without_end_abbreviations=self.__match_without_end_abbreviations)
    last_window_size = len(last_window)

    # The last window have only compared its first element to the rest
    # We want to iteratively compare the first element with the rest,
    # and then remove the first element and do the same for the new first element

    while last_window_size > 2:
      mappings, last_window = sliding_collapse(strings_sorted=last_window,
                                               mappings=mappings,
                                               lev_thresholds=self.__levenshtein_thresholds,
                                               w_size=last_window_size - 1,
                                               overlap=last_window_size - 2,
                                               match_without_end_abbreviations=self.__match_without_end_abbreviations)
      last_window_size = len(last_window)

    mappings = self.__reverse_collapse_mappings(mappings)
    inverse_mappings = invert_dict_with_duplicate_values(mappings)

    if inplace:
      self.__mappings = mappings
      self.__inverse_mappings = inverse_mappings
    else:
      return mappings, inverse_mappings

  def __reverse_collapse_mappings(self, mappings):
    # Now we go through the dict in reverse order,
    # checking if a key is also a value. If so, the key and its mapped value are mapped that value's key.

    for i in range(self.__w_size):  # Multiple runs. TODO Could also run until the dict hasn't changed.
      for k, v in reversed(mappings.items()):
        if v in mappings:
          mappings[k] = mappings[v]

    return mappings

  def pick_common_name(self, by="first", mappings=None, inplace=True):
    """

    :param by: The order of names to pick by.
        "first": First alphabetically
        "last": Last alphabetically
        "longest_first": largest number of characters, first alphabetically
        "longest_last": largest number of characters, last alphabetically
        "shortest_first": smallest number of characters, first alphabetically
        "shortest_last": smallest number of characters, last alphabetically
    :param mappings: (Optional) An OrderedDict with mappings,
        where *value* is the word to map to and *key* is the word to remove from vocab.
        If None, the object's mappings are used.
    :param inplace: Whether to write directly to object's mappings or return mappings
    :return: None if inplace is True, else two DefaultDicts:
      1. DefaultDict where key is the string to change and value is the target to change it too,
      2. DefaultDict where key is the target to change to and value is the list of values that need to be changed.
    """

    if mappings is None:
      mappings = self.__mappings

    unique_values = np.unique(list(mappings.values()))

    # we invert the dictionary, so values become keys and vice versa
    # Since the same value can occur twice, the new values are lists of all the keys that shared the old value.
    inverted_dict = invert_dict_with_duplicate_values(mappings)

    def pick_name(keys, by):
      sorted_keys = sorted(keys)
      name = {
        "first": sorted_keys[0],
        "last": sorted_keys[-1],
        "longest_first": sorted(sorted_keys, key=len, reverse=True)[0],
        "longest_last": sorted(sorted_keys, key=len, reverse=False)[-1],
        "shortest_first": sorted(sorted_keys, key=len, reverse=False)[0],
        "shortest_last": sorted(sorted_keys, key=len, reverse=True)[-1],
      }.get(by, None)
      if name is None:
        raise ValueError("name is None. Check argument 'by' for errors.")
      return name

    def get_group(dict_, key):  # a group is the list of all keys collapsed to a value plus the value itself.
      return dict_[key] + [key]

    def map_group(key, by, dict_):
      group = get_group(dict_, key)
      name = pick_name(group, by)
      new_dict = defaultdict(list)
      for e in group:
        if e != name:
          new_dict[name].append(e)
      return new_dict

    new_maps = [map_group(v, by=by, dict_=inverted_dict) for v in unique_values]

    def merge_dicts(dicts):
      merged = defaultdict(dict)
      for d_ in dicts:
        merged.update(d_)
      return merged

    name_to_values_dict = merge_dicts(new_maps)

    value_to_name_dict = invert_dict_with_lists(name_to_values_dict)

    if inplace:
      self.__mappings = value_to_name_dict
      self.__inverse_mappings = name_to_values_dict
    else:
      return value_to_name_dict, name_to_values_dict

  def apply_mappings(self, strings):
    """
    Apply object's mappings to a given a list of strings.
    Replaces the keys with their associated values.
    Does not affect the object, only the given list of strings.
    :param strings: List of strings.
    :return: Strings where tokens that are a key in mappings are replaced by the associated value.
    """
    return [self.__mappings[s] if s in self.__mappings else s for s in strings]
