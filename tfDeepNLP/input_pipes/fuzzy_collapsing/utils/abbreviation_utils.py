""" Abbreviation handling """
import re


def extract_end_abbreviation(inputString):
  if inputString.endswith(")"):
    found = re.findall(r'\s\(\w*\)$', inputString)
    if len(found) == 0:
      return ""
    elif len(found) > 1:
      raise ValueError("'found' was larger than 1. This should not happen.")
    return found[0][1:]
  else:
    return ""


def remove_end_abbreviation(inputString):
  abbr = extract_end_abbreviation(inputString)
  if abbr:
    return inputString[:-(len(abbr) + 1)]
  else:
    return inputString
