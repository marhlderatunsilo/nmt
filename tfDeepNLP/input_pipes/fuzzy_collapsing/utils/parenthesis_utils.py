""" Paranthesis handling

Last paranthesis in string is considered an abbreviation. See abbreviation_utils.py.
"""
import re

from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.abbreviation_utils import remove_end_abbreviation


def extract_non_end_parantheses(inputString, remove_end_paranthesis=True):
  if remove_end_paranthesis:
    inputString = remove_end_abbreviation(inputString)
  found = re.findall(r'\s?\(\w*\)', inputString)
  if len(found) == 0:
    return []
  return [found[i].strip()[1:-1] for i in range(len(found))]
