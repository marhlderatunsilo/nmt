""" Compare element to target """
from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.numbers_utils import has_number, extract_numbers


def check_numbers(element, target):
  if has_number(target) and has_number(element):
    target_numbers = extract_numbers(target)
    token_numbers = extract_numbers(element)
    if len(target_numbers) == 0 or len(token_numbers) == 0:
      raise ValueError("Did not find numbers as expected.")
    if target_numbers == token_numbers:
      return True
    else:
      return False
  elif has_number(target) or has_number(element):
    return False
  else:
    return True


def test_check_numbers():  # TODO Add to unit tests
  assert check_numbers('(r) malhylde-8-1 3', '(s) malhylde-8-1 3')
  assert not check_numbers('(r) malhylde-8-1 3', '(s) malhylde-8-2 3')
  assert not check_numbers('(r) malhylde-8-2 3', '(s) malhylde-8-1 3')


def check_paranthesis_contents(element_paranthesis_contents, target_paranthesis_contents):
  if len(target_paranthesis_contents) > 0 or len(element_paranthesis_contents) > 0:
    if not len(target_paranthesis_contents) == len(element_paranthesis_contents):
      return False
    if not all([tpc == epc for tpc, epc in zip(target_paranthesis_contents, element_paranthesis_contents)]):
      return False
  return True


def test_check_parantheses_contents():  # TODO Add to unit tests
  assert check_paranthesis_contents(['r', 't'], ['r', 't'])
  assert not check_paranthesis_contents(['r', 't'], ['e', 't'])
  assert not check_paranthesis_contents(['r', 't'], ['r', '2'])
  assert not check_paranthesis_contents(['r', 't'], ['r', 't', 'er'])
