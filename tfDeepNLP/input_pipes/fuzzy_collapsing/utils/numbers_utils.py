""" Numbers handling """
import re


def has_number(inputString):
  return bool(re.search(r'\d', inputString))


def extract_numbers(inputString):
  return re.findall(r'\d+', inputString)
