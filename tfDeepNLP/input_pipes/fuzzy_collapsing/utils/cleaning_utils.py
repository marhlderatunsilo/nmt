""" Cleaning """
import re


def find_letterless(strings):
  return [s for s in strings if re.search('[a-zA-Z]', s) is None]


def remove_strings(vocab, strings_to_remove):
  return [v for v in vocab if v not in strings_to_remove]
