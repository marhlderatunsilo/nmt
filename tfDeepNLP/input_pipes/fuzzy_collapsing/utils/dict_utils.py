""" Dictionary handling"""
from collections import defaultdict


def invert_dict_with_duplicate_values(dict_):
  """
  Convert dictionary where value is the same for multiple keys
  to {value: [key1, key2, key3, ...]}.
  :param dict_: dictionary
  :return: defaultdict where value is a list.
  """
  new_dict = defaultdict(list)
  for k, v in dict_.items():
    new_dict[v].append(k)
  return new_dict


def invert_dict_with_lists(dict_):
  """
  Convert dictionary where value is a list to
  {val1:key1, val2:key1, val3:key1}.
  :param dict_: dictionary
  :return: defaultdict with one entrance per value in input.
  """
  new_dict = defaultdict(str)
  for k, vals in dict_.items():
    for v in vals:
      new_dict[v] = k
  return new_dict


def get_keys(dict_):
  """
  Get keys in dictionary as a list.
  """
  return list(dict_.keys())
