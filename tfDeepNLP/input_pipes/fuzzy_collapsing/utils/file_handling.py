""" File handling """
import pickle

from tfDeepNLP.input_pipes.text_preprocessor import append_slash


def open_vocab(path):
  with open(path) as f:
    vocab = f.readlines()
    print(len(vocab))
    return [sv[:-1] for sv in vocab]


def save_strings(strings, path):
  with open(path, "w") as f:
    for item in strings:
      f.write("%s\n" % item)


# Pickle write
def save_obj(obj, path, name):
  path = append_slash(path)
  with open(path + name + '.pkl', 'wb') as f:
    pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)


# Pickle read
def load_obj(path, name):
  path = append_slash(path)
  with open(path + name + '.pkl', 'rb') as f:
    return pickle.load(f)
