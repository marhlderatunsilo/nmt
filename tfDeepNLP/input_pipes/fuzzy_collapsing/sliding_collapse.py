from fuzzywuzzy import fuzz
from window_slider import Slider

from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.abbreviation_utils import remove_end_abbreviation
from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.comparison_utils import check_numbers, check_paranthesis_contents
from tfDeepNLP.input_pipes.fuzzy_collapsing.utils.parenthesis_utils import extract_non_end_parantheses


def sliding_collapse(strings_sorted, mappings, lev_thresholds,
                     w_size, overlap, match_without_end_abbreviations):
  slider = Slider(w_size, overlap)
  slider.fit(strings_sorted)
  while True:
    window_data = slider.slide()

    if len(window_data) <= 1:
      break

    target = window_data[0]

    (target, target_no_abbreviation, target_paranthesis_contents) = get_string_versions(
      target, match_without_end_abbreviations=match_without_end_abbreviations)

    if match_without_end_abbreviations:
      effective_threshold = get_threshold(len(target_no_abbreviation), lev_thresholds)
    else:
      effective_threshold = get_threshold(len(target), lev_thresholds)

    if effective_threshold == 1.0:
      if slider.reached_end_of_list():
        break
      else:
        continue

    for i in range(len(window_data) - 1):
      element = window_data[i + 1]

      (element, element_no_abbreviation, element_paranthesis_contents) = get_string_versions(
        element, match_without_end_abbreviations=match_without_end_abbreviations)

      if match_without_end_abbreviations:
        lev_similarity = fuzz.ratio(target_no_abbreviation, element_no_abbreviation) / 100.0
      else:
        lev_similarity = fuzz.ratio(target, element) / 100.0

      if lev_similarity >= effective_threshold:

        numbers_equality = check_numbers(element, target)
        if not numbers_equality:
          continue

        # Check that the content of parantheses (not located in end of string) are exact matches
        # Usually paranthesis are used to disambiguate similar concepts. End parantheses are more often
        # just abbreviations though.
        non_end_parantheses_equality = check_paranthesis_contents(element_paranthesis_contents,
                                                                  target_paranthesis_contents)
        if not non_end_parantheses_equality:
          continue

        mappings[element] = target
      elif element[:-1] == target and element[-1] == "s" and effective_threshold != 1.0:
        mappings[element] = target

    if slider.reached_end_of_list(): break

  # We want the updated mappings and the last window
  return mappings, window_data


def get_string_versions(string, match_without_end_abbreviations):
  string_no_abbreviation = None

  if match_without_end_abbreviations:
    string_no_abbreviation = remove_end_abbreviation(string)
    string_paranthesis_contents = extract_non_end_parantheses(string_no_abbreviation,
                                                              remove_end_paranthesis=False)
  else:
    string_paranthesis_contents = extract_non_end_parantheses(string,
                                                              remove_end_paranthesis=True)

  return string, string_no_abbreviation, string_paranthesis_contents


def get_threshold(size, threshold_map):
  min_size = min(threshold_map.keys())
  max_size = max(threshold_map.keys())
  if size <= min_size:
    return threshold_map[min_size]
  elif size >= max_size:
    return threshold_map[max_size]
  else:
    return threshold_map[size]
