import tensorflow as tf

from tfDeepNLP.attention.monotonic_bahdanau_with_initialization import BahdanauMonotonicAttentionWithInit
from .coverage_bahdanau_attention import CoverageBahdanauAttention

def create_attention_mechanism(attention_option, num_units, memory,
                               memory_sequence_length, init_op=None, name=None):
  """Create attention mechanism based on the attention_option."""
  # Mechanism
  if attention_option == "luong":
    attention_mechanism = tf.contrib.seq2seq.LuongAttention(
      num_units, memory, memory_sequence_length=memory_sequence_length, name=name)
  elif attention_option == "scaled_luong":
    attention_mechanism = tf.contrib.seq2seq.LuongAttention(
      num_units,
      memory,
      memory_sequence_length=memory_sequence_length,
      scale=True,
      name=name)
  elif attention_option == "bahdanau":
    attention_mechanism = tf.contrib.seq2seq.BahdanauAttention(
      num_units, memory, memory_sequence_length=memory_sequence_length,
      name=name)
  elif attention_option == "monotonic_bahdanau":
    attention_mechanism = BahdanauMonotonicAttentionWithInit(
      num_units,
      memory,
      score_bias_init=-1.0,
      #mode='hard',
      memory_sequence_length=memory_sequence_length,
      normalize=False,
      init_op=init_op,
      name=name)
  elif attention_option == "normed_bahdanau":
    attention_mechanism = tf.contrib.seq2seq.BahdanauAttention(
      num_units,
      memory,
      memory_sequence_length=memory_sequence_length,
      normalize=True,
      name=name)
  elif attention_option == "normed_monotonic_bahdanau":
    attention_mechanism = BahdanauMonotonicAttentionWithInit(
      num_units,
      memory,
      score_bias_init=-1.0,
      #mode='hard',
      memory_sequence_length=memory_sequence_length,
      normalize=True,
      init_op=init_op,
      name=name)
  elif attention_option == "scaled_monotonic_luong":
    attention_mechanism = tf.contrib.seq2seq.LuongMonotonicAttention(
      num_units,
      memory,
      score_bias_init=0.0,
      memory_sequence_length=memory_sequence_length,
      scale=True,
      name=name)
  elif attention_option == "monotonic_luong":
    attention_mechanism = tf.contrib.seq2seq.LuongMonotonicAttention(
      num_units,
      memory,
      score_bias_init=0.0,
      memory_sequence_length=memory_sequence_length,
      scale=False,
      name=name)
  elif attention_option == "coverage_normed_bahdanau":
    attention_mechanism = CoverageBahdanauAttention(
      num_units,
      memory,
      memory_sequence_length=memory_sequence_length,
      normalize=True,
      use_coverage=True,
      name=name)
  else:
    raise ValueError("Unknown attention option %s" % attention_option)

  return attention_mechanism
