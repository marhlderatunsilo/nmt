import tensorflow as tf


class BahdanauMonotonicAttentionWithInit(tf.contrib.seq2seq.BahdanauMonotonicAttention):

  def __init__(self,
               num_units,
               memory,
               memory_sequence_length=None,
               normalize=False,
               score_mask_value=float("-inf"),
               sigmoid_noise=0.,
               sigmoid_noise_seed=None,
               score_bias_init=0.,
               mode="parallel",
               name="BahdanauMonotonicAttention",
               init_op=None):
    super(BahdanauMonotonicAttentionWithInit, self).__init__(
      num_units,
      memory,
      memory_sequence_length=memory_sequence_length,
      normalize=normalize,
      score_mask_value=score_mask_value,
      sigmoid_noise=sigmoid_noise,
      sigmoid_noise_seed=sigmoid_noise_seed,
      score_bias_init=score_bias_init,
      mode=mode,
      #dtype=dtype,
      name=name)

    self.init_op = init_op

  def __call__(self, query, state=None, previous_alignments=None):

    if previous_alignments is not None:
      state = previous_alignments

    with tf.variable_scope("BahdanauMonotonicAttentionWithInit", initializer=self.init_op):
      return super(BahdanauMonotonicAttentionWithInit, self).__call__(query, state)
