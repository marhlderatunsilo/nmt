from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file

CKPT_PATHS = ["../out_normed_bahdanau/translate.ckpt-20"]

[print_tensors_in_checkpoint_file(file_name=path, tensor_name='', all_tensors=False) for path in CKPT_PATHS]

