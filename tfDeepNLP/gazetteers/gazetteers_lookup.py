from nltk.corpus import names, gazetteers

from tfDeepNLP.gazetteers.tfgazetteer import create_gazetteer_generator, read_gazetteer_file_to_list
from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser


def lookup_gazetteers(gazetteer_feature_files, tokens, person_names_file=''):
  NUMBERS_LOWER = ['one', 'two', 'three', 'four', 'five', 'six', 'seven',
                   'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen',
                   'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen',
                   'nineteen', 'twenty', 'thirty', 'fourty', 'fifty',
                   'sixty', 'seventy', 'eighty', 'ninety', 'hundred',
                   'thousand', 'million', 'billion', 'trillion']
  ORG_SUFFIXES_LOWER = ['ltd', 'inc', 'co', 'corp', 'plc', 'llc', 'llp', 'gmbh',
                        'corporation', 'associates', 'partners', 'committee',
                        'institute', 'commission', 'university', 'college',
                        'airlines', 'magazine']
  PERSON_SUFFIXES_LOWER = ['sr', 'jr', 'phd', 'md']
  COUNTRIES = [country for filename in ('isocountries.txt', 'countries.txt')
               for country in gazetteers.words(filename)] + ['EU']

  gazetteer_features_lists = [NUMBERS_LOWER, ORG_SUFFIXES_LOWER, COUNTRIES,
                              PERSON_SUFFIXES_LOWER]

  FEATURES_FROM_HPARAMS = [read_gazetteer_file_to_list(gazetteer_file) for gazetteer_file in
                           gazetteer_feature_files] if gazetteer_feature_files != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE else []

  if len(FEATURES_FROM_HPARAMS) > 0:
    gazetteer_features_lists = gazetteer_features_lists + FEATURES_FROM_HPARAMS

  if person_names_file:
    with open(person_names_file) as f:
      NAMES = [name.lower().strip() for name in f.readlines()]

    NAMES = [name.lower() for filename in ('male.txt', 'female.txt') for name in names.words(filename)] + NAMES

    NAMES = list(set(NAMES))
    gazetteer_features_lists = [NAMES] + gazetteer_features_lists

  gazetteer_generator = create_gazetteer_generator(gazetteer_features_lists)
  gazetteer_features = gazetteer_generator(tokens)

  return gazetteer_features
