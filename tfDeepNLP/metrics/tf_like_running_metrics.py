import tensorflow as tf
from tensorflow.python import ops
from tensorflow.python.eager import context

__all__ = ["f1"]

def f1(precision_metric,
       recall_metric,
       metrics_collections=None,
       updates_collections=None,
       name=None):
  """Computes the recall of the predictions with respect to the labels.

  The `recall` function creates two local variables, `true_positives`
  and `false_negatives`, that are used to compute the recall. This value is
  ultimately returned as `recall`, an idempotent operation that simply divides
  `true_positives` by the sum of `true_positives`  and `false_negatives`.

  For estimation of the metric over a stream of data, the function creates an
  `update_op` that updates these variables and returns the `recall`. `update_op`
  weights each prediction by the corresponding value in `weights`.

  If `weights` is `None`, weights default to 1. Use weights of 0 to mask values.

  Args:
    labels: The ground truth values, a `Tensor` whose dimensions must match
      `predictions`. Will be cast to `bool`.
    predictions: The predicted values, a `Tensor` of arbitrary dimensions. Will
      be cast to `bool`.
    weights: Optional `Tensor` whose rank is either 0, or the same rank as
      `labels`, and must be broadcastable to `labels` (i.e., all dimensions must
      be either `1`, or the same as the corresponding `labels` dimension).
    metrics_collections: An optional list of collections that `recall` should
      be added to.
    updates_collections: An optional list of collections that `update_op` should
      be added to.
    name: An optional variable_scope name.

  Returns:
    recall: Scalar float `Tensor` with the value of `true_positives` divided
      by the sum of `true_positives` and `false_negatives`.
    update_op: `Operation` that increments `true_positives` and
      `false_negatives` variables appropriately and whose value matches
      `recall`.

  Raises:
    ValueError: If `predictions` and `labels` have mismatched shapes, or if
      `weights` is not `None` and its shape doesn't match `predictions`, or if
      either `metrics_collections` or `updates_collections` are not a list or
      tuple.
    RuntimeError: If eager execution is enabled.
  """
  if context.executing_eagerly():
    raise RuntimeError('tf.metrics.recall is not supported is not '
                       'supported when eager execution is enabled.')

  with tf.variable_scope(name, 'f1'):

    def compute_f1(precision, recall, name):
      return tf.identity(2 * precision * recall / (precision + recall), name=name)

    precision_metric, precision_update_op = precision_metric
    recall_metric, recall_update_op = recall_metric

    f1 = compute_f1(precision_metric, recall_metric, 'value')
    update_op = compute_f1(precision_update_op,
                           recall_update_op, 'update_op')

    if metrics_collections:
      ops.add_to_collections(metrics_collections, f1)

    if updates_collections:
      ops.add_to_collections(updates_collections, update_op)

    return f1, update_op
