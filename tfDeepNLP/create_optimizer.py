import tensorflow as tf


def create_optimizer(hparams, global_step, batch_size=None):

  if hparams.get('lr_decay', {}):
    assert 0 < hparams.lr_decay['decay_factor'] < 1.0
    learning_rate = tf.cond(
      global_step < hparams.lr_decay['start_decay_step'],
      lambda: tf.constant(hparams.learning_rate),
      lambda: tf.train.exponential_decay(
        hparams.learning_rate,
        (global_step - hparams.lr_decay['start_decay_step']),
        hparams.lr_decay['decay_steps'],
        hparams.lr_decay['decay_factor'],
        staircase=True),
      name="learning_rate")

  elif hparams.get('lr_range', {}):  # find best learning rate
    assert hparams.get('optimizer', '')
    lr_progress = tf.divide(tf.cast(global_step, tf.float32), float(hparams.max_steps))
    if hparams.linear is True:
      learning_rate = hparams.lr_range['min_lr'] + (hparams.lr_range['max_lr'] - hparams.lr_range['min_lr']) * lr_progress
    else:
      mult = (hparams.lr_range['max_lr'] / hparams.lr_range['min_lr']) ** lr_progress
      learning_rate = hparams.lr_range['min_lr'] * mult

  else:
    learning_rate = tf.constant(hparams.learning_rate)

  if hparams.optimizer == "sgd":
    optimizer_op = tf.train.GradientDescentOptimizer(learning_rate)

  elif hparams.optimizer == "sgdm":
    optimizer_op = tf.train.MomentumOptimizer(learning_rate, momentum=0.9)

  elif hparams.optimizer == "adam":
    optimizer_op = tf.train.AdamOptimizer(learning_rate)

  elif hparams.optimizer == "lazy_adam":
    optimizer_op = tf.contrib.opt.LazyAdamOptimizer(learning_rate)

  elif hparams.optimizer == "nadam":  # not tested
    optimizer_op = tf.contrib.opt.NadamOptimizer(learning_rate)

  elif hparams.optimizer == "adamw":  # not tested
    W_NORM = hparams.l2_scale
    weight_decay = W_NORM * tf.sqrt(batch_size / hparams.num_training_samples * hparams.num_train_epochs)
    optimizer_op = tf.contrib.opt.AdamWOptimizer(weight_decay, learning_rate)
    learning_rate = tf.constant(hparams.learning_rate)

  elif hparams.optimizer == "adamw+super_convergence":   # not tested
    W_NORM = hparams.l2_scale
    schedule = tf.train.cosine_decay_restarts(1, global_step, first_decay_steps=hparams.start_decay_step, t_mul=2.0,
                                              m_mul=1.0, alpha=0.0)
    lr = hparams.learning_rate * schedule
    weight_decay = W_NORM * tf.sqrt(batch_size / hparams.num_training_samples * hparams.num_train_epochs) * schedule
    optimizer_op = tf.contrib.opt.AdamWOptimizer(weight_decay, lr)
    learning_rate = tf.constant(hparams.learning_rate)

  else:
    raise ValueError("Unknown optimizer type %s!" % hparams.optimizer)

  return optimizer_op, learning_rate
