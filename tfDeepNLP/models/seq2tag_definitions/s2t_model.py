"""S2C: Basic sequence-to-classification model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.data_augmentation_utils import embedding_augmentation
from .s2t_base_model import S2TBaseModel

utils.check_tensorflow_version()

__all__ = ["S2TModel"]


class S2TModel(S2TBaseModel):
  """Sequence-to-classification dynamic model.

  This class implements a multi-layer recurrent neural network as encoder and a softmax classifier.
  """

  def _build_token_encoder(self, hparams, additional_embeddings=None):
    """Build an encoder."""

    # num_residual_layers = hparams.token_rnn_num_residual_layers

    iterator = self.iterator

    source = iterator.source_tokens
    source_sequence_lengths = iterator.source_sequence_length

    with tf.variable_scope("token_encoder") as scope:
      dtype = scope.dtype
      # Look up embedding, emp_inp: [batch_size, max_time, num_units]
      encoder_emb_inp = tf.nn.embedding_lookup(
        self.token_embedding_encoder, source)

      if self.augment_embeddings:
        encoder_emb_inp = embedding_augmentation(encoder_emb_inp,
                                                 noise_amount_mean=self.noise_amount_mean,
                                                 noise_amount_stddev=self.noise_amount_stddev)

      if additional_embeddings is not None:
        encoder_emb_inp = tf.concat([encoder_emb_inp, additional_embeddings], axis=2)

      if self.time_major:
        encoder_emb_inp = tf.transpose(encoder_emb_inp, perm=[1, 0, 2])

      if hparams.use_word_features:
        # Concatenate embeddings and word features
        encoder_emb_inp = tf.transpose(tf.concat([
          tf.transpose(encoder_emb_inp), [iterator.word_length], [iterator.title_cased], [iterator.all_upper_case],
          [iterator.all_lower_case], [iterator.mixed_case], [iterator.punctuation], [iterator.numeric]], axis=0))

      with tf.variable_scope("bi-lstm"):
        cells_fw = []
        cells_bw = []
        for i in range(hparams.token_rnn_num_layers):
          # _build_encoder_cell_token() # TODO Use this
          cell_fw = tf.contrib.rnn.LSTMBlockCell(hparams.token_rnn_num_units)
          cell_bw = tf.contrib.rnn.LSTMBlockCell(hparams.token_rnn_num_units)

          cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw,
                                                  hparams.tagger_dropout)  # TODO Add tagger_dropout to hparams
          cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, hparams.tagger_dropout)

          cells_fw.append(cell_fw)
          cells_bw.append(cell_bw)

        attention_mechanism_fw = tf.contrib.seq2seq.BahdanauAttention(
          hparams.token_rnn_num_units,
          encoder_emb_inp,
          normalize=True,
          memory_sequence_length=source_sequence_lengths,  # TODO
        )

        cells_fw[0] = tf.contrib.seq2seq.AttentionWrapper(
          cells_fw[0],
          attention_mechanism_fw,
          attention_layer_size=hparams.token_rnn_num_units,
          alignment_history=False,
          name="attention")

        attention_mechanism_bw = tf.contrib.seq2seq.BahdanauAttention(
          hparams.token_rnn_num_units,
          encoder_emb_inp,
          normalize=True,
          memory_sequence_length=source_sequence_lengths,
        )

        cells_bw[0] = tf.contrib.seq2seq.AttentionWrapper(
          cells_bw[0],
          attention_mechanism_bw,
          attention_layer_size=hparams.token_rnn_num_units,
          alignment_history=False,
          name="attention")

        encoder_outputs, _, __ = tf.contrib.rnn.stack_bidirectional_dynamic_rnn(cells_fw, cells_bw, encoder_emb_inp,
                                                                                sequence_length=source_sequence_lengths,
                                                                                dtype=tf.float32,
                                                                                parallel_iterations=hparams.parallel_iterations)

        return encoder_outputs

  def _build_char_encoder(self, hparams):
    """Build a CNN encoder."""

    num_layers = hparams.char_rnn_num_layers
    num_residual_layers = hparams.char_rnn_num_residual_layers

    iterator = self.iterator
    source_chars = iterator.source_chars

    source_sequence_lengths = iterator.source_sequence_length
    source_sequence_length_scalar = tf.reduce_max(source_sequence_lengths)  # TODO Padded to same length, right?
    dynamic_batch_size = tf.shape(source_chars)[0]

    source_chars = tf.reshape(source_chars,
                              shape=(dynamic_batch_size, source_sequence_length_scalar,
                                     hparams.char_rnn_chars_per_token))

    if self.time_major:  # TODO GET THE FORMATS RIGHT
      source_chars = tf.transpose(source_chars, perm=[1, 0, 2])

    with tf.variable_scope("char_encoder") as scope:
      dtype = scope.dtype
      # Look up embedding, emp_inp: [max_time, batch_size, num_units]
      encoder_emb_inp = tf.nn.embedding_lookup(
        self.char_embedding_encoder, source_chars)

      if self.augment_embeddings:
        encoder_emb_inp = embedding_augmentation(encoder_emb_inp,
                                                 noise_amount_mean=self.noise_amount_mean,
                                                 noise_amount_stddev=self.noise_amount_stddev)

      # shape = (batch size, max length of sentence, max length of word, dim_char)
      char_embeddings = tf.nn.embedding_lookup(encoder_emb_inp, self.source_char_vocab_table, name="char_embeddings")

      shape = tf.shape(char_embeddings)
      # shape: [batch size * max length of sentence, max length of word, dim_char], -1 is to infer the shape
      char_embeddings = tf.reshape(char_embeddings, shape=[-1, shape[-2], hparams.dim_char])

      # bilstm on chars, need 2 instances of cells
      cells_fw = []
      cells_bw = []

      for i in range(num_layers):
        cell_fw = tf.contrib.rnn.LSTMBlockCell(hparams.char_rnn_num_units)
        cell_bw = tf.contrib.rnn.LSTMBlockCell(hparams.char_rnn_num_units)

        cell_fw = tf.contrib.rnn.DropoutWrapper(cell_fw, hparams.char_rnn_dropout)
        cell_bw = tf.contrib.rnn.DropoutWrapper(cell_bw, hparams.char_rnn_dropout)

        if i > max(0, num_layers - num_residual_layers):
          cell_fw = tf.contrib.rnn.ResidualWrapper(cell_fw)
          cell_bw = tf.contrib.rnn.ResidualWrapper(cell_bw)

        cells_fw.append(cell_fw)
        cells_bw.append(cell_bw)

      cell_fw = tf.contrib.rnn.MultiRNNCell(cells_fw)
      cell_bw = tf.contrib.rnn.MultiRNNCell(cells_bw)

      # shape: (batch_size * max length of sentence) # TODO Is this working in def. version?
      word_lengths = tf.reshape(iterator.word_length, shape=[-1])
      _, (output_state_fw, output_state_bw) = tf.nn.bidirectional_dynamic_rnn(cell_fw, cell_bw,
                                                                              char_embeddings,
                                                                              sequence_length=word_lengths,
                                                                              dtype=tf.float32,
                                                                              parallel_iterations=hparams.parallel_iterations)

      # axis = -1, concat at axis of shape[-1]
      char_level_embeddings = tf.concat([output_state_fw[-1][0], output_state_bw[-1][0]], axis=-1)

      # shape = (batch, max length of sentence, 2 * char hidden size)
      encoder_outputs = tf.reshape(char_level_embeddings,
                                   shape=[-1, shape[1], 2 * hparams.char_num_units])
      return encoder_outputs

  def _do_crf_inference(self, logits, labels):
    with tf.variable_scope("crf_inference"):
      if self.mode != tf.contrib.learn.ModeKeys.INFER:

        print("logits:", logits)
        print("labels:", labels)
        print("self.sequence_lengths:", self.sequence_lengths)

        log_likelihood, transition_params = tf.contrib.crf.crf_log_likelihood(
          logits, labels, self.sequence_lengths)

        return log_likelihood, transition_params
      else:
        transition_params = tf.get_variable("transitions", [self.ntags, self.ntags])
        transition_params = tf.identity(transition_params, name="transition_params")

        return None, transition_params

  def _do_argmax_inference(self, logits):
    with tf.variable_scope("argmax_inference"):
      labels_pred = tf.cast(tf.argmax(logits, axis=-1), tf.int32, name="predicted_label")
      return labels_pred
