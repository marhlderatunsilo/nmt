# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""S2C: TensorFlow NMT model implementation."""
from __future__ import print_function

import argparse
import random
import sys

# import matplotlib.image as mpimg
import numpy as np
import tensorflow as tf
from tfDeepNLP.models.seq2tag_definitions.s2t_arguments_parser import S2TArgumentsParser
from tfDeepNLP.models.seq2tag_definitions.s2t_hparams_creator import S2THyperParamsCreator

from tfDeepNLP.models.seq2tag_definitions import train
from tfDeepNLP.utils import misc_utils as utils

utils.check_tensorflow_version()

FLAGS = None


def run_main(flags, default_hparams, train_fn, hparams_creator):
  """Run main."""
  # Job
  jobid = flags.jobid
  utils.print_out("# Job id %d" % jobid)

  # Random
  random_seed = flags.random_seed
  if random_seed is not None and random_seed > 0:
    utils.print_out("# Set random seed to %d" % random_seed)
    random.seed(random_seed + jobid)
    np.random.seed(random_seed + jobid)

  ## Train / Decode
  out_dir = flags.out_dir
  if not tf.gfile.Exists(out_dir): tf.gfile.MakeDirs(out_dir)

  # Load hparams.
  # Load hparams.
  hparams = hparams_creator.create_or_load_hparams(out_dir, default_hparams, flags.hparams_path, flags)

  if flags.mode in ["infer", "inference"]:
    print("Entering inference mode.")
  elif flags.mode in ["train", "training"]:
    print("Entering training mode.")
  elif flags.mode in ["save", "saving"]:
    print("Entering save mode.")
  # Train
  train_fn(hparams, mode=flags.mode)


def main(unused_argv):
  hparams_creator = S2THyperParamsCreator()
  default_hparams = hparams_creator(FLAGS)
  train_fn = train.s2t_train
  run_main(FLAGS, default_hparams, train_fn, hparams_creator)


if __name__ == "__main__":
  tfDeepNLP_parser = argparse.ArgumentParser()
  s2t_arguments_parser = S2TArgumentsParser()
  s2t_arguments_parser(tfDeepNLP_parser)
  FLAGS, unparsed = tfDeepNLP_parser.parse_known_args()
  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
