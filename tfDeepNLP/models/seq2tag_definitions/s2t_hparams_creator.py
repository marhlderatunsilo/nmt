from tfDeepNLP.utils import vocab_utils
from tfDeepNLP.utils.common_hparams_creator import CommonHyperParamsCreator


class S2THyperParamsCreator(CommonHyperParamsCreator):
  def __call__(self, flags):

    self.hparams = super(S2THyperParamsCreator, self).__call__(flags)

    # Networks
    self.hparams.add_hparam("classifier_num_units", flags.classifier_num_units)
    self.hparams.add_hparam("token_rnn_num_units", flags.token_rnn_num_units)
    self.hparams.add_hparam("char_rnn_num_units", flags.char_rnn_num_units)
    self.hparams.add_hparam("token_rnn_num_layers", flags.token_rnn_num_layers)

    self.hparams.add_hparam("char_rnn_num_layers", flags.char_rnn_num_layers)
    self.hparams.add_hparam("classifier_zoneout", flags.classifier_zoneout)
    self.hparams.add_hparam("token_rnn_zoneout", flags.token_rnn_zoneout)
    self.hparams.add_hparam("char_rnn_zoneout", flags.char_rnn_zoneout)
    self.hparams.add_hparam("classifier_dropout", flags.classifier_dropout)
    self.hparams.add_hparam("token_rnn_dropout", flags.token_rnn_dropout)
    self.hparams.add_hparam("char_rnn_dropout", flags.char_rnn_dropout)
    self.hparams.add_hparam("token_rnn_unit_type", flags.token_rnn_unit_type)
    self.hparams.add_hparam("char_rnn_unit_type", flags.char_rnn_unit_type)
    self.hparams.add_hparam("use_peepholes", flags.use_peepholes)
    self.hparams.add_hparam("token_rnn_encoder_type", flags.token_rnn_encoder_type)
    self.hparams.add_hparam("char_rnn_encoder_type", flags.char_rnn_encoder_type)

    self.hparams.add_hparam("token_rnn_forget_bias", flags.token_rnn_forget_bias)
    self.hparams.add_hparam("char_rnn_forget_bias", flags.char_rnn_forget_bias)
    self.hparams.add_hparam("token_rnn_norm_gain", flags.token_rnn_norm_gain)
    self.hparams.add_hparam("char_rnn_norm_gain", flags.char_rnn_norm_gain)

    self.hparams.add_hparam("use_token_rnn", flags.use_token_rnn)
    self.hparams.add_hparam("use_token_cnn", flags.use_token_cnn)

    self.hparams.add_hparam("use_char_rnn", flags.use_char_rnn)

    self.hparams.add_hparam("use_char_cnn", flags.use_char_cnn)
    self.hparams.add_hparam("send_token_rnn_to_classifier", flags.send_token_rnn_to_classifier)
    self.hparams.add_hparam("send_token_cnn_to_classifier", flags.send_token_cnn_to_classifier)
    self.hparams.add_hparam("send_char_rnn_to_classifier", flags.send_char_rnn_to_classifier)
    self.hparams.add_hparam("send_char_cnn_to_classifier", flags.send_char_cnn_to_classifier)
    self.hparams.add_hparam("send_char_rnn_to_token_rnn", flags.send_char_rnn_to_token_rnn)
    self.hparams.add_hparam("use_chars", False)  # Reserve. Automatically assigned based on network architecture
    self.hparams.add_hparam("use_tokens", False)

    self.hparams.add_hparam("char_cnn_preset", flags.char_cnn_preset)
    self.hparams.add_hparam("char_cnn_num_token_level_layers", flags.char_cnn_num_token_level_layers)
    self.hparams.add_hparam("char_cnn_token_level_skip_connections", flags.char_cnn_token_level_skip_connections)
    self.hparams.add_hparam("token_cnn_preset", flags.token_cnn_preset)
    self.hparams.add_hparam("token_cnn_num_token_level_layers", flags.token_cnn_num_token_level_layers)
    self.hparams.add_hparam("token_cnn_token_level_skip_connections", flags.token_cnn_token_level_skip_connections)

    self.hparams.add_hparam("use_word_features", flags.use_word_features)

    # Ensemble training
    self.hparams.add_hparam("char_rnn_use_ensemble", flags.char_rnn_use_ensemble)
    self.hparams.add_hparam("token_rnn_use_ensemble", flags.token_rnn_use_ensemble)
    self.hparams.add_hparam("char_cnn_use_ensemble", flags.char_cnn_use_ensemble)
    self.hparams.add_hparam("token_cnn_use_ensemble", flags.token_cnn_use_ensemble)
    self.hparams.add_hparam("char_rnn_send_ensemble_softmax_to_classifier",
                            flags.char_rnn_send_ensemble_softmax_to_classifier)
    self.hparams.add_hparam("token_rnn_send_ensemble_softmax_to_classifier",
                            flags.token_rnn_send_ensemble_softmax_to_classifier)
    self.hparams.add_hparam("char_cnn_send_ensemble_softmax_to_classifier",
                            flags.char_cnn_send_ensemble_softmax_to_classifier)
    self.hparams.add_hparam("token_cnn_send_ensemble_softmax_to_classifier",
                            flags.token_cnn_send_ensemble_softmax_to_classifier)

    self.hparams.add_hparam("char_rnn_send_ensemble_logits_to_classifier",
                            flags.char_rnn_send_ensemble_logits_to_classifier)
    self.hparams.add_hparam("token_rnn_send_ensemble_logits_to_classifier",
                            flags.token_rnn_send_ensemble_logits_to_classifier)
    self.hparams.add_hparam("char_cnn_send_ensemble_logits_to_classifier",
                            flags.char_cnn_send_ensemble_logits_to_classifier)
    self.hparams.add_hparam("token_cnn_send_ensemble_logits_to_classifier",
                            flags.token_cnn_send_ensemble_logits_to_classifier)

    self.hparams.add_hparam("ensemble_loss_weight", flags.ensemble_loss_weight)
    self.hparams.add_hparam("use_ensemble", False)  # Reserve. Automatically assigned based on network architecture

    self.hparams.add_hparam("token_rnn_residual", flags.token_rnn_residual)
    self.hparams.add_hparam("char_rnn_residual", flags.char_rnn_residual)

    self.hparams.add_hparam("token_embedding_size", flags.token_embedding_size)
    self.hparams.add_hparam("char_embedding_size", flags.char_embedding_size)
    self.hparams.add_hparam("augment_embeddings", flags.augment_embeddings)
    self.hparams.add_hparam("embedding_noise_amount_mean", flags.embedding_noise_amount_mean)
    self.hparams.add_hparam("embedding_noise_amount_stddev", flags.embedding_noise_amount_stddev)

    # Data constraints
    self.hparams.add_hparam("src_token_max_len", flags.src_token_max_len)
    self.hparams.add_hparam("char_cnn_tokens_per_sentence", flags.char_cnn_tokens_per_sentence)
    self.hparams.add_hparam("char_cnn_chars_per_token", flags.char_cnn_chars_per_token)
    self.hparams.add_hparam("token_cnn_tokens_per_sentence", flags.token_cnn_tokens_per_sentence)

    # Inference
    self.hparams.add_hparam("src_token_max_len_infer", flags.src_token_max_len_infer)

    # Vocab
    self.hparams.add_hparam("token_vocab_prefix", flags.token_vocab_prefix)
    self.hparams.add_hparam("char_vocab_prefix", flags.char_vocab_prefix)
    self.hparams.add_hparam("tags_vocab_prefix", flags.tags_vocab_prefix)

    # Char RNN conv_lstm
    self.hparams.add_hparam("char_rnn_conv_output_channels", flags.char_rnn_conv_output_channels)
    self.hparams.add_hparam("char_rnn_conv_kernel_shape", flags.char_rnn_conv_kernel_shape)

    # Misc
    self.hparams.add_hparam("ckpt_last_n_epochs", flags.ckpt_last_n_epochs)

    return self.hparams

  def extend_hparams(self, hparams, flags):

    self.hparams = super(S2THyperParamsCreator, self).extend_hparams(hparams, flags)

    if hparams.token_rnn_encoder_type == "bi" and hparams.token_rnn_num_layers % 2 != 0:
      raise ValueError("For bi, num_layers %d should be even" %
                       hparams.token_rnn_num_layers)
    if hparams.char_rnn_encoder_type == "bi" and hparams.char_rnn_num_layers % 2 != 0:
      raise ValueError("For bi, char_rnn_num_layers %d should be even" %
                       hparams.char_rnn_num_layers)

    # use chars and/or tokens
    if hparams.use_char_rnn or hparams.use_char_cnn:
      hparams.use_chars = True
    if hparams.use_token_rnn or hparams.use_token_cnn:
      hparams.use_tokens = True

    # use ensemble
    if hparams.char_rnn_use_ensemble or hparams.token_rnn_use_ensemble or \
        hparams.char_cnn_use_ensemble or hparams.token_cnn_use_ensemble:
      hparams.use_ensemble = True

    if hparams.use_ensemble:
      if hparams.use_chars:
        if hparams.char_rnn_send_ensemble_softmax_to_classifier and hparams.char_rnn_send_ensemble_logits_to_classifier:
          raise ValueError("You can't send both softmax AND logits from char rnn ensemble model to classifier")
        if hparams.char_cnn_send_ensemble_softmax_to_classifier and hparams.char_cnn_send_ensemble_logits_to_classifier:
          raise ValueError("You can't send both softmax AND logits from char cnn ensemble model to classifier")
        if hparams.token_rnn_send_ensemble_softmax_to_classifier and hparams.token_rnn_send_ensemble_logits_to_classifier:
          raise ValueError("You can't send both softmax AND logits from token rnn ensemble model to classifier")
        if hparams.token_cnn_send_ensemble_softmax_to_classifier and hparams.token_cnn_send_ensemble_logits_to_classifier:
          raise ValueError("You can't send both softmax AND logits from token cnn ensemble model to classifier")

    # Set num_residual_layers
    if hparams.token_rnn_residual and hparams.token_rnn_num_layers > 1:
      if hparams.token_rnn_encoder_type == "gnmt":
        # The first unidirectional layer (after the bi-directional layer) in
        # the GNMT encoder can't have residual connection due to the input is
        # the concatenation of fw_cell and bw_cell's outputs.
        token_rnn_num_residual_layers = hparams.token_rnn_num_layers - 2
      else:
        token_rnn_num_residual_layers = hparams.token_rnn_num_layers - 1
    else:
      token_rnn_num_residual_layers = 0
    hparams.add_hparam("token_rnn_num_residual_layers", token_rnn_num_residual_layers)

    # Set num_residual_layers for character network
    if hparams.char_rnn_residual and hparams.char_rnn_num_layers > 1:
      char_rnn_num_residual_layers = hparams.char_rnn_num_layers - 1
    else:
      char_rnn_num_residual_layers = 0
    hparams.add_hparam("char_rnn_num_residual_layers", char_rnn_num_residual_layers)

    if hparams.use_tokens:
      ## Vocab
      # Get vocab file names first
      if hparams.token_vocab_prefix:
        src_token_vocab_file = hparams.token_vocab_prefix + "." + "txt"  # hparams.src
      else:
        raise ValueError("hparams.token_vocab_prefix must be provided.")

      # Source vocab
      src_token_vocab_size, src_token_vocab_file = vocab_utils.check_and_copy_vocab(
        src_token_vocab_file,
        hparams.out_dir,
        sos=hparams.sos,
        eos=hparams.eos,
        unk=vocab_utils.UNK)

      hparams.add_hparam("src_token_vocab_size", src_token_vocab_size)
      hparams.add_hparam("src_token_vocab_file", src_token_vocab_file)

    if hparams.use_chars:
      ## Vocab chars
      # Get vocab file names first
      if hparams.char_vocab_prefix:
        src_char_vocab_file = hparams.char_vocab_prefix + "." + "txt"  # hparams.src
      else:
        raise ValueError("hparams.char_vocab_prefix must be provided.")

      # Source char vocab
      src_char_vocab_size, src_char_vocab_file = vocab_utils.check_and_copy_vocab(
        src_char_vocab_file,
        hparams.out_dir,
        sos=hparams.sos,
        eos=hparams.eos,
        unk=vocab_utils.UNK)

      hparams.add_hparam("src_char_vocab_size", src_char_vocab_size)
      hparams.add_hparam("src_char_vocab_file", src_char_vocab_file)

    ## Vocab tags
    # Get vocab file names first
    if hparams.tags_vocab_prefix:
      tgt_tags_vocab_file = hparams.tags_vocab_prefix + "." + "txt"
    else:
      raise ValueError("hparams.tags_vocab_prefix must be provided.")

    # Source char vocab
    tgt_tags_vocab_size, tgt_tags_vocab_file = vocab_utils.check_and_copy_vocab(
      tgt_tags_vocab_file,
      hparams.out_dir,
      sos=hparams.sos,
      eos=hparams.eos,
      unk=vocab_utils.UNK)

    hparams.add_hparam("tgt_tags_vocab_size", tgt_tags_vocab_size)
    hparams.add_hparam("tgt_tags_vocab_file", tgt_tags_vocab_file)

    return self.hparams
