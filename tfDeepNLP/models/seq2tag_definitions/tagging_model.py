"""S2C: Sequence-to-classification model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

from tfDeepNLP.models.seq2tag_definitions.s2t_model import S2TModel

__all__ = ["TaggingModel"]


class TaggingModel(S2TModel):
  """Sequence-to-classification dynamic model.

  This class implements a multi-layer recurrent neural network as encoder.
  This class also allows to use GRU cells in addition to LSTM cells with
  support for dropout.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_token_vocab_table,
               source_char_vocab_table,
               target_tags_vocab_table,
               augment_embeddings=False,
               noise_amount_mean=None,
               noise_amount_stddev=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None
               ):
    super(TaggingModel, self).__init__(
      hparams=hparams,
      mode=mode,
      iterator=iterator,
      source_token_vocab_table=source_token_vocab_table,
      source_char_vocab_table=source_char_vocab_table,
      target_tags_vocab_table=target_tags_vocab_table,
      augment_embeddings=augment_embeddings,
      noise_amount_mean=noise_amount_mean,
      noise_amount_stddev=noise_amount_stddev,
      scope=scope,
      single_cell_fn=single_cell_fn,
      model_device_fn=model_device_fn,
      optimizer_op=optimizer_op,
      learning_rate=learning_rate,
      first_gpu_id=first_gpu_id
    )

  def _build_tagger(self, x, hparams=None):
    """
    Finds logits for each word.

    :param x: Input
    :param hparams:
    :return:
    """
    with tf.variable_scope("proj"):
      W = tf.get_variable(
        "W",
        shape=[2 * hparams.num_units, self.ntags],
        dtype=tf.float32)

      b = tf.get_variable(
        "b",
        shape=[self.ntags],
        dtype=tf.float32,
        initializer=tf.zeros_initializer())

      n_time_steps = tf.shape(x)[1]
      output = tf.reshape(x, [-1, 2 * hparams.num_units])
      pred = tf.nn.xw_plus_b(output, W, b)

      logits = tf.reshape(pred, [-1, n_time_steps, self.ntags], name="logits")
      return logits




      # def _build_classifier(self, x, x2=None, hparams=None):
      #   with tf.variable_scope("build_classifier"):
      #
      #     if x is not None:
      #       if x2 is not None:
      #         input_ = tf.concat([x, x2], axis=-1)
      #       else:
      #         input_ = x
      #     elif x2 is not None:
      #       input_ = x2
      #     else:
      #       raise ValueError("Both x and x2 were None. Not input to classifier.")
      #
      #     print("Begin building dense layers")
      #
      #     input_ = tf.nn.tanh(input_)
      #
      #     fully_connected_layer_1 = layers_core.dense(
      #       inputs=input_,
      #       units=hparams.classifier_num_units,
      #       activation=tf.nn.tanh,
      #       use_bias=True,
      #       name="fully_connected_layer_1",
      #     )
      #
      #     dropout_1 = layers_core.dropout(fully_connected_layer_1, hparams.classifier_dropout * 0.5, name="fc_dropout_1")
      #
      #     logits = layers_core.dense(
      #       inputs=dropout_1,
      #       units=hparams.num_classes,
      #       activation=None,
      #       use_bias=True,
      #       name="logits",
      #     )
      #
      #     return logits
      #
      # def _build_softmax_with_loss(self, x, hparams, scope=None):
      #   with cond_scope(scope):
      #
      #     to_dropout = layers_core.dropout(x, hparams.classifier_dropout,
      #                                      # TODO Perhaps this should be specific to the model?
      #                                      name="pre_softmax_dropout")
      #
      #     logits = layers_core.dense(
      #       inputs=to_dropout,
      #       units=hparams.num_classes,
      #       activation=None,
      #       use_bias=True,
      #       name="logits",
      #     )
      #
      #     predictions, probabilities = self.extract_preds_and_probs(logits)
      #
      #     if hparams.num_classes == 2:
      #
      #       # Get only probs for class 2
      #       probabilities = tf.identity(probabilities[:, 1:], name="ensemble_probability")
      #
      #       if self.mode != tf.contrib.learn.ModeKeys.INFER:
      #         ensemble_evaluator = EvaluateMetricsBinaryClassification(self.targets, predictions,
      #                                                                  probabilities)
      #     else:
      #       raise ValueError("s2c_base_model only works with binary classification for now. ")
      #
      #     if self.mode != tf.contrib.learn.ModeKeys.INFER:
      #       loss = self.setup_loss_computation(hparams, logits, self.targets)
      #
      #       return probabilities, logits, loss, ensemble_evaluator
      #     else:
      #       return probabilities, logits, 0.0, None
      #
      # def _build_token_cnn_pipeline(self, x, hparams):
      #   """
      #       Convolution pipeline.
      #
      #       :param x: input tensor with shape - [batch, in_height, in_width, in_channels].
      #         e.g. [batch_size, src_token_max_len, embedding_size_token, 1]
      #       :param hparams: hyperparameters
      #       :return: Flattened tensor [batch_size, token_size]
      #       """
      #
      #   cnn2d = CNN2D(hparams.verbose_tensors)
      #
      #   preset_fn = cnn2d.get_preset_fn(hparams.token_cnn_preset)
      #
      #   output = preset_fn(x, num_token_level_layers=hparams.token_cnn_num_token_level_layers,
      #                      token_level_skip_connections=hparams.token_cnn_token_level_skip_connections)
      #
      #   return cnn2d.flatten(output)
      #
      # def _build_char_cnn_pipeline(self, x, hparams):
      #   """
      #   Convolution pipeline.
      #
      #   :param x: input tensor with shape - [batch, in_depth, in_height, in_width, in_channels].
      #     e.g. [batch_size, src_token_max_len, chars_per_token, chars_embedding_size, 1]
      #   :param hparams: hyperparameters
      #   :return: Flattened tensor [batch_size, token_size]
      #   """
      #
      #   cnn3d = CNN3D(hparams.verbose_tensors)
      #
      #   preset_fn = cnn3d.get_preset_fn(hparams.char_cnn_preset)
      #
      #   output = preset_fn(x, num_token_level_layers=hparams.char_cnn_num_token_level_layers,
      #                      token_level_skip_connections=hparams.char_cnn_token_level_skip_connections)
      #
      #   return cnn3d.flatten(output)
      #
      # def _cnn1d(self, x, n_features, scope=None):
      #   with tf.variable_scope(scope + "/CNN1D" if scope else "CNN1D"):
      #     # Look at entire token embedding - collapses the embedding dimension (2) to 1 element
      #     filter = tf.get_variable('filter', [3, x.get_shape()[-1].value, n_features],
      #                              initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
      #                              dtype=tf.float32)
      #     conv = tf.nn.conv1d(x, filters=filter,
      #                         stride=3,
      #                         padding="VALID", name="convolution")
      #     conv = tf.nn.relu(conv, name="relu")
      #
      #     return tf_print_if(conv, [tf.shape(conv)], summarize=10, message="CNN1D: ",
      #                        verbose=hparams.verbose_tensors)
