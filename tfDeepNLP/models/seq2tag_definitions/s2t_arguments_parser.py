from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser


class S2TArgumentsParser(CommonArgumentsParser):
  """Build S2CArgumentsParser.

    Naming standards:
    If a hparam is specific to token or character models, they start with "token_" or "char_".
      E.g. "token_name"
    If a hparam is specific to the CNN or RNN networks, they prefix the name with "cnn_" or "rnn_".
      E.g. "token_rnn_name"

    """

  def __call__(self, parser):
    parser = super(S2TArgumentsParser, self).__call__(parser)

    # Vocab
    parser.add_argument("--token_vocab_prefix", type=str, default=None, help="""\
          Vocab prefix, expect file with file suffix. If None, extract from
          train files - Not sure auto extraction is supported anymore!\ 
          """)
    parser.add_argument("--char_vocab_prefix", type=str, default=None, help="""\
            Char vocab prefix, expect file with file suffix.
            """)
    parser.add_argument("--tags_vocab_prefix", type=str, default=None, help="""\
                Tags vocab prefix, expect file with file suffix.
                """)

    # network
    parser.add_argument("--classifier_num_units", type=int, default=1024,
                        help="Number of units in dense layer in classifier.")
    parser.add_argument("--token_rnn_num_units", type=int, default=32, help="Number of units in token RNN.")
    parser.add_argument("--char_rnn_num_units", type=int, default=32, help="Number of units in char RNN.")
    parser.add_argument("--token_rnn_num_layers", type=int, default=2,
                        help="Token RNN depth.")
    parser.add_argument("--char_rnn_num_layers", type=int, default=2,
                        help="Character RNN depth.")
    parser.add_argument("--token_rnn_encoder_type", type=str, default="uni", help="""\
          uni | bi | gnmt. For bi, we build token_rnn_num_layers/2 bi-directional layers.For
          gnmt, we build 1 bi-directional layer, and (token_rnn_num_layers - 1) uni-
          directional layers.\
          """)
    parser.add_argument("--token_rnn_unit_type", type=str, default="lstm",
                        help="lstm | gru | layer_norm_lstm")
    parser.add_argument("--char_rnn_unit_type", type=str, default="conv_lstm",
                        help="conv_lstm | lstm | gru | layer_norm_lstm")
    parser.add_argument("--token_rnn_forget_bias", type=float, default=1.0,
                        help="Forget bias for BasicLSTMCell used with tokens.")
    parser.add_argument("--char_rnn_forget_bias", type=float, default=1.0,
                        help="Forget bias for conv_lstm used with characters.")
    parser.add_argument("--classifier_dropout", type=float, default=0.0,
                        help="Dropout rate (not keep_prob) for classifier")
    parser.add_argument("--token_rnn_dropout", type=float, default=0.0,
                        help="Dropout rate (not keep_prob) for token RNN")
    parser.add_argument("--char_rnn_dropout", type=float, default=0.0,
                        help="Dropout rate (not keep_prob) for character RNN. "
                             "Only used when cell is not conv_lstm.")
    parser.add_argument("--classifier_zoneout", type=float, default=0.0,
                        help="Zoneout rate for classifier")
    parser.add_argument("--token_rnn_zoneout", type=float, default=0.0,
                        help="Zoneout rate for token RNN")
    parser.add_argument("--char_rnn_zoneout", type=float, default=0.0,
                        help="Zoneout rate for character RNN. "
                             "Only used when cell is not conv_lstm.")
    parser.add_argument("--token_rnn_norm_gain", type=float, default=0.5,
                        help="Norm gain for certain lstm types. For token RNN.")
    parser.add_argument("--char_rnn_norm_gain", type=float, default=0.5,
                        help="orm gain for certain lstm types. For char RNN.")

    # Design architecture
    parser.add_argument("--use_token_rnn", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to use rnn on token embeddings.")
    parser.add_argument("--use_token_cnn", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to use convolution on token embeddings.")
    parser.add_argument("--use_char_rnn", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to use rnn on char embeddings.")
    parser.add_argument("--use_char_cnn", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to use convolution on char embeddings.")

    parser.add_argument("--use_word_features", type=str, default=True,
                        help="Concatenates word embeddings with word features in token_rnn.")

    parser.add_argument("--send_token_rnn_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the encoder state from char rnn directly to classifier.")
    parser.add_argument("--send_token_cnn_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the output from token cnn directly to classifier.")
    parser.add_argument("--send_char_rnn_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the encoder state from char rnn directly to classifier.")
    parser.add_argument("--send_char_cnn_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the output from char cnn directly to classifier.")

    parser.add_argument("--char_rnn_send_ensemble_softmax_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the ensemble softmax from char rnn to classifier.")
    parser.add_argument("--token_rnn_send_ensemble_softmax_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the ensemble softmax from token rnn to classifier.")
    parser.add_argument("--char_cnn_send_ensemble_softmax_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the ensemble softmax from char cnn to classifier.")
    parser.add_argument("--token_cnn_send_ensemble_softmax_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the ensemble softmax from token cnn to classifier.")
    # Instead of sending softmax normalized, it is also possible to send the non-normalized logits.
    parser.add_argument("--char_rnn_send_ensemble_logits_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the ensemble logits from char rnn to classifier.")
    parser.add_argument("--token_rnn_send_ensemble_logits_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the ensemble logits from token rnn to classifier.")
    parser.add_argument("--char_cnn_send_ensemble_logits_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the ensemble logits from char cnn to classifier.")
    parser.add_argument("--token_cnn_send_ensemble_logits_to_classifier", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the ensemble logits from token cnn to classifier.")

    #
    parser.add_argument("--send_char_rnn_to_token_rnn", type="bool", nargs="?", const=True,
                        default=True,
                        help="Whether to send the output from char rnn to the token rnn.")
    parser.add_argument("--char_rnn_use_ensemble", type="bool", nargs="?", const=False,
                        default=True,
                        help="Whether to include character RNN output in ensemble.")
    parser.add_argument("--token_rnn_use_ensemble", type="bool", nargs="?", const=False,
                        default=True,
                        help="Whether to include token RNN output in ensemble.")
    parser.add_argument("--char_cnn_use_ensemble", type="bool", nargs="?", const=False,
                        default=True,
                        help="Whether to include character CNN output in ensemble.")
    parser.add_argument("--token_cnn_use_ensemble", type="bool", nargs="?", const=False,
                        default=True,
                        help="Whether to include token CNN output in ensemble.")
    parser.add_argument("--ensemble_loss_weight", type=float, default=1.0,
                        help="Weight of summed ensemble loss.")

    parser.add_argument("--char_rnn_encoder_type", type=str, default="uni",
                        help="uni | bi. For bi, we build char_rnn_num_layers/2 bi-directional layers.")
    parser.add_argument("--token_rnn_residual", type="bool", nargs="?", const=True,
                        default=False,
                        help="Whether to add residual connections to token RNN.")
    parser.add_argument("--char_rnn_residual", type="bool", nargs="?", const=True,
                        default=False,
                        help="Whether to add residual connections to character RNN. "
                             "NOTE: When using dynamic RNN with convLSTM, it currently only "
                             "works without skip_connections/residual")

    # Char RNN conv_lstm
    parser.add_argument("--char_rnn_conv_output_channels", type=int, default=32,
                        help="Number of output channels in convolution in conv_lstm.")
    parser.add_argument("--char_rnn_conv_kernel_shape", type=int, nargs='+', default=[5, 5],
                        help="Kernel shape for 2D convolution in conv_lstm. Whitespace-separated list.")

    # Char CNN
    parser.add_argument("--char_cnn_preset", type=str, default="preset_1",
                        help=("Name of CNN preset to use on characters."))
    parser.add_argument("--char_cnn_num_token_level_layers", type=int, default=1,
                        help="Number of token level layers in character CNN.")
    parser.add_argument("--char_cnn_token_level_skip_connections", type=bool, default=False,
                        help="Whether to use add skip connections between token level layers.")

    # Token CNN
    parser.add_argument("--token_cnn_preset", type=str, default="preset_1",
                        help=("Name of CNN preset to use on tokens."))
    parser.add_argument("--token_cnn_num_token_level_layers", type=int, default=1,
                        help="Number of token level layers in token CNN.")
    parser.add_argument("--token_cnn_token_level_skip_connections", type=bool, default=False,
                        help="Whether to use add skip connections between token level layers.")

    # Sequence lengths
    parser.add_argument("--src_token_max_len", type=int, default=None,
                        help="Max length of src sequences during training.")
    parser.add_argument("--src_token_max_len_infer", type=int, default=None,
                        help="Max length of src sequences during inference.")
    parser.add_argument("--char_cnn_tokens_per_sentence", type=int, default=50,
                        help="Fixed number of tokens per sentence in character CNN.")
    parser.add_argument("--char_cnn_chars_per_token", type=int, default=50,
                        # Max length in train set was 192 (prob. a chemical)
                        help="Fixed number of chars per token (padded or reduced to fix). "
                             "Ensures fixed dimensions for convolution.")
    parser.add_argument("--token_cnn_tokens_per_sentence", type=int, default=250,
                        help="Fixed number of tokens per sentence in token CNN.")

    # Embeddings
    parser.add_argument("--augment_embeddings", type=bool, default=False,
                        help="Whether to augment embeddings. Currently only adds normally distributed noise.")
    parser.add_argument("--embedding_noise_amount_mean", type=float, default=0.04,
                        help="Mean amount of noise (percentage) to add to embeddings."
                             "The actual amount is drawn from a truncated normal distribution.")
    parser.add_argument("--embedding_noise_amount_stddev", type=float, default=0.01,
                        help="Standard deviation of the amount of noise (percentage) to add to embeddings."
                             "Truncated to 2 standard deviations.")

    # Should be for specific networks where they are usable ?
    parser.add_argument("--use_peepholes", type=bool, default=False,
                        help="Whether to use peepholes in the selected unit architecture. (Ignored if not supported)"),

    # Misc
    parser.add_argument("--ckpt_last_n_epochs", type=int, default=5,
                        help="Number of epochs to save checkpoints for. From last epoch and backward.")

    return parser
