"""S2T: Basic sequence-to-tag model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc
import collections

import tensorflow as tf

from tfDeepNLP import model_helper
from tfDeepNLP.model_base.base_model import BaseModel
from tfDeepNLP.models.seq2tag_definitions import dataset_loader
from tfDeepNLP.rnn_utils import create_rnn_cell, unpack_encoder_state
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.data_augmentation_utils import embedding_augmentation
from tfDeepNLP.utils.gradient_utils import gradient_clip
from tfDeepNLP.utils.initializer_utils import get_initializer

utils.check_tensorflow_version()

__all__ = ["S2TBaseModel"]


class S2TBaseModel(BaseModel):
  """Sequence-to-classification base class.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_token_vocab_table,
               source_char_vocab_table,
               target_tags_vocab_table,
               augment_embeddings=False,
               noise_amount_mean=None,
               noise_amount_stddev=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None):
    super(S2TBaseModel, self).__init__(hparams,
                                       mode,
                                       iterator,
                                       single_cell_fn,
                                       model_device_fn,
                                       optimizer_op,
                                       learning_rate,
                                       first_gpu_id)

    assert isinstance(iterator, dataset_loader.BatchedInput)

    self.source_token_vocab_table = source_token_vocab_table
    self.source_char_vocab_table = source_char_vocab_table
    self.target_tags_vocab_table = target_tags_vocab_table
    self.augment_embeddings = augment_embeddings if self.mode == tf.contrib.learn.ModeKeys.TRAIN else False
    self.noise_amount_mean = noise_amount_mean
    self.noise_amount_stddev = noise_amount_stddev
    # self.gradient_norm_summary = None  # Initialize

    # Initializer
    initializer = get_initializer(
      hparams.init_op, hparams.random_seed, hparams.init_weight)
    tf.get_variable_scope().set_initializer(initializer)

    self.target_tags = self.iterator.target_tags

    self.batch_size = tf.shape(self.iterator.source_sequence_length)[0]
    self.init_embeddings(hparams, scope)

    ## Train graph
    res = self.build_graph(hparams, scope=scope)

    self.logits = res[0]
    self.loss = res[1]
    self.ensemble_statistics_ = res[2]
    self.word_count = tf.reduce_sum(self.iterator.source_sequence_length)

    self.setup_gradient_computation(hparams)

  def setup_gradient_computation(self, hparams, **kwargs):
    # Gradients and SGD update operation for training the model.
    # Arrange for the embedding vars to appear at the beginning.
    if self.mode == tf.contrib.learn.ModeKeys.TRAIN:

      gradients = self.optimizer_op.compute_gradients(
        self.loss,
        # params,
        colocate_gradients_with_ops=hparams.colocate_gradients_with_ops,
        aggregation_method=tf.AggregationMethod.ADD_N
      )
      print("Computed gradients.")
      if self.clip_gradients:
        gradients, self.params = zip(*gradients)

        print("Unzipped gradients.")

        self.clipped_gradients, self.gradient_norm_summary = gradient_clip(
          gradients, max_gradient_norm=hparams.max_gradient_norm)

        print("Clipped gradients")

        self.gradients = zip(self.clipped_gradients, self.params)

        print("Zipped gradients")

      else:
        self.gradients = gradients
        self.gradient_norm_summary = None

  def init_embeddings(self, hparams, scope):
    """Init embeddings."""

    if hparams.use_tokens:
      self.token_embedding_encoder = (
        model_helper.create_emb_for_encoder_and_decoder(
          share_vocab=False,
          src_vocab_size=hparams.src_token_vocab_size,
          tgt_vocab_size=None,
          src_embed_size=hparams.token_embedding_size,
          tgt_embed_size=None,
          num_partitions=hparams.num_gpus,  # max(hparams.num_worker_hosts, hparams.num_ps_hosts),
          scope=scope + "/tokens" if scope else "tokens",
          decode=False))
    if hparams.use_chars:
      self.char_embedding_encoder = (
        model_helper.create_emb_for_encoder_and_decoder(
          share_vocab=False,
          src_vocab_size=hparams.src_char_vocab_size,
          tgt_vocab_size=None,
          src_embed_size=hparams.char_embedding_size,
          tgt_embed_size=None,
          num_partitions=hparams.num_gpus,  # max(hparams.num_worker_hosts, hparams.num_ps_hosts),
          scope=scope + "/chars" if scope else "chars",
          decode=False))

  def statistics(self):
    if self.mode != tf.contrib.learn.ModeKeys.INFER:
      return (
        self.loss,
        self.evaluator.accuracy,
        self.evaluator.f1,
        self.evaluator.auc,
        self.evaluator.precision,
        self.evaluator.recall,
        self.evaluator.specificity,
        self.evaluator.class_ratio,

        # self.predict_count, # TODO Add in again
        self.word_count,
        self.batch_size,
        # self.gradient_norm_summary
      )
    else:
      print("Statistics does nothing in infer mode.")
      return tf.no_op()

  @property
  def ensemble_statistics(self):
    return self.ensemble_statistics_

  def get_classifications(self):
    return (self.predictions, self.probabilities)

  def build_graph(self, hparams, scope=None, **kwargs):
    """
    Creates a sequence-to-class model with dynamic RNN.
    Args:
      hparams: Hyperparameter configurations.
      scope: VariableScope for the created subgraph; default "dynamic_seq2class".

    Returns:
      A tuple of the form (logits, loss, final_context_state),
      where:
        logits: float32 Tensor [batch_size x num_decoder_symbols].
        loss: the total loss / batch_size.
        final_context_state: The final state of decoder RNN.

    Raises:
      ValueError: if encoder_type differs from mono and bi, or
        attention_option is not (luong | scaled_luong |
        bahdanau | normed_bahdanau).
    """
    utils.print_out("# creating %s graph ..." % self.mode)
    # dtype = tf.float32
    # num_layers = hparams.num_layers

    with utils.cond_scope(scope):  # only set variable scope if scope not None
      with tf.name_scope("dynamic_seq2class"):  # scope or

        states_to_classifier = []
        outputs_to_token_rnn = []
        ensemble_softmaxes = []
        ensemble_logits = []
        ensemble_loss = 0.0
        ensemble_statistics = []

        """ RNNs """
        if hparams.use_char_rnn:
          char_rnn_output, char_rnn_h_state = self._build_char_rnn(hparams)

          if hparams.send_char_rnn_to_classifier:
            states_to_classifier.append(char_rnn_h_state)

          if hparams.send_char_rnn_to_token_rnn:
            outputs_to_token_rnn.append(char_rnn_output)

          if hparams.char_rnn_use_ensemble:
            char_rnn_softmax, char_rnn_logits, char_rnn_loss, char_rnn_evaluator = self._build_softmax_with_loss(
              char_rnn_h_state,
              hparams,
              scope="char_rnn")
            ensemble_loss += char_rnn_loss
            if hparams.char_rnn_send_ensemble_softmax_to_classifier:
              ensemble_softmaxes.append(char_rnn_softmax)
            if hparams.char_rnn_send_ensemble_logits_to_classifier:
              ensemble_logits.append(char_rnn_logits)

            ensemble_statistics.append(self.extract_ensemble_statistics("char_rnn", char_rnn_evaluator, char_rnn_loss))

        if hparams.use_token_rnn:
          # RNN
          token_rnn_output, token_rnn_state = self._build_token_rnn(hparams, outputs_to_token_rnn)

          if hparams.send_token_rnn_to_classifier:
            states_to_classifier.append(token_rnn_state)

          if hparams.token_rnn_use_ensemble:
            token_rnn_softmax, token_rnn_logits, token_rnn_loss, token_rnn_evaluator = self._build_softmax_with_loss(
              token_rnn_state,
              hparams,
              scope="token_rnn")
            ensemble_loss += token_rnn_loss
            if hparams.token_rnn_send_ensemble_softmax_to_classifier:
              ensemble_softmaxes.append(token_rnn_softmax)
            if hparams.token_rnn_send_ensemble_logits_to_classifier:
              ensemble_logits.append(token_rnn_logits)
            ensemble_statistics.append(
              self.extract_ensemble_statistics("token_rnn", token_rnn_evaluator, token_rnn_loss))

        """ CNNs """
        if hparams.use_char_cnn:
          char_cnn_state = self._build_char_cnn(hparams)

          if hparams.send_char_cnn_to_classifier:
            states_to_classifier.append(char_cnn_state)

          if hparams.char_cnn_use_ensemble:
            char_cnn_softmax, char_cnn_logits, char_cnn_loss, char_cnn_evaluator = self._build_softmax_with_loss(
              char_cnn_state,
              hparams,
              scope="char_cnn")
            ensemble_loss += char_cnn_loss
            if hparams.char_cnn_send_ensemble_softmax_to_classifier:
              ensemble_softmaxes.append(char_cnn_softmax)
            if hparams.char_cnn_send_ensemble_logits_to_classifier:
              ensemble_logits.append(char_cnn_logits)
            ensemble_statistics.append(self.extract_ensemble_statistics("char_cnn", char_cnn_evaluator, char_cnn_loss))

        if hparams.use_token_cnn:
          token_cnn_state = self._build_token_cnn(hparams)

          if hparams.send_token_cnn_to_classifier:
            states_to_classifier.append(token_cnn_state)

          if hparams.token_cnn_use_ensemble:
            token_cnn_softmax, token_cnn_logits, token_cnn_loss, token_cnn_evaluator = self._build_softmax_with_loss(
              token_cnn_state,
              hparams,
              scope="token_cnn")
            ensemble_loss += token_cnn_loss
            if hparams.token_cnn_send_ensemble_softmax_to_classifier:
              ensemble_softmaxes.append(token_cnn_softmax)
            if hparams.token_cnn_send_ensemble_logits_to_classifier:
              ensemble_logits.append(token_cnn_logits)
            ensemble_statistics.append(self.extract_ensemble_statistics("token_cnn", token_cnn_evaluator,
                                                                        token_cnn_loss))

        """ To dense """
        if len(states_to_classifier) == 0 and len(ensemble_softmaxes) == 0 and len(ensemble_logits) == 0:
          raise ValueError("states_to_classifier and ensemble softmaxes/logits were empty. No input to dense network.")
        if len(states_to_classifier) > 0:
          final_encoder_state = tf.concat(states_to_classifier, axis=1)
        else:
          final_encoder_state = None
        if len(ensemble_softmaxes) > 0:
          ensemble_to_classifier = tf.concat(ensemble_softmaxes, axis=1)
        elif len(ensemble_logits) > 0:
          ensemble_to_classifier = tf.concat(ensemble_logits, axis=1)
        else:
          ensemble_to_classifier = None

        # Classification # FIXME needs output, not final state - I think, find out
        logits = self._build_tagger(final_encoder_state, ensemble_to_classifier, hparams)

        def inference_wrapper():

          loss = None
          log_likelihood = None
          transition_params = None
          labels_predicted = None  # TODO Find predicted from crf method?

          if hparams.use_crf_loss:  # TODO called crf but fix the hparam
            log_likelihood, transition_params = self._do_crf_inference(self.logits, self.targets)
            loss = self.setup_loss_computation(hparams=hparams, logits=logits, targets=self.targets,
                                               log_likelihood=log_likelihood)
          else:
            labels_predicted = self._do_argmax_inference(self.logits)

          if self.mode != tf.contrib.learn.ModeKeys.INFER:
            loss = self.setup_loss_computation(hparams=hparams, logits=logits, targets=self.targets,
                                               log_likelihood=log_likelihood)

          return loss, labels_predicted, transition_params, log_likelihood

        # Only place on GPU if self.first_gpu_id is not None.

        if self.first_gpu_id:
          with tf.device(tf.DeviceSpec(device_type="GPU", device_index=self.first_gpu_id)):
            loss, labels_predicted, transition_params, log_likelihood = inference_wrapper()
        else:
          loss, labels_predicted, transition_params, log_likelihood = inference_wrapper()

        print("build_graph done")
        return logits, loss + (ensemble_loss * hparams.ensemble_loss_weight), ensemble_statistics

  def _build_token_rnn(self, hparams, outputs_to_token_rnn):
    with tf.variable_scope("build_token_rnn"):
      if outputs_to_token_rnn:
        if len(outputs_to_token_rnn) == 1:
          additional_embeddings = outputs_to_token_rnn[0]
        else:
          additional_embeddings = tf.concat(outputs_to_token_rnn, axis=1)
        encoder_outputs, encoder_state = self._build_token_encoder(hparams, additional_embeddings=additional_embeddings)
      else:
        encoder_outputs, encoder_state = self._build_token_encoder(hparams)
      encoder_h_state = unpack_encoder_state(encoder_state, cell_type=hparams.token_rnn_unit_type,
                                             encoder_type=hparams.token_rnn_encoder_type)
      return encoder_outputs, encoder_h_state

  def _build_char_rnn(self, hparams):
    with tf.variable_scope("build_char_rnn"):
      encoder_outputs_char, encoder_state_char = self._build_char_encoder(hparams)

      # Char h state
      h_state = unpack_encoder_state(encoder_state_char, cell_type=hparams.char_rnn_unit_type,
                                     encoder_type=hparams.char_rnn_encoder_type)
      h_state_shape = tf.shape(h_state)

      h_state = tf.reshape(h_state, shape=[h_state_shape[0],
                                           hparams.char_cnn_chars_per_token * hparams.char_embedding_size
                                           * (2 if hparams.char_rnn_encoder_type == "bi" else 1)])

      # Char output
      if self.time_major:
        encoder_outputs_char = tf.transpose(tf.squeeze(encoder_outputs_char), perm=[1, 0, 2, 3])
      h_outputs_shape = tf.shape(encoder_outputs_char)
      h_outputs = tf.reshape(encoder_outputs_char, shape=[h_outputs_shape[0], h_outputs_shape[1],
                                                          hparams.char_cnn_chars_per_token * hparams.char_embedding_size])
      return h_outputs, h_state

  def _build_token_cnn(self, hparams):
    with tf.variable_scope("build_token_cnn"):
      source_tokens_fixed = self.iterator.source_tokens_fixed
      # Inform the model of the actual shapes. Not actually reshaping the input.
      source_tokens_fixed = tf.reshape(source_tokens_fixed,
                                       [-1, hparams.token_cnn_tokens_per_sentence])
      source_tokens_fixed_emb_inp = tf.nn.embedding_lookup(
        self.token_embedding_encoder, source_tokens_fixed)

      if self.augment_embeddings:
        source_tokens_fixed_emb_inp = embedding_augmentation(
          source_tokens_fixed_emb_inp,
          noise_amount_mean=self.noise_amount_mean,
          noise_amount_stddev=self.noise_amount_stddev)

      source_tokens_fixed_emb_inp = tf.expand_dims(source_tokens_fixed_emb_inp, -1)
      cnn_state = self._build_token_cnn_pipeline(source_tokens_fixed_emb_inp, hparams)
      return cnn_state

  def _build_char_cnn(self, hparams):
    with tf.variable_scope("build_character_cnn"):
      source_chars_fixed = self.iterator.source_chars_fixed
      # Inform the model of the actual shapes. Not actually reshaping the input.
      source_chars_fixed = tf.reshape(source_chars_fixed,
                                      [-1, hparams.char_cnn_tokens_per_sentence, hparams.char_cnn_chars_per_token])
      source_chars_fixed_emb_inp = tf.nn.embedding_lookup(
        self.char_embedding_encoder, source_chars_fixed)

      if self.augment_embeddings:
        source_chars_fixed_emb_inp = embedding_augmentation(source_chars_fixed_emb_inp,
                                                            noise_amount_mean=self.noise_amount_mean,
                                                            noise_amount_stddev=self.noise_amount_stddev)

      source_chars_fixed_emb_inp = tf.expand_dims(source_chars_fixed_emb_inp, -1)
      cnn_state = self._build_char_cnn_pipeline(source_chars_fixed_emb_inp, hparams)
      return cnn_state

  @abc.abstractmethod
  def _build_token_encoder(self, hparams, additional_embeddings=None):
    """Subclass must implement this.

    Build and run an RNN encoder.

    Args:
      hparams: Hyperparameters configurations.

    Returns:
      A tuple of encoder_outputs and encoder_state.
    """
    pass

  @abc.abstractmethod
  def _build_char_encoder(self, hparams):
    """Subclass must implement this.

    Build and run an RNN encoder.

    Args:
      hparams: Hyperparameters configurations.

    Returns:
      A tuple of encoder_outputs and encoder_state.
    """
    pass

  @abc.abstractmethod
  def _build_softmax_with_loss(self, x, hparams, scope):
    """Subclass must implement this.

        Build a softmax classifier with loss computation and evaluation metrics.

        Args:
          x: input tensor
          hparams: Hyperparameters configurations.
          scope: Name of model. Str.

        Returns:
          softmax classification with num_classes neurons, loss, evaluator
        """
    pass

  def _build_encoder_cell_token(self, hparams, num_layers, num_residual_layers):
    """Build a multi-layer RNN cell that can be used by encoder."""

    return create_rnn_cell(
      unit_type=hparams.token_rnn_unit_type,
      num_units=hparams.token_rnn_num_units,
      num_layers=num_layers,
      num_residual_layers=num_residual_layers,
      forget_bias=hparams.token_rnn_forget_bias,
      dropout=hparams.token_rnn_dropout,
      zoneout=hparams.token_rnn_zoneout,
      norm_gain=hparams.token_rnn_norm_gain,
      mode=self.mode,
      single_cell_fn=self.single_cell_fn,
      model_device_fn=self.model_device_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes
    )

  def _build_encoder_cell_char(self, hparams, num_layers, num_residual_layers):
    """Build a multi-layer RNN cell that can be used by encoder."""

    return create_rnn_cell(
      unit_type=hparams.char_rnn_unit_type,
      num_units=None if hparams.char_rnn_unit_type == "conv_lstm" else hparams.char_rnn_num_units,
      num_layers=num_layers,
      num_residual_layers=num_residual_layers,
      forget_bias=hparams.char_rnn_forget_bias,
      dropout=0.0 if hparams.char_rnn_unit_type == "conv_lstm" else hparams.char_rnn_dropout,
      zoneout=0.0 if hparams.char_rnn_unit_type == "conv_lstm" else hparams.char_rnn_zoneout,
      norm_gain=hparams.char_rnn_norm_gain,
      mode=self.mode,
      single_cell_fn=None,
      model_device_fn=self.model_device_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes,
      chars_per_token=hparams.char_cnn_chars_per_token,
      conv_output_channels=hparams.char_rnn_conv_output_channels,
      char_embedding_size=hparams.char_embedding_size,
      conv_kernel_shape=hparams.char_rnn_conv_kernel_shape
    )

  @abc.abstractmethod
  def _build_char_cnn_pipeline(self, x, hparams):
    """Subclass must implement this.

        Build convolution pipeline on fixed size chars or tokens.

        Args:
          x: Input.
          hparams: Hyperparameters configuration.

        Returns:
          Flattened to [batch_size, n_tokens] or similar.
        """

  @abc.abstractmethod
  def _build_token_cnn_pipeline(self, x, hparams):
    """Subclass must implement this.

        Build convolution pipeline on fixed size chars or tokens.

        Args:
          x: Input.
          hparams: Hyperparameters configuration.

        Returns:
          Flattened to [batch_size, n_tokens] or similar.
        """

  @abc.abstractmethod
  def _build_classifier(self, x, x2=None, hparams=None):
    """Subclass must implement this.

    Build dense layer from RNN output state.

    Args:
      x: Input, i.e. unpacked encoder h state.
      x2: Second input, i.e. the ensemble softmaxes
      hparams: Hyperparameters configuration.

    Returns:
      Logits
    """

  def get_max_time(self, tensor):
    time_axis = 0 if self.time_major else 1
    return tensor.shape[time_axis].value or tf.shape(tensor)[time_axis]

  def setup_loss_computation(self, logits=None, targets=None, log_likelihood=None, hparams=None):

    with tf.variable_scope("loss"):

      if hparams.crf:
        if log_likelihood is None:
          raise ValueError("log_likelihood cannot be None when calculating loss from CRF.")

        loss = tf.reduce_mean(-log_likelihood)

      else:
        losses = tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits, labels=targets)
        mask = tf.sequence_mask(self.sequence_lengths)
        losses = tf.boolean_mask(losses, mask)
        loss = tf.reduce_mean(losses)

      #tf.summary.scalar("loss", loss)

    return loss

  def extract_ensemble_statistics(self, model_name, evaluator, loss):

    return EnsembleStatistics(model_name=model_name,
                              loss=loss,
                              accuracy=evaluator.accuracy,
                              f1=evaluator.f1,
                              AUC=evaluator.auc,
                              precision=evaluator.precision,
                              recall=evaluator.recall,
                              specificity=evaluator.specificity
                              )


class EnsembleStatistics(collections.namedtuple("EnsembleStatistics",
                                                ("model_name",
                                                 "loss",
                                                 "accuracy",
                                                 "f1",
                                                 "AUC",
                                                 "precision",
                                                 "recall",
                                                 "specificity",
                                                 ))):
  pass
