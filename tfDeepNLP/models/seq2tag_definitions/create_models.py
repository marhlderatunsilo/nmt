"""S2C: Create sequence-to-class models."""

import collections

__all__ = ["create_model"]


class Model(
  collections.namedtuple("Model", ("model", "iterator",
                                   ))):
  pass


def create_model(
    model_creator, hparams, source_token_vocab_table, source_char_vocab_table, augment_embeddings=None,
    noise_amount_mean=None, noise_amount_stddev=None, iterator=None,
    scope=None, single_cell_fn=None, optimizer_op=None, learning_rate=None, first_gpu_id=None, mode=None):
  """Create model"""

  model = model_creator(
    hparams,
    iterator=iterator,
    mode=mode,
    source_token_vocab_table=source_token_vocab_table,
    source_char_vocab_table=source_char_vocab_table,
    augment_embeddings=augment_embeddings,
    noise_amount_mean=noise_amount_mean,
    noise_amount_stddev=noise_amount_stddev,
    scope=scope,
    single_cell_fn=single_cell_fn,
    optimizer_op=optimizer_op,
    learning_rate=learning_rate,
    first_gpu_id=first_gpu_id
  )

  return Model(
    model=model,
    iterator=iterator)
