import tensorflow as tf
from tfDeepNLP.models.seq2seq.dataset_iterator.iterator_utils import BatchedInput, \
  create_hierarchical_token_split_counter, \
  create_first_hierarchy_level_sequence_clipper
from tfDeepNLP.dataset_iterator_methods import DatasetIteratorMethods
from tfDeepNLP.models.seq2seq.hierarchical.util import create_ids_to_pointer_ids_mapper
from tfDeepNLP.utils import vocab_utils
from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser

__all__ = ["get_iterator"]


def get_base_dataset(src_dataset,
                     tgt_dataset,
                     src_vocab_table,
                     tgt_vocab_table,
                     source_reverse,
                     random_seed,
                     src_max_len,
                     tgt_max_len,
                     num_threads,
                     output_buffer_size,
                     skip_count,
                     tgt_sos_id,
                     tgt_eos_id,
                     unk_id,
                     projection_vocab_size,
                     hierarchical_split_tokens,
                     pointer_generation,
                     max_first_hierarchy_level_sequence_length
                     ):
  src_tgt_dataset = tf.data.Dataset.zip((src_dataset, tgt_dataset))

  is_hierarchical = hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE

  # if num_shards > 0:
  #   src_tgt_dataset = src_tgt_dataset.shard(num_shards, shard_index)

  if skip_count is not None:
    src_tgt_dataset = src_tgt_dataset.skip(skip_count)

  src_tgt_dataset = src_tgt_dataset.apply(tf.contrib.data.shuffle_and_repeat(output_buffer_size, seed=random_seed))

  # Cut down long lowest level sequences in the hierarchy
  if is_hierarchical and max_first_hierarchy_level_sequence_length is not None:
    src_tgt_dataset = src_tgt_dataset.map(
      create_first_hierarchy_level_sequence_clipper(hierarchical_split_tokens,
                                                    max_first_hierarchy_level_sequence_length,
                                                    src_max_len),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )

  def split_sequence_into_tokens(src, tgt):

    src_split = tf.string_split([src], skip_empty=True).values
    tgt_split = tf.string_split([tgt], skip_empty=True).values

    if is_hierarchical or pointer_generation:
      # We will need the raw sequence (string) input for further processing in case of hierarchical model
      return src_split, tgt_split, src_split, tgt_split
    else:
      return src_split, tgt_split

  src_tgt_dataset = src_tgt_dataset.map(
    split_sequence_into_tokens,
    num_parallel_calls=num_threads,
    # output_buffer_size=output_buffer_size
  )

  # Filter zero length input sequences.
  src_tgt_dataset = src_tgt_dataset.filter(DatasetIteratorMethods.is_zero_length_sequence)

  if src_max_len:
    src_tgt_dataset = src_tgt_dataset.map(
      lambda src, tgt, *unused: (src[:src_max_len], tgt, *unused),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )
  if tgt_max_len:
    src_tgt_dataset = src_tgt_dataset.map(
      lambda src, tgt, *unused: (src, tgt[:tgt_max_len], *unused),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )
  if source_reverse:
    src_tgt_dataset = src_tgt_dataset.map(
      lambda src, tgt, *unused: (tf.reverse(src, axis=[0]), tgt, *unused),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )
  # Convert the word strings to ids.  Word strings that are not in the
  # vocab get the lookup table's default_value integer.
  src_tgt_dataset = src_tgt_dataset.map(
    lambda src, tgt, *unused: (tf.cast(src_vocab_table.lookup(src), tf.int32),
                               tf.cast(tgt_vocab_table.lookup(tgt), tf.int32), *unused),
    num_parallel_calls=num_threads,
    # output_buffer_size=output_buffer_size
  )

  if pointer_generation:
    src_tgt_dataset = src_tgt_dataset.map(
      create_ids_to_pointer_ids_mapper(projection_vocab_size, tgt_vocab_table.size(), unk_id),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )
  elif projection_vocab_size: # Else cut target ids to projection_vocab_size

    def cut_to_projection_vocab_size(src, tgt, *unused):
      tgt_in = tgt
      tgt_out = tf.where(tgt >= projection_vocab_size, tf.fill(tf.shape(tgt), unk_id), tgt)
      return (src, tgt_in, tgt_out, *unused)

    src_tgt_dataset = src_tgt_dataset.map(
      cut_to_projection_vocab_size,
      #create_ids_to_pointer_ids_mapper(projection_vocab_size, unk_id),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )
  else:
    src_tgt_dataset = src_tgt_dataset.map(
      lambda src, tgt, *unused: (src, tgt, tgt, *unused),
      # create_ids_to_pointer_ids_mapper(projection_vocab_size, unk_id),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )

  # Create a tgt_input prefixed with <sos> and a tgt_output suffixed with <eos>.
  src_tgt_dataset = src_tgt_dataset.map(
    lambda src, tgt_inp, tgt_out, *unused: (src,
                               tf.concat(([tgt_sos_id], tgt_inp), 0),
                               tf.concat((tgt_out, [tgt_eos_id]), 0), *unused),
    num_parallel_calls=num_threads,
    # output_buffer_size=output_buffer_size
  )

  return src_tgt_dataset


def add_lengths_to_dataset(dataset, num_threads,
                           hierarchical_split_tokens=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE):
  # Add in sequence lengths.
  if hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE:

    print("DOING HIERARCHICAL SPLIT!")

    hierarchical_token_split_counter = create_hierarchical_token_split_counter(hierarchical_split_tokens)
    # We need to reorder to arguments to fit with hierarchical_token_splitter
    # Its a bit of a hack in order to support create_hierarchical_token_splitter being used elsewhere
    src_tgt_dataset = dataset.map(
      lambda src, tgt_in, tgt_out, raw_src: (src, raw_src, tgt_in, tgt_out),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )

    src_tgt_dataset = src_tgt_dataset.map(hierarchical_token_split_counter,
                                          num_parallel_calls=num_threads)

  else:
    src_tgt_dataset = dataset.map(
      lambda src, tgt_in, tgt_out, *unused: (src, tgt_in, tgt_out, tf.size(src), tf.size(tgt_in)),
      num_parallel_calls=num_threads,
      # output_buffer_size=output_buffer_size
    )

  return src_tgt_dataset


def bucket_batch_dataset(src_tgt_dataset, batch_size, num_buckets, src_max_len, num_splits, src_eos_id, tgt_eos_id,
                         hierarchical_split_tokens=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE):
  is_hierarchical = hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE

  # Bucket by source sequence length (buckets for lengths 0-9, 10-19, ...)
  def batching_func(x):

    print("hierarchical_split_tokens: ", hierarchical_split_tokens)

    return x.padded_batch(
      batch_size,
      # The first three entries are the source and target line rows;
      # these have unknown-length vectors.  The last two entries are
      # the source and target row sizes; these are scalars.
      padded_shapes=(
        tf.TensorShape([None]),  # src
        tf.TensorShape([None]),  # tgt_input
        tf.TensorShape([None]),  # tgt_output
        tf.TensorShape([None, None] if is_hierarchical else []),  # src_len
        tf.TensorShape([])),  # tgt_len
      # Pad the source and target sequences with eos tokens.
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        src_eos_id,  # src
        tgt_eos_id,  # tgt_input
        tgt_eos_id,  # tgt_output
        0,  # src_len -- unused
        0))  # tgt_len -- unused

  def split_batching_func(x):

    print("hierarchical_split_tokens: ", hierarchical_split_tokens)

    return x.padded_batch(
      num_splits,
      # The first three entries are the source and target line rows;
      # these have unknown-length vectors.  The last two entries are
      # the source and target row sizes; these are scalars.
      padded_shapes=(
        tf.TensorShape([None, None]),  # src
        tf.TensorShape([None, None]),  # tgt_input
        tf.TensorShape([None, None]),  # tgt_output
        tf.TensorShape([None, None, None] if is_hierarchical else [None]),  # src_len
        tf.TensorShape([None])),  # tgt_len
      # Pad the source and target sequences with eos tokens.
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        src_eos_id,  # src
        tgt_eos_id,  # tgt_input
        tgt_eos_id,  # tgt_output
        0,  # src_len -- unused
        0))  # tgt_len -- unused

  def key_func(unused_1, unused_2, unused_3, src_len, tgt_len):
    # Calculate bucket_width by maximum source sequence length.
    # Pairs with length [0, bucket_width) go to bucket 0, length
    # [bucket_width, 2 * bucket_width) go to bucket 1, etc.  Pairs with length
    # over ((num_bucket-1) * bucket_width) words all go into the last bucket.
    if src_max_len:
      bucket_width = (src_max_len + num_buckets - 1) // num_buckets
    else:
      bucket_width = 10

    # Bucket sentence pairs by the length of their source sentence and target
    # sentence.

    # If we run hierarchical we need to select the correct lengths
    if is_hierarchical:
      # Reduce the individual lengths of each subsequence of the first hierarchy level
      src_len = tf.reduce_sum(src_len[0])

    bucket_id = tf.maximum(src_len // bucket_width, tgt_len // bucket_width)
    return tf.to_int64(tf.minimum(num_buckets, bucket_id))

  def reduce_func(unused_key, windowed_data):
    return batching_func(windowed_data)

  def key_func_split(unused_1, unused_2, unused_3, src_len, tgt_len):

    if src_max_len and num_buckets > 1:
      bucket_width = (src_max_len * batch_size + num_buckets - 1) // num_buckets
    else:
      bucket_width = 10 * batch_size

    if is_hierarchical:
      print("iterator split, src_len1: ", src_len)
      # Reduce the individual lengths of each subsequence of the first hierarchy level in the first batch
      src_len = tf.reduce_sum(src_len[0][0])
      print("iterator split, src_len2: ", src_len)

    # Bucket sentence pairs by the length of their source sentence and target
    # sentence.
    bucket_id = tf.maximum(tf.reduce_sum(src_len) // bucket_width, tf.reduce_sum(tgt_len) // bucket_width)
    return tf.to_int64(tf.minimum(num_buckets, bucket_id))

  def reduce_func_split(unused_key, windowed_data):
    return split_batching_func(windowed_data)

  if num_buckets > 1:

    batched_dataset = src_tgt_dataset.apply(
      tf.contrib.data.group_by_window(
        key_func=key_func, reduce_func=reduce_func, window_size=batch_size))

  else:
    batched_dataset = batching_func(src_tgt_dataset)

  if num_splits:
    batched_dataset = batched_dataset.apply(
      tf.contrib.data.group_by_window(
        key_func=key_func_split, reduce_func=reduce_func_split, window_size=num_splits))

  return batched_dataset


def get_iterator(src_dataset,
                 tgt_dataset,
                 src_vocab_table,
                 tgt_vocab_table,
                 batch_size,
                 sos,
                 eos,
                 source_reverse,
                 random_seed,
                 num_buckets,
                 src_max_len=None,
                 tgt_max_len=None,
                 num_threads=32,
                 output_buffer_size=None,
                 skip_count=None,
                 num_splits=None,
                 num_shards=0,
                 shard_index=0,
                 projection_vocab_size=None,
                 hierarchical_split_tokens=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE,
                 pointer_generation=False,
                 max_first_hierarchy_level_sequence_length=None):
  if not output_buffer_size:
    output_buffer_size = batch_size * 50
  src_eos_id = tf.cast(src_vocab_table.lookup(tf.constant(eos)), tf.int32)
  tgt_sos_id = tf.cast(tgt_vocab_table.lookup(tf.constant(sos)), tf.int32)
  tgt_eos_id = tf.cast(tgt_vocab_table.lookup(tf.constant(eos)), tf.int32)
  unk_id = tf.cast(tgt_vocab_table.lookup(tf.constant(vocab_utils.UNK)), tf.int32)

  is_hierarchical = hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE

  base_dataset = get_base_dataset(src_dataset,
                                  tgt_dataset,
                                  src_vocab_table,
                                  tgt_vocab_table,
                                  source_reverse,
                                  random_seed,
                                  src_max_len,
                                  tgt_max_len,
                                  num_threads,
                                  output_buffer_size,
                                  skip_count,
                                  tgt_sos_id,
                                  tgt_eos_id,
                                  unk_id,
                                  projection_vocab_size,
                                  hierarchical_split_tokens,
                                  pointer_generation,
                                  max_first_hierarchy_level_sequence_length)

  base_dataset_with_lengths = add_lengths_to_dataset(base_dataset, num_threads, hierarchical_split_tokens)

  batched_dataset = bucket_batch_dataset(base_dataset_with_lengths, batch_size, num_buckets, src_max_len, num_splits,
                                         src_eos_id,
                                         tgt_eos_id, hierarchical_split_tokens=hierarchical_split_tokens)

  if num_splits == 1:
    # batched_dataset = batched_dataset.apply(tf.contrib.data.prefetch_to_device(
    #   "/device:GPU:0",
    #   buffer_size=4
    # ))
    batched_dataset = batched_dataset.prefetch(4)

  batched_iter = batched_dataset.make_initializable_iterator()

  (src_ids, tgt_input_ids, tgt_output_ids, src_seq_len,
   tgt_seq_len) = (batched_iter.get_next())

  return BatchedInput(
    initializer=batched_iter.initializer,
    source=src_ids,
    source_raw=None,
    target_input=tgt_input_ids,
    target_output=tgt_output_ids,
    source_sequence_length=src_seq_len,
    target_sequence_length=tgt_seq_len)
