# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""S2S: For loading data into NMT models."""
from __future__ import print_function

import tensorflow as tf

from tfDeepNLP.utils import vocab_utils
from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser
from tfDeepNLP.utils.string_utils import split_token_string_by_token

__all__ = [
           #"BatchedInput",
           #"SplitBatchedInput",
           "get_infer_iterator",
           "split_batched_input",
           "create_hierarchical_token_split_counter"]


# NOTE(ebrevdo): When we subclass this, instances' __dict__ becomes empty.
# class BatchedInput(collections.namedtuple("BatchedInput",
#                                           ("initializer",
#                                            "source",
#                                            "target_input",
#                                            "target_output",
#                                            "source_sequence_length",
#                                            "target_sequence_length",
#                                            "source_raw"))):
#   pass
#
#
# class SplitBatchedInput(collections.namedtuple("SplitBatchedInput",
#                                                ("source",
#                                                 "target_input",
#                                                 "target_output",
#                                                 "source_sequence_length",
#                                                 "target_sequence_length"))):
#   pass


def get_infer_iterator(src_dataset,
                       src_vocab_table,
                       batch_size,
                       source_reverse,
                       eos,
                       src_max_len=None,
                       num_threads=32,
                       hierarchical_split_tokens=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE,
                       max_first_hierarchy_level_sequence_length=None):
  src_eos_id = tf.cast(src_vocab_table.lookup(tf.constant(eos)), tf.int32)

  is_hierarchical = hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE

  # Cut down long lowest level sequences in the hierarchy
  if is_hierarchical and max_first_hierarchy_level_sequence_length is not None:
    src_dataset = src_dataset.map(
      create_first_hierarchy_level_sequence_clipper(hierarchical_split_tokens,
                                                    max_first_hierarchy_level_sequence_length,
                                                    src_max_len,
                                                    ),
      num_parallel_calls=num_threads
    )

  def split_src(src):
    split_src = tf.string_split([src], skip_empty=True).values
    return (split_src, split_src)

  src_dataset = src_dataset.map(split_src, num_parallel_calls=num_threads)

  if src_max_len:
    src_dataset = src_dataset.map(lambda src, *unused: (src[:src_max_len], *unused),
                                  num_parallel_calls=num_threads)
  # Convert the word strings to ids
  src_dataset = src_dataset.map(
    lambda split_src, *unused: (tf.cast(src_vocab_table.lookup(split_src), tf.int32), *unused),
    num_parallel_calls=num_threads)
  if source_reverse:
    src_dataset = src_dataset.map(lambda src, *unused: (tf.reverse(src, axis=[0]), *unused),
                                  num_parallel_calls=num_threads)

  # Add in the word counts.
  if is_hierarchical:

    hierarchical_token_splitter = create_hierarchical_token_split_counter(hierarchical_split_tokens)

    print("Did hierarchical token split (inference)!")

    src_dataset = src_dataset.map(hierarchical_token_splitter,
                                  num_parallel_calls=num_threads)
  else:
    src_dataset = src_dataset.map(lambda src, *unused: (src, tf.size(src), *unused),
                                  num_parallel_calls=num_threads)

  def batching_func(x):

    return x.padded_batch(
      batch_size,
      # The entry is the source line rows;
      # this has unknown-length vectors.  The last entry is
      # the source row size; this is a scalar.
      padded_shapes=(
        tf.TensorShape([None]),  # src
        tf.TensorShape([None, None] if is_hierarchical else []),  # src_len
        tf.TensorShape([None])),
      # Pad the source sequences with eos tokens.
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        src_eos_id,  # src
        0,
        vocab_utils.UNK))  # src_len -- unused

  batched_dataset = batching_func(src_dataset)
  batched_iter = batched_dataset.make_initializable_iterator()
  (src_ids, src_seq_len, source_raw) = batched_iter.get_next()
  # return BatchedInput(
  #   initializer=batched_iter.initializer,
  #   source=src_ids,
  #   source_raw=source_raw,
  #   target_input=None,
  #   target_output=None,
  #   source_sequence_length=src_seq_len,
  #   target_sequence_length=None)

  features = {"source": src_ids,
              "source_raw": source_raw,
              "source_sequence_length":src_seq_len}

  labels = {
  }

  return batched_iter.initializer, features, labels

# def split_batched_input(batched_input, num_splits, is_hierarchical=False):
#   sources, target_inputs, target_outputs, source_sequence_lengths, target_sequence_lengths = (
#     batched_input.source, batched_input.target_input, batched_input.target_output,
#     batched_input.source_sequence_length, batched_input.target_sequence_length)
#
#   split_batches = []
#
#   # sources = tf.Print(sources, [tf.shape(sources)], message="before split: ")
#
#   # TODO: NOTICE!!
#   # Some of the training ops expect the dimensionality of the time dimension of target_output, i.e. the labels, and
#   # tf.reduce_max(target_sequence_length) to match.
#   # This is not the case because of the padding created by the global tf.reduce_max(target_sequence_length)
#   # (before the split)
#   # We therefore clip/slice away the excess padding
#
#   with tf.name_scope("split_batched_input"):
#     def create_slicer(i):
#       def create_slice():
#         source_sequence_length = source_sequence_lengths[i]
#         source = sources[i]
#         target_sequence_length = target_sequence_lengths[i]
#         target_output = target_outputs[i]
#
#         # Cut away excess padding
#         if is_hierarchical:
#           lengths_used_to_cut = tf.reduce_sum(source_sequence_length, -1)
#         else:
#           lengths_used_to_cut = source_sequence_length
#
#         source = source[:, :(tf.reduce_max(lengths_used_to_cut))]
#         target_output = target_output[:, :(tf.reduce_max(target_sequence_length))]
#
#         # target_output = tf.Print(target_output, [i, target_output], message="CALCULATED TARGET OUTPUT", f)
#
#         split_batch = SplitBatchedInput(
#           source=source,
#           target_input=target_inputs[i],
#           target_output=target_output,
#           source_sequence_length=source_sequence_length,
#           target_sequence_length=target_sequence_length)
#
#         return split_batch
#
#       return create_slice
#
#     first_slice = create_slicer(0)()
#     with tf.device(tf.DeviceSpec(device_type="GPU", device_index=0)):
#       split_batch = SplitBatchedInput(
#         source=tf.identity(first_slice.source),
#         target_input=tf.identity(first_slice.target_input),
#         target_output=tf.identity(first_slice.target_output),
#         source_sequence_length=tf.identity(first_slice.source_sequence_length),
#         target_sequence_length=tf.identity(first_slice.target_sequence_length))
#
#     split_batches.append(split_batch)
#
#     def zeros_slice():
#       return SplitBatchedInput(
#         source=first_slice.source,
#         target_input=first_slice.target_input,
#         target_output=first_slice.target_output,
#         source_sequence_length=first_slice.source_sequence_length,
#         target_sequence_length=first_slice.target_sequence_length)
#
#     available_batches_count = tf.shape(sources)[0]
#
#     for i in range(1, num_splits, 1):
#       split_batch = tf.cond(i < available_batches_count,
#                             true_fn=create_slicer(i),
#                             false_fn=zeros_slice)
#
#       with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):
#         split_batch = SplitBatchedInput(
#           source=tf.identity(split_batch.source),
#           target_input=tf.identity(split_batch.target_input),
#           target_output=tf.identity(split_batch.target_output),
#           source_sequence_length=tf.identity(split_batch.source_sequence_length),
#           target_sequence_length=tf.identity(split_batch.target_sequence_length))
#
#         split_batches.append(split_batch)
#
#     return split_batches


def create_first_hierarchy_level_sequence_clipper(hierarchical_split_tokens, max_first_hierarchy_level_sequence_length,
                                                  src_max_len):
  def clip_first_level_sequences(src, tgt=None):

    tokens_vector = tf.string_split([src], delimiter=" ", skip_empty=True).values

    print("tokens_vector:", tokens_vector)

    highest_level_token_split_vector_ta, substring_lengths = split_token_string_by_token(tokens_vector,
                                                                                         hierarchical_split_tokens[-1],
                                                                                         consume_extra_token=len(
                                                                                           hierarchical_split_tokens) == 1)

    print("substring_lengths:", substring_lengths)
    # token_count_per_substring = substring_lengths #tf.map_fn(calculate_subsequence_token_count, token_split_vector, dtype=tf.int32)
    # print("token_count_per_substring:", token_count_per_substring)
    # lengths.append(token_count_per_substring)

    token_split_vector_ta = highest_level_token_split_vector_ta

    # print("hierarchical_split_tokens[:-1]:", hierarchical_split_tokens[:-1])

    # Calculate for all but the first level (which does not include tokens)
    for level_index, split_token in reversed(list(enumerate(hierarchical_split_tokens[:-1]))):
      # input_string = tf.reshape(input_string, [-1])

      i = tf.constant(0)
      substrings_ta = tf.TensorArray(dtype=tf.string, size=0, dynamic_size=True, element_shape=[None],
                                     infer_shape=False)  # infer_shape=False
      loop_vars = (i, token_split_vector_ta, substrings_ta)

      cond = lambda i, _, __: i < token_split_vector_ta.size()

      def body(i, input_token_vector_ta, substrings_ta):

        tokens_vector = input_token_vector_ta.read(i)
        print("tokens_vector: ", tokens_vector)
        token_split_vector_ta, substring_lengths = split_token_string_by_token(tokens_vector, split_token, True)

        j = tf.constant(0)
        loop_vars = (j, token_split_vector_ta, substrings_ta)
        cond = lambda j, _, __: j < token_split_vector_ta.size()

        def join_loop_fn(j, input_ta, output_ta):
          subsequence_tensor = input_ta.read(j)

          if level_index == 0:
            token_count = tf.size(subsequence_tensor)
            subsequence_tensor = tf.cond(token_count > max_first_hierarchy_level_sequence_length,
                                         lambda: tf.concat(
                                           [subsequence_tensor[:max_first_hierarchy_level_sequence_length - len(
                                             hierarchical_split_tokens)],
                                            subsequence_tensor[
                                            tf.reduce_min(tf.where(tf.equal(subsequence_tensor, split_token))):]],
                                           axis=0),
                                         lambda: subsequence_tensor)

          output_ta = output_ta.write(output_ta.size(), subsequence_tensor)

          return j + 1, input_ta, output_ta

        loop_vars = tf.while_loop(loop_vars=loop_vars, cond=cond, body=join_loop_fn, name="join_loop", back_prop=False)

        return i + 1, input_token_vector_ta, loop_vars[2]  # , substrings_lengths_ta

      loop_vars = tf.while_loop(loop_vars=loop_vars, cond=cond, body=body, name="calculate_level_lengths",
                                back_prop=False)

      token_split_vector_ta = loop_vars[2]

    src = token_split_vector_ta.concat()
    src = src[:src_max_len]
    src = tf.reduce_join(src, axis=0, separator=" ")

    # src = tf.Print(src, [src], "clipped src: ", summarize=100)

    if tgt is not None:  # Training
      return src, tgt
    else:  # Inference
      return src

  return clip_first_level_sequences


def create_hierarchical_token_split_counter(hierarchical_split_tokens):
  def calculate_level_lengths(src, tokens_vector, tgt_in=None, tgt_out=None):

    lengths = []
    # input_string = raw_source

    print("tokens_vector:", tokens_vector)

    # Calculate for first level which includes tokens
    # def calculate_subsequence_token_count(str):
    #   return tf.size(tf.string_split([str], skip_empty=True).values) # + 1 Plus one because split removes the token

    # tokens_vector = tf.string_split([raw_source], delimiter=" ", skip_empty=True).values

    highest_level_token_split_vector_ta, substring_lengths = split_token_string_by_token(tokens_vector,
                                                                                         hierarchical_split_tokens[-1],
                                                                                         consume_extra_token=len(
                                                                                           hierarchical_split_tokens) == 1)
    # src = tf.Print(src, [tf.shape(src), raw_source, substring_lengths],
    #                        summarize=100, message="token_split_string: ")

    print("substring_lengths:", substring_lengths)
    # token_count_per_substring = substring_lengths #tf.map_fn(calculate_subsequence_token_count, token_split_vector, dtype=tf.int32)
    # print("token_count_per_substring:", token_count_per_substring)
    # lengths.append(token_count_per_substring)

    token_split_vector_ta = highest_level_token_split_vector_ta

    # print("hierarchical_split_tokens[:-1]:", hierarchical_split_tokens[:-1])

    # Calculate for all but the first level (which does not include tokens)
    for split_token in hierarchical_split_tokens[:-1]:
      # input_string = tf.reshape(input_string, [-1])

      i = tf.constant(0)
      substrings_ta = tf.TensorArray(dtype=tf.string, size=0, dynamic_size=True, element_shape=[None],
                                     infer_shape=False)  # infer_shape=False
      substrings_lengths_ta = tf.TensorArray(dtype=tf.int32, size=0, dynamic_size=True)
      loop_vars = (i, token_split_vector_ta, substrings_ta, substrings_lengths_ta)

      cond = lambda i, _, __, ___: i < token_split_vector_ta.size()

      def body(i, input_token_vector_ta, substrings_ta, substrings_lengths_ta):
        tokens_vector = input_token_vector_ta.read(i)
        print("tokens_vector: ", tokens_vector)
        token_split_vector_ta, substring_lengths = split_token_string_by_token(tokens_vector, split_token, True)

        j = tf.constant(0)
        loop_vars = (j, token_split_vector_ta, substrings_ta)
        cond = lambda j, _, __: j < token_split_vector_ta.size()

        def join_loop_fn(j, input_ta, output_ta):
          output_ta = output_ta.write(output_ta.size(), input_ta.read(j))
          return j + 1, input_ta, output_ta

        loop_vars = tf.while_loop(loop_vars=loop_vars, cond=cond, body=join_loop_fn, name="join_loop", back_prop=False)

        substrings_lengths_ta = substrings_lengths_ta.write(substrings_lengths_ta.size(), token_split_vector_ta.size())

        return i + 1, input_token_vector_ta, loop_vars[2], substrings_lengths_ta

      loop_vars = tf.while_loop(loop_vars=loop_vars, cond=cond, body=body, name="calculate_level_lengths",
                                back_prop=False)

      token_split_vector_ta = loop_vars[2]
      substrings_lengths_ta = loop_vars[3]
      lengths.append(substrings_lengths_ta.stack())

    # Go through each of the n-level subsequence vectors and count the number of tokens
    i = tf.constant(0)
    first_level_lengths_ta = tf.TensorArray(dtype=tf.int32, size=0, dynamic_size=True)
    loop_vars = (i, token_split_vector_ta, first_level_lengths_ta)
    cond = lambda i, _, __: i < token_split_vector_ta.size()

    def count_token_vector_elements(i, token_vector_ta, first_level_lengths_ta):
      first_level_lengths_ta = first_level_lengths_ta.write(i, tf.size(token_vector_ta.read(i)))
      return i + 1, token_vector_ta, first_level_lengths_ta

    loop_vars = tf.while_loop(loop_vars=loop_vars, cond=cond, body=count_token_vector_elements)
    lengths.append(loop_vars[2].stack())

    lengths = list(reversed(lengths))

    # def calculate_subsequence_count(str):
    #   return tf.size(tf.string_split([str], delimiter=hierarchical_split_tokens[idx], skip_empty=True).values)
    #
    # print("token_split_string:", token_split_vector)
    # token_count_per_substring = tf.map_fn(calculate_subsequence_count, token_split_vector, dtype=tf.int32)
    # print("token_count_per_substring:", token_count_per_substring)
    # lengths.append(token_count_per_substring)

    print("lengths: ", lengths)

    longest_length_of_lengths = tf.size(lengths[0])

    padded_lowest_level_first_lengths = []
    for lengths_vector in lengths:
      padding_size = tf.convert_to_tensor([longest_length_of_lengths]) - tf.shape(lengths_vector)
      # padding_size = tf.Print(padding_size, [padding_size, [longest_length_of_lengths], tf.shape(lengths_vector)], summarize=100, message="padding_size: ")

      lengths_padding = tf.zeros(padding_size, dtype=tf.int32)

      print("lengths_padding: ", lengths_padding)
      # Concatenate `lengths_vector` with the padding.
      lengths_vector = tf.concat([lengths_vector, lengths_padding], 0)
      padded_lowest_level_first_lengths.append(lengths_vector)

    lengths = tf.stack(padded_lowest_level_first_lengths)

    # lengths = tf.Print(lengths, [raw_source, lengths, tf.shape(lengths)], summarize=100, message="lengths: ")

    if tgt_in is not None and tgt_out is not None:  # Training
      return src, tgt_in, tgt_out, lengths, tf.size(tgt_in)
    else:  # Inference
      return src, lengths, tokens_vector

  return calculate_level_lengths
