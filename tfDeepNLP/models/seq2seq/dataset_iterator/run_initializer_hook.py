from tensorflow.python.training import session_run_hook
import tensorflow as tf

class RunInitializerHook(session_run_hook.SessionRunHook):
  """Runs `feed_fn` and sets the `feed_dict` accordingly."""

  def __init__(self):
    """Initializes a `FeedFnHook`.
    Args:
      feed_fn: function that takes two arguments 'session', 'coord' and returns nothing
    """
    self.initializer = None

  def after_create_session(self, session, coord):
    session.run(self.initializer)
