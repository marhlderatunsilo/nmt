import tensorflow as tf

from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser

__all__ = ["get_dataset"]


def bucket_batch_dataset(src_tgt_dataset, batch_size, num_buckets, src_max_len, src_eos_id, tgt_eos_id,
                         hierarchical_split_tokens=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE):
  is_hierarchical = hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE

  # Bucket by source sequence length (buckets for lengths 0-9, 10-19, ...)
  def batching_func(x):

    print("hierarchical_split_tokens: ", hierarchical_split_tokens)

    return x.padded_batch(
      batch_size,
      # The first three entries are the source and target line rows;
      # these have unknown-length vectors.  The last two entries are
      # the source and target row sizes; these are scalars.
      padded_shapes=(
        tf.TensorShape([None]),  # src
        tf.TensorShape([None]),  # tgt_input
        tf.TensorShape([None]),  # tgt_output
        tf.TensorShape([None, None] if is_hierarchical else []),  # src_len
        tf.TensorShape([])),  # tgt_len
      # Pad the source and target sequences with eos tokens.
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        src_eos_id,  # src
        tgt_eos_id,  # tgt_input
        tgt_eos_id,  # tgt_output
        0,  # src_len -- unused
        0))  # tgt_len -- unused

  def key_func(unused_1, unused_2, unused_3, src_len, tgt_len):
    # Calculate bucket_width by maximum source sequence length.
    # Pairs with length [0, bucket_width) go to bucket 0, length
    # [bucket_width, 2 * bucket_width) go to bucket 1, etc.  Pairs with length
    # over ((num_bucket-1) * bucket_width) words all go into the last bucket.
    if src_max_len:
      bucket_width = (src_max_len + num_buckets - 1) // num_buckets
    else:
      bucket_width = 10

    # Bucket sentence pairs by the length of their source sentence and target
    # sentence.

    # If we run hierarchical we need to select the correct lengths
    if is_hierarchical:
      # Reduce the individual lengths of each subsequence of the first hierarchy level
      src_len = tf.reduce_sum(src_len[0])

    bucket_id = tf.maximum(src_len // bucket_width, tgt_len // bucket_width)
    return tf.to_int64(tf.minimum(num_buckets, bucket_id))

  def reduce_func(unused_key, windowed_data):
    return batching_func(windowed_data)

  # def key_func_split(unused_1, unused_2, unused_3, src_len, tgt_len):
  #
  #   if src_max_len and num_buckets > 1:
  #     bucket_width = (src_max_len * batch_size + num_buckets - 1) // num_buckets
  #   else:
  #     bucket_width = 10 * batch_size
  #
  #   if is_hierarchical:
  #     print("iterator split, src_len1: ", src_len)
  #     # Reduce the individual lengths of each subsequence of the first hierarchy level in the first batch
  #     src_len = tf.reduce_sum(src_len[0][0])
  #     print("iterator split, src_len2: ", src_len)
  #
  #   # Bucket sentence pairs by the length of their source sentence and target
  #   # sentence.
  #   bucket_id = tf.maximum(tf.reduce_sum(src_len) // bucket_width, tf.reduce_sum(tgt_len) // bucket_width)
  #   return tf.to_int64(tf.minimum(num_buckets, bucket_id))

  # def reduce_func_split(unused_key, windowed_data):
  #   return split_batching_func(windowed_data)

  if num_buckets > 1:

    batched_dataset = src_tgt_dataset.apply(
      tf.contrib.data.group_by_window(
        key_func=key_func, reduce_func=reduce_func, window_size=batch_size))

  else:
    batched_dataset = batching_func(src_tgt_dataset)

  # if num_splits:
  #   batched_dataset = batched_dataset.apply(
  #     tf.contrib.data.group_by_window(
  #       key_func=key_func_split, reduce_func=reduce_func_split, window_size=num_splits))

  return batched_dataset


def unpack_tf_record(content):
  keys_to_features = {'source': tf.VarLenFeature(tf.int64),
                      'target': tf.VarLenFeature(tf.int64),
                      'target_input': tf.VarLenFeature(tf.int64),
                      'src_seq_len': tf.FixedLenFeature((), tf.int64),
                      'tgt_seq_len': tf.FixedLenFeature((), tf.int64),
                      }
  parsed_features = tf.parse_single_example(content, keys_to_features)

  source_sequence_length = tf.cast(parsed_features['src_seq_len'], tf.int32)
  target_sequence_length = tf.cast(parsed_features['tgt_seq_len'], tf.int32)

  source = tf.cast(tf.sparse_tensor_to_dense(parsed_features['source']), tf.int32)
  target = tf.cast(tf.sparse_tensor_to_dense(parsed_features['target']), tf.int32)
  target_input = tf.cast(tf.sparse_tensor_to_dense(parsed_features['target_input']), tf.int32)

  return source, target, target_input, source_sequence_length, target_sequence_length


def get_dataset(dataset,
                py_src_vocab_table,
                py_tgt_vocab_table,
                batch_size,
                eos,
                num_buckets,
                shuffle=True,
                random_seed=None,
                src_max_len=None,
                num_threads=32,
                output_buffer_size=None,
                hierarchical_split_tokens=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE):
  if not output_buffer_size:
    output_buffer_size = batch_size * 50
  src_eos_id = py_src_vocab_table[eos]  # tf.cast(src_vocab_table.lookup(tf.constant(eos)), tf.int32)
  tgt_eos_id = py_tgt_vocab_table[eos]  # tf.cast(tgt_vocab_table.lookup(tf.constant(eos)), tf.int32)

  if shuffle:
    dataset = dataset.apply(tf.contrib.data.shuffle_and_repeat(output_buffer_size, seed=random_seed))
  else:
    dataset = dataset.repeat()

  dataset = dataset.map(unpack_tf_record, num_parallel_calls=num_threads)

  batched_dataset = bucket_batch_dataset(dataset, batch_size, num_buckets, src_max_len,
                                         src_eos_id,
                                         tgt_eos_id, hierarchical_split_tokens=hierarchical_split_tokens)

  # Make the tuple shape fit requirements of estimator
  batched_dataset = batched_dataset.map(lambda src, tgt_input, tgt_output, src_len, tgt_len:
                                        ({"source": src,
                                          "source_sequence_length": src_len},
                                         {"target_output": tgt_output,
                                          "target_input": tgt_input,
                                          "target_sequence_length": tgt_len}
                                         ), num_parallel_calls=num_threads)

  # dataset_iter = base_dataset_with_lengths.make_initializable_iterator()
  #
  # (src_ids, tgt_input_ids, tgt_output_ids, src_seq_len,
  #  tgt_seq_len) = (dataset_iter.get_next())
  #
  # features = {"source": src_ids,
  #             "source_sequence_length": src_seq_len,
  #             "target_input": tgt_input_ids,
  #             }
  #
  # labels = {
  #   "target_output": tgt_output_ids,
  #   "target_sequence_length": tgt_seq_len
  # }

  return batched_dataset
