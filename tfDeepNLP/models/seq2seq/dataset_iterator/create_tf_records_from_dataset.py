from tfDeepNLP.models.seq2seq.dataset_iterator.seq2seq_initializable_iterator import get_iterator
from tfDeepNLP.utils import vocab_utils
from utils.tf_records_utils import *


def create_tf_records(hparams):
  for prefix in [hparams.train_prefix, hparams.dev_prefix, hparams.test_prefix]:

    src_file = "%s.%s" % (prefix, hparams.src)
    tgt_file = "%s.%s" % (prefix, hparams.tgt)

    src_vocab_file = hparams.src_vocab_file
    tgt_vocab_file = hparams.tgt_vocab_file

    src_dataset = tf.data.TextLineDataset(src_file)
    tgt_dataset = tf.data.TextLineDataset(tgt_file)

    src_vocab_table, tgt_vocab_table = vocab_utils.create_vocab_tables(
      src_vocab_file, tgt_vocab_file, hparams.share_vocab)

    initializer, features, labels = get_iterator(
      src_dataset,
      tgt_dataset,
      src_vocab_table,
      tgt_vocab_table,
      batch_size=hparams.batch_size,
      sos=hparams.sos,
      eos=hparams.eos,
      source_reverse=hparams.source_reverse,
      random_seed=None,
      num_buckets=hparams.num_buckets,
      src_max_len=hparams.src_max_len,
      tgt_max_len=hparams.tgt_max_len,
      num_shards=hparams.num_gpus,  # hparams.num_worker_hosts,
      num_splits=hparams.num_gpus,
      num_threads=hparams.parallel_iterations,
      skip_count=None,
      # shard_index=hparams.task_index,
      shuffle=False,
      projection_vocab_size=hparams.projection_vocab_size,
      hierarchical_split_tokens=hparams.hierarchical_split_tokens,
      pointer_generation=hparams.pointer_generation,
      max_first_hierarchy_level_sequence_length=hparams.max_first_hierarchy_level_sequence_length)

    with tf.Session() as sess:

      sess.run(tf.tables_initializer())
      sess.run(initializer)

      writer = create_tfr_writer(prefix + '_seq2seq.tfrecords')  # out_dir=hparams.out_dir
      # features_writer = create_tfr_writer(prefix + '_seq2seq.src.tfrecords') #out_dir=hparams.out_dir
      # labels_writer = create_tfr_writer(prefix + '_seq2seq.tgt.tfrecords')  # out_dir=hparams.out_dir

      while (True):
        try:

          py_features, py_labels = sess.run((features, labels))

          # Write to records

          # features_example = tf.train.Example(features=tf.train.Features(feature={
          #   'source': int64_array_feature(py_features["source"]),
          #   'src_seq_len': int64_feature(py_features["source_sequence_length"]),
          # }))
          #
          # labels_example = tf.train.Example(features=tf.train.Features(feature={
          #   'target': int64_array_feature(py_labels["target_output"]),
          #   'target_input': int64_array_feature(py_features["target_input"]),
          #   'tgt_seq_len': int64_feature(py_labels["target_sequence_length"]),
          # }))

          example = tf.train.Example(features=tf.train.Features(feature={
            'source': int64_array_feature(py_features["source"]),
            'target': int64_array_feature(py_labels["target_output"]),
            'target_input': int64_array_feature(py_features["target_input"]),
            'src_seq_len': int64_feature(py_features["source_sequence_length"]),
            'tgt_seq_len': int64_feature(py_labels["target_sequence_length"]),
          }))

          # features_writer.write(features_example.SerializeToString())
          # labels_writer.write(labels_example.SerializeToString())
          writer.write(example.SerializeToString())

        except tf.errors.OutOfRangeError:
          # features_writer.close()
          # labels_writer.close()
          writer.close()
          break

  return
