# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""S2S: Basic sequence-to-sequence model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc

import tensorflow as tf
from tensorflow.python.layers import core as layers_core
from tensorflow.python.ops import lookup_ops

from tfDeepNLP import model_helper
from tfDeepNLP.create_optimizer import create_optimizer
from tfDeepNLP.layers.TransposedDense import TransposedDense
from tfDeepNLP.model_base.base_model import BaseModel
from tfDeepNLP.rnn_utils import create_rnn_cell
from tfDeepNLP.utils import misc_utils as utils, vocab_utils
from tfDeepNLP.utils.gradient_utils import gradient_clip
from tfDeepNLP.utils.initializer_utils import get_initializer

utils.check_tensorflow_version()

__all__ = ["S2SBaseModel"]


class S2SBaseModel(BaseModel):
  """Sequence-to-sequence base class.
  """

  def __init__(self,
               hparams,
               # mode,
               # iterator,
               # source_vocab_table,
               # target_vocab_table,
               # reverse_tgt_vocab_table=None,
               # scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               # optimizer_op=None,
               # learning_rate=None,
               # first_gpu_id=None,
               output_projection_dim=None):

    super(S2SBaseModel, self).__init__(hparams,
                                       # mode,
                                       # iterator,
                                       single_cell_fn,
                                       # model_device_fn,
                                       # optimizer_op,
                                       # learning_rate,
                                       # first_gpu_id
                                       )
    self.output_projection_dim = output_projection_dim

    """Create the model.

    Args:
      hparams: Hyperparameter configurations.
      mode: TRAIN | EVAL | INFER
      iterator: Dataset Iterator that feeds data.
      source_vocab_table: Lookup table mapping source words to ids.
      target_vocab_table: Lookup table mapping target words to ids.
      reverse_target_vocab_table: Lookup table mapping ids to target words. Only
        required in INFER mode. Defaults to None.
      scope: scope of the model.
      single_cell_fn: allow for adding customized cell. When not specified,
        we default to model_helper._single_cell
    """

  def __call__(self, features, labels, mode, params):
    super(S2SBaseModel, self).__call__(features, labels, mode, params)

    hparams = params
    self.mode = mode
    self.features = features
    self.labels = labels

    # source_vocab_table,
    # target_vocab_table,
    # reverse_tgt_vocab_table = reverse_tgt_vocab_table,

    src_vocab_file = hparams.src_vocab_file
    tgt_vocab_file = hparams.tgt_vocab_file

    src_vocab_table, tgt_vocab_table = vocab_utils.create_vocab_tables(
      src_vocab_file, tgt_vocab_file, hparams.share_vocab)
    reverse_tgt_vocab_table = lookup_ops.index_to_string_table_from_file(
      tgt_vocab_file, default_value=vocab_utils.UNK)

    self.source_vocab_table = src_vocab_table
    self.tgt_vocab_table = tgt_vocab_table
    self.reverse_tgt_vocab_table = reverse_tgt_vocab_table

    self.src_vocab_size = hparams.src_vocab_size
    self.tgt_vocab_size = hparams.tgt_vocab_size
    self.num_layers = hparams.num_layers
    self.time_major = hparams.time_major
    self.clip_gradients = hparams.clip_gradients

    self.global_step = tf.train.get_or_create_global_step()

    # Initializer
    initializer = get_initializer(
      hparams.init_op, hparams.random_seed, hparams.init_weight)
    tf.get_variable_scope().set_initializer(initializer)

    # Embeddings
    self.batch_size = tf.shape(features["source_sequence_length"])[0]
    self.init_embeddings(hparams)



    #distribution_strategy = tf.contrib.distribute.get_distribution_strategy()

    # Projection
    # with utils.cond_scope(scope):  # only set variable scope if scope not None
    with tf.variable_scope("decoder/output_projection", ):

      projection_size = hparams.tgt_vocab_size

      if hparams.projection_vocab_size is not None:
        projection_size = hparams.projection_vocab_size
        print("projection_size: ", projection_size)

      if hparams.use_sampled_softmax:
        self.output_layer = TransposedDense(
          projection_size, use_bias=True, name="output_projection",
          # kernel_initializer=tf.orthogonal_initializer,
          transpose=not (mode == tf.contrib.learn.ModeKeys.TRAIN and hparams.use_sampled_softmax))

        proj_num_units = hparams.num_units if self.output_projection_dim is None else self.output_projection_dim

        self.output_layer.build([None, proj_num_units])

      else:
        self.output_layer = layers_core.Dense(
          projection_size, use_bias=True, name="output_projection",
          # kernel_initializer=tf.orthogonal_initializer
        )

    ## Train graph
    res = self.build_graph(hparams, features, labels)
    train_loss = None

    if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
      train_loss = res[1]
      self.train_loss = train_loss
      self.final_context_state = res[2]
      self.word_count = tf.reduce_sum(
        features["source_sequence_length"]) + tf.reduce_sum(
        labels["target_sequence_length"])
    elif self.mode == tf.contrib.learn.ModeKeys.EVAL:
      self.eval_loss = res[1]
      self.final_context_state = res[2]
    elif self.mode == tf.contrib.learn.ModeKeys.INFER:
      self.infer_logits, _, self.final_context_state, self.samples = res

      if self.samples.dtype == tf.int32:
        self.sample_words = reverse_tgt_vocab_table.lookup(
          tf.to_int64(self.samples))
      elif self.samples.dtype == tf.string:
        self.sample_words = self.samples

    if self.mode != tf.contrib.learn.ModeKeys.INFER:
      ## Count the number of predicted words for compute ppl.
      self.predict_count = tf.reduce_sum(
        labels["target_sequence_length"])

    ## Learning rate
    print("  start_decay_step=%d, learning_rate=%g, decay_steps %d,"
          "decay_factor %g" % (hparams.start_decay_step, hparams.learning_rate,
                               hparams.decay_steps, hparams.decay_factor))

    # params = tf.trainable_variables()

    # Gradients and SGD update operation for training the model.
    # Arrage for the embedding vars to appear at the beginning.

    if mode == tf.estimator.ModeKeys.TRAIN:

      optimizer_op, learning_rate = self.create_optimizer(hparams, mode)

      gradients = self.setup_gradient_computation(hparams, self.train_loss, optimizer_op, learning_rate)

      print("gradients: ", gradients)

      train_op = optimizer_op.apply_gradients(gradients, global_step=self.global_step) #  optimizer_op.minimize(train_loss)
      train_op = tf.group(train_op)


      print("train_op: ", train_op)

      return tf.estimator.EstimatorSpec(mode, loss=train_loss, train_op=train_op)
    elif mode == tf.estimator.ModeKeys.EVAL:
      return tf.estimator.EstimatorSpec(mode, loss=train_loss, eval_metric_ops=None)
    elif mode == tf.estimator.ModeKeys.PREDICT:
      predictions = {
        'output_sequence_ids': self.samples,
      }

      self.infer_summary = self._get_infer_summary(hparams)

      return tf.estimator.EstimatorSpec(mode, predictions=predictions)

  def setup_gradient_computation(self, hparams, train_loss, optimizer_op, learning_rate, **kwargs):

    gradients = None

    # Gradients and update operation for training the model.
    # Arrange for the embedding vars to appear at the beginning.
    if self.mode == tf.contrib.learn.ModeKeys.TRAIN:

      gradients = optimizer_op.compute_gradients(
        train_loss,
        # params,
        colocate_gradients_with_ops=hparams.colocate_gradients_with_ops,
        #aggregation_method=tf.AggregationMethod.EXPERIMENTAL_ACCUMULATE_N
      )

      if self.clip_gradients:

        gradients, self.params = zip(*gradients)

        clipped_gradients, gradient_norm_summary = gradient_clip(
          gradients, max_gradient_norm=hparams.max_gradient_norm)

        gradients = zip(clipped_gradients, self.params)

        # Summary
        self.train_summary = tf.summary.merge([
                                                tf.summary.scalar("lr", learning_rate),
                                                tf.summary.scalar("train_loss", self.train_loss),
                                              ] + gradient_norm_summary)
      else:

        # Summary
        self.train_summary = tf.summary.merge([
          tf.summary.scalar("lr", learning_rate),
          tf.summary.scalar("train_loss", self.train_loss),
        ])

        #

    return gradients

  def create_look_up_fn(self, embeddings, cut_to_size=False):
    def look_up_fn(ids):
      # with tf.device('/cpu:0'):

      if cut_to_size:
        ids = tf.where(ids >= self.hparams.projection_vocab_size, tf.fill(tf.shape(ids), vocab_utils.UNK_ID), ids)

      return tf.nn.embedding_lookup(embeddings, ids)

    return look_up_fn

  def init_embeddings(self, hparams, scope=None):
    """Init embeddings."""
    # with tf.device('/cpu:0'):
    if hparams.embeddings_option == "glove":
      self.embedding_encoder = tf.get_variable("embedding_share",
                                               shape=[self.src_vocab_size, hparams.token_embedding_size],
                                               initializer=tf.zeros_initializer(),
                                               # partitioner=tf.fixed_size_partitioner(4),
                                               # We have to use tf.assign() elsewhere
                                               # initializer=tf.constant_initializer(
                                               # initialize_word_emb_from_numpy_array(self.hparams,
                                               #                                      self.pretrained_embeddings,
                                               #                                      self.hparams.dim_word,
                                               #                                      self.nwords)),
                                               dtype=tf.float32,
                                               trainable=True)

      self.embedding_decoder = self.embedding_encoder

    elif hparams.embeddings_option is None or hparams.embeddings_option == "train":
      self.embedding_encoder, self.embedding_decoder = (
        model_helper.create_emb_for_encoder_and_decoder(
          share_vocab=hparams.share_vocab,
          src_vocab_size=self.src_vocab_size,
          tgt_vocab_size=self.tgt_vocab_size,
          src_embed_size=hparams.token_embedding_size,
          tgt_embed_size=hparams.token_embedding_size,
          num_partitions=0,  # max(hparams.num_gpus, 0),  # max(hparams.num_worker_hosts, hparams.num_ps_hosts),
          scope=scope, ))
    else:
      raise ValueError("Embedding option: " + hparams.embeddings_option + " not supported")

    if hparams.embeddings_option != "train" and self.mode == tf.estimator.ModeKeys.TRAIN:
      print("has_distribution_strategy: ", tf.contrib.distribute.has_distribution_strategy())

      # def merge_fn(distribution, assigned_embeddings):
      #   return distribution.unwrap(assigned_embeddings)[0]
      #
      # def fn(var, value):
      #   tower_ctx = tf.contrib.distribute.get_tower_context()
      #   assigned_embeddings = var.assign(value)
      #   return tower_ctx.merge_call(merge_fn, assigned_embeddings)



      #with tf.contrib.distribute.get_cross_tower_context().scope():
      #self.assign_embeddings = strategy.call_for_each_tower(fn, self.embedding_encoder, self.embeddings_placeholder)


      #self.assign_embeddings = strategy.update(self.embedding_encoder, assign_embeddings, self.embeddings_placeholder)

      #self.assign_embeddings = self.embedding_encoder.assign(self.embeddings_placeholder)
      #tf.contrib.distribute.get_tower_context().merge_call(assign_embeddings, self.embedding_encoder, self.embeddings_placeholder)


      #with (strategy):
         #tf.contrib.distribute.get_distribution_strategy().update(self.embedding_encoder,
             #                                                                           assign_embeddings,
               #                                                                         self.embeddings_placeholder)  #

  def statistics(self):
    assert self.mode == tf.contrib.learn.ModeKeys.TRAIN
    return (
      self.train_loss,
      self.predict_count,
      self.train_summary,
      self.word_count,
      self.batch_size
    )

  @property
  def eval_ops(self):
    assert self.mode == tf.contrib.learn.ModeKeys.EVAL
    return [self.eval_loss,
            self.predict_count,
            self.batch_size]

  def eval(self, sess):
    assert self.mode == tf.contrib.learn.ModeKeys.EVAL
    return sess.run(self.eval_ops)

  def build_graph(self, hparams, features, labels, **kwargs):
    """Subclass must implement this method.

    Creates a sequence-to-sequence model with dynamic RNN decoder API.
    Args:
      hparams: Hyperparameter configurations.
      scope: VariableScope for the created subgraph; default "dynamic_seq2seq".

    Returns:
      A tuple of the form (logits, loss, final_context_state),
      where:
        logits: float32 Tensor [batch_size x num_decoder_symbols].
        loss: the total loss / batch_size.
        final_context_state: The final state of decoder RNN.

    Raises:
      ValueError: if encoder_type differs from mono and bi, or
        attention_option is not (luong | scaled_luong |
        bahdanau | normed_bahdanau).
    """
    utils.print_out("# creating %s graph ..." % self.mode)
    dtype = tf.float32
    num_layers = hparams.num_layers

    # with utils.cond_scope(scope):  # only set variable scope if scope not None
    with tf.name_scope("dynamic_seq2seq"):  # scope or
      # Encoder
      encoder_outputs, encoder_state = self._build_encoder(hparams, features)

      ## Decoder
      # device_type = "GPU" if self.first_gpu_id >= 0 else "CPU"
      # device_index = self.first_gpu_id + 1 if self.first_gpu_id >= 0 else 0
      # with tf.device(tf.DeviceSpec(device_type=device_type, device_index=device_index)):
      logits, sample_id, final_context_state, final_sequence_lengths = self._build_decoder(
        encoder_outputs, encoder_state, hparams)

      ## Loss
      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        loss = self.setup_loss_computation(hparams, logits, labels=labels,
                                           final_context_state=final_context_state,
                                           final_sequence_lengths=final_sequence_lengths)
      else:
        loss = None

      return logits, loss, final_context_state, sample_id

  @abc.abstractmethod
  def _build_encoder(self, hparams, features):
    """Subclass must implement this.

    Build and run an RNN encoder.

    Args:
      hparams: Hyperparameters configurations.

    Returns:
      A tuple of encoder_outputs and encoder_state.
    """
    pass

  def _build_encoder_cell(self, hparams, num_layers, num_residual_layers, num_units=None):
    """Build a multi-layer RNN cell that can be used by encoder."""

    return create_rnn_cell(
      unit_type=hparams.unit_type,
      num_units=num_units if num_units else hparams.num_units,
      num_layers=num_layers,
      num_residual_layers=num_residual_layers,
      forget_bias=hparams.forget_bias,
      dropout=hparams.dropout,
      zoneout=hparams.zoneout,
      norm_gain=hparams.norm_gain,
      mode=self.mode,
      single_cell_fn=self.single_cell_fn,
      # model_device_fn=self.model_device_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes)

  def _build_decoder(self, encoder_outputs, encoder_state, hparams):
    """Build and run a RNN decoder with a final projection layer.

    Args:
      encoder_outputs: The outputs of encoder for every time step.
      encoder_state: The final state of the encoder.
      hparams: The Hyperparameters configurations.

    Returns:
      A tuple of final logits and final decoder state:
        logits: size [time, batch_size, vocab_size] when time_major=True.
    """
    tgt_sos_id = tf.cast(self.tgt_vocab_table.lookup(tf.constant(hparams.sos)),
                         tf.int32)
    tgt_eos_id = tf.cast(self.tgt_vocab_table.lookup(tf.constant(hparams.eos)),
                         tf.int32)

    num_layers = hparams.num_layers

    # iterator = self.iterator

    # maximum_iteration: The maximum decoding steps.
    if hparams.tgt_max_len_infer:
      maximum_iterations = hparams.tgt_max_len_infer
      utils.print_out("  decoding maximum_iterations %d" % maximum_iterations)
    else:
      # TODO(thangluong): add decoding_length_factor flag
      decoding_length_factor = 2.0
      max_encoder_length = tf.reduce_max(self.features["source_sequence_length"])
      maximum_iterations = tf.to_int32(tf.round(
        tf.to_float(max_encoder_length) * decoding_length_factor))

    ## Decoder.
    with tf.variable_scope("decoder") as decoder_scope:
      cell, decoder_initial_state = self._build_decoder_cell(
        hparams, encoder_outputs, encoder_state,
        self.features["source_sequence_length"])

      ## Train or eval
      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        # decoder_emp_inp: [max_time, batch_size, num_units]
        target_input = self.features["target_input"]

        # device_type = "GPU" if self.first_gpu_id >= 0 else "CPU"
        # device_index = self.first_gpu_id if self.first_gpu_id >= 0 else 0
        # with tf.device(tf.DeviceSpec(device_type=device_type, device_index=device_index)):
        if self.time_major:
          target_input = tf.transpose(target_input)

        self.decoder_emb_inp = self.create_look_up_fn(self.embedding_decoder)(
          target_input)  # tf.nn.embedding_lookup(, )

        # target_sequence_length = tf.Print(
        #   iterator.target_sequence_length,
        #   [tf.reduce_max(iterator.target_sequence_length)],
        #   message="maximum iterator.target_sequence_length:", summarize=10)

        # Helper
        helper = tf.contrib.seq2seq.TrainingHelper(
          self.decoder_emb_inp, self.labels["target_sequence_length"],
          time_major=self.time_major)

        # Decoder
        # Marhlder: *Heuristic* if targets are more than 50 long we want to calculate logits at each timestep
        if hparams.tgt_max_len > 50 and not hparams.use_sampled_softmax:
          my_decoder = tf.contrib.seq2seq.BasicDecoder(
            cell,
            helper,
            decoder_initial_state,
            output_layer=self.output_layer)
        else:
          my_decoder = tf.contrib.seq2seq.BasicDecoder(
            cell,
            helper,
            decoder_initial_state, )

        # Dynamic decoding
        outputs, final_context_state, final_sequence_lengths = tf.contrib.seq2seq.dynamic_decode(
          my_decoder,
          output_time_major=self.time_major,
          parallel_iterations=min(hparams.batch_size, 32),
          swap_memory=True,
          scope=decoder_scope)

        sample_id = outputs.sample_id

        # Note: there's a subtle difference here between train and inference.
        # We could have set output_layer when create my_decoder
        #   and shared more code between train and inference.
        # We chose to apply the output_layer to all timesteps for speed:
        #   10% improvements for small models & 20% for larger ones.
        # If memory is a concern, we should apply output_layer per timestep.

        # Marhlder: *Heuristic* if targets are more than 50 long we want to calculate logits at each timestep
        if hparams.tgt_max_len > 50 or hparams.use_sampled_softmax:
          self.logits = outputs.rnn_output
        else:
          # else we calculate logits for entire batch and all timesteps at once (Faster on GPU's)
          self.logits = self.output_layer(outputs.rnn_output)

      ## Inference
      else:
        beam_width = hparams.beam_width
        length_penalty_weight = hparams.length_penalty_weight
        start_tokens = tf.fill([self.batch_size], tgt_sos_id)
        end_token = tgt_eos_id

        if beam_width > 0:
          my_decoder = tf.contrib.seq2seq.BeamSearchDecoder(
            cell=cell,
            embedding=self.create_look_up_fn(self.embedding_decoder),
            start_tokens=start_tokens,
            end_token=end_token,
            initial_state=decoder_initial_state,
            beam_width=beam_width,
            output_layer=self.output_layer,
            length_penalty_weight=length_penalty_weight)
        else:
          # Helper
          helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
            self.create_look_up_fn(self.embedding_decoder), start_tokens, end_token)

          # Decoder
          my_decoder = tf.contrib.seq2seq.BasicDecoder(
            cell,
            helper,
            decoder_initial_state,
            output_layer=self.output_layer  # applied per timestep
          )

        # Dynamic decoding
        outputs, final_context_state, final_sequence_lengths = tf.contrib.seq2seq.dynamic_decode(
          my_decoder,
          maximum_iterations=maximum_iterations,
          # parallel_iterations=hparams.infer_batch_size,
          parallel_iterations=min(hparams.infer_batch_size, 32),
          output_time_major=self.time_major,
          swap_memory=True,
          scope=decoder_scope)

        if beam_width > 0:
          self.logits = tf.no_op()
          sample_id = outputs.predicted_ids
        else:
          self.logits = outputs.rnn_output
          sample_id = outputs.sample_id

    return self.logits, sample_id, final_context_state, final_sequence_lengths  # , target_output

  def get_max_time(self, tensor):
    time_axis = 0 if self.time_major else 1
    return tensor.shape[time_axis].value or tf.shape(tensor)[time_axis]

  @abc.abstractmethod
  def _build_decoder_cell(self, hparams, encoder_outputs=None, encoder_state=None,
                          source_sequence_length=None):
    """Subclass must implement this.

    Args:
      hparams: Hyperparameters configurations.
      encoder_outputs: The outputs of encoder for every time step.
      encoder_state: The final state of the encoder.
      source_sequence_length: sequence length of encoder_outputs.

    Returns:
      A tuple of a multi-layer RNN cell used by decoder
        and the initial state of the decoder RNN.
    """
    return None, None

  def setup_loss_computation(self, hparams, logits, labels, final_context_state=None, final_sequence_lengths=None,
                             **kwargs):
    """Compute optimization loss."""
    # target_output = self.iterator.target_output
    targets = labels["target_output"]
    if self.time_major:
      targets = tf.transpose(targets)
      # target_output = tf.Print(target_output, [tf.shape(target_output)],
      #                          message="\nModel.py: Time major target_output shape: ")
    # else:
    #   target_output = tf.Print(target_output, [tf.shape(target_output)], message="\nModel.py: target_output shape: ")
    max_time = self.get_max_time(targets)
    # max_time = tf.Print(max_time, [max_time], summarize=100, message="Model.py: max_time: ")

    if self.hparams.use_sampled_softmax and self.mode == tf.contrib.learn.ModeKeys.TRAIN:

      # target_output = tf.one_hot(self.iterator.target_output, self.tgt_vocab_size, axis=0)
      # target_output = tf.transpose(target_output)
      targets = tf.reshape(tf.transpose(targets), [max_time, -1, 1])
      # target_output = tf.Print(target_output, [tf.shape(target_output)], summarize=10,
      #                         message="\nModel.py: target_output transpose(reshape()) shape: ")

      print("self.output_layer.kernel ", self.output_layer.kernel)
      print("logits ", logits)
      print("target_output ", targets)

      def sample_softmax(input_output):

        inputs, labels = input_output

        crossent = tf.nn.sampled_softmax_loss(
          weights=self.output_layer.kernel,
          biases=self.output_layer.bias,
          labels=labels,
          inputs=inputs,
          num_true=1,
          num_sampled=self.hparams.sampled_shortlist_size,
          num_classes=self.tgt_vocab_size,
          partition_strategy="div")
        return crossent, labels

      # assert_op = tf.Assert(tf.equal(tf.shape(target_output)[0], tf.shape(logits)[0]), [tf.shape(target_output)[0], tf.shape(logits)[0]], summarize=100)
      # with tf.control_dependencies([assert_op]):

      crossent, _ = tf.map_fn(sample_softmax, (logits, targets),
                              swap_memory=True, parallel_iterations=min(self.hparams.batch_size, 32))

      # crossent = tf.Print(crossent,
      #                     [tf.shape(crossent), tf.shape(logits), tf.shape(target_output)],
      #                     message="CROSSENT DEBUG", summarize=100)

      # print("crossent ", crossent)


    else:

      # logits = tf.Print(logits, data=[tf.shape(target_output)[0], tf.shape(logits)[0]], message="labels and logits: ",
      #                   summarize=100)
      #
      # assert_op = tf.Assert(tf.equal(tf.shape(target_output)[0], tf.shape(logits)[0]),
      #                       [tf.shape(target_output)[0], tf.shape(logits)[0], target_output[-1]], summarize=1000)

      # with tf.control_dependencies([assert_op]):
      crossent = tf.nn.sparse_softmax_cross_entropy_with_logits(
        labels=targets, logits=logits)

      # crossent = tf.Print(crossent,
      #                     [tf.shape(crossent)],
      #                     message="CROSSENT DEBUG", summarize=100)

    target_weights = tf.sequence_mask(
      labels["target_sequence_length"], max_time, dtype=logits.dtype)
    if self.time_major:
      target_weights = tf.transpose(target_weights)

    loss = tf.reduce_sum(
      crossent * target_weights) / tf.to_float(self.batch_size)

    # loss = tf.Print(loss, [self.batch_size, loss, tf.reduce_max(targets), tf.reduce_sum(logits), tf.reduce_sum(crossent), tf.reduce_sum(target_weights), ], message="LOSS: ")

    return loss

  def _get_infer_summary(self, hparams):
    return tf.no_op()

  def infer(self, sess):
    assert self.mode == tf.contrib.learn.ModeKeys.INFER
    return sess.run([
      self.infer_summary, self.sample_words  # self.infer_logits,
    ])

  def decode(self, sess):
    """Decode a batch.

    Args:
      sess: Tensorflow session to use.

    Returns:
      A tuple consiting of outputs, infer_summary.
        outputs: of size [batch_size, time]
    """
    infer_summary, sample_words = self.infer(sess)  # _,

    # make sure outputs is of shape [batch_size, time]
    if self.time_major:
      sample_words = sample_words.transpose()
    return sample_words, infer_summary
