import tensorflow as tf
from tensorflow.python.framework import tensor_shape
from tensorflow.python.util.nest import map_structure, map_structure_up_to

from tfDeepNLP.rnn_utils import create_rnn_cell
from tfDeepNLP.models.seq2seq.hierarchical.loop_functions import create_decoder_level_loop_fn, create_encoder_level_loop_fn
from tfDeepNLP.models.seq2seq.hierarchical.util import reverse_sequence, create_invert_splits, \
  pick_subsequence_elements, wrap_decoder_cell_in_attention, create_intial_decoder_attention_cell_state, \
  reverse_time_batch_major
from tfDeepNLP.models.seq2seq.s2s_base_model import S2SBaseModel
from tfDeepNLP.single_cells.pointer_generator_probability_wrapper import PointerGeneratorProbabilityWrapper
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser
from tfDeepNLP.utils.initializer_utils import get_initializer


class HierarchicalModel(S2SBaseModel):

  def __init__(self,
               hparams,
               # mode,
               # iterator,
               # source_vocab_table,
               # target_vocab_table,
               # reverse_tgt_vocab_table=None,
               # scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               # optimizer_op=None,
               learning_rate=None,
               # first_gpu_id=None
               ):

    hierarchical_split_tokens = hparams.hierarchical_split_tokens
    self.is_hierarchical = hierarchical_split_tokens is not None and len(
      hierarchical_split_tokens) > 0 and hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE

    super(HierarchicalModel, self).__init__(
      hparams,
      # mode,
      # iterator,

      # scope=scope,
      single_cell_fn=single_cell_fn,
      # model_device_fn=model_device_fn,
      # optimizer_op=optimizer_op,
      # learning_rate=learning_rate,
      # first_gpu_id=first_gpu_id,
      # output_projection_dim=hparams.num_units * 2
    )

  def build_graph(self, hparams, features, labels, **kwargs):
    """Subclass must implement this method.

    Creates a sequence-to-sequence model with dynamic RNN decoder API.
    Args:
      hparams: Hyperparameter configurations.
      scope: VariableScope for the created subgraph; default "dynamic_seq2seq".

    Returns:
      A tuple of the form (logits, loss, final_context_state),
      where:
        logits: float32 Tensor [batch_size x num_decoder_symbols].
        loss: the total loss / batch_size.
        final_context_state: The final state of decoder RNN.

    Raises:
      ValueError: if encoder_type differs from mono and bi, or
        attention_option is not (luong | scaled_luong |
        bahdanau | normed_bahdanau).
    """

    self.regularizer = tf.contrib.layers.l2_regularizer(0.3)
    utils.print_out("# creating %s graph ..." % self.mode)

    # with utils.cond_scope(scope):  # only set variable scope if scope not None
    with tf.name_scope("dynamic_seq2seq"):  # scope or

      # Minus one because we are moving from lengths to indexes
      self.split_indexes = tf.cumsum(features["source_sequence_length"], axis=-1)

      # Encoder
      encoded_outputs, encoded_state = self._build_encoder(hparams, features)

      # Decoder
      # logits, sample_id, final_context_state, final_sequence_lengths, final_dists, alignment_history = self.split_sequence_decoding(
      #  encoded_outputs, encoded_state, hparams)

      logits, sample_id, final_context_state, final_sequence_lengths, final_dists, alignment_history = self._build_sequence_decoder(
        hparams, features, labels,
        encoded_outputs, encoded_state)

      ## Loss
      if self.mode != tf.contrib.learn.ModeKeys.INFER:

        # Supervised sequence loss
        with tf.variable_scope("sequence_loss"):

          if False:

            max_output_length = tf.shape(final_dists)[0]
            # final_dists #= tf.unstack(final_dists)
            batch_nums = tf.range(0, limit=self.batch_size)  # shape (batch_size)
            dec_steps = tf.range(0, limit=max_output_length)

            def calculate_loss_per(dec_step_dist):

              dec_step, dist = dec_step_dist

              targets = labels["target_output"][:,
                        dec_step]  # The indices of the target words. shape (batch_size)

              indices = tf.stack((batch_nums, targets), axis=1)  # shape (batch_size, 2)
              gold_probs = tf.gather_nd(dist, indices)  # shape (batch_size). prob of correct words on this step
              gold_probs = gold_probs + 1.0e-20  # For numeric stability

              # gold_probs = tf.Print(gold_probs, [tf.reduce_max(gold_probs), tf.reduce_min(gold_probs)], message="gold_probs_min_max")

              losses = -tf.log(gold_probs)
              return losses

            loss_per_step = tf.map_fn(calculate_loss_per, (dec_steps, final_dists),
                                      parallel_iterations=max(hparams.batch_size, hparams.parallel_iterations),
                                      dtype=tf.float32, swap_memory=True)

            # loss_per_step = []  # will be list length max_dec_steps containing shape (batch_size)
            # batch_nums = tf.range(0, limit=self.batch_size)  # shape (batch_size)
            # for dec_step, dist in enumerate(final_dists):
            #   targets = self.iterator.target_output[:, dec_step]  # The indices of the target words. shape (batch_size)
            #   indices = tf.stack((batch_nums, targets), axis=1)  # shape (batch_size, 2)
            #   gold_probs = tf.gather_nd(dist, indices)  # shape (batch_size). prob of correct words on this step
            #   losses = -tf.log(gold_probs)
            #   loss_per_step.append(losses)

            # loss_per_step = tf.Print(loss_per_step, [loss_per_step, final_sequence_lengths], message="loss_per_step: ", summarize=100)

            loss = self._mask_and_avg(loss_per_step, max_output_length, labels["target_sequence_length"])

            # print("Variables: \n", )
            # final_dist_id = tf.argmax(final_dists, axis=-1, output_type=tf.int32)
            # sample_id = tf.transpose(sample_id)

            # Punish the pointer mechanism if it points to word in the vocab
            # in_vocab_term = tf.reduce_mean(
            #   tf.cast(tf.logical_and(final_dist_id >= hparams.projection_vocab_size,
            #                          tf.logical_and(vocab_utils.UNK_ID < sample_id,
            #                                         sample_id < hparams.projection_vocab_size)), dtype=tf.float32))

            # in_vocab_term = tf.Print(in_vocab_term, [in_vocab_term], message="in_vocab_term: ", summarize=100)

            # loss = loss + 0.1 * in_vocab_term

            if hparams.add_coverage_loss:
              coverage_term = self._compute_coverage_loss(alignment_history, final_sequence_lengths)
              coverage_term = tf.multiply(hparams.cov_loss_wt, coverage_term)

              # coverage_term = tf.Print(coverage_term, [coverage_term], message="COVERAGE TERM")

              # Add the coverage termn to the loss function
              loss = tf.add(loss, coverage_term)

            # loss = tf.Print(loss, [loss], message="loss: ", summarize=100)

          else:
            loss = self.setup_loss_computation(hparams, logits, labels,
                                               final_context_state=final_context_state,
                                               final_sequence_lengths=final_sequence_lengths)

          # Regularize p_gens parameters (They tend to quickly explode)
          if hparams.p_gen_l2_scale > 0:
            l2 = hparams.p_gen_l2_scale * sum(tf.nn.l2_loss(tf_var) for tf_var in
                                              tf.trainable_variables(
                                                "create_train_model_template/sequence_decoder/calculate_pgen/"))

            loss = loss + l2

        # The hierarchical decoder is not used during inference (It's meant to guide the training)
        # hierarchical_logits, hierarchical_final_sequence_lengths = self._build_hierarchical_decoder(
        #  encoded_outputs, encoded_state, hparams)

        # Unsupervised Hierarchical loss
        with tf.variable_scope("hierarchical_loss"):
          print("logits: ", logits)
          # print("hierarchical_logits: ", hierarchical_logits)

          # hierarchical_logits = tf.Print(hierarchical_logits, [tf.shape(logits), tf.shape(hierarchical_logits)], message="logits: ", summarize=100)

          loss = loss  # + self.setup_loss_computation(hparams, hierarchical_logits, self.features["source"], final_context_state=None,
          #                             final_sequence_lengths=hierarchical_final_sequence_lengths)

        # loss = tf.Print(loss, [loss], message="LOSS: ")
      else:
        loss = None

    return logits, loss, final_context_state, sample_id

  def _build_encoder(self, hparams, features):

    if self.is_hierarchical:
      return self._build_hierarchical_encoder(hparams, features)
    else:
      return self._build_flat_encoder(hparams, features)

  def _build_flat_encoder(self, hparams, features):

    """Build an encoder."""
    num_layers = hparams.num_layers
    num_residual_layers = hparams.num_residual_layers

    # iterator = self.iterator

    source = features["source"]
    if self.time_major:
      source = tf.transpose(source)

    with tf.variable_scope("encoder") as scope:
      dtype = scope.dtype
      # Look up embedding, emp_inp: [max_time, batch_size, num_units]

      encoder_emb_inp = self.create_look_up_fn(self.embedding_encoder)(source)  # tf.nn.embedding_lookup(, )

      # Encoder_outpus: [max_time, batch_size, num_units]
      if hparams.encoder_type == "uni":
        utils.print_out("  num_layers = %d, num_residual_layers=%d" %
                        (num_layers, num_residual_layers))
        cell = self._build_encoder_cell(
          hparams, num_layers, num_residual_layers)

        encoder_outputs, encoder_state = tf.nn.dynamic_rnn(
          cell,
          encoder_emb_inp,
          dtype=dtype,
          parallel_iterations=min(hparams.batch_size, hparams.parallel_iterations),
          swap_memory=True,
          sequence_length=features["source_sequence_length"],
        time_major = self.time_major)
      elif hparams.encoder_type == "bi":
        num_bi_layers = num_layers
        num_bi_residual_layers = num_residual_layers
        utils.print_out("  num_bi_layers = %d, num_bi_residual_layers=%d" %
                        (num_bi_layers, num_bi_residual_layers))

        encoder_outputs, bi_encoder_state = (
          self._build_bidirectional_rnn(
            inputs=encoder_emb_inp,
            sequence_length=features["source_sequence_length"],
        dtype=dtype,
        hparams=hparams,
        num_bi_layers=num_bi_layers,
        num_bi_residual_layers=num_bi_residual_layers))

        if num_bi_layers == 1:
          encoder_state = bi_encoder_state
        else:
          # alternatively concat forward and backward states
          last_fw_state = bi_encoder_state[0][-1]
          last_bw_state = bi_encoder_state[1][-1]

          encoder_state = map_structure_up_to(last_fw_state,
                                              lambda fw, bw: tf.concat([fw, bw],
                                                                       -1),
                                              last_fw_state,
                                              last_bw_state)

      else:
        raise ValueError("Unknown encoder_type %s" % hparams.encoder_type)
    return encoder_outputs, encoder_state

  def _build_hierarchical_encoder(self, hparams, features):
    """Build an encoder."""

    num_hierarchical_levels = len(hparams.hierarchical_split_tokens)
    num_layers_per_level = hparams.num_layers
    num_residual_layers = hparams.num_residual_layers

    # iterator = self.iterator

    source = features["source"]

    batch_size = tf.shape(source)[0]

    source_sequence_length = features["source_sequence_length"]
    print("build_encoder, source_sequence_length: ", source_sequence_length)
    source_sequence_element_counts = tf.count_nonzero(features["source_sequence_length"], axis=2, dtype=tf.int32)
    encoder_emb_inp = None
    bi_encoder_state = None

    encoder_level_outputs = []
    encoder_level_memories = []

    assert num_hierarchical_levels > 0
    for level_number in range(num_hierarchical_levels):

      level_lengths = source_sequence_length[:, level_number]
      print("source_sequence_element_counts: ", source_sequence_element_counts)
      level_element_count = source_sequence_element_counts[:, level_number]  # Essentially the length of lengths
      # max_level_element_count = tf.reduce_max(level_element_count)
      print("level_element_count: ", level_element_count)
      level_split_indexes = self.split_indexes[:, level_number]

      with tf.variable_scope("encoder_level_%d" % level_number, initializer=get_initializer(
          hparams.rnn_init_op, hparams.random_seed, hparams.init_weight)) as scope:
        # dtype = scope.dtype
        # Look up embedding, emp_inp: [max_time, batch_size, num_units]

        # If first encoder level lookup embeddings
        if encoder_emb_inp is None:
          # with tf.device('/cpu:0'):
          encoder_emb_inp = source

        num_bi_layers = int(num_layers_per_level)
        num_bi_residual_layers = int(num_residual_layers)
        utils.print_out("  num_bi_layers = %d, num_bi_residual_layers=%d" %
                        (num_bi_layers, num_bi_residual_layers))

        # Construct forward and backward multi-layer cells
        with tf.variable_scope("fw", initializer=get_initializer(hparams.rnn_init_op, hparams.random_seed,
                                                                 hparams.init_weight)):
          fw_cell = self._build_encoder_cell(hparams,
                                             num_bi_layers,
                                             num_bi_residual_layers, num_units=hparams.encoder_num_units)
        with tf.variable_scope("bw", initializer=get_initializer(hparams.rnn_init_op, hparams.random_seed,
                                                                 hparams.init_weight)):
          bw_cell = self._build_encoder_cell(hparams,
                                             num_bi_layers,
                                             num_bi_residual_layers, num_units=hparams.encoder_num_units)

        encoder_outputs, encoder_states, memory = (
          self._build_bidirectional_async_timescale_encoder_level(
            hparams,
            batch_size=batch_size,
            fw_cell=fw_cell,
            bw_cell=bw_cell,
            inputs=encoder_emb_inp,
            sequence_partitioning=level_lengths,
            splits=level_split_indexes,
            output_lengths=level_element_count,
            level_number=level_number,
            is_first_level=level_number == 0))

        encoder_level_outputs.append(encoder_outputs)
        encoder_level_memories.append(memory)

        # Assign input (input embeddings for next hierarchical level)
        encoder_emb_inp = encoder_outputs
        bi_encoder_state = encoder_states

    print("encoder_emb_inp: ", encoder_emb_inp)
    print("bi_encoder_state: ", bi_encoder_state)

    return encoder_level_memories[0] if hparams.pointer_generation else encoder_level_outputs[-1], bi_encoder_state

  def _build_decoder(self, encoded_outputs_ta, encoded_state_ta, hparams):
    pass

  def _build_sequence_decoder(self, hparams, features, labels, encoded_outputs, encoded_state):
    """Build and run a RNN decoder with a final projection layer.

          Args:
            encoder_outputs: The outputs of encoder for every time step.
            encoder_state: The final state of the encoder.
            hparams: The Hyperparameters configurations.

          Returns:
            A tuple of final logits and final decoder state:
              logits: size [time, batch_size, vocab_size] when time_major=True.
          """
    # p_gens = None
    # attn_dists = None
    final_dists = None
    alignment_history = None

    ## Decoder.
    with tf.variable_scope("sequence_decoder", initializer=get_initializer(
        hparams.rnn_init_op, hparams.random_seed, hparams.init_weight)) as decoder_scope:

      # We are no longer working hierarchically
      # Pick the lengths of the last hierarchical level

      if self.is_hierarchical:
        orginal_input_sequence_lengths = features["source_sequence_length"][:, 0]
        orginal_input_sequence_lengths = tf.reduce_sum(orginal_input_sequence_lengths,
                                                       axis=1)  # Sum subsequence lengths

        encoded_sequence_lengths = features["source_sequence_length"][:, -1]
        encoded_sequence_lengths = tf.reduce_sum(encoded_sequence_lengths, axis=1)  # Sum subsequence lengths

        encoded_state = map_structure(lambda state: state[-1, :, :], encoded_state)  # .read(state_length - 1)
      else:
        orginal_input_sequence_lengths = features["source_sequence_length"]

        encoded_sequence_lengths = features["source_sequence_length"]

        # orginal_input_sequence_lengths = tf.Print(orginal_input_sequence_lengths, [orginal_input_sequence_lengths, tf.shape(encoded_outputs)], summarize=100, message="orginal_input_sequence_lengths")

        encoded_state = encoded_state  # .read(state_length - 1)

      # if hparams.pointer_generation:
      #   memory_sequence_lengths = self.features["source"]_sequence_length[:, 0]
      #   memory_sequence_lengths = tf.reduce_sum(memory_sequence_lengths, axis=1)
      # else:
      #   memory_sequence_lengths = None

      # Read the last state

      print("decoder encoded_state:", encoded_state)
      print("decoder encoded_outputs:", encoded_outputs)
      print("decoder level_lengths: ", encoded_sequence_lengths)

      tgt_sos_id = tf.cast(self.tgt_vocab_table.lookup(tf.constant(hparams.sos)),
                           tf.int32)
      tgt_eos_id = tf.cast(self.tgt_vocab_table.lookup(tf.constant(hparams.eos)),
                           tf.int32)

      # iterator = self.iterator

      # maximum_iteration: The maximum decoding steps.
      if hparams.tgt_max_len_infer:
        maximum_iterations = hparams.tgt_max_len_infer
        utils.print_out("  decoding maximum_iterations %d" % maximum_iterations)
      else:
        # TODO(thangluong): add decoding_length_factor flag
        decoding_length_factor = 2.0
        max_encoder_length = tf.reduce_max(orginal_input_sequence_lengths)
        maximum_iterations = tf.to_int32(tf.round(
          tf.to_float(max_encoder_length) * decoding_length_factor))

      def initial_layer_wrapper(initial_layer_cell):
        return wrap_decoder_cell_in_attention(
          hparams,
          self,
          initial_layer_cell,
          hparams.encoder_num_units * 2,
          # Double the amount of units in the sequence decoder to match bi-lstm in encoder
          encoded_outputs,
          encoded_sequence_lengths,
          memory_length=orginal_input_sequence_lengths)

      self.sequence_decoder_cell = self._build_decoder_cell(
        hparams,
        initial_layer_wrapper=initial_layer_wrapper
      )

      if hparams.pointer_generation:
        self.sequence_decoder_cell = PointerGeneratorProbabilityWrapper(self.sequence_decoder_cell, self.output_layer,
                                                                        mode=self.mode,
                                                                        extended_vsize=self.hparams.src_max_len,
                                                                        initializer=get_initializer(
                                                                          hparams.init_op, hparams.random_seed,
                                                                          hparams.init_weight),
                                                                        softmax_vocab_dist=False)

      decoder_initial_state = create_intial_decoder_attention_cell_state(hparams, self, self.sequence_decoder_cell,
                                                                         encoded_state)

      print("decoder_initial_state: ", decoder_initial_state)

      ## Train or eval
      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        # decoder_emp_inp: [max_time, batch_size, num_units]
        target_input = labels["target_input"]

        # device_type = "GPU" if self.first_gpu_id >= 0 else "CPU"
        # device_index = self.first_gpu_id if self.first_gpu_id >= 0 else 0
        # with tf.device(tf.DeviceSpec(device_type=device_type, device_index=device_index)):
        # with tf.device('/cpu:0'):
        if self.time_major:
          target_input = tf.transpose(target_input)

        self.decoder_emb_inp = self.create_look_up_fn(self.embedding_decoder, cut_to_size=True)(
          target_input)  # tf.nn.embedding_lookup(self.embedding_decoder, target_input)

        # target_sequence_length = tf.Print(
        #   iterator.target_sequence_length,
        #   [tf.reduce_max(iterator.target_sequence_length)],
        #   message="maximum iterator.target_sequence_length:", summarize=10)

        print("decoder_emb_inp: ", self.decoder_emb_inp)

        # Helper
        helper = tf.contrib.seq2seq.TrainingHelper(
          self.decoder_emb_inp, labels["target_sequence_length"],
          time_major=self.time_major)

        # Decoder
        # *Heuristic* if targets are more than 50 long we want to calculate logits at each timestep

        my_decoder = tf.contrib.seq2seq.BasicDecoder(
          self.sequence_decoder_cell,
          helper,
          decoder_initial_state, )

        # Dynamic decoding
        outputs, final_context_state, final_sequence_lengths = tf.contrib.seq2seq.dynamic_decode(
          my_decoder,
          output_time_major=self.time_major,
          # parallel_iterations=32,  # min(hparams.batch_size, 32),
          swap_memory=True,
          scope=decoder_scope)

        sample_id = outputs.sample_id

        # Note: there's a subtle difference here between train and inference.
        # We could have set output_layer when create my_decoder
        #   and shared more code between train and inference.
        # We chose to apply the output_layer to all timesteps for speed:
        #   10% improvements for small models & 20% for larger ones.
        # If memory is a concern, we should apply output_layer per timestep.

        # Marhlder: *Heuristic* if targets are more than 50 long we want to calculate logits at each timestep
        # if hparams.tgt_max_len > 50 or hparams.use_sampled_softmax:
        self.logits = outputs.rnn_output
        # else:
        #   # else we calculate logits for entire batch and all timesteps at once (Faster on GPU's)
        #   self.logits = self.output_layer(outputs.rnn_output)

        # print("final_context_state: ", final_context_state)
        # self.logits = tf.Print(self.logits, [tf.reduce_sum(final_context_state[0].alignments)],
        #                                  message="Attention? : ", summarize=100)

      ## Inference
      else:
        beam_width = hparams.beam_width
        length_penalty_weight = hparams.length_penalty_weight
        start_tokens = tf.fill([self.batch_size], tgt_sos_id)
        end_token = tgt_eos_id

        if beam_width > 0:  # BeamSearchDecoder does not support TensorArrays in RNN state yet as of tf 1.5
          my_decoder = tf.contrib.seq2seq.BeamSearchDecoder(
            cell=self.sequence_decoder_cell,
            embedding=self.create_look_up_fn(self.embedding_decoder, cut_to_size=hparams.pointer_generation),
            start_tokens=start_tokens,
            end_token=end_token,
            initial_state=decoder_initial_state,
            beam_width=beam_width,
            output_layer=None if hparams.pointer_generation else self.output_layer,
            length_penalty_weight=length_penalty_weight)
        elif hparams.pointer_generation:

          def pgen_sample_fn(outputs):

            sample_ids = tf.argmax(outputs, axis=-1, output_type=tf.int32)

            return sample_ids

          def pgen_end_fn(sample_ids):
            finished = tf.equal(sample_ids, end_token)
            all_finished = tf.reduce_all(finished)
            return all_finished

          helper = tf.contrib.seq2seq.InferenceHelper(
            sample_fn=pgen_sample_fn,
            end_fn=pgen_end_fn,
            sample_shape=tensor_shape.TensorShape([]),
            sample_dtype=tf.int32,
            start_inputs=self.create_look_up_fn(self.embedding_decoder, cut_to_size=hparams.pointer_generation)(
              start_tokens),
            next_inputs_fn=self.create_look_up_fn(self.embedding_decoder, cut_to_size=hparams.pointer_generation)
          )

          my_decoder = tf.contrib.seq2seq.BasicDecoder(
            self.sequence_decoder_cell,
            helper,
            decoder_initial_state,
            # output_layer=self.output_layer  # applied per timestep
          )

        else:
          # Helper
          # with tf.device('/cpu:0'):
          helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(
            self.create_look_up_fn(self.embedding_decoder), start_tokens, end_token)

          # Decoder
          my_decoder = tf.contrib.seq2seq.BasicDecoder(
            self.sequence_decoder_cell,
            helper,
            decoder_initial_state,
            # output_layer=self.output_layer  # applied per timestep
          )

        # maximum_iterations = tf.Print(maximum_iterations, [maximum_iterations], message="maximum_iterations", summarize=100)

        # Dynamic decoding
        outputs, final_context_state, final_sequence_lengths = tf.contrib.seq2seq.dynamic_decode(
          my_decoder,
          maximum_iterations=maximum_iterations,
          # parallel_iterations=32,
          output_time_major=self.time_major,
          swap_memory=True,
          scope=decoder_scope)

        if beam_width > 0:
          # self.logits = tf.no_op()
          self.logits = None  # outputs.beam_search_decoder_output.scores
          sample_id = outputs.predicted_ids
        else:
          self.logits = outputs.rnn_output
          sample_id = outputs.sample_id

      if self.mode == tf.contrib.learn.ModeKeys.INFER:
        sample_id = tf.Print(sample_id, [sample_id], message="sample_ids: ", summarize=1000)
        # self.logits = tf.Print(self.logits , [self.iterator.target_output], message="target_ids: ", summarize=1000)
      # if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
      #   self.logits = tf.Print(self.logits , [sample_id], message="sample_ids: ", summarize=1000)
      #   self.logits = tf.Print(self.logits , [self.iterator.target_output], message="target_ids: ", summarize=1000)

      if hparams.pointer_generation and self.mode == tf.contrib.learn.ModeKeys.EVAL:
        # p_gens = final_context_state.p_gens.stack()
        # alignment_history = final_context_state.cell_state[0].alignment_history.stack()
        # attn_dists = alignment_history
        #
        # org_sample_id = sample_id
        # vocab_dists = tf.nn.softmax(self.logits)
        # final_dists = self.calc_final_dist(vocab_dists, attn_dists, p_gens)
        # self.logits = None
        sample_id = tf.argmax(self.logits, axis=-1, output_type=tf.int32)
        #
        # # sample_id = tf.Print(sample_id, [final_dists, attn_dists], message="final_dists: ", summarize=100)
        #
        # # We need to map the sample ids from final_dists to the actual vocabulary

        # sample_id = tf.Print(sample_id, [tf.reduce_any(sample_id > hparams.projection_vocab_size)], summarize=hparams.batch_size, message="Prediction includes oov: ")

        sample_id = self.map_final_dist_ids_to_vocab(tf.transpose(sample_id))

        sample_id = tf.transpose(sample_id)

      elif hparams.pointer_generation and self.mode == tf.contrib.learn.ModeKeys.INFER:

        if hparams.beam_width > 0:
          print("sample_id!: ", sample_id)
          # sample_id = tf.Print(sample_id, [tf.shape(sample_id)], message="beamsearch output shape: ", summarize=100)

          if hparams.combine_beam_search_to_strings:
            sample_id, final_sequence_lengths = self.combine_beam_search_to_strings(sample_id)
          else:
            sample_id = sample_id[:, :, 0]

            sample_id = self.map_final_dist_ids_to_vocab(tf.transpose(sample_id), self.reverse_tgt_vocab_table,
                                                         map_to_strings=True)


        else:
          sample_id = self.map_final_dist_ids_to_vocab(tf.transpose(sample_id), self.reverse_tgt_vocab_table,
                                                       map_to_strings=True)

        sample_id = tf.transpose(sample_id)
      else:
        sample_id = None

      # print("org_sample_id: ", org_sample_id)
      # print("sample_id: ", sample_id)
      # print("outputs.sample_id: ", outputs.sample_id)

    return self.logits, sample_id, final_context_state, final_sequence_lengths, self.logits, alignment_history  # , target_output

  def _build_hierarchical_decoder(self, hparams, features, labels, encoded_outputs, encoded_state):
    """Build and run a RNN decoder with a final projection layer.

    Args:
      encoded_outputs: The outputs of encoder for every time step.
      encoded_state: The final states of the encoder.
      hparams: The hyperparameter configurations.

    Returns:
      A tuple of final logits and final decoder state:
        logits: size [time, batch_size, vocab_size] when time_major=True.
    """

    # iterator = self.iterator

    num_hierarchical_levels = len(hparams.hierarchical_split_tokens)

    assert num_hierarchical_levels > 0

    with tf.variable_scope("hierarchical_decoder"):
      source_sequence_length = features["source_sequence_length"]

      source_sequence_element_counts = tf.count_nonzero(features["source_sequence_length"]
                                                        , 2, dtype=tf.int32,
                                                        keep_dims=True)
      batch_size = tf.shape(source_sequence_length)[0]
      decoded_outputs, decoded_states = None, None

      # When decoding we run through the hierarchy in reverse,
      # first level (or last level in the decoding)
      # will be handled separately as it will have to project to a vocabulary (Hence the start from idx 1)
      for level_number in reversed(range(0, num_hierarchical_levels)):
        level_subsequence_lengths = source_sequence_element_counts[:, level_number]  # Essentially the length of lengths
        print("hi-decoder source_sequence_element_counts: ", source_sequence_element_counts)
        level_output_lengths = tf.reduce_sum(source_sequence_length[:, level_number], axis=-1)
        print("hi-decoder level_output_lengths: ", level_output_lengths)
        level_split_indexes = self.split_indexes[:, level_number]

        ## Decoder.
        with tf.variable_scope("decoder_level_%d" % level_number) as decoder_scope:
          cell = self._build_decoder_cell(
            hparams,
            hparams.num_units * 2,
          )

          print("decoded_outputs, org: ", encoded_outputs)

          # level_output_lengths = tf.Print(level_output_lengths, [level_output_lengths], message="level %d element count: " %level_number, summarize=100)

          decoded_outputs, decoded_states = self._build_async_timescale_decoder_level(
            hparams,
            batch_size,
            cell,
            encoded_outputs,
            encoded_state,
            level_split_indexes,
            level_output_lengths,
            level_subsequence_lengths)

          # Prepare data for next hierarchical level
          encoded_outputs = decoded_outputs
          encoded_state = decoded_states

          print("decoded_outputs!!: ", encoded_outputs)

      # Reshape output to match expectations of loss function
      final_sequence_lengths = tf.reduce_sum(source_sequence_length[:, -1], axis=-1)

      # decoded_outputs = tf.Print(decoded_outputs, [tf.shape(decoded_outputs), final_sequence_lengths], message="hi-decoder output: ", summarize=100)

    return decoded_outputs, final_sequence_lengths  # , target_output

  def _build_bidirectional_rnn(self, inputs, sequence_length,
                               dtype, hparams,
                               num_bi_layers,
                               num_bi_residual_layers):
    """Create and call biddirectional RNN cells.

    Args:
      num_residual_layers: Number of residual layers from top to bottom. For
        example, if `num_bi_layers=4` and `num_residual_layers=2`, the last 2 RNN
        layers in each RNN cell will be wrapped with `ResidualWrapper`.

    Returns:
      The concatenated bidirectional output and the bidirectional RNN cell"s
      state.
    """

    # Construct forward and backward cells
    # Construct forward and backward multi-layer cells
    with tf.variable_scope("fw", initializer=get_initializer(hparams.rnn_init_op, hparams.random_seed,
                                                             hparams.init_weight)
                           ):
      fw_cell = self._build_encoder_cell(hparams,
                                         num_bi_layers,
                                         num_bi_residual_layers, num_units=hparams.encoder_num_units)
    with tf.variable_scope("bw", initializer=get_initializer(hparams.rnn_init_op, hparams.random_seed,
                                                             hparams.init_weight)
                           ):
      bw_cell = self._build_encoder_cell(hparams,
                                         num_bi_layers,
                                         num_bi_residual_layers, num_units=hparams.encoder_num_units)

    bi_outputs, bi_state = tf.nn.bidirectional_dynamic_rnn(
      fw_cell,
      bw_cell,
      inputs,
      dtype=dtype,
      sequence_length=sequence_length,
      swap_memory=True,
      time_major=self.time_major)

    return tf.concat(bi_outputs, -1), bi_state

  def _build_bidirectional_async_timescale_encoder_level(self,
                                                         hparams,
                                                         batch_size,
                                                         fw_cell, bw_cell,
                                                         inputs, sequence_partitioning, splits,
                                                         output_lengths,
                                                         level_number,
                                                         is_first_level,
                                                         initial_state_fw=None, initial_state_bw=None):
    """Create and call biddirectional RNN cells.
    Returns:
      The concatenated bidirectional output and the bidirectional RNN cell"s
      state.
    """
    input_lengths = tf.reduce_sum(sequence_partitioning, axis=-1)
    max_input_lengths = tf.reduce_max(input_lengths)

    # Convert splits to indexes
    split_indexes = tf.maximum(splits - 1, 0)

    print("_build_bidirectional_async_timescale_rnn, inputs: ", inputs.get_shape())

    if is_first_level:

      bi_output_element_size = fw_cell.output_size + bw_cell.output_size

      source_unstacked_ta = tf.TensorArray(dtype=tf.int32, size=self.batch_size, dynamic_size=False).unstack(inputs)
      partitioning_unstacked_ta = tf.TensorArray(dtype=tf.int32, size=self.batch_size, dynamic_size=False).unstack(
        sequence_partitioning)

      outputs_ta = tf.TensorArray(dtype=tf.float32, size=self.batch_size, dynamic_size=False)

      def one_batch_element_as_batch(batch_index, outputs_ta):

        batch_element_source = source_unstacked_ta.read(batch_index)
        batch_element_partitioning = partitioning_unstacked_ta.read(batch_index)

        # Remove padding of partitioning
        number_of_partitions = tf.count_nonzero(batch_element_partitioning, dtype=tf.int32)
        batch_element_partitioning = batch_element_partitioning[:number_of_partitions]

        # remove padding of source sequence:
        sequence_length = input_lengths[batch_index]
        batch_element_source = batch_element_source[:sequence_length]

        max_subsequence_length = tf.reduce_max(batch_element_partitioning)

        batch_element_inputs_embeddings = self.create_look_up_fn(self.embedding_encoder)(batch_element_source)

        embedding_size = batch_element_inputs_embeddings.get_shape()[-1].value

        print("embedding_size: ", embedding_size)

        partitioned_batch_element_inputs_ta = tf.TensorArray(dtype=tf.float32, size=number_of_partitions,
                                                             dynamic_size=False, infer_shape=False,
                                                             element_shape=[None, embedding_size]).split(
          batch_element_inputs_embeddings,
          batch_element_partitioning)

        padded_partitioned_batch_element_inputs_ta = tf.TensorArray(dtype=tf.float32, size=number_of_partitions,
                                                                    dynamic_size=False, infer_shape=False,
                                                                    element_shape=[None, embedding_size])

        def pad(tensor):
          tensor_shape = tf.shape(tensor)
          last_dim_size = tensor.get_shape()[-1].value

          padded_tensor = tf.concat(
            [tensor, tf.zeros([max_subsequence_length - tensor_shape[0], last_dim_size])], 0)

          return padded_tensor

        cond = lambda index_counter, _, __: index_counter < partitioned_batch_element_inputs_ta.size()
        _, __, padded_partitioned_batch_element_inputs_ta = tf.while_loop(cond, lambda index_counter, split_source_ta,
                                                                                       padded_split_source_ta: (
          index_counter + 1, split_source_ta, padded_split_source_ta.write(index_counter,
                                                                           pad(split_source_ta.read(index_counter)))),
                                                                          loop_vars=(
                                                                            tf.constant(0),
                                                                            partitioned_batch_element_inputs_ta,
                                                                            padded_partitioned_batch_element_inputs_ta))

        padded_split_batch_element_inputs = padded_partitioned_batch_element_inputs_ta.stack()

        print("padded_split_batch_element_inputs: ", padded_split_batch_element_inputs)

        partitioned_source_tm = reverse_time_batch_major(padded_split_batch_element_inputs)

        print("partitioned_source_tm", partitioned_source_tm)

        ((output_fw, output_bw), (unused_output_state_fw, unused_output_state_bw)) = tf.nn.bidirectional_dynamic_rnn(
          fw_cell, bw_cell,
          partitioned_source_tm,
          dtype=tf.float32,
          time_major=True,
          swap_memory=True,
          parallel_iterations=1,  # This shouldn't matter as the next iteration always depends on the previous
          sequence_length=batch_element_partitioning)

        bi_output = tf.concat([output_fw, output_bw], 0)

        flat_bi_output = tf.reshape(bi_output, [-1, bi_output_element_size])

        print("flat_bi_output: ", flat_bi_output)

        # Prepend a zero vector for use as padding via gather operation
        zero_prepend_flat_bi_output = tf.concat([tf.zeros([1, bi_output_element_size]), flat_bi_output], 0)

        multipliers = tf.range(1, number_of_partitions + 1, dtype=tf.int32)

        flat_bi_output_indexes_ta = tf.TensorArray(dtype=tf.int32, size=number_of_partitions,
                                                   dynamic_size=False, infer_shape=False,
                                                   element_shape=[None])

        def create_indexes(counter, flat_bi_output_indexes_ta):
          partitioning = batch_element_partitioning[counter]
          multiplier = multipliers[counter]

          # Multiply range of partitioning with multiplier to get the correct index
          indexes_by_row = (tf.range(0, partitioning, dtype=tf.int32) * multiplier)
          # Offset indexes by 1 (index zero has a special meaning as padding)
          offset_indexes = indexes_by_row + 1

          return counter + 1, flat_bi_output_indexes_ta.write(counter, offset_indexes)  # padded_indexes

        _, flat_bi_output_indexes_ta = tf.while_loop(cond=lambda counter, _: counter < number_of_partitions,
                                                     body=create_indexes,
                                                     loop_vars=(tf.constant(0), flat_bi_output_indexes_ta))

        # collect indexes in a single vector
        flat_bi_output_indexes = flat_bi_output_indexes_ta.concat()

        padded_flat_bi_output_indexes = tf.concat(
          [flat_bi_output_indexes, tf.zeros([max_input_lengths - sequence_length], dtype=tf.int32)], 0)

        # Zero indexes in flat_bi_output_indexes correspond to padding
        output_sequence = tf.gather(zero_prepend_flat_bi_output, padded_flat_bi_output_indexes)

        outputs_ta = outputs_ta.write(batch_index, output_sequence)

        return batch_index + 1, outputs_ta

      cond = lambda batch_element_counter, _: batch_element_counter < self.batch_size
      _, outputs_ta = tf.while_loop(cond, one_batch_element_as_batch, (tf.constant(0), outputs_ta), swap_memory=True,
                                    parallel_iterations=1)

      memory_bm = outputs_ta.stack()
      memory = reverse_time_batch_major(memory_bm)

      # memory = tf.Print(memory, [tf.shape(memory)], message="memory shape: ", summarize=100)

      print("memory_bm: ", memory_bm)
      states = None

      embeddings_tm = pick_subsequence_elements(memory, split_indexes)

    else:

      with tf.variable_scope('forward_raw_rnn'):
        fw_loop_fn = create_encoder_level_loop_fn(fw_cell, inputs, batch_size, input_lengths,
                                                  splits, initial_state=initial_state_fw,
                                                  stop_gradients_for_splits=is_first_level)

        fw_embeddings_and_state_ta_with_junk, fw_final_state, _ = tf.nn.raw_rnn(
          fw_cell,
          fw_loop_fn,
          # parallel_iterations=32,
          swap_memory=True
        )

        fw_embeddings_and_state_with_junk = map_structure(lambda structure: structure.stack(),
                                                          fw_embeddings_and_state_ta_with_junk)

        # fw_embeddings_tm = tf.Print(fw_embeddings_tm, [tf.shape(inputs), tf.shape(fw_embeddings_tm), splits, output_lengths], message="level number %d in/out sizes: " % level_number, summarize=100)

      with tf.variable_scope('backward_raw_rnn'):
        # inputs = tf.Print(inputs, [tf.shape(inputs), sequence_partitioning, input_lengths], message="inputs: ", summarize=100)

        bw_input = reverse_sequence(inputs, input_lengths, 0, 1)

        inverted_splits = create_invert_splits(sequence_partitioning)

        bw_loop_fn = create_encoder_level_loop_fn(bw_cell, bw_input, batch_size, input_lengths,
                                                  inverted_splits, initial_state=initial_state_bw,
                                                  stop_gradients_for_splits=is_first_level)

        bw_embeddings_and_state_ta_with_junk, bw_final_state, _ = tf.nn.raw_rnn(
          bw_cell,
          bw_loop_fn,
          # parallel_iterations=32,
          swap_memory=True
        )

        bw_embeddings_and_state_with_junk = map_structure(lambda structure: structure.stack(),
                                                          bw_embeddings_and_state_ta_with_junk)

        # Convert splits to indexes
        inverted_split_indexes = tf.maximum(inverted_splits - 1, 0)

        def reverse_sequence_and_pick_subsequence_elements(structure):
          return reverse_sequence(
            pick_subsequence_elements(
              structure,
              inverted_split_indexes),
            output_lengths, 0, 1)

      with tf.name_scope("output"):

        fw_embeddings_tm, fw_states_tm = map_structure(
          lambda structure: pick_subsequence_elements(structure, split_indexes),
          fw_embeddings_and_state_with_junk)

        bw_embeddings_tm, bw_states_tm = map_structure(
          reverse_sequence_and_pick_subsequence_elements,
          bw_embeddings_and_state_with_junk)

        if hparams.pointer_generation and level_number == 0:
          fw_memory, _ = fw_embeddings_and_state_with_junk
          bw_memory, _ = bw_embeddings_and_state_with_junk
          bw_memory = reverse_sequence(bw_memory, output_lengths, 0, 1)

          memory = tf.concat([fw_memory, bw_memory], -1)
        else:
          memory = None

      # states = tf.Print(states, [tf.shape(states)], message="states shape: ", summarize=100)

      embeddings_tm = tf.concat([fw_embeddings_tm, bw_embeddings_tm], -1)
      states = map_structure_up_to(fw_states_tm,
                                   lambda fw, bw: tf.concat([fw, bw],
                                                            -1),
                                   fw_states_tm,
                                   bw_states_tm)  # tf.concat([fw_states_tm.c, bw_states_tm.c], -1)

    return embeddings_tm, states, memory

  def _build_async_timescale_decoder_level(self,
                                           hparams,
                                           batch_size,
                                           cell,
                                           encoded_inputs,
                                           encoded_state,
                                           split_indexes,
                                           output_lengths,
                                           input_subsequence_lengths,
                                           ):

    loop_fn = create_decoder_level_loop_fn(hparams, self, cell, encoded_inputs, encoded_state, batch_size,
                                           output_lengths, split_indexes, input_subsequence_lengths)

    (decoded_outputs, decoded_states), _, __ = tf.nn.raw_rnn(  # (fw_embeddings_with_junk_ta, fw_states_with_junk_ta)
      cell,
      loop_fn,
      parallel_iterations=max(hparams.batch_size, hparams.parallel_iterations),
      swap_memory=True
    )

    decoded_outputs = decoded_outputs.stack()
    decoded_states = map_structure(lambda structure: structure.stack(), decoded_states)

    return decoded_outputs, decoded_states

  def _build_decoder_cell(self, hparams, num_units=None, initial_layer_wrapper=None, **kwargs):

    num_layers = hparams.num_decoder_layers
    num_residual_layers = hparams.num_residual_layers

    if not num_units:
      num_units = hparams.num_units

    cell = create_rnn_cell(
      unit_type=hparams.unit_type,
      num_units=num_units,
      initial_layer_wrapper=initial_layer_wrapper,
      # Double the amount of units in the sequence decoder to match bi-lstm in encoder
      initial_layer_num_units=hparams.encoder_num_units * 2,
      num_layers=num_layers,
      num_residual_layers=num_residual_layers,
      forget_bias=hparams.forget_bias,
      dropout=hparams.dropout,
      zoneout=hparams.zoneout,
      mode=self.mode,
      single_cell_fn=self.single_cell_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes
    )

    return cell

  def _mask_and_avg(self, values, max_output_length, target_sequence_length):
    """Applies mask to values then returns overall average (a scalar)
    Args:
      values: a list length max_dec_steps containing arrays shape (batch_size).
      padding_mask: tensor shape (batch_size, max_dec_steps) containing 1s and 0s.
    Returns:
      a scalar
    """

    target_weights = tf.sequence_mask(
      target_sequence_length, max_output_length, dtype=values.dtype)

    target_weights = tf.transpose(target_weights)

    masked_loss = values * target_weights

    masked_loss = tf.reduce_sum(masked_loss) / tf.to_float(self.batch_size)  # shape (batch_size); normalized value for each batch member
    return masked_loss  # overall average

  def _compute_coverage_loss(self, alignment_history, lengths):

    with tf.name_scope("coverage_loss"):
      size = tf.shape(alignment_history)[0]
      self.alignment_history = alignment_history
      i = (alignment_history, tf.zeros_like(alignment_history[0]), tf.zeros([self.batch_size]), tf.constant(0))
      c = lambda _, __, ___, i: tf.less(i, size)

      def body(alignment_history, coverage, covloss_sum, i):
        a = alignment_history[i]
        covloss = tf.reduce_sum(tf.minimum(a, coverage), [1])

        return alignment_history, tf.add(coverage, a), tf.add(covloss_sum, covloss), tf.add(i, 1)

      result = tf.while_loop(c, body, i, swap_memory=True, back_prop=True,
                             parallel_iterations=1)  # This should be done sequentially

      coverage_losses = result[2]
      coverage_losses = coverage_losses / tf.to_float(lengths)
      coverage_loss = tf.reduce_mean(coverage_losses)

    return coverage_loss

  def map_final_dist_ids_to_vocab(self, sample_ids, in_vocab_ids_mapping=None, map_to_strings=False, source=None):

    def look_up(src, sample_ids):
      indexes = tf.maximum(sample_ids - (self.hparams.projection_vocab_size), 0)
      indexes = tf.minimum(indexes, tf.size(src) - 1)

      result = tf.gather(src, indices=indexes)
      # result = tf.Print(result, [tf.shape(src_ids), indexes, sample_ids, result], summarize=100, message="indexes: ")

      return result

    def create_batch_sequence_checker(map_to_strings):
      def check_batch_sequence(src_ids_sample_ids):
        src_ids, sample_ids = src_ids_sample_ids

        if not map_to_strings:
          return tf.where(sample_ids >= self.hparams.projection_vocab_size, look_up(src_ids, sample_ids), sample_ids)
        else:
          return tf.where(sample_ids >= self.hparams.projection_vocab_size, look_up(src_ids, sample_ids),
                          in_vocab_ids_mapping.lookup(
                            tf.to_int64(sample_ids)))

      return check_batch_sequence

    def create_batch_element_checker(map_to_strings):
      def check_batch_element(src_ids_sample_id):
        src_ids, sample_id = src_ids_sample_id

        if not map_to_strings:
          return tf.cond(sample_id >= self.hparams.projection_vocab_size,
                         lambda: look_up(src_ids, sample_id),
                         lambda: sample_id)
        else:
          return tf.cond(sample_id >= self.hparams.projection_vocab_size,
                         lambda: look_up(src_ids, sample_id),
                         lambda: in_vocab_ids_mapping.lookup(
                           tf.to_int64(sample_id)))

      return check_batch_element

    if not map_to_strings:

      if source is None:
        source = self.features["source"]

      if len(sample_ids.get_shape()) == 2:
        sample_ids = tf.map_fn(create_batch_sequence_checker(map_to_strings=False), (source, sample_ids),
                               dtype=tf.int32,
                               parallel_iterations=max(self.hparams.batch_size, self.hparams.parallel_iterations))
      elif len(sample_ids.get_shape()) == 1:
        sample_ids = tf.map_fn(create_batch_element_checker(map_to_strings=False), (source, sample_ids), dtype=tf.int32,
                               parallel_iterations=max(self.hparams.batch_size, self.hparams.parallel_iterations))
      return sample_ids
    else:
      sample_tokens = None
      if source is None:
        source = self.features["source_raw"]


      if len(sample_ids.get_shape()) == 2:
        sample_tokens = tf.map_fn(create_batch_sequence_checker(map_to_strings=True), (source, sample_ids),
                                  dtype=tf.string,
                                  parallel_iterations=max(self.hparams.batch_size, self.hparams.parallel_iterations))
      elif len(sample_ids.get_shape()) == 1:
        sample_tokens = tf.map_fn(create_batch_element_checker(map_to_strings=True), (source, sample_ids),
                                  dtype=tf.string,
                                  parallel_iterations=max(self.hparams.batch_size, self.hparams.parallel_iterations))
      return sample_tokens

    # sample_ids = tf.where(sample_ids >= self.hparams.projection_vocab_size, look_up(source, sample_ids), sample_ids)
    # sample_ids = tf.Print(sample_ids, [tf.shape(sample_ids), sample_ids], summarize=100, message="sample_ids: ")

  def combine_beam_search_to_strings(self, beam_search_output):
    bm_sample_id = reverse_time_batch_major(beam_search_output)
    print("bm_sample_id!: ", bm_sample_id)

    def body(input):
      ids, source = input
      # Transpose to have beam dimension first
      # beam_major_ids = tf.transpose(ids)

      print("source before: ", source)

      source = tf.tile(tf.expand_dims(source, 0), [self.hparams.beam_width, 1])

      print("source after: ", source)

      key_phrase_tokens = self.map_final_dist_ids_to_vocab(tf.transpose(ids), self.reverse_tgt_vocab_table,
                                                           map_to_strings=True, source=source)

      key_phrases = tf.reduce_join(key_phrase_tokens, axis=1, separator=" ")
      key_phrases = tf.regex_replace(key_phrases, self.hparams.eos, "\t")
      key_phrases_no_padding = tf.string_split(key_phrases, delimiter="\t")
      key_phrases_no_padding = tf.sparse_tensor_to_dense(key_phrases_no_padding, default_value="")[:, 0]

      final_string = tf.string_join([tf.reduce_join(key_phrases_no_padding, separator=" "), self.hparams.eos],
                                    separator=" ")
      final_tokens = tf.string_split([final_string], delimiter=" ").values
      final_string_token_count = tf.size(final_tokens)
      padding_size = tf.maximum((tf.size(key_phrase_tokens) + 1) - final_string_token_count, 0)
      padded_final_tokens = tf.concat([final_tokens, tf.fill([padding_size], value=self.hparams.eos)], 0)

      return (padded_final_tokens, final_string_token_count)

    sample_id, final_sequence_lengths = tf.map_fn(fn=body, elems=(bm_sample_id, self.features["source_raw"]),
                                                  parallel_iterations=self.hparams.batch_size,
                                                  dtype=(tf.string, tf.int32))

    return sample_id, final_sequence_lengths
