# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""S2S: TensorFlow NMT model implementation."""
from __future__ import print_function

import argparse
import random
import sys

import numpy as np
import tensorflow as tf

from tfDeepNLP.models.seq2seq import train, inference, autoencoder_train
from tfDeepNLP.models.seq2seq.s2s_arguments_parser import S2SArgumentsParser
from tfDeepNLP.models.seq2seq.s2s_hparams_creator import Seq2SeqHyperParamsCreator
from tfDeepNLP.utils import evaluate_files_utils
from tfDeepNLP.utils import misc_utils as utils

utils.check_tensorflow_version()

FLAGS = None


def run_main(flags, default_hparams, train_fn, inference_fn, hparams_creator):
  """Run main."""
  # Job
  jobid = flags.jobid
  num_workers = flags.num_workers
  utils.print_out("# Job id %d" % jobid)

  # Random
  random_seed = flags.random_seed
  if random_seed is not None and random_seed > 0:
    utils.print_out("# Set random seed to %d" % random_seed)
    random.seed(random_seed + jobid)
    np.random.seed(random_seed + jobid)

  ## Train / Decode
  out_dir = flags.out_dir
  if not tf.gfile.Exists(out_dir): tf.gfile.MakeDirs(out_dir)

  # Load hparams.
  hparams = hparams_creator.create_or_load_hparams(out_dir, default_hparams, flags.hparams_path, flags)

  if flags.inference_input_file:
    # Inference indices
    hparams.inference_indices = None
    if flags.inference_list:
      (hparams.inference_indices) = (
        [int(token) for token in flags.inference_list.split(",")])

    # Inference
    trans_file = flags.inference_output_file
    ckpt = flags.ckpt
    if not ckpt:
      ckpt = tf.train.latest_checkpoint(out_dir)
    inference_fn(ckpt, flags.inference_input_file,
                 trans_file, hparams, num_workers, jobid, scope=None)

    # Evaluation
    ref_file = flags.inference_ref_file
    if ref_file and tf.gfile.Exists(trans_file):
      for metric in hparams.metrics:
        score = evaluate_files_utils.evaluate(
          ref_file,
          trans_file,
          metric,
          hparams.bpe_delimiter)
        utils.print_out("  %s: %.1f" % (metric, score))
  else:
    # Train
    train_fn(hparams)


def main(unused_argv):
  hparams_creator = Seq2SeqHyperParamsCreator()
  default_hparams = hparams_creator(FLAGS)
  train_fn = autoencoder_train.s2s_autoencoder_train
  inference_fn = inference.inference
  run_main(FLAGS, default_hparams, train_fn, inference_fn, hparams_creator)


if __name__ == "__main__":
  tfDeepNLP_parser = argparse.ArgumentParser()
  s2s_arguments_parser = S2SArgumentsParser()
  s2s_arguments_parser(tfDeepNLP_parser)
  FLAGS, unparsed = tfDeepNLP_parser.parse_known_args()

  tf.app.run(main=main, argv=[sys.argv[0]] + unparsed)
