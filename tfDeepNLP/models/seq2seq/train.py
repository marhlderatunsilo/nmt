# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""S2S: For training NMT models on single machine multi-gpu setup."""
from __future__ import print_function

import os
import time

import numpy as np
import tensorflow as tf
from tensorflow.python.ops import lookup_ops

from tfDeepNLP.create_optimizer import create_optimizer
from tfDeepNLP.data_parallelization_utils import average_gradients
from tfDeepNLP.models.seq2seq import inference
from tfDeepNLP.models.seq2seq import model as nmt_model, attention_model, hierarchical_model
from tfDeepNLP.models.seq2seq.dataset_iterator import seq2seq_iterator
from tfDeepNLP.models.seq2seq.dataset_iterator.iterator_utils import split_batched_input
from tfDeepNLP.models.seq2seq.evaluation import _get_best_results, run_internal_eval, run_external_eval, run_full_eval
from tfDeepNLP.models.seq2seq.sample_decode import run_sample_decode

from tfDeepNLP.models.seq2seq.create_models import create_eval_model, create_train_model
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils import vocab_utils
from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser

utils.check_tensorflow_version()

__all__ = [
  "s2s_train"
]

def s2s_train(hparams,
              scope=None,
              single_cell_fn=None):
  """
  Train a translation model!

  This is the single machine multi-GPU version! See the "distributed" branch for multi machine training.

  :param hparams: hyper parameters
  :param scope:
  :param single_cell_fn:
  :return:
  """

  """
  Model settings
  """

  # Logging and output
  log_device_placement = hparams.log_device_placement
  out_dir = hparams.out_dir
  model_dir = hparams.out_dir
  log_file = os.path.join(out_dir, "log_%d" % time.time())
  log_f = tf.gfile.GFile(log_file, mode="a")
  utils.print_out("# log_file=%s" % log_file, log_f)
  summary_name = "train_log_%d" % hparams.task_index

  # Training
  is_hierarchical = hparams.hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE
  num_train_steps = hparams.num_train_steps
  steps_per_stats = hparams.steps_per_stats
  steps_per_external_eval = hparams.steps_per_external_eval
  steps_per_eval = steps_per_stats * 10
  if not steps_per_external_eval:
    steps_per_external_eval = 10 * steps_per_eval
  config_proto = utils.get_config_proto(
    log_device_placement=log_device_placement)

  # Select model
  if hparams.hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE or hparams.pointer_generation:
    model_creator = hierarchical_model.HierarchicalModel
  elif not hparams.attention:
    model_creator = nmt_model.S2SModel
  elif hparams.attention_architecture == "standard":
    model_creator = attention_model.AttentionModel
  #elif hparams.attention_architecture in ["gnmt", "gnmt_v2"]:
  #  model_creator = gnmt_model.GNMTModel
  else:
    raise ValueError("Unknown model architecture")

  train_graph = tf.Graph()

  with train_graph.as_default():

    src_vocab_file = hparams.src_vocab_file
    tgt_vocab_file = hparams.tgt_vocab_file

    src_vocab_table, tgt_vocab_table = vocab_utils.create_vocab_tables(
      src_vocab_file, tgt_vocab_file, hparams.share_vocab)
    reverse_tgt_vocab_table = lookup_ops.index_to_string_table_from_file(
      tgt_vocab_file, default_value=vocab_utils.UNK)

    create_train_model_template = tf.make_template('create_train_model_template',
                                                   create_train_model,
                                                   create_scope_now_=False,
                                                   model_creator=model_creator,
                                                   hparams=hparams,
                                                   single_cell_fn=single_cell_fn,
                                                   src_vocab_table=src_vocab_table,
                                                   tgt_vocab_table=tgt_vocab_table,
                                                   reverse_tgt_vocab_table=reverse_tgt_vocab_table,
                                                   )

  """
  Preload data for sample decoding.
  """

  dev_src_file = "%s.%s" % (hparams.dev_prefix, hparams.src)
  dev_tgt_file = "%s.%s" % (hparams.dev_prefix, hparams.tgt)

  print("dev_src_file: ", dev_src_file)
  print("dev_tgt_file: ", dev_tgt_file)

  sample_src_data = inference.load_data(dev_src_file)
  sample_tgt_data = inference.load_data(dev_tgt_file)

  """
  Create evaluation models and iterator.
  """

  # Create common iterator:
  with train_graph.as_default(), tf.device('/cpu:0'), tf.name_scope("eval_model"):
    #src_file_placeholder = tf.placeholder(shape=(), dtype=tf.string)
    #tgt_file_placeholder = tf.placeholder(shape=(), dtype=tf.string)

    dev_src_file = "%s.%s" % (hparams.dev_prefix, hparams.src)
    dev_tgt_file = "%s.%s" % (hparams.dev_prefix, hparams.tgt)

    src_dataset = tf.data.TextLineDataset(dev_src_file)
    tgt_dataset = tf.data.TextLineDataset(dev_tgt_file)

    eval_iterator = seq2seq_iterator.get_iterator(
      src_dataset,
      tgt_dataset,
      src_vocab_table,
      tgt_vocab_table,
      hparams.infer_batch_size,
      sos=hparams.sos,
      eos=hparams.eos,
      source_reverse=hparams.source_reverse,
      random_seed=hparams.random_seed,
      num_threads=hparams.parallel_iterations,
      num_buckets=hparams.num_buckets,
      num_shards=hparams.num_worker_hosts,
      src_max_len=hparams.src_max_len_infer,
      tgt_max_len=hparams.tgt_max_len_infer,
      num_splits=hparams.num_gpus,
      projection_vocab_size=hparams.projection_vocab_size,
      hierarchical_split_tokens=hparams.hierarchical_split_tokens,
      pointer_generation=hparams.pointer_generation,
      max_first_hierarchy_level_sequence_length=hparams.max_first_hierarchy_level_sequence_length)

    # Split eval data
    eval_iterator_split_batches = split_batched_input(eval_iterator, hparams.num_gpus, is_hierarchical=is_hierarchical)

  # Create eval models
  eval_models = []
  with tf.device(tf.DeviceSpec(device_type="GPU", device_index=0)):
    print("\nEVAL NAMESCOPE: " + str(tf.contrib.framework.get_name_scope()))
    for i in range(0, hparams.num_gpus, 1):
      with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):
        with tf.name_scope('%s_%d' % ('eval_tower', i)):
          #src_file_placeholder, tgt_file_placeholder,
          eval_model = create_eval_model(train_graph, create_train_model_template, eval_iterator_split_batches[i],  first_gpu_id=0)
          eval_models.append(eval_model)

  """
    Create inference model.
  """
  with tf.device(tf.DeviceSpec(device_type="GPU", device_index=0)):
    print("\nINFERENCE NAMESCOPE: " + str(tf.contrib.framework.get_name_scope()))
    infer_model = inference.create_infer_model(train_graph, create_train_model_template, hparams, src_vocab_table,
                                               first_gpu_id=0)
  """
    Create training models.
    
    Single machine multi-gpu:
      We create a shared dataset iterator.
      We create a model per gpu - prefixed "tower_%d".
      We calculate loss and gradients for each model.
      We average gradients .
        On gpu:0 by default but the best performing gpu can be specified.
      We apply averaged gradients with the shared optimizer (to all params in all models I assume, 
        it was done like this in a tutorial and seems to work).
      
      Each model is created in the same graph and run from the same session.
  """

  with train_graph.as_default(), tf.device('/cpu:0'), tf.name_scope("train_model"):

    global_step = tf.train.get_or_create_global_step()

    src_file = "%s.%s" % (hparams.train_prefix, hparams.src)
    tgt_file = "%s.%s" % (hparams.train_prefix, hparams.tgt)

    src_dataset = tf.data.TextLineDataset(src_file)
    tgt_dataset = tf.data.TextLineDataset(tgt_file)

    buffer_size = hparams.batch_size * 100

    opt, learning_rate = create_optimizer(hparams, global_step)

    iterator = seq2seq_iterator.get_iterator(
      src_dataset,
      tgt_dataset,
      src_vocab_table,
      tgt_vocab_table,
      batch_size=hparams.batch_size,
      sos=hparams.sos,
      eos=hparams.eos,
      source_reverse=hparams.source_reverse,
      random_seed=None,
      num_buckets=hparams.num_buckets,
      src_max_len=hparams.src_max_len,
      tgt_max_len=hparams.tgt_max_len,
      num_shards=hparams.num_gpus,  # hparams.num_worker_hosts,
      num_splits=hparams.num_gpus,
      num_threads=hparams.parallel_iterations,
      skip_count=None,
      # shard_index=hparams.task_index,
      output_buffer_size=buffer_size,
      projection_vocab_size=hparams.projection_vocab_size,
      hierarchical_split_tokens=hparams.hierarchical_split_tokens,
      pointer_generation=hparams.pointer_generation,
      max_first_hierarchy_level_sequence_length=hparams.max_first_hierarchy_level_sequence_length
    )

    batches = split_batched_input(iterator, hparams.num_gpus, is_hierarchical=is_hierarchical)

    # WARNING/HACK This is created to cheat and put the variables on the CPU
    unused_train_model = create_train_model_template(
      iterator=batches[0],
      mode=tf.contrib.learn.ModeKeys.TRAIN,
      optimizer_op=opt,
      learning_rate=learning_rate,
      first_gpu_id=-1
    )  # if hparams.num_gpus > 1 else batches

    tower_grads = []
    train_models = []
    with tf.variable_scope(tf.get_variable_scope()):
      for i in range(0, hparams.num_gpus, 1):
        with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):
          with tf.name_scope('%s_%d' % ('tower', i)):
            print("\nTRAIN NAMESCOPE: " + str(tf.contrib.framework.get_name_scope()))

            # src_vocab_table, tgt_vocab_table = vocab_utils.create_vocab_tables(
            #   src_vocab_file, tgt_vocab_file, hparams.share_vocab)

            index = i

            train_model = create_train_model_template(
              iterator=batches[index],
              mode=tf.contrib.learn.ModeKeys.TRAIN,
              optimizer_op=opt,
              learning_rate=learning_rate,
              first_gpu_id=i
            )  # if hparams.num_gpus > 1 else batches

            tf.get_variable_scope().reuse_variables()

            train_models.append(train_model)
            tower_grads.append(train_model.model.gradients)

    # with tf.device('gpu:%d' % hparams.single_task_gpu):  # Put on best performing GPU

    # Calculate the mean of each gradient
    # This is the synchronization point across all towers
    with tf.device(tf.DeviceSpec(device_type="CPU", device_index=0)):
      grads = average_gradients(tower_grads)
      update = opt.apply_gradients(grads, global_step=global_step)

    # Create a saver
    saver = tf.train.Saver(sharded=True, name='knownSaver')

    assign_embeddings = None
    embeddings_placeholder = None
    if hparams.embeddings_option != "train":
      embeddings_placeholder = tf.placeholder(dtype=tf.float32, shape=[hparams.src_vocab_size, hparams.token_embedding_size])
      assign_embeddings = tf.assign(train_models[0].model.embedding_encoder, embeddings_placeholder)


    # Create hooks for MonitoredTrainingSession
    train_hooks = [
      #tf.train.StopAtStepHook(last_step=num_train_steps),
      tf.train.CheckpointSaverHook(
        save_steps=steps_per_eval,
        checkpoint_dir=model_dir,
        saver=saver,
        checkpoint_basename="translate.ckpt"
      )
    ]

    # Create training session
    # MonitoredTrainingSession handles loading/saving of checkpoints, variables initialization, with more
    train_sess = tf.train.MonitoredTrainingSession(
      is_chief=True,
      checkpoint_dir=model_dir,
      hooks=train_hooks,
      save_summaries_secs=None,
      save_summaries_steps=None,
      save_checkpoint_secs=None,
      #log_step_count_steps=None,  # Needed as we use the session for other things than strictly training
      config=config_proto,
    )

    infer_sess = train_sess
    eval_sess = train_sess

  # Initialize iterator
  train_sess.run(iterator.initializer)

  global_step_python = tf.train.global_step(train_sess, global_step)

  # Assign embeddings first time if needed:
  if not global_step_python > 0 and hparams.embeddings_option != "train":
    print("Assigning Embeddings")
    # Assign embeddings
    # We only need to do this for the first model because of the way they share parameters

    with np.load(hparams.embeddings_path) as data:
      pretrained_embeddings = data["embeddings"]

    print("shape: ", pretrained_embeddings.shape)

    train_sess.run(assign_embeddings, {
      embeddings_placeholder: pretrained_embeddings})

  # Init counters
  last_stats_step = 0
  last_eval_step = 0
  last_external_eval_step = 0

  """
  Get ready for training! 
  """

  # Create summary writer for tensorboard
  summary_writer = tf.summary.FileWriter(
    os.path.join(out_dir, summary_name), train_graph)

  # Get the evaluated version of global step (Can't use a tensor for comparison in python!)
  global_step_python = tf.train.global_step(train_sess, global_step)

  # Pre-training evaluation
  # if global_step_python == 0:
  #   print("First evaluation")
  #   run_full_eval(
  #     model_dir, infer_model, infer_sess,
  #     eval_model, eval_sess, hparams,
  #     summary_writer, sample_src_data,
  #     sample_tgt_data)
  #
  #   dataset_iterator.print_out(
  #     "# Start step %d, lr %g, %s" %
  #     (global_step_python, learning_rate.eval(session=train_sess),
  #      time.ctime()),
  #     log_f)

  # Initialize evaluation metrics
  step_time, checkpoint_loss, checkpoint_predict_count = 0.0, 0.0, 0.0
  checkpoint_total_count = 0.0
  speed, train_ppl = 0.0, 0.0
  avg_step_time = 0

  """
  Start training
  """

  print("Start training loop")

  statistics_ops = tuple([t_model.model.statistics() for t_model in train_models])

  while global_step_python < num_train_steps:
    global_step_python = train_sess.run(global_step)

    start_time = time.time()

    # Run a step
    try:
      with train_graph.as_default():

        # Magic happens
        _, step_results = train_sess.run((update, statistics_ops))

        # Init arrays for model stats for this step
        step_losses = []
        step_predict_counts = []
        step_summaries = []
        step_word_counts = []
        batch_sizes = []

        # Get stats from each train model
        for step_result in step_results:
          (step_loss, step_predict_count, step_summary,
           step_word_count, batch_size) = step_result

          step_losses.append(step_loss)
          step_predict_counts.append(step_predict_count)
          step_summaries.append(step_summary)
          step_word_counts.append(step_word_count)
          batch_sizes.append(batch_size)

        # Reduce stats arrays to scalars
        avg_step_loss = np.mean(step_losses)
        avg_step_predict_count = np.mean(step_predict_counts)
        avg_batch_size = np.mean(batch_sizes)
        # avg_step_word_count = np.mean(step_word_counts) * 1.0
        sum_step_word_count = sum(step_word_counts) * 1.0
        # min_step_word_count = min(step_word_counts)
        # max_step_word_count = max(step_word_counts)

    except tf.errors.OutOfRangeError:

      if not train_sess.should_stop():
        """
        We decode a sample and run an external evaluation to inspect the current ability of the model.
        There is no more data, so we initialize the model iterators again 
          (to load more data) and go to next epoch.
        """

        print("OutOfRangeError!! Restarting iterators!")

        utils.print_out(
          "# Finished an epoch, step %d. Perform external evaluation" %
          global_step_python)

        # run_sample_decode(infer_model, infer_sess,
        #                   model_dir, hparams, summary_writer, sample_src_data,
        #                   sample_tgt_data)
        # dev_scores, test_scores, _ = run_external_eval(
        #   infer_model, infer_sess, model_dir,
        #   hparams, summary_writer)

        # Reinitialize the iterator
        train_sess.run(iterator.initializer)
      continue

    # Update statistics
    step_time += (time.time() - start_time)
    checkpoint_loss += (avg_step_loss * avg_batch_size)
    checkpoint_predict_count += avg_step_predict_count
    checkpoint_total_count += sum_step_word_count  # TODO Is it correct?

    if not train_sess.should_stop():

      # Write step summaries. TODO Is this the best way to add all summaries?
      for gpu_id, summary in enumerate(step_summaries):
        summary_writer.add_summary(summary, global_step_python)

      # Once in a while, we print statistics.
      if global_step_python - last_stats_step >= steps_per_stats:
        last_stats_step = global_step_python

        # Print statistics for the previous epoch.
        avg_step_time = step_time / steps_per_stats
        train_ppl = utils.safe_exp(checkpoint_loss / checkpoint_predict_count)
        speed = checkpoint_total_count / (1000 * step_time)
        utils.print_out(
          "  global step %d lr %g "
          "step-time %.2fs wps %.2fK ppl %.2f %s" %
          (global_step_python,
           learning_rate.eval(session=train_sess),
           avg_step_time, speed, train_ppl, _get_best_results(hparams)),
          log_f)

        # Reset timer and loss.
        step_time, checkpoint_loss, checkpoint_predict_count = 0.0, 0.0, 0.0
        checkpoint_total_count = 0.0

        # Print trainable variables
        # with train_graph.as_default():
        #   variables_names = [v.name for v in tf.trainable_variables()]
        #   values = train_sess.run(tf.trainable_variables())
        #   for k, v in zip(variables_names, values):
        #     dataset_iterator.print_out("Variable: {}, shape: {}, v:".format(k, v.shape))  # , v))

        # TODO What is the difference between internal and external evaluations?
        # it seems that they use the same data,
        # and the calculated perplexity in internal evaluation (the first of the next two blocks) is not used

    # Once in a while, we evaluate the model
    if global_step_python - last_eval_step >= steps_per_eval:
      last_eval_step = global_step_python

      utils.print_out("# Save eval, global step %d" % global_step_python)
      utils.add_summary(summary_writer, global_step_python, "train_ppl", train_ppl)

      # Evaluate on dev/test
      run_sample_decode(infer_model, infer_sess,
                        model_dir, hparams, summary_writer, sample_src_data,
                        sample_tgt_data)
      dev_ppl, test_ppl = run_internal_eval(
      eval_models, eval_sess, eval_iterator, hparams, summary_writer, max_elements=100)

    # Once in a while, we evaluate the model
    if global_step_python - last_external_eval_step >= steps_per_external_eval:
      last_external_eval_step = global_step_python

      run_sample_decode(infer_model, infer_sess,
                        model_dir, hparams, summary_writer, sample_src_data,
                        sample_tgt_data)
      dev_scores, test_scores, _ = run_external_eval(
        infer_model, infer_sess, model_dir,
        hparams, summary_writer)

  """
  Done training
  
  We run a full evaluation of the model.
  
  Here we should consider gathering a single model and saving it on it's own, instead of having one per GPU
  as we might not want to run the model on the same machine later.
  
  """  # TODO Is it a single model or all the models that we evaluate?

  result_summary, _, dev_scores, test_scores, dev_ppl, test_ppl = run_full_eval(
    model_dir, infer_model, infer_sess,
    eval_models, eval_sess, hparams,
    summary_writer, sample_src_data,
    sample_tgt_data)
  utils.print_out(
    "# Final, step %d lr %g "
    "step-time %.2f wps %.2fK ppl %.2f, %s, %s" %
    (global_step_python, learning_rate.eval(session=train_sess),
     avg_step_time, speed, train_ppl, result_summary, time.ctime()),
    log_f)
  start_train_time = time.time()
  utils.print_time("# Done training!", start_train_time)

  utils.print_out("# Start evaluating saved best models.")
  for metric in hparams.metrics:
    best_model_dir = getattr(hparams, "best_" + metric + "_dir")
    result_summary, best_global_step, _, _, _, _ = run_full_eval(
      best_model_dir, infer_model, infer_sess, eval_models, eval_sess, hparams,
      summary_writer, sample_src_data, sample_tgt_data)
    utils.print_out("# Best %s, step %d "
                    "step-time %.2f wps %.2fK, %s, %s" %
                    (metric, best_global_step, avg_step_time, speed,
                     result_summary, time.ctime()), log_f)

  summary_writer.close()
  return dev_scores, test_scores, dev_ppl, test_ppl, global_step_python
  # return 0, 0, 0, 0, 0
