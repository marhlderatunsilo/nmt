import tensorflow as tf
from tensorflow.python.ops.rnn_cell_impl import LSTMStateTuple

from tfDeepNLP.attention.attention import create_attention_mechanism
from tfDeepNLP.attention.coverage_bahdanau_attention import AttentionWrapper
from tfDeepNLP.utils.initializer_utils import get_initializer


def partition_predicates(time, splits):
  # batch_size = tf.shape(splits)[0]

  # For each batch, see if time is equal to a value in splits
  is_end = tf.reduce_any(tf.equal(tf.expand_dims(time, -1), splits), axis=1)

  return is_end, tf.cast(is_end, dtype=tf.int32)


def reverse_sequence(input_, seq_lengths, seq_dim, batch_dim):
  if seq_lengths is not None:
    return tf.reverse_sequence(
      input=input_, seq_lengths=seq_lengths,
      seq_dim=seq_dim, batch_dim=batch_dim)
  else:
    return tf.reverse(input_, axis=[seq_dim])


def create_invert_splits(seq_part):
  # seq_part_shape = tf.shape(seq_part)
  # seq_part_max_length = seq_part_shape[-1]
  # max_index = tf.reduce_max(seq_part, axis=1)
  # max_index_minus_one = max_index - 1
  # reverse_seq_part = tf.reverse(seq_part, axis=[1])
  #
  # tiled_max_index_minus_one = tf.tile(max_index_minus_one, [seq_part_max_length])
  # reshaped = tf.reshape(tiled_max_index_minus_one, [seq_part_shape[1], seq_part_shape[0]])
  # new_indexes = tf.transpose(reshaped, [1, 0]) - reverse_seq_part
  #
  # elements = (new_indexes, seq_part_lengths, max_index)
  # new_indexes = tf.map_fn(lambda x:
  #                         tf.concat([tf.slice(x[0], [seq_part_max_length - x[1] + 1], [-1]), [x[2]],
  #                                    (tf.zeros([seq_part_max_length - x[1]], dtype=tf.int32) - 1)], 0),
  #                         elements, dtype=tf.int32)

  reversed = tf.reverse_sequence(seq_part, tf.count_nonzero(seq_part, axis=-1), batch_axis=0, seq_axis=1)

  # Minus 1 because we are moving from lengths to indexes
  split = tf.cumsum(reversed, axis=-1)

  # Cumsum has created extra entries for

  return split


def pick_subsequence_elements(elements, splits):
  # elements = elements_ta.stack()

  elements_shape_tm = tf.shape(elements)
  num_hidden = elements.get_shape()[-1]

  splits_shape = tf.shape(splits)

  idxs = tf.add(tf.transpose(splits),
                tf.multiply(elements_shape_tm[0], tf.range(0, elements_shape_tm[1])),
                )

  flattened = tf.reshape(elements, [-1, num_hidden])
  # Concat (prepend) an extra zeroes vector to be used at padding for the gather op
  padded_flattened = tf.concat([tf.zeros([1, num_hidden]), flattened], axis=0)

  unique_idxs = unique2d(idxs, splits_shape[1], -1, is_time_major=True) + 1

  result = tf.gather(padded_flattened, unique_idxs, validate_indices=True)

  # result = tf.Print(result, [tf.shape(flattened), idxs], message="result: ", summarize=100)

  return result


def extract_events_tm(batched_sequences_tm, idxs_per_batch_element):
  if isinstance(batched_sequences_tm, tf.TensorArray):
    batched_sequences_tm = batched_sequences_tm.stack()

  batched_sequences_shape_tm = tf.shape(batched_sequences_tm)

  # length_assert = tf.assert_less(idxs_per_batch_element, batched_sequences_shape[1],
  #                                [batched_sequences, batched_sequences_shape[1], idxs_per_batch_element],
  #                                summarize=10)
  # with tf.control_dependencies([length_assert]):
  # Flatten batch and sequence dimensions
  flattened = tf.reshape(batched_sequences_tm, [-1, batched_sequences_tm.get_shape()[-1]])
  idxs = tf.add(tf.multiply(batched_sequences_shape_tm[0], tf.range(0, batched_sequences_shape_tm[1])),
                idxs_per_batch_element)

  result = tf.gather(flattened, idxs)

  return result

def gather_2d_bm(params, indices):
  # only for two dim now
  shape = params.get_shape().as_list()
  assert len(shape) == 2, 'only support 2d matrix, len of shape was: %d' % len(shape)
  idx_flattened = tf.range(0, tf.shape(params)[0]) * tf.shape(params)[1] + indices
  y = tf.gather(tf.reshape(params, [-1]),  # flatten input
                idx_flattened)  # use flattened indices
  return y


# Performs unique
def unique2d(tensor, max_sequence_length, padding_value=0, is_time_major=False):
  if is_time_major:
    tensor = tf.transpose(tensor)

  def make_unique_and_pad(vector):
    vector, _ = tf.unique(vector)
    return tf.concat([vector, tf.fill([max_sequence_length - tf.size(vector)], padding_value)], axis=0)

  unique_tensor = tf.map_fn(make_unique_and_pad, tensor)

  if is_time_major:
    unique_tensor = tf.transpose(unique_tensor)

  return unique_tensor


def reverse_time_batch_major(tensor, name=None):
  # (T,B,D) => (B,T,D) or (B,T,D) => (T,B,D)
  transposed_tensor = tf.transpose(tensor, [1, 0, 2], name=name)

  return transposed_tensor


def create_start_token(input_size):
  start_of_sequence_token = tf.zeros([input_size], dtype=tf.float32)
  start_of_sequence_token = tf.concat([[1], start_of_sequence_token], 0)

  return start_of_sequence_token


def create_cell_state_with_embedding(cell, embedding):
  print("create_cell_state_with_embedding, embedding: ", embedding)

  batch_size = tf.shape(embedding)[0]
  zero_state = cell.zero_state(batch_size, tf.float32)

  print("create_cell_state_with_embedding, zero_state: ", zero_state)

  # Initialize first layer with embeddings, rest are zeros
  cell_state_with_embedding = []
  cell_state_with_embedding.append(LSTMStateTuple(c=embedding, h=zero_state[0].h))
  cell_state_with_embedding += [t for t in zero_state[1:]]

  return tuple(cell_state_with_embedding)


def wrap_decoder_cell_in_attention(hparams, model, cell, num_units=None, memory=None,
                                   input_sequence_length=None, memory_length=None):
  if not num_units:
    num_units = hparams.num_units

  # Create multi-layer rnn-cell:
  # num_layers = hparams.num_layers
  # num_residual_layers = hparams.num_residual_layers
  beam_width = hparams.beam_width

  """Build a RNN cell with attention mechanism that can be used by decoder."""
  attention_option = hparams.attention
  attention_architecture = hparams.attention_architecture

  if attention_architecture != "standard":
    raise ValueError(
      "Unknown attention architecture %s" % attention_architecture)

  # with tf.device(tf.DeviceSpec(device_type="GPU", device_index=self.first_gpu_id)):
  if model.time_major:
    memory = tf.transpose(memory, [1, 0, 2])
  else:
    memory = memory

  if model.mode == tf.contrib.learn.ModeKeys.INFER and beam_width > 0:
    memory = tf.contrib.seq2seq.tile_batch(
      memory, multiplier=beam_width)
    memory_length = tf.contrib.seq2seq.tile_batch(
      memory_length, multiplier=beam_width)

  print("memory: ", memory)
  print("input_sequence_length: ", input_sequence_length)

  # Only generate alignment in greedy INFER mode.
  alignment_history = (model.mode == tf.contrib.learn.ModeKeys.INFER and beam_width == 0) or \
                      (model.mode == tf.contrib.learn.ModeKeys.TRAIN and hparams.add_coverage_loss) or \
                      (model.mode == tf.contrib.learn.ModeKeys.EVAL and hparams.add_coverage_loss)

  attention_mechanism = create_attention_mechanism(
    attention_option,
    num_units,
    memory,
    memory_sequence_length=memory_length if memory_length is not None else input_sequence_length,
    init_op=get_initializer(hparams.init_op, hparams.random_seed, hparams.init_weight)
  )

  # assert not alignment_history

  if hparams.add_coverage_loss and attention_option == "coverage_normed_bahdanau":
    attention_cell = AttentionWrapper(
      cell,
      attention_mechanism,
      attention_layer_size=num_units,
      alignment_history=alignment_history or hparams.pointer_generation,
      name="attention")
  else:
    attention_cell = tf.contrib.seq2seq.AttentionWrapper(
      cell,
      attention_mechanism,
      attention_layer_size=num_units,
      #alignment_history=alignment_history or hparams.pointer_generation,
      name="attention")

  return attention_cell


def create_intial_decoder_attention_cell_state(hparams, model, attention_cell, encoder_state):
  beam_width = hparams.beam_width
  dtype = tf.float32

  if model.mode == tf.contrib.learn.ModeKeys.INFER and beam_width > 0:

    if isinstance(encoder_state, LSTMStateTuple):
      encoder_state = LSTMStateTuple(
        #c=tf.contrib.seq2seq.tile_batch(encoder_state.c, multiplier=beam_width),
        c = None,
        h=tf.contrib.seq2seq.tile_batch(encoder_state.h, multiplier=beam_width)
      )
    else:
      encoder_state = tf.contrib.seq2seq.tile_batch(
        encoder_state, multiplier=beam_width)

    batch_size = model.batch_size * beam_width
  else:
    batch_size = model.batch_size

  # We assing the state of the last layer of the encoder to the first layer of the decoder:
  zero_state = attention_cell.zero_state(batch_size, dtype)  # encoder_state[0].dtype
  print("zero_state:", zero_state)

  if hparams.pointer_generation:
    zero_state_list = list(zero_state) #list(zero_state.cell_state)
  else:
    zero_state_list = list(zero_state)

  if isinstance(encoder_state, LSTMStateTuple):
    c = zero_state_list[0].cell_state.h if isinstance(zero_state_list[0], tf.contrib.seq2seq.AttentionWrapperState) else zero_state_list[0].c
    first_layer_cell_state = LSTMStateTuple(c=c, h=encoder_state.h)
  elif isinstance(encoder_state, tuple) and len(encoder_state) > 2:

    first_layer_cell_state = encoder_state #tuple( [encoder_state[0]] + list(zero_state_list[0].cell_state)[1:])

    print("Length of nlstm state: ", len(encoder_state), " Length of decoder input state: ", len(first_layer_cell_state))

  else:
    h = zero_state_list[0].cell_state.h if isinstance(zero_state_list[0], tf.contrib.seq2seq.AttentionWrapperState) else zero_state_list[0].h
    first_layer_cell_state = LSTMStateTuple(c=encoder_state, h=h)

  if hparams.pass_hidden_state:
    print("State is to be copied")
    decoder_initial_state = zero_state_list[0].clone(
      cell_state=first_layer_cell_state)
  else:
    decoder_initial_state = zero_state_list[0]

  zero_state_list[0] = decoder_initial_state

  if hparams.pointer_generation:
    decoder_initial_state = tuple(zero_state_list)#¤ zero_state.clone(cell_state=tuple(zero_state_list))
  else:
    decoder_initial_state = tuple(zero_state_list)

  print("create cell, decoder_initial_state: ", decoder_initial_state)
  print("create cell, encoder_state: ", encoder_state)
  print("attention_cell: ", attention_cell)

  return decoder_initial_state





def create_ids_to_pointer_ids_mapper(projection_vocab_size, tgt_vocab_table_size, unk_token_id):
  def map_to_pointer_ids(src, tgt, src_split, tgt_split):

    tgt_indexes = tf.range(0, tf.size(tgt))

    def map_tgt_id_to_pointer_id(tgt_token_id_tgt_index):

      tgt_token_id, tgt_index = tgt_token_id_tgt_index

      is_out_of_vocab = tf.logical_or(tgt_token_id >= projection_vocab_size, tf.equal(tgt_token_id, unk_token_id))

      # is_out_of_vocab = tf.Print(is_out_of_vocab, [
      #   is_out_of_vocab
      # ], message="is_out_of_vocab: ", summarize=100)

      def look_up_in_src():

        matches = tf.equal(src_split, tgt_split[tgt_index])



        # If there were no matches in the src text we default to unk_token_id
        mapped_index = tf.cond(tf.reduce_any(matches),
                               true_fn=lambda: projection_vocab_size + tf.cast(
                               tf.reduce_max(tf.where(matches)), dtype=tf.int32),
                               false_fn=lambda: unk_token_id)

        # mapped_index = tf.Print(mapped_index, [
        #                               tgt_split[tgt_index],
        #                               mapped_index,
        #                               tf.reduce_any(matches, axis=-1)
        #                              ], message="!! oov word: ")

        return mapped_index

      return tf.cond(is_out_of_vocab,
                      true_fn=look_up_in_src,
                      false_fn=lambda: tgt_token_id)

    tgt_in = tgt  # tf.where(tgt >= tgt_vocab_table_size, tf.fill(tf.shape(tgt), unk_token_id), tgt)
    tgt_out = tf.map_fn(map_tgt_id_to_pointer_id, (tgt, tgt_indexes), dtype=tf.int32)

    # tgt_out = tf.Print(tgt_out, [
    #                               tf.reduce_any(tf.equal(unk_token_id, tgt), axis=-1),
    #                               #unk_token_id,
    #                               #tgt,
    #                               #tf.shape(tgt)
    #                              ], message="Includes oov words: ", summarize=100)

    return (src, tgt_in, tgt_out, src_split)

  return map_to_pointer_ids
