import tensorflow as tf
from tensorflow.contrib.seq2seq import AttentionWrapperState
from tensorflow.python.ops.rnn_cell_impl import LSTMStateTuple
from tensorflow.python.util.nest import map_structure, map_structure_up_to

from tfDeepNLP.models.seq2seq.hierarchical.util import partition_predicates, create_start_token, \
  create_cell_state_with_embedding, wrap_decoder_cell_in_attention, extract_events_tm, gather_2d_bm, \
  create_intial_decoder_attention_cell_state
from tfDeepNLP.single_cells.identity_rnn_cell import IdentityRNN


def create_encoder_level_loop_fn(cell, inputs, batch_size, hierarchy_sequence_lengths,
                                 splits, initial_state=None, stop_gradients_for_splits=True):
  # Inputs in time major order as a tensor array
  input_size = tf.shape(inputs)[0]
  inputs_ta = tf.TensorArray(dtype=tf.float32, size=input_size, dynamic_size=True)
  inputs_ta = inputs_ta.unstack(inputs)

  def loop_fn(time, cell_output, cell_state, loop_state):

    # Input is in time major order, so dimension 1 is batch size
    if cell_output is None:  # time == 0, special first call to loop_fn by raw_rnn
      next_cell_state = cell.zero_state(batch_size, tf.float32)

      emit_output = (tf.zeros([cell.output_size]),
                     map_structure(lambda _: tf.zeros([cell.output_size]), next_cell_state[-1] )
        )  # == None for time == 0
    else:  # time > 0, normal operation
      next_cell_state = cell_state
      # emit_output = tf.slice(tf.stack([s[0] for s in cell_state]), [num_layers - 1, 0, 0, 0],
      #                       [1, 1, batch_size, num_hidden])
      # emit_output = tf.squeeze(emit_output)
      emit_output = cell_output

    # lengths = tf.minimum(hierarchy_sequence_lengths, input_size)
    # time = tf.Print(time, [stop_gradients_for_splits, hierarchy_sequence_lengths, splits, input_size], summarize=100, message="lengths: ")

    elements_finished = (time >= tf.minimum(input_size, hierarchy_sequence_lengths))
    finished = tf.reduce_all(elements_finished)

    print("batch_size: ", batch_size)
    print("inputs: ", inputs)

    next_input = tf.cond(
      finished,
      lambda: tf.zeros([batch_size, inputs.get_shape()[-1]], dtype=tf.float32),
      lambda: inputs_ta.read(time))

    # print("next_cell_state: ", next_cell_state)
    # print("batch_splits_bool: ", batch_splits_bool)
    # print("tf.transpose(no_gradient_state, [2, 1, 0, 3]): ", tf.transpose(next_cell_state, [2, 1, 0, 3]))

    # Stop gradients for splitting timesteps
    # if stop_gradients_for_splits:
    #   # no_gradient_state = map_structure(tf.stop_gradient, next_cell_state)
    #   # next_cell_state = map_structure_up_to(next_cell_state,
    #   #                                       lambda no_grad, org: tf.where(batch_splits_bool, no_grad, org),
    #   #                                       no_gradient_state, next_cell_state)
    #   batch_splits_bool, batch_splits = partition_predicates(time, splits)
    #   no_gradient_state = map_structure(tf.zeros_like, next_cell_state)
    #   next_cell_state = map_structure_up_to(next_cell_state,
    #                                         lambda no_grad, org: tf.where(batch_splits_bool, no_grad, org),
    #                                         no_gradient_state, next_cell_state)

    print("next_cell_state: ", next_cell_state)

    if cell_output is not None:
      print("! emit_output: ", emit_output)
      print("! next_cell_state: ", next_cell_state[-1])
      emit_output = (emit_output, next_cell_state[-1])

    next_loop_state = loop_state
    return (elements_finished, next_input, next_cell_state,
            emit_output, next_loop_state)

  return loop_fn


def create_decoder_level_loop_fn(
    hparams,
    model,
    cell,
    encoded_inputs_tm,
    encoded_states_tm,
    batch_size,
    target_lengths,
    split_indexes,
    subsequence_lengths,
    # mode,
    # targets_tm
):
  input_size = cell.output_size
  start_of_sequence_token = create_start_token(input_size)
  # Input size is 1 entry wider because of start of sequence tokens
  input_size += 1

  start_of_sequence_batch = (tf.zeros([batch_size, input_size], dtype=tf.float32) + start_of_sequence_token)
  print("start_of_sequence_batch: ", start_of_sequence_batch)
  zero_embeddings_state = tf.zeros_like(encoded_states_tm.c[0])

  def construct_hack_cell(cell_state, safe_emb_idx):

    identity_hack_cell = IdentityRNN(cell_state, cell.output_size)

    embeddings_to_use = map_structure(lambda structure: extract_events_tm(structure, safe_emb_idx), encoded_states_tm)

    print("encoded_states_tm.c: ", encoded_states_tm.c)
    print("safe_emb_idx: ", safe_emb_idx)

    attention_hack_cell = wrap_decoder_cell_in_attention(hparams,
                                                         model,
                                                         identity_hack_cell,
                                                         hparams.num_units * 2,
                                                         tf.gather(encoded_inputs_tm,
                                                                   safe_emb_idx),
                                                         gather_2d_bm(subsequence_lengths,
                                                                      safe_emb_idx))

    initial_hack_state = create_intial_decoder_attention_cell_state(hparams, model, attention_hack_cell,
                                                                    embeddings_to_use)

    return attention_hack_cell, initial_hack_state

  # Initialize cell state to first embedding
  initial_next_cell_state = create_cell_state_with_embedding(cell, encoded_states_tm.c[0])

  safe_emb_idx_var = tf.zeros([batch_size],
                              dtype=tf.int32)  # tf.get_variable("safe_emb_idx", initializer=tf.zeros_initializer, shape=[hparams.batch_size], dtype=tf.int32,
  #                               trainable=False)

  attention_hack_cell, initial_hack_state = construct_hack_cell(
    initial_next_cell_state,
    safe_emb_idx_var)

  def loop_fn(time, cell_output, cell_state, loop_state):

    elements_finished = (time >= target_lengths)
    finished = tf.reduce_all(elements_finished)
    # time_state = tf.reshape(tf.tile([time], [batch_size]), shape=(batch_size, 1))

    if cell_output is None:  # time == 0, special first call to loo_fn by raw_rnn
      with tf.variable_scope(tf.get_variable_scope(), reuse=tf.AUTO_REUSE):
        cur_emb_idx = tf.zeros([batch_size], dtype=tf.int32)

        # print("t=0, next_cell_state: ", next_cell_state)

        # Output element depth for first emit, batch size is prepended by raw_rnn
        # emit_output = tf.zeros([cell.output_size]) # The values of this output are ignored, shape is used
        emit_output = (tf.zeros([cell.output_size]), LSTMStateTuple(c=tf.zeros([cell.output_size]), h=tf.zeros(
          [cell.output_size])))

        next_loop_state = tuple([cur_emb_idx, initial_hack_state])
        print("t=0, hack_state: ", initial_hack_state)
        print("next_loop_state: ", next_loop_state)

        next_cell_state = initial_next_cell_state
        next_input = start_of_sequence_batch
    else:  # time > 0, normal operation
      with tf.variable_scope(tf.get_variable_scope(), reuse=tf.AUTO_REUSE):
        cur_emb_idx = loop_state[0]

        # Set next memory state
        # Get sequences where next input is new subsequence
        # time - 1 Because the index in sequence_partitioning indicates the last element in the subsequence
        batch_splits_bool, batch_splits = partition_predicates(time - 1, split_indexes)

        # Increment cur_emb_idx
        cur_emb_idx = cur_emb_idx + batch_splits

        # Some sequences are finished, do not include these in the indexes used for setting embedding state
        safe_emb_idx = tf.where(elements_finished, tf.zeros_like(cur_emb_idx), cur_emb_idx)

        embeddings_for_loop_state = tf.cond(finished,
                                            lambda: zero_embeddings_state,
                                            lambda: extract_events_tm(encoded_states_tm.c, safe_emb_idx)
                                            )

        embeddings_for_loop_state = create_cell_state_with_embedding(cell, embeddings_for_loop_state)

        print("embeddings_for_loop_state: ", embeddings_for_loop_state)
        print("cell_state: ", cell_state)

        # batch_splits_bool = tf.Print(batch_splits_bool, [tf.shape(embeddings_for_loop_state), tf.shape(cell_state)], message="batch_splits_bool :", summarize=100)

        # Set new cell states, for sequences where next input is new sequence
        next_cell_state = []
        for i in range(0, len(cell_state)):  # Iterate over the number of layers
          next_cell_state.append(
            map_structure_up_to(cell_state[i], lambda emb_for_loop_state, cell_state: tf.where(batch_splits_bool,
                                                                                               emb_for_loop_state,
                                                                                               cell_state),
                                embeddings_for_loop_state[i],
                                cell_state[i]))

        # Format as tuple
        next_cell_state = tuple(next_cell_state)

        print("next_cell_state: ", next_cell_state)

        # Calculate next input for continuing sequences
        # We prepend a zero to ensure the same size as the first special start of decoding index
        # if mode == tf.contrib.learn.ModeKeys.TRAIN:
        #   next_input = tf.concat([tf.zeros([batch_size, 1]), targets_tm[time - 1]], 1)
        # else:
        next_input = cell_output

        # This is a hack to allow the attention mechanism to switch between different memory segments / subsequebces
        # attention_hack_cell, initial_hack_state = construct_hack_cell(cell_state, safe_emb_idx)

        hack_state = loop_state[1]

        hack_state = AttentionWrapperState(cell_state=next_cell_state, attention=hack_state.attention,
                                           time=hack_state.time, alignments=hack_state.alignments, alignment_history=())

        no_grad_hack_state = map_structure(tf.stop_gradient, hack_state)

        hack_state = AttentionWrapperState(
          cell_state=map_structure_up_to(
            hack_state.cell_state,
            lambda no_grad, org: tf.where(batch_splits_bool, no_grad, org),
            no_grad_hack_state.cell_state, hack_state.cell_state),
          attention=tf.where(batch_splits_bool, no_grad_hack_state.attention, hack_state.attention),
          time=hack_state.time,
          alignments=tf.where(batch_splits_bool, no_grad_hack_state.alignments, hack_state.alignments),
          alignment_history=())

        next_input, hack_state = attention_hack_cell(next_input, hack_state)

        next_input = tf.concat([tf.zeros([batch_size, 1]), next_input], 1)

        # Calculate next input
        # next_input = tf.cond(finished,
        #                      # If all sequences finished, return zeros
        #                      lambda: tf.zeros([batch_size, input_size], dtype=tf.float32),
        #                      # Else return next_input
        #                      lambda: next_input)

        # We extract cell output and the state of the last RNN Layer for use in further decoding in the next levels
        emit_output = (cell_output, cell_state[-1])

        next_loop_state = tuple([cur_emb_idx, hack_state])

    return (elements_finished, next_input, next_cell_state, emit_output, next_loop_state)

  return loop_fn
