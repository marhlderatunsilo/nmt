# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""S2S: For training NMT models on single machine multi-gpu setup."""
from __future__ import print_function

import os
import time

import numpy as np
import tensorflow as tf

from tfDeepNLP.models.seq2seq import model as nmt_model, attention_model, hierarchical_model
from tfDeepNLP.models.seq2seq.dataset_iterator.tf_records_dataset import get_dataset
from tfDeepNLP.training.train_estimator import train_estimator
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils import vocab_utils
from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser
from tfDeepNLP.utils.embeddings_utils.init_embeddings_hook import AssignEmbeddingsFnHook

utils.check_tensorflow_version()

__all__ = [
  "s2s_train"
]


def s2s_train(hparams,
              scope=None,
              single_cell_fn=None):
  """
  Train a translation model!

  This is the single machine multi-GPU version! See the "distributed" branch for multi machine training.

  :param hparams: hyper parameters
  :param scope:
  :param single_cell_fn:
  :return:
  """

  """
  Model settings
  """

  # Logging and output
  log_device_placement = hparams.log_device_placement
  out_dir = hparams.out_dir
  model_dir = hparams.out_dir
  log_file = os.path.join(out_dir, "log_%d" % time.time())
  log_f = tf.gfile.GFile(log_file, mode="a")
  utils.print_out("# log_file=%s" % log_file, log_f)
  summary_name = "train_log_%d" % hparams.task_index

  # Training
  is_hierarchical = hparams.hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE
  num_train_steps = hparams.num_train_steps
  steps_per_stats = hparams.steps_per_stats
  steps_per_external_eval = hparams.steps_per_external_eval
  steps_per_eval = steps_per_stats * 10
  if not steps_per_external_eval:
    steps_per_external_eval = 10 * steps_per_eval
  config_proto = utils.get_config_proto(
    log_device_placement=log_device_placement)

  # Select model
  if hparams.hierarchical_split_tokens != CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE or hparams.pointer_generation:
    model_creator = hierarchical_model.HierarchicalModel
  elif not hparams.attention:
    model_creator = nmt_model.S2SModel
  elif hparams.attention_architecture == "standard":
    model_creator = attention_model.AttentionModel
  # elif hparams.attention_architecture in ["gnmt", "gnmt_v2"]:
  #  model_creator = gnmt_model.GNMTModel
  else:
    raise ValueError("Unknown model architecture")

  """
  Get ready for training! 
  """

  model = model_creator(hparams, single_cell_fn=single_cell_fn)

  def load_embeddings(hook_obj, session, coord):
    if hparams.embeddings_option != "train":

      #assign_embeddings = distribution_strategy.broadcast(model.embeddings_placeholder, model.embedding_encoder)

      print("Assigning Embeddings")
      # Assign embeddings
      # We only need to do this for the first model because of the way they share parameters

      with np.load(hparams.embeddings_path) as data:
        pretrained_embeddings = data["embeddings"]

      print("shape: ", pretrained_embeddings.shape)
      print("model.embeddings_placeholder: ", hook_obj.embeddings_placeholder)

      session.run(hook_obj.assign_embeddings, {
        hook_obj.embeddings_placeholder: pretrained_embeddings})

  train_hooks = [
    AssignEmbeddingsFnHook(hparams, assign_embeddings_fn=load_embeddings, target_embeddings_variable_getter=lambda: model.embedding_encoder),
    #train_initializer_hook,
    # tf.train.CheckpointSaverHook(
    #   save_steps=steps_per_eval,
    #   checkpoint_dir=model_dir,
    #   saver=saver,
    #   checkpoint_basename="translate.ckpt"
    # )
  ]

  dev_hooks = [
  ]

  initializer = None

  train_records_names = [hparams.train_prefix + '_seq2seq.tfrecords']
  dev_records_names = [hparams.dev_prefix + '_seq2seq.tfrecords']

  def train_input_fn():

    with tf.device(tf.DeviceSpec(device_type="CPU", device_index=0)):

      src_vocab_file = hparams.src_vocab_file
      tgt_vocab_file = hparams.tgt_vocab_file

      src_vocab_table, tgt_vocab_table, py_src_vocab_table, py_tgt_vocab_table = vocab_utils.create_vocab_tables(
        src_vocab_file, tgt_vocab_file, hparams.share_vocab, create_python_vocabs=True)

      model.src_vocab_table, model.tgt_vocab_table = src_vocab_table, tgt_vocab_table

      dataset = tf.data.TFRecordDataset(train_records_names)

      dataset = get_dataset(dataset, #feature_dataset, label_dataset
                            py_src_vocab_table,
                            py_tgt_vocab_table,
                            hparams.batch_size,
                            hparams.eos,
                            hparams.num_buckets,
                            shuffle=True,
                            random_seed=None,
                            src_max_len=None,
                            num_threads=hparams.parallel_iterations,
                            output_buffer_size=None,
                            hierarchical_split_tokens=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE)

    print("input_fn dataset: ", dataset)

    return dataset#tf.data.Dataset.zip((feature_dataset, label_dataset))

  def dev_input_fn():

    dev_src_file = "%s.%s" % (hparams.dev_prefix, hparams.src)
    dev_tgt_file = "%s.%s" % (hparams.dev_prefix, hparams.tgt)

    with tf.device(tf.DeviceSpec(device_type="CPU", device_index=0)):

      src_vocab_file = hparams.src_vocab_file
      tgt_vocab_file = hparams.tgt_vocab_file

      src_vocab_table, tgt_vocab_table, py_src_vocab_table, py_tgt_vocab_table = vocab_utils.create_vocab_tables(
        src_vocab_file, tgt_vocab_file, hparams.share_vocab, create_python_vocabs=True)

      model.src_vocab_table, model.tgt_vocab_table = src_vocab_table, tgt_vocab_table

      dataset = tf.data.TFRecordDataset(dev_records_names)

      dataset = get_dataset(dataset,
                            py_src_vocab_table,
                            py_tgt_vocab_table,
                            hparams.batch_size,
                            hparams.eos,
                            hparams.num_buckets,
                            shuffle=True,
                            random_seed=None,
                            src_max_len=None,
                            num_threads=hparams.parallel_iterations,
                            output_buffer_size=None,
                            hierarchical_split_tokens=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE)

    return dataset

  elements_per_epoch = 0
  for fn in train_records_names:
    for _ in tf.python_io.tf_record_iterator(fn):
      elements_per_epoch += 1

  train_estimator(hparams, train_input_fn=train_input_fn,
              train_hooks=train_hooks,
              dev_input_fn=dev_input_fn,
              dev_hooks=dev_hooks,
              model_fn=model,
              elements_per_epoch=elements_per_epoch)

