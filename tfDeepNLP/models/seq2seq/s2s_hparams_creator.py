from tfDeepNLP.utils import vocab_utils, misc_utils
from tfDeepNLP.utils.common_hparams_creator import CommonHyperParamsCreator


class Seq2SeqHyperParamsCreator(CommonHyperParamsCreator):
  def __call__(self, flags):

    self.hparams = super(Seq2SeqHyperParamsCreator, self).__call__(flags)

    # Hierarchical
    print("flags.hierarchical_split_tokens: ", flags.hierarchical_split_tokens)
    self.hparams.add_hparam("hierarchical_split_tokens", flags.hierarchical_split_tokens)
    self.hparams.add_hparam("pointer_generation", flags.pointer_generation)
    self.hparams.add_hparam("projection_vocab_size", flags.projection_vocab_size)
    self.hparams.add_hparam("p_gen_l2_scale", flags.p_gen_l2_scale)
    self.hparams.add_hparam("num_decoder_layers", flags.num_decoder_layers)
    self.hparams.add_hparam("rnn_init_op", flags.rnn_init_op)
    self.hparams.add_hparam("encoder_num_units", flags.encoder_num_units)
    self.hparams.add_hparam("max_first_hierarchy_level_sequence_length", flags.max_first_hierarchy_level_sequence_length)
    self.hparams.add_hparam("max_first_hierarchy_level_sequence_length",
                            flags.max_first_hierarchy_level_sequence_length)

    self.hparams.add_hparam("num_units", flags.num_units)

    self.hparams.add_hparam("vocab_prefix", flags.vocab_prefix)
    self.hparams.add_hparam("embeddings_option", flags.embeddings_option)
    self.hparams.add_hparam("embeddings_path", flags.embeddings_path)

    self.hparams.add_hparam("num_layers", flags.num_layers)
    self.hparams.add_hparam("dropout", flags.dropout)
    self.hparams.add_hparam("zoneout", flags.zoneout)
    self.hparams.add_hparam("norm_gain", flags.norm_gain)
    self.hparams.add_hparam("unit_type", flags.unit_type)
    self.hparams.add_hparam("use_peepholes", flags.use_peepholes)
    self.hparams.add_hparam("encoder_type", flags.encoder_type)
    self.hparams.add_hparam("residual", flags.residual)
    self.hparams.add_hparam("num_residual_layers", flags.num_residual_layers)
    self.hparams.add_hparam("src_max_len", flags.src_max_len)
    self.hparams.add_hparam("tgt_max_len", flags.tgt_max_len)
    self.hparams.add_hparam("src_max_len_infer", flags.src_max_len_infer)
    self.hparams.add_hparam("tgt_max_len_infer", flags.tgt_max_len_infer)
    self.hparams.add_hparam("forget_bias", flags.forget_bias)

    self.hparams.add_hparam("infer_batch_size", flags.infer_batch_size)

    self.hparams.add_hparam("beam_width", flags.beam_width)
    self.hparams.add_hparam("combine_beam_search_to_strings", flags.combine_beam_search_to_strings)
    self.hparams.add_hparam("attention_architecture", flags.flags.attention_architecture)

    return self.hparams

  def extend_hparams(self, hparams, flags):

    self.hparams = super(Seq2SeqHyperParamsCreator, self).extend_hparams(hparams, flags)

    ## Vocab
    # Get vocab file names first
    if self.hparams.vocab_prefix:
      src_vocab_file = self.hparams.vocab_prefix + "." + self.hparams.src
      tgt_vocab_file = self.hparams.vocab_prefix + "." + self.hparams.tgt
    else:
      raise ValueError("hparams.vocab_prefix must be provided.")

    # Source vocab
    src_vocab_size, src_vocab_file = vocab_utils.check_and_copy_vocab(
      src_vocab_file,
      self.hparams.out_dir,
      sos=self.hparams.sos,
      eos=self.hparams.eos,
      unk=vocab_utils.UNK)

    # Target vocab
    if self.hparams.share_vocab:
      misc_utils.print_out("  using source vocab for target")
      tgt_vocab_file = src_vocab_file
      tgt_vocab_size = src_vocab_size
    else:
      tgt_vocab_size, tgt_vocab_file = vocab_utils.check_and_copy_vocab(
        tgt_vocab_file,
        self.hparams.out_dir,
        sos=self.hparams.sos,
        eos=self.hparams.eos,
        unk=vocab_utils.UNK)
    self.hparams.add_hparam("src_vocab_size", src_vocab_size)
    self.hparams.add_hparam("tgt_vocab_size", tgt_vocab_size)
    self.hparams.add_hparam("src_vocab_file", src_vocab_file)
    self.hparams.add_hparam("tgt_vocab_file", tgt_vocab_file)

    # Sanity checks
    # if hparams.encoder_type == "bi" and hparams.num_layers % 2 != 0:
    #   raise ValueError("For bi, num_layers %d should be even" %
    #                    hparams.num_layers)
    if (hparams.attention_architecture in ["gnmt"] and
            hparams.num_layers < 2):
      raise ValueError("For gnmt attention architecture, "
                       "num_layers %d should be >= 2" % hparams.num_layers)

    return self.hparams
