from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser


class S2SArgumentsParser(CommonArgumentsParser):
  def __call__(self, parser):
    parser = super(S2SArgumentsParser, self).__call__(parser)

    parser.add_argument(
      "--attention_architecture",
      type=str,
      default="standard",
      help="""\
                standard | gnmt | gnmt_v2.
                standard: use top layer to compute attention.
                gnmt: GNMT style of computing attention, use previous bottom layer to
                    compute attention.
                gnmt_v2: similar to gnmt, but use current bottom layer to compute
                    attention.\
                """)

    parser.add_argument("--hierarchical_split_tokens", nargs='+', type=str,
                        default=CommonArgumentsParser.DEFAULT_EMPTY_LIST_VALUE,
                        help="Tokens to base the hierarchy on .")

    parser.add_argument("--pointer_generation", type=bool,
                        default=False,
                        help="Whether to enable pointer generation for the decoder .")

    parser.add_argument("--projection_vocab_size", type=int,
                        default=None,
                        help="The number of tokens which the decoder should project to.")

    parser.add_argument("--p_gen_l2_scale", type=float,
                        default=0.0,
                        help="The scale of which to apply l2 regluarization to the p_gen parameters.")

    parser.add_argument("--num_decoder_layers", type=int,
                        default=None,
                        help="Number of layers to use in the decoder RNN .")

    parser.add_argument("--rnn_init_op", type=int,
                        default=None,
                        help="The init up used for RNN layers, options are : orthogonal | uniform | glorot_normal | glorot_uniform | orthogonal+glorot_normal")

    parser.add_argument("--encoder_num_units", type=int,
                        default=None,
                        help="Number of units in the encoder RNN layers")

    parser.add_argument("--inference_input_file", type=str, default=None,
                        help="Set to the text to decode.")
    parser.add_argument("--inference_list", type=str, default=None,
                        help=("A comma-separated list of sentence indices "
                              "(0-based) to decode."))
    parser.add_argument("--infer_batch_size", type=int, default=32,
                        help="Batch size for inference mode.")
    parser.add_argument("--inference_pred_output_file", type=str, default=None,
                        help="Output file to store classification results.")
    parser.add_argument("--inference_prob_output_file", type=str, default=None,
                        help="Output file to store classification probability results.")
    parser.add_argument("--inference_ref_file", type=str, default=None,
                        help="""Reference file to compute evaluation scores (if provided).""")
    parser.add_argument("--max_first_hierarchy_level_sequence_length", type=int,
                        default=None,
                        help="Max lengths of subsequence at the first hierarchy level for hierarchical models")

    parser.add_argument("--src_max_len", type=int, default=50,
                        help="Max length of src sequences during training.")
    parser.add_argument("--tgt_max_len", type=int, default=50,
                        help="Max length of tgt sequences during training.")
    parser.add_argument("--src_max_len_infer", type=int, default=None,
                        help="Max length of src sequences during inference.")
    parser.add_argument("--tgt_max_len_infer", type=int, default=None,
                        help="""Max length of tgt sequences during inference.
                            Also used to restrict the maximum decoding length.""")

    parser.add_argument("--unit_type", type=str, default="lstm",
                        help="lstm | gru | layer_norm_lstm")
    parser.add_argument("--use_peepholes", type=bool, default=False,
                        help="Whether to use peepholes in the selected unit architecture. (Ignored if not supported)"),
    parser.add_argument("--forget_bias", type=float, default=1.0,
                        help="Forget bias for BasicLSTMCell.")

    parser.add_argument("--encoder_type", type=str, default="uni", help="""\
            uni | bi | gnmt. For bi, we build num_layers/2 bi-directional layers.For
            gnmt, we build 1 bi-directional layer, and (num_layers - 1) uni-
            directional layers.\
            """)
    parser.add_argument("--residual", type="bool", nargs="?", const=True,
                        default=False,
                        help="Whether to add residual connections.")
    parser.add_argument("--num_residual_layers", type=int, default=0, help="The number of residual layers")

    parser.add_argument("--dropout", type=float, default=0.0,
                        help="Dropout rate (not keep_prob)")
    parser.add_argument("--zoneout", type=float, default=0.0,
                        help="Zoneout prob")

    parser.add_argument("--num_units", type=int, default=None, help="General number of units for model layers.")

    parser.add_argument("--num_layers", type=int, default=None, help="General number of layers in the model.")

    parser.add_argument("--embeddings_option", type=str, default="train", help="What type of embeddings to use, currently supported: glove | train")

    parser.add_argument("--embeddings_path", type=str, default=None,
                        help="Where to find the embeddings, must be a numpy file (npz) and the order must match vocab")

    parser.add_argument("--beam_width", type=int, default=0,
                               help="""Beam width when using beam search decoder. If 0 (default), use standard
                                      decoder with greedy helper.""")

    parser.add_argument("--combine_beam_search_to_strings", type=bool, default=False,
                        help="""Whether to combine all the results from beam search into a single output.""")

    parser.add_argument("--norm_gain", type=float, default=1.0,
                        help="""The norm gain used for layer normalization.""")

    parser.add_argument("--create_tf_records", type="bool", default=False, help="Mode option: Whether to create Tensorflow records (Instead of training of inference)  ")

    return parser
