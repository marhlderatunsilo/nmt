"""Basic sequence-to-sequence model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tfDeepNLP.rnn_utils import create_rnn_cell

from tfDeepNLP.utils import misc_utils as utils
from .s2s_base_model import S2SBaseModel

utils.check_tensorflow_version()

__all__ = ["S2SModel"]


class S2SModel(S2SBaseModel):
  """S2S: Sequence-to-sequence dynamic model.

  This class implements a multi-layer recurrent neural network as encoder,
  and a multi-layer recurrent neural network decoder.
  """

  def _build_encoder(self, hparams):
    """Build an encoder."""
    num_layers = hparams.num_layers
    num_residual_layers = hparams.num_residual_layers

    iterator = self.iterator

    source = iterator.source
    if self.time_major:
      source = tf.transpose(source)

    with tf.variable_scope("encoder") as scope:
      dtype = scope.dtype
      # Look up embedding, emp_inp: [max_time, batch_size, num_units]
      encoder_emb_inp = self.create_look_up_fn(self.embedding_encoder)(source) #tf.nn.embedding_lookup(, )

      # Encoder_outpus: [max_time, batch_size, num_units]
      if hparams.encoder_type == "uni":
        utils.print_out("  num_layers = %d, num_residual_layers=%d" %
                        (num_layers, num_residual_layers))
        cell = self._build_encoder_cell(
          hparams, num_layers, num_residual_layers)

        encoder_outputs, encoder_state = tf.nn.dynamic_rnn(
          cell,
          encoder_emb_inp,
          dtype=dtype,
          parallel_iterations=min(hparams.batch_size, hparams.parallel_iterations),
          swap_memory=True,
          sequence_length=iterator.source_sequence_length,
          time_major=self.time_major)
      elif hparams.encoder_type == "bi":
        num_bi_layers = int(num_layers / 2)
        num_bi_residual_layers = int(num_residual_layers / 2)
        utils.print_out("  num_bi_layers = %d, num_bi_residual_layers=%d" %
                        (num_bi_layers, num_bi_residual_layers))

        encoder_outputs, bi_encoder_state = (
          self._build_bidirectional_rnn(
            inputs=encoder_emb_inp,
            sequence_length=iterator.source_sequence_length,
            dtype=dtype,
            hparams=hparams,
            num_bi_layers=num_bi_layers,
            num_bi_residual_layers=num_bi_residual_layers))

        if num_bi_layers == 1:
          encoder_state = bi_encoder_state
        else:
          # alternatively concat forward and backward states
          encoder_state = []
          for layer_id in range(num_bi_layers):
            encoder_state.append(bi_encoder_state[0][layer_id])  # forward
            encoder_state.append(bi_encoder_state[1][layer_id])  # backward
          encoder_state = tuple(encoder_state)
      else:
        raise ValueError("Unknown encoder_type %s" % hparams.encoder_type)
    return encoder_outputs, encoder_state

  def _build_bidirectional_rnn(self, inputs, sequence_length,
                               dtype, hparams,
                               num_bi_layers,
                               num_bi_residual_layers):
    """Create and call biddirectional RNN cells.

    Args:
      num_residual_layers: Number of residual layers from top to bottom. For
        example, if `num_bi_layers=4` and `num_residual_layers=2`, the last 2 RNN
        layers in each RNN cell will be wrapped with `ResidualWrapper`.

    Returns:
      The concatenated bidirectional output and the bidirectional RNN cell"s
      state.
    """

    # Construct forward and backward cells
    fw_cell = self._build_encoder_cell(hparams,
                                       num_bi_layers,
                                       num_bi_residual_layers, )
    bw_cell = self._build_encoder_cell(hparams,
                                       num_bi_layers,
                                       num_bi_residual_layers,
                                       )

    bi_outputs, bi_state = tf.nn.bidirectional_dynamic_rnn(
      fw_cell,
      bw_cell,
      inputs,
      dtype=dtype,
      parallel_iterations=min(hparams.batch_size, hparams.parallel_iterations),
      sequence_length=sequence_length,
      swap_memory=True,
      time_major=self.time_major)

    return tf.concat(bi_outputs, -1), bi_state

  def _build_decoder_cell(self, hparams, encoder_outputs=None, encoder_state=None,
                          source_sequence_length=None):
    """Build an RNN cell that can be used by decoder."""
    # We only make use of encoder_outputs in attention-based models
    if hparams.attention:
      raise ValueError("BasicModel doesn't support attention.")

    num_layers = hparams.num_layers
    num_residual_layers = hparams.num_residual_layers

    cell = create_rnn_cell(
      unit_type=hparams.unit_type,
      num_units=hparams.num_units,
      num_layers=num_layers,
      num_residual_layers=num_residual_layers,
      forget_bias=hparams.forget_bias,
      dropout=hparams.dropout,
      zoneout=hparams.zoneout,
      mode=self.mode,
      single_cell_fn=self.single_cell_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes)

    # For beam search, we need to replicate encoder infos beam_width times
    if self.mode == tf.contrib.learn.ModeKeys.INFER and hparams.beam_width > 0:
      decoder_initial_state = tf.contrib.seq2seq.tile_batch(
        encoder_state, multiplier=hparams.beam_width)
    else:
      decoder_initial_state = encoder_state

    return cell, decoder_initial_state
