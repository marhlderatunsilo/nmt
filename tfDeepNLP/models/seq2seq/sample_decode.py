"""
Sample decoding

"""

import random

from tfDeepNLP import model_helper
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils import nmt_utils

__all__ = ["run_sample_decode"]


def run_sample_decode(infer_model, infer_sess, model_dir, hparams,
                      summary_writer, src_data, tgt_data):
  """Sample decode a random sentence from src_data."""
  with infer_model.graph.as_default():
    loaded_infer_model, global_step = model_helper.create_or_load_model(
      infer_model.model, model_dir, infer_sess, "infer")

    _sample_decode(loaded_infer_model, global_step, infer_sess, hparams,
                   infer_model.iterator, src_data, tgt_data,
                   infer_model.src_placeholder,
                   infer_model.batch_size_placeholder, summary_writer)


def _sample_decode(model, global_step, sess, hparams, iterator, src_data,
                   tgt_data, iterator_src_placeholder,
                   iterator_batch_size_placeholder, summary_writer):
  """Pick a sentence and decode."""
  decode_id = random.randint(0, len(src_data) - 1)
  utils.print_out("  # %d" % decode_id)

  iterator_feed_dict = {
    iterator_src_placeholder: [src_data[decode_id]],
    iterator_batch_size_placeholder: 1,
  }
  sess.run(iterator.initializer, feed_dict=iterator_feed_dict)

  nmt_outputs, attention_summary = model.decode(sess)

  if hparams.beam_width > 0 and not hparams.pointer_generation:
    # get the top translation.
    nmt_outputs = nmt_outputs[0]

  translation = nmt_utils.get_translation(
    nmt_outputs,
    sent_id=0,
    tgt_eos=hparams.eos,
    bpe_delimiter=hparams.bpe_delimiter)
  utils.print_out("    src: %s" % src_data[decode_id])
  utils.print_out("    ref: %s" % tgt_data[decode_id])
  utils.print_out(b"    tfDeepNLP: %s" % translation)

  # Summary
  if attention_summary is not None:
    summary_writer.add_summary(attention_summary, global_step)
