# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""S2S: Attention-based sequence-to-sequence model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

from tfDeepNLP.attention.attention import create_attention_mechanism
from tfDeepNLP.models.seq2seq import model
from tfDeepNLP.rnn_utils import create_rnn_cell

__all__ = ["AttentionModel"]


class AttentionModel(model.S2SModel):
  """Sequence-to-sequence dynamic model with attention.

  This class implements a multi-layer recurrent neural network as encoder,
  and an attention-based decoder. This is the same as the model described in
  (Luong et al., EMNLP'2015) paper: https://arxiv.org/pdf/1508.04025v5.pdf.
  This class also allows to use GRU cells in addition to LSTM cells with
  support for dropout.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_vocab_table,
               target_vocab_table,
               reverse_tgt_vocab_table=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None
               ):
    super(AttentionModel, self).__init__(
      hparams=hparams,
      mode=mode,
      iterator=iterator,
      source_vocab_table=source_vocab_table,
      target_vocab_table=target_vocab_table,
      reverse_tgt_vocab_table=reverse_tgt_vocab_table,
      scope=scope,
      single_cell_fn=single_cell_fn,
      model_device_fn=model_device_fn,
      optimizer_op=optimizer_op,
      learning_rate=learning_rate,
      first_gpu_id=first_gpu_id
    )

    # if self.mode == tf.contrib.learn.ModeKeys.INFER:
    # self.infer_summary = self._get_infer_summary(hparams)

    # if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
    #
    #   summaries_to_merge = [self.train_summary]
    #
    #   if hparams.add_coverage_loss:
    #     summaries_to_merge.append(tf.summary.scalar("coverage_term", self.coverage_term))

    # if self.alignment_history is None and self.final_context_state:
    #   self.alignment_history = self.final_context_state.alignment_history.stack()
    #
    # self.infer_summary = _create_attention_images_summary(self.alignment_history)
    #
    # summaries_to_merge.append(self.infer_summary)
    #
    # self.train_summary = tf.summary.merge(summaries_to_merge)

  def _build_decoder_cell(self, hparams, encoder_outputs=None, encoder_state=None,
                          source_sequence_length=None):

    """Build a RNN cell with attention mechanism that can be used by decoder."""
    attention_option = hparams.attention
    attention_architecture = hparams.attention_architecture

    if attention_architecture != "standard":
      raise ValueError(
        "Unknown attention architecture %s" % attention_architecture)

    num_units = hparams.num_units
    num_layers = hparams.num_layers
    num_residual_layers = hparams.num_residual_layers
    beam_width = hparams.beam_width

    dtype = tf.float32

    # with tf.device(tf.DeviceSpec(device_type="GPU", device_index=self.first_gpu_id)):
    if self.time_major:
      memory = tf.transpose(encoder_outputs, [1, 0, 2])
    else:
      memory = encoder_outputs

    if self.mode == tf.contrib.learn.ModeKeys.INFER and beam_width > 0:
      memory = tf.contrib.seq2seq.tile_batch(
        memory, multiplier=beam_width)
      source_sequence_length = tf.contrib.seq2seq.tile_batch(
        source_sequence_length, multiplier=beam_width)
      encoder_state = tf.contrib.seq2seq.tile_batch(
        encoder_state, multiplier=beam_width)
      batch_size = self.batch_size * beam_width
    else:
      batch_size = self.batch_size

    attention_mechanism = create_attention_mechanism(
      attention_option, num_units, memory, source_sequence_length)

    cell = create_rnn_cell(
      unit_type=hparams.unit_type,
      num_units=num_units,
      num_layers=num_layers,
      num_residual_layers=num_residual_layers,
      forget_bias=hparams.forget_bias,
      dropout=hparams.dropout,
      zoneout=hparams.zoneout,
      mode=self.mode,
      single_cell_fn=self.single_cell_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes
     )

    # Only generate alignment in greedy INFER mode.
    alignment_history = (self.mode == tf.contrib.learn.ModeKeys.INFER and beam_width == 0) or \
                        (self.mode == tf.contrib.learn.ModeKeys.TRAIN and hparams.add_coverage_loss) or \
                        (self.mode == tf.contrib.learn.ModeKeys.EVAL and hparams.add_coverage_loss)

    assert not alignment_history

    cell = tf.contrib.seq2seq.AttentionWrapper(
      cell,
      attention_mechanism,
      attention_layer_size=num_units,
      alignment_history=alignment_history,
      name="attention")

    if hparams.pass_hidden_state:
      decoder_initial_state = cell.zero_state(batch_size, dtype).clone(
        cell_state=encoder_state)
    else:
      decoder_initial_state = cell.zero_state(batch_size, dtype)

    print("decoder_initial_state:", decoder_initial_state)

    return cell, decoder_initial_state

  # def _get_infer_summary(self, hparams):
  #   if hparams.beam_width > 0:
  #     return tf.no_op()
  #   return _create_attention_images_summary(self.final_context_state.alignment_history.stack())

  def setup_loss_computation(self, hparams, logits, targets, final_context_state=None, final_sequence_lengths=None, **kwargs):
    """Compute optimization loss."""

    loss = super(AttentionModel, self).setup_loss_computation(hparams, logits,
                                                              targets,
                                                              final_context_state=final_context_state,
                                                              final_sequence_lengths=final_sequence_lengths)

    # if hparams.add_coverage_loss:
    #   coverage_term = self._compute_coverage_loss(hparams, final_context_state.alignment_history,
    #                                               final_sequence_lengths)
    #   self.coverage_term = tf.multiply(hparams.cov_loss_wt, coverage_term)

    # coverage_term = tf.Print(coverage_term, [coverage_term], message="COVERAGE TERM")

    # Add the coverage termn to the loss function
    # loss = tf.add(loss, coverage_term)


    return loss

    # def _compute_coverage_loss(self, hparams, alignment_history, lengths):
    #
    #   size = alignment_history.size()
    #   alignment_history = alignment_history.stack()
    #   self.alignment_history = alignment_history
    #   i = (alignment_history, tf.zeros_like(alignment_history[0]), tf.zeros([self.batch_size]), tf.constant(0))
    #   c = lambda alignment_history, _, __, i: tf.less(i, size)
    #
    #   def body(alignment_history, coverage, covloss_sum, i):
    #     a = alignment_history[i]
    #     covloss = tf.reduce_sum(tf.minimum(a, coverage), [1])
    #
    #     return alignment_history, tf.add(coverage, a), tf.add(covloss_sum, covloss), tf.add(i, 1)
    #
    #   result = tf.while_loop(c, body, i, swap_memory=True, back_prop=True)
    #
    #   coverage_lossses = result[2]
    #   coverage_lossses = coverage_lossses / tf.to_float(lengths)
    #
    #   return tf.reduce_mean(coverage_lossses)

# def _create_attention_images_summary(alignment_history):
#   """create attention image and attention summary."""
#   attention_images = (alignment_history)
#   # Reshape to (batch, src_seq_len, tgt_seq_len,1)
#   attention_images = tf.expand_dims(
#     tf.transpose(attention_images, [1, 2, 0]), -1)
#   # Scale to range [0, 255]
#
#   attention_images = tf.reduce_sum(attention_images, axis=2, keep_dims=True)
#   attention_images *= 255
#
#   #attention_images = tf.reduce_mean(attention_images, axis=)
#
#   attention_summary = tf.summary.image("attention_images", attention_images, max_outputs=10)
#   return attention_summary
