"""
Evaluation pipelines

"""

import os

import tensorflow as tf

from tfDeepNLP import model_helper
from tfDeepNLP.models.seq2seq import inference
from tfDeepNLP.models.seq2seq.sample_decode import run_sample_decode

from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils import nmt_utils
from tfDeepNLP.utils.cost_fn_utils import compute_perplexity

__all__ = ["run_internal_eval", "run_external_eval", "run_full_eval"]

"""
Internal evaluation
"""


def run_internal_eval(
    eval_models, eval_sess, iterator, hparams, summary_writer, max_elements=None):
  """S2S: Compute internal evaluation (perplexity) for both dev / test."""
  with eval_models[0].graph.as_default():
    global_step = tf.train.global_step(eval_sess, eval_models[0].model.global_step)

    # model_helper.create_or_load_model(
    # eval_model.model, model_dir, eval_sess, "eval")

    # dev_src_file = "%s.%s" % (hparams.dev_prefix, hparams.src)
    # dev_tgt_file = "%s.%s" % (hparams.dev_prefix, hparams.tgt)
    #
    # dev_eval_iterator_feed_dict = {
    #   eval_models[0].src_file_placeholder: dev_src_file,
    #   eval_models[0].tgt_file_placeholder: dev_tgt_file
    # }

    dev_ppl = _internal_eval([eval_model.model for eval_model in eval_models], global_step, eval_sess,
                             iterator, #dev_eval_iterator_feed_dict,
                             summary_writer, "dev", max_elements)
    test_ppl = None
    # if hparams.test_prefix:
    #   test_src_file = "%s.%s" % (hparams.test_prefix, hparams.src)
    #   test_tgt_file = "%s.%s" % (hparams.test_prefix, hparams.tgt)
    #   test_eval_iterator_feed_dict = {
    #     eval_models[0].src_file_placeholder: test_src_file,
    #     eval_models[0].tgt_file_placeholder: test_tgt_file
    #   }
    #
    #   test_ppl = _internal_eval([eval_model.model for eval_model in eval_models], global_step, eval_sess,
    #                             iterator, test_eval_iterator_feed_dict,
    #                             summary_writer, "test", max_elements)
  return dev_ppl, test_ppl


def _internal_eval(models, global_step, sess, iterator, #iterator_feed_dict,
                   summary_writer, label, max_elements):
  """Computing perplexity."""
  sess.run(iterator.initializer,
           #feed_dict=iterator_feed_dict
           )
  ppl = compute_perplexity(models, sess, label, max_elements=max_elements)
  utils.add_summary(summary_writer, global_step, "%s_ppl" % label, ppl)
  return ppl


"""
External evaluation
"""


def run_external_eval(infer_model, infer_sess, model_dir, hparams,
                      summary_writer, max_elements=None, save_best_dev=True):
  """Compute external evaluation (bleu, rouge, etc.) for both dev / test."""
  with infer_model.graph.as_default():
    loaded_infer_model, global_step = model_helper.create_or_load_model(
      infer_model.model, model_dir, infer_sess, "infer")

    dev_src_file = "%s.%s" % (hparams.dev_prefix, hparams.src)
    dev_tgt_file = "%s.%s" % (hparams.dev_prefix, hparams.tgt)
    dev_infer_iterator_feed_dict = {
      infer_model.src_placeholder: inference.load_data(dev_src_file),
      infer_model.batch_size_placeholder: hparams.infer_batch_size,
    }
    dev_scores = _external_eval(
      loaded_infer_model,
      global_step,
      infer_sess,
      hparams,
      infer_model.iterator,
      dev_infer_iterator_feed_dict,
      dev_tgt_file,
      "dev",
      summary_writer,
      max_elements,
      save_on_best=save_best_dev)

    test_scores = None
    if hparams.test_prefix:
      test_src_file = "%s.%s" % (hparams.test_prefix, hparams.src)
      test_tgt_file = "%s.%s" % (hparams.test_prefix, hparams.tgt)
      test_infer_iterator_feed_dict = {
        infer_model.src_placeholder: inference.load_data(test_src_file),
        infer_model.batch_size_placeholder: hparams.infer_batch_size,
      }
      test_scores = _external_eval(
        loaded_infer_model,
        global_step,
        infer_sess,
        hparams,
        infer_model.iterator,
        test_infer_iterator_feed_dict,
        test_tgt_file,
        "test",
        summary_writer,
        max_elements,
        save_on_best=False)
  return dev_scores, test_scores, global_step


def _external_eval(model, global_step, sess, hparams, iterator,
                   iterator_feed_dict, tgt_file, label, summary_writer,
                   max_elements, save_on_best):
  """External evaluation such as BLEU and ROUGE scores."""
  out_dir = hparams.out_dir
  decode = global_step > 0
  if decode:
    utils.print_out("# External evaluation, global step %d" % global_step)

  sess.run(iterator.initializer, feed_dict=iterator_feed_dict)

  output = os.path.join(out_dir, "output_%s" % label)
  scores = nmt_utils.decode_and_evaluate(
    label,
    model,
    sess,
    output,
    ref_file=tgt_file,
    metrics=hparams.metrics,
    bpe_delimiter=hparams.bpe_delimiter,
    beam_width=hparams.beam_width,
    tgt_eos=hparams.eos,
    decode=decode)

  # Save on best metrics
  if decode:
    for metric in hparams.metrics:
      utils.add_summary(summary_writer, global_step, "%s_%s" % (label, metric),
                        scores[metric])
      # metric: larger is better
      if save_on_best and scores[metric] > getattr(hparams, "best_" + metric):
        setattr(hparams, "best_" + metric, scores[metric])
        # model.saver.save(
        #   sess,
        #   os.path.join(
        #     getattr(hparams, "best_" + metric + "_dir"), "translate.ckpt"),
        #   global_step=model.global_step)
    utils.save_hparams(out_dir, hparams)
  return scores


"""
Full evaluation
"""


def run_full_eval(model_dir, infer_model, infer_sess, eval_models, eval_sess,
                  hparams, summary_writer, sample_src_data, sample_tgt_data, max_elements=None):
  """Wrapper for running sample_decode, internal_eval and external_eval."""
  run_sample_decode(infer_model, infer_sess, model_dir, hparams, summary_writer,
                    sample_src_data, sample_tgt_data)

  dev_ppl, test_ppl = run_internal_eval(
    eval_models, eval_sess, model_dir, hparams, summary_writer, max_elements)
  dev_scores, test_scores, global_step = run_external_eval(
    infer_model, infer_sess, model_dir, hparams, summary_writer)

  result_summary = _format_results("dev", dev_ppl, dev_scores, hparams.metrics)
  if hparams.test_prefix:
    result_summary += ", " + _format_results("test", test_ppl, test_scores,
                                             hparams.metrics)

  return result_summary, global_step, dev_scores, test_scores, dev_ppl, test_ppl


"""
Helpers
"""


def _format_results(name, ppl, scores, metrics):
  """Format results."""
  result_str = "%s ppl %.2f" % (name, ppl)
  if scores:
    for metric in metrics:
      result_str += ", %s %s %.1f" % (name, metric, scores[metric])
  return result_str


def _get_best_results(hparams):
  """Summary of the current best results."""
  tokens = []
  for metric in hparams.metrics:
    tokens.append("%s %.2f" % (metric, getattr(hparams, "best_" + metric)))
  return ", ".join(tokens)
