"""S2S: Create sequence-to-sequence models."""

import collections

import tensorflow as tf

__all__ = ["create_train_model", "create_eval_model"]


class TrainModel(
  collections.namedtuple("TrainModel", ("model", "iterator",
                                        ))):
  pass


def create_train_model(
    model_creator, hparams, src_vocab_table, tgt_vocab_table, reverse_tgt_vocab_table, iterator=None, mode=None,
    scope=None, single_cell_fn=None, model_device_fn=None, optimizer_op=None, learning_rate=None, first_gpu_id=None):
  """Create train model"""

  model = model_creator(
    hparams,
    iterator=iterator,
    mode=mode,
    source_vocab_table=src_vocab_table,
    target_vocab_table=tgt_vocab_table,
    reverse_tgt_vocab_table=reverse_tgt_vocab_table,
    scope=scope,
    single_cell_fn=single_cell_fn,
    model_device_fn=model_device_fn,
    optimizer_op=optimizer_op,
    learning_rate=learning_rate,
    first_gpu_id=first_gpu_id
  )

  return TrainModel(
    model=model,
    iterator=iterator)


class EvalModel(
  collections.namedtuple("EvalModel",
                         ("graph", "model",
                          #"src_file_placeholder",
                          #"tgt_file_placeholder",
                          "iterator"))):
  pass


def create_eval_model(graph, model_creator, iterator, scope=None, model_device_fn=None, first_gpu_id=None): # src_file_placeholder, tgt_file_placeholder,
  """Create train graph, model, src/tgt file holders, and iterator."""

  with graph.as_default():

    model = model_creator(
      iterator=iterator,
      mode=tf.contrib.learn.ModeKeys.EVAL,
      scope=scope,
      model_device_fn=model_device_fn,
      first_gpu_id=first_gpu_id)

  return EvalModel(
    graph=graph,
    model=model.model,
    #src_file_placeholder=src_file_placeholder,
    #tgt_file_placeholder=tgt_file_placeholder,
    iterator=iterator)
