import tensorflow as tf
import argparse
import numpy as np

def export_static_bert_embeddings(checkpoint_path):
  with tf.Session() as sess:

    new_saver = tf.train.import_meta_graph(checkpoint_path + '.meta')
    new_saver.restore(sess, checkpoint_path)

    static_embeddings_variable = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="bert/embeddings")
    static_embeddings_variable = static_embeddings_variable[0]

    print("static_embeddings_variable: ", static_embeddings_variable)

    numpy_static_embeddings_variable = sess.run(static_embeddings_variable)


    for i in tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="bert/embeddings"):
      print(i)

    np.savetxt('static_bert_embeddings.txt', numpy_static_embeddings_variable, delimiter=',')

def main():

  parser = argparse.ArgumentParser(description='Extract the static embeddings of a BERT model.')
  parser.add_argument('--checkpoint_path', type=str, required=True,
                      help='The path to a BERT model checkpoint')

  args = parser.parse_args()

  export_static_bert_embeddings(args.checkpoint_path)

if __name__ == '__main__':
  main()