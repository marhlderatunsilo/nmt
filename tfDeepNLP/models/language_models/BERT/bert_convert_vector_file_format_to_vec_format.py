import argparse
import json


def convert_bert_json_to_vec(feature_extractor_output_file_path):

  with open(feature_extractor_output_file_path, mode="r", encoding="utf8") as input_file_p, \
      open("C:/Users/marhl/nmt/models/language_models/BERT/data/BERT-cased_L-24_H-1024_A-16/dynamic_bert_output.vec", mode="w+",
           encoding="utf8") as output_file_p:
    pass

    for line in input_file_p:

      bert_sentence_json = json.loads(line)
      bert_sentence_features = bert_sentence_json["features"]

      if len(bert_sentence_features) == 3:

        token = bert_sentence_features[1]["token"]
        vector = bert_sentence_features[1]["layers"][0]["values"]
        output_line = token + " " + " ".join(map(str, vector)) + "\n"
        output_file_p.write(output_line)

      # Ignore lines that did not result in a single token by the bert tokenizer
      else:
        pass


def aggregate_vocab_and_numpy_embeddings_text(static_bert_numpy_text_embeddings_path, vocab_path):
  with open(vocab_path, mode="r", encoding="utf8") as vocab_file_p, \
      open(static_bert_numpy_text_embeddings_path, mode="r", encoding="utf8") as vector_file_p, \
      open("C:/Users/marhl/nmt/models/language_models/BERT/data/BERT-cased_L-24_H-1024_A-16/static_bert_output.vec",
           mode="w+",
           encoding="utf8") as output_file_p:

    for token, vector in zip(vocab_file_p, vector_file_p):
      output_line = token[:-1] + " " + vector[:-1] + "\n"
      output_file_p.write(output_line)

def main():
  parser = argparse.ArgumentParser(description='Convert BERT embeddings to .VEC format.')
  parser.add_argument('--feature_extractor_output_file_path', type=str,
                      help='The path to the out json file produced by the bert script "extract_features.py"')
  parser.add_argument('--static_bert_numpy_text_embeddings_path', type=str,
                      help='A path to an output of "bert_static_embeddings_extractor.py"')
  parser.add_argument('--vocab_path', type=str,
                      help='A path to the vocab corresponding to static_bert_numpy_text_embeddings_path')

  args = parser.parse_args()

  convert_bert_json_to_vec(args.feature_extractor_output_file_path)

  if args.static_bert_numpy_text_embeddings_path is not None and args.vocab_path is None:
    print(
      "Error: You did not provide a vocab argument along with your 'static_bert_numpy_text_embeddings_path' argument")
  elif args.static_bert_numpy_text_embeddings_path is not None:
    aggregate_vocab_and_numpy_embeddings_text(args.static_bert_numpy_text_embeddings_path, args.vocab_path)


if __name__ == '__main__':
  main()
