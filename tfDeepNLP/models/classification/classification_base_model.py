"""S2C: Basic sequence-to-classification model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc
import collections

import tensorflow as tf

from tfDeepNLP import model_helper
from tfDeepNLP.model_base.base_model import BaseModel
from tfDeepNLP.rnn_utils import create_rnn_cell, unpack_encoder_state
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.cost_fn_utils import compute_softmax_cross_entropy_loss, compute_sigmoid_cross_entropy_loss
from tfDeepNLP.utils.data_augmentation_utils import embedding_augmentation
from tfDeepNLP.utils.gradient_utils import gradient_clip
from tfDeepNLP.utils.initializer_utils import get_initializer

utils.check_tensorflow_version()

__all__ = ["ClassificationBaseModel", "EnsembleBinaryStatistics"]


class ClassificationBaseModel(BaseModel):
  """Sequence-to-classification base class.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_token_vocab_table,
               source_char_vocab_table=None,
               target_token_vocab_table=None,
               target_char_vocab_table=None,
               augment_embeddings=False,
               noise_amount_mean=None,
               noise_amount_stddev=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None):

    super(ClassificationBaseModel, self).__init__(
      hparams=hparams,
      single_cell_fn=single_cell_fn,
      )

    self.mode = mode
    self.iterator = iterator
    self.model_device_fn = model_device_fn
    self.optimizer_op = optimizer_op
    self.learning_rate = learning_rate
    self.first_gpu_id = first_gpu_id

    assert isinstance(iterator, self.batched_input_class)

    self.source_token_vocab_table = source_token_vocab_table
    self.source_char_vocab_table = source_char_vocab_table
    self.target_token_vocab_table = target_token_vocab_table
    self.target_char_vocab_table = target_char_vocab_table
    self.augment_embeddings = augment_embeddings if self.mode == tf.contrib.learn.ModeKeys.TRAIN else False
    self.noise_amount_mean = noise_amount_mean
    self.noise_amount_stddev = noise_amount_stddev
    # self.gradient_norm_summary = None  # Initialize

    # Initializer
    initializer = get_initializer(
      hparams.init_op, hparams.random_seed, hparams.init_weight)
    tf.get_variable_scope().set_initializer(initializer)

    self.batch_size = self.dynamic_batch_size
    self.init_embeddings(hparams, scope)

    ## Train graph
    res = self.build_graph(hparams, scope=scope, allowed_models=self.allowed_models)

    self.logits = res[0]
    self.loss = res[1]
    self.__ensemble_statistics = res[2]
    self.word_count = tf.reduce_sum(self.iterator.source_sequence_length)
    self.predictions, self.probabilities = self.extract_preds_and_probs(self.logits)

    if self.mode != tf.contrib.learn.ModeKeys.INFER:
      self.evaluator = self.evaluate(self.targets, self.predictions, self.probabilities)

    #self.loss = tf.Print(self.loss, [tf.reduce_sum(self.predictions, -1), tf.reduce_sum(self.targets, -1)], message="predictions:", summarize=100)

    self.setup_gradient_computation(hparams)

  def setup_gradient_computation(self, hparams, **kwargs):
    # Gradients and SGD update operation for training the model.
    # Arrange for the embedding vars to appear at the beginning.
    if self.mode == tf.contrib.learn.ModeKeys.TRAIN:

      gradients = self.optimizer_op.compute_gradients(
        self.loss,
        # params,
        colocate_gradients_with_ops=hparams.colocate_gradients_with_ops,
        aggregation_method=tf.AggregationMethod.ADD_N
      )
      print("Computed gradients.")
      if self.clip_gradients:
        gradients, self.params = zip(*gradients)

        print("Unzipped gradients.")

        self.clipped_gradients, self.gradient_norm_summary = gradient_clip(
          gradients, max_gradient_norm=hparams.max_gradient_norm)

        print("Clipped gradients")

        self.gradients = zip(self.clipped_gradients, self.params)

        print("Zipped gradients")



      else:
        self.gradients = gradients
        self.gradient_norm_summary = None
    else:
      self.gradients = None
      self.gradient_norm_summary = None

  def init_embeddings(self, hparams, scope):
    """Init embeddings."""

    if hparams.use_tokens:
      self.token_embedding_encoder = (
        model_helper.create_emb_for_encoder_and_decoder(
          share_vocab=False,
          src_vocab_size=hparams.src_token_vocab_size,
          tgt_vocab_size=None,
          src_embed_size=hparams.token_embedding_size,
          tgt_embed_size=None,
          #num_partitions=hparams.num_gpus,  # max(hparams.num_worker_hosts, hparams.num_ps_hosts),
          train_embeddings=hparams.train_embeddings,
          scope=scope, #+ "/tokens" if scope else "tokens"
          decode=False))
    if hparams.use_chars:
      self.char_embedding_encoder = (
        model_helper.create_emb_for_encoder_and_decoder(
          share_vocab=False,
          src_vocab_size=hparams.src_char_vocab_size,
          tgt_vocab_size=None,
          src_embed_size=hparams.char_embedding_size,
          tgt_embed_size=None,
          num_partitions=hparams.num_gpus,  # max(hparams.num_worker_hosts, hparams.num_ps_hosts),
          train_embeddings=hparams.train_embeddings,
          scope=scope + "/chars" if scope else "chars",
          decode=False))

  def statistics(self):
    if self.mode != tf.contrib.learn.ModeKeys.INFER:
      return (
        self.loss,
        self.evaluator.accuracy,
        self.evaluator.f1,
        self.evaluator.auc,
        self.evaluator.precision,
        self.evaluator.recall,
        self.evaluator.specificity,
        self.evaluator.class_ratio,

        # self.predict_count, # TODO Add in again
        self.word_count,
        self.batch_size,
        self.gradient_norm_summary
      )
    else:
      print("Statistics does nothing in infer mode.")
      return tf.no_op()

  def get_classifications(self):
    return (self.predictions, self.probabilities)

  def build_graph(self, hparams, scope=None, allowed_models=None, **kwargs):
    """
    Creates a sequence-to-class model with dynamic RNN.
    Args:
      hparams: Hyperparameter configurations.
      scope: VariableScope for the created subgraph; default "dynamic_seq2class".
      allowed_models: List of names of allowed models.
        Currently: "token_rnn", "token_cnn", "char_rnn", "char_cnn". Set to None for all models.

    Returns:
      A tuple of the form (logits, loss, final_context_state),
      where:
        logits: float32 Tensor [batch_size x num_decoder_symbols].
        loss: The total loss / batch_size.
        ensemble_statistics: List of statistics for ensemble models

    Raises:
      ValueError: if encoder_type differs from mono and bi, or
        attention_option is not (luong | scaled_luong |
        bahdanau | normed_bahdanau).

    """
    utils.print_out("# creating %s graph ..." % self.mode)
    # dtype = tf.float32
    # num_layers = hparams.num_layers

    if allowed_models is None:
      allowed_models = ["token_rnn", "token_cnn", "char_rnn", "char_cnn"]

    with utils.cond_scope(scope):  # only set variable scope if scope not None
      with tf.name_scope("dynamic_seq2class"):  # scope or

        states_to_classifier = []
        outputs_to_token_rnn = []
        ensemble_softmaxes = []
        ensemble_logits = []
        ensemble_loss = 0.0
        ensemble_statistics = []

        """ RNNs """
        if hparams.use_char_rnn and "char_rnn" in allowed_models:
          char_rnn_output, char_rnn_h_state = self._build_char_rnn(hparams)

          if hparams.send_char_rnn_to_classifier:
            states_to_classifier.append(char_rnn_h_state)

          if hparams.send_char_rnn_to_token_rnn:
            outputs_to_token_rnn.append(char_rnn_output)

          if hparams.char_rnn_use_ensemble:
            char_rnn_softmax, char_rnn_logits, char_rnn_loss, char_rnn_evaluator = self._build_softmax_with_loss(
              char_rnn_h_state,
              hparams,
              scope="char_rnn")
            ensemble_loss += char_rnn_loss
            if hparams.char_rnn_send_ensemble_softmax_to_classifier:
              ensemble_softmaxes.append(char_rnn_softmax)
            if hparams.char_rnn_send_ensemble_logits_to_classifier:
              ensemble_logits.append(char_rnn_logits)

            ensemble_statistics.append(self.extract_ensemble_statistics("char_rnn", char_rnn_evaluator, char_rnn_loss))

        if hparams.use_token_rnn and "token_rnn" in allowed_models:
          # RNN
          token_rnn_output, token_rnn_state = self._build_token_rnn(hparams, outputs_to_token_rnn)

          if hparams.send_token_rnn_to_classifier:
            states_to_classifier.append(token_rnn_state)
            print("token_rnn_state: ", token_rnn_state)

          if hparams.token_rnn_use_ensemble:
            token_rnn_softmax, token_rnn_logits, token_rnn_loss, token_rnn_evaluator = self._build_softmax_with_loss(
              token_rnn_state,
              hparams,
              scope="token_rnn")
            ensemble_loss += token_rnn_loss
            if hparams.token_rnn_send_ensemble_softmax_to_classifier:
              ensemble_softmaxes.append(token_rnn_softmax)
            if hparams.token_rnn_send_ensemble_logits_to_classifier:
              ensemble_logits.append(token_rnn_logits)
            ensemble_statistics.append(
              self.extract_ensemble_statistics("token_rnn", token_rnn_evaluator, token_rnn_loss))

        """ CNNs """
        if hparams.use_char_cnn and "char_cnn" in allowed_models:
          char_cnn_state = self._build_char_cnn(hparams)

          if hparams.send_char_cnn_to_classifier:
            states_to_classifier.append(char_cnn_state)

          if hparams.char_cnn_use_ensemble:
            char_cnn_softmax, char_cnn_logits, char_cnn_loss, char_cnn_evaluator = self._build_softmax_with_loss(
              char_cnn_state,
              hparams,
              scope="char_cnn")
            ensemble_loss += char_cnn_loss
            if hparams.char_cnn_send_ensemble_softmax_to_classifier:
              ensemble_softmaxes.append(char_cnn_softmax)
            if hparams.char_cnn_send_ensemble_logits_to_classifier:
              ensemble_logits.append(char_cnn_logits)
            ensemble_statistics.append(self.extract_ensemble_statistics("char_cnn", char_cnn_evaluator, char_cnn_loss))

        if hparams.use_token_cnn and "token_cnn" in allowed_models:
          token_cnn_state = self._build_token_cnn(hparams)

          if hparams.send_token_cnn_to_classifier:
            states_to_classifier.append(token_cnn_state)

          if hparams.token_cnn_use_ensemble:
            token_cnn_softmax, token_cnn_logits, token_cnn_loss, token_cnn_evaluator = self._build_softmax_with_loss(
              token_cnn_state,
              hparams,
              scope="token_cnn")
            ensemble_loss += token_cnn_loss
            if hparams.token_cnn_send_ensemble_softmax_to_classifier:
              ensemble_softmaxes.append(token_cnn_softmax)
            if hparams.token_cnn_send_ensemble_logits_to_classifier:
              ensemble_logits.append(token_cnn_logits)
            ensemble_statistics.append(self.extract_ensemble_statistics("token_cnn", token_cnn_evaluator,
                                                                        token_cnn_loss))

        """ To dense """
        if len(states_to_classifier) == 0 and len(ensemble_softmaxes) == 0 and len(ensemble_logits) == 0:
          raise ValueError("states_to_classifier and ensemble softmaxes/logits were empty. No input to dense network.")
        if len(states_to_classifier) > 0:
          print("states_to_classifier: ", states_to_classifier)
          final_encoder_state = tf.concat(states_to_classifier, axis=-1)
          print("final_encoder_state: ", final_encoder_state)
        else:
          final_encoder_state = None
        if len(ensemble_softmaxes) > 0:
          ensemble_to_classifier = tf.concat(ensemble_softmaxes, axis=-1)
        elif len(ensemble_logits) > 0:
          ensemble_to_classifier = tf.concat(ensemble_logits, axis=-1)
        else:
          ensemble_to_classifier = None

        #final_encoder_state = tf.Print(final_encoder_state, [final_encoder_state], message="final_encoder_state: ", summarize=3000)

        # Classification
        logits = self._build_classifier(final_encoder_state, ensemble_to_classifier, hparams)

        # Only place on GPU if self.first_gpu_id is not None.
        if self.first_gpu_id:
          with tf.device(tf.DeviceSpec(device_type="GPU", device_index=self.first_gpu_id)):
            ## Loss
            if self.mode != tf.contrib.learn.ModeKeys.INFER:
              loss = self.setup_loss_computation(hparams=hparams, logits=logits, targets=self.targets,
                                                 multilabel=self.does_multilabel_classification, label_counts=self.iterator.target_sequence_length)
            else:
              loss = None
        else:
          ## Loss
          if self.mode != tf.contrib.learn.ModeKeys.INFER:
            loss = self.setup_loss_computation(hparams=hparams, logits=logits, targets=self.targets,
                                               multilabel=self.does_multilabel_classification, label_counts=self.iterator.target_sequence_length)
          else:
            loss = None
        print("build_graph done")
        return logits, loss + (ensemble_loss * hparams.ensemble_loss_weight), ensemble_statistics

  def _build_token_rnn(self, hparams, outputs_to_token_rnn):
    with tf.variable_scope("build_token_rnn"): #  initializer=tf.orthogonal_initializer()
      if outputs_to_token_rnn:
        if len(outputs_to_token_rnn) == 1:
          additional_embeddings = outputs_to_token_rnn[0]
        else:
          additional_embeddings = tf.concat(outputs_to_token_rnn, axis=1)
        encoder_outputs, encoder_state = self._build_token_encoder(hparams, additional_embeddings=additional_embeddings)
      else:
        encoder_outputs, encoder_state = self._build_token_encoder(hparams)
      encoder_h_state = unpack_encoder_state(encoder_state, cell_type=hparams.token_rnn_unit_type,
                                             encoder_type=hparams.token_rnn_encoder_type)
      print("encoder_h_state: ", encoder_h_state)

      return encoder_outputs, encoder_h_state

  def _build_char_rnn(self, hparams):
    with tf.variable_scope("build_char_rnn"):
      encoder_outputs_char, encoder_state_char = self._build_char_encoder(hparams)

      # Char h state
      h_state = unpack_encoder_state(encoder_state_char, cell_type=hparams.char_rnn_unit_type,
                                     encoder_type=hparams.char_rnn_encoder_type)
      h_state_shape = tf.shape(h_state)

      h_state = tf.reshape(h_state, shape=[h_state_shape[0],
                                           hparams.char_cnn_chars_per_token * hparams.char_embedding_size
                                           * (2 if hparams.char_rnn_encoder_type == "bi" else 1)])

      # Char output
      if self.time_major:
        encoder_outputs_char = tf.transpose(tf.squeeze(encoder_outputs_char), perm=[1, 0, 2, 3])
      h_outputs_shape = tf.shape(encoder_outputs_char)
      h_outputs = tf.reshape(encoder_outputs_char, shape=[h_outputs_shape[0], h_outputs_shape[1],
                                                          hparams.char_cnn_chars_per_token * hparams.char_embedding_size])
      return h_outputs, h_state

  def _build_token_cnn(self, hparams):
    with tf.variable_scope("build_token_cnn"):
      source_tokens_fixed = self.iterator.source_tokens_fixed
      # Inform the model of the actual shapes. Not actually reshaping the input.
      source_tokens_fixed = tf.reshape(source_tokens_fixed,
                                       [-1, hparams.token_cnn_tokens_per_sentence])
      source_tokens_fixed_emb_inp = tf.nn.embedding_lookup(
        self.token_embedding_encoder, source_tokens_fixed)

      if self.augment_embeddings:
        source_tokens_fixed_emb_inp = embedding_augmentation(
          source_tokens_fixed_emb_inp,
          noise_amount_mean=self.noise_amount_mean,
          noise_amount_stddev=self.noise_amount_stddev
        )

      source_tokens_fixed_emb_inp = tf.expand_dims(source_tokens_fixed_emb_inp, -1)
      cnn_state = self._build_token_cnn_pipeline(source_tokens_fixed_emb_inp, hparams)
      return cnn_state

  def _build_char_cnn(self, hparams):
    with tf.variable_scope("build_character_cnn"):
      source_chars_fixed = self.iterator.source_chars_fixed
      # Inform the model of the actual shapes. Not actually reshaping the input.
      source_chars_fixed = tf.reshape(source_chars_fixed,
                                      [-1, hparams.char_cnn_tokens_per_sentence, hparams.char_cnn_chars_per_token])
      source_chars_fixed_emb_inp = tf.nn.embedding_lookup(
        self.char_embedding_encoder, source_chars_fixed)

      if self.augment_embeddings:
        source_chars_fixed_emb_inp = embedding_augmentation(source_chars_fixed_emb_inp,
                                                            noise_amount_mean=self.noise_amount_mean,
                                                            noise_amount_stddev=self.noise_amount_stddev)

      source_chars_fixed_emb_inp = tf.expand_dims(source_chars_fixed_emb_inp, -1)
      cnn_state = self._build_char_cnn_pipeline(source_chars_fixed_emb_inp, hparams)
      return cnn_state

  def _build_encoder_cell_token(self, hparams, num_layers, num_residual_layers):
    """Build a multi-layer RNN cell that can be used by encoder."""

    return create_rnn_cell(
      unit_type=hparams.token_rnn_unit_type,
      num_units=hparams.token_rnn_num_units,
      num_layers=num_layers,
      num_residual_layers=num_residual_layers,
      forget_bias=hparams.token_rnn_forget_bias,
      dropout=hparams.token_rnn_dropout,
      zoneout=hparams.token_rnn_zoneout,
      norm_gain=hparams.token_rnn_norm_gain,
      mode=self.mode,
      single_cell_fn=self.single_cell_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes
    )

  def _build_encoder_cell_char(self, hparams, num_layers, num_residual_layers):
    """Build a multi-layer RNN cell that can be used by encoder."""

    return create_rnn_cell(
      unit_type=hparams.char_rnn_unit_type,
      num_units=None if hparams.char_rnn_unit_type == "conv_lstm" else hparams.char_rnn_num_units,
      num_layers=num_layers,
      num_residual_layers=num_residual_layers,
      forget_bias=hparams.char_rnn_forget_bias,
      dropout=0.0 if hparams.char_rnn_unit_type == "conv_lstm" else hparams.char_rnn_dropout,
      zoneout=0.0 if hparams.char_rnn_unit_type == "conv_lstm" else hparams.char_rnn_zoneout,
      norm_gain=hparams.token_char_norm_gain,
      mode=self.mode,
      single_cell_fn=None,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes,
      chars_per_token=hparams.char_cnn_chars_per_token,
      conv_output_channels=hparams.char_rnn_conv_output_channels,
      char_embedding_size=hparams.char_embedding_size,
      conv_kernel_shape=hparams.char_rnn_conv_kernel_shape
    )

  def _build_bidirectional_rnn(self, inputs, sequence_length,
                               dtype, hparams,
                               num_bi_layers,
                               num_bi_residual_layers):
    """Create and call bidirectional RNN cells.

    Args:
      num_residual_layers: Number of residual layers from top to bottom. For
        example, if `num_bi_layers=4` and `num_residual_layers=2`, the last 2 RNN
        layers in each RNN cell will be wrapped with `ResidualWrapper`.

    Returns:
      The concatenated bidirectional output and the bidirectional RNN cell"s
      state.
    """

    # Construct forward and backward cells
    with tf.variable_scope("fw_cell"):
      fw_cell = self._build_encoder_cell_token(hparams,
                                               num_bi_layers,
                                               num_bi_residual_layers)
    with tf.variable_scope("bw_cell"):
      bw_cell = self._build_encoder_cell_token(hparams,
                                               num_bi_layers,
                                               num_bi_residual_layers)

    bi_outputs, bi_state = tf.nn.bidirectional_dynamic_rnn(
      fw_cell,
      bw_cell,
      inputs,
      dtype=dtype,
      sequence_length=sequence_length,
      swap_memory=True,
      time_major=self.time_major)

    return tf.concat(bi_outputs, -1), bi_state

  def _build_bidirectional_rnn_char(self, inputs, sequence_length,
                                    dtype, hparams,
                                    num_bi_layers,
                                    num_bi_residual_layers):
    """Create and call bidirectional RNN cells for the character network.

    Args:
      num_residual_layers: Number of residual layers from top to bottom. For
        example, if `num_bi_layers=4` and `num_residual_layers=2`, the last 2 RNN
        layers in each RNN cell will be wrapped with `ResidualWrapper`.

    Returns:
      The concatenated bidirectional output and the bidirectional RNN cell"s
      state.
    """

    # Construct forward and backward cells
    fw_cell = self._build_encoder_cell_char(hparams,
                                            num_bi_layers,
                                            num_bi_residual_layers)
    bw_cell = self._build_encoder_cell_char(hparams,
                                            num_bi_layers,
                                            num_bi_residual_layers)

    bi_outputs, bi_state = tf.nn.bidirectional_dynamic_rnn(
      fw_cell,
      bw_cell,
      inputs,
      dtype=dtype,
      parallel_iterations=min(hparams.batch_size, 32),
      sequence_length=sequence_length,
      swap_memory=True,
      time_major=self.time_major)

    return tf.concat(bi_outputs, -1), bi_state

  def _prepare_classifier_input(self, x=None, x2=None):
    """
    Reads the inputs in _build_classifier and returns it as one tensor.
    """
    if x is not None:
      if x2 is not None:
        return tf.concat([x, x2], axis=-1)
      else:
        return x
    elif x2 is not None:
      return x2
    else:
      raise ValueError("Both x and x2 were None. Not input to classifier.")

  def get_max_time(self, tensor):
    time_axis = 0 if self.time_major else 1
    return tensor.shape[time_axis].value or tf.shape(tensor)[time_axis]

  def setup_loss_computation(self, hparams, logits, targets, multilabel=False, label_counts = None):
    """Compute optimization loss.
    Returns: Cross entropy divided by batch_size.
    """

    if hparams.loss_type == "softmax_cross_entropy":
      return compute_softmax_cross_entropy_loss(targets, logits, self.batch_size, label_smoothing=hparams.label_smoothing, multilabel=multilabel, label_counts=label_counts)
    elif hparams.loss_type == "sigmoid_cross_entropy":

      if not multilabel:
        raise ValueError("sigmoid_cross_entropy is only supported for multilabel")

      return compute_sigmoid_cross_entropy_loss(targets, logits, self.batch_size,)
    else:
      raise ValueError("Unsupported loss_type from hparams")

  def extract_ensemble_statistics(self, model_name, evaluator, loss):

    return EnsembleBinaryStatistics(model_name=model_name,
                                    loss=loss,
                                    accuracy=evaluator.accuracy,
                                    f1=evaluator.f1,
                                    AUC=evaluator.auc,
                                    precision=evaluator.precision,
                                    recall=evaluator.recall,
                                    specificity=evaluator.specificity
                                    )

  @property
  def ensemble_statistics(self):
    return self.__ensemble_statistics

  @property
  def dynamic_batch_size(self):
    """
    Gets the current batch size.
    """

    print(self.iterator)

    return tf.shape(self.iterator.source_sequence_length)[0]

  @property
  @abc.abstractmethod
  def batched_input_class(self):
    """Subclass must implement this.

    Gets BatchedInput class from respective dataset_loader.

    Returns:
      BatchedInput class.
      """
    pass

  @property
  @abc.abstractmethod
  def source_tokens(self):
    """Subclass must implement this.

      Gets source tokens from iterator.

      Returns:
        Tensor with targets.
        """
    pass

  @property
  @abc.abstractmethod
  def source_chars(self):
    """Subclass must implement this.

      Gets source chars from iterator.

      Returns:
        Tensor with targets.
        """
    pass

  @property
  @abc.abstractmethod
  def targets(self):
    """Subclass must implement this.

      Gets target from iterator.

      Returns:
        Tensor with targets.
        """
    pass

  @property
  @abc.abstractmethod
  def allowed_models(self):
    """Subclass must implement this.

    Gets list of allowed models. Or None for all models.

    Currently available: "token_rnn", "token_cnn", "char_rnn", "char_cnn"
    """
    pass

  @property
  @abc.abstractmethod
  def does_multilabel_classification(self):
    """Subclass must implement this.

    Gets whether the model is a multilabel classifier or not.

    Bool.
    """
    pass

  @abc.abstractmethod
  def add_word_features_to_embeddings(self, embeddings):
    """Subclass must implement this.

    Add word features to embeddings.
    Different models could have different sets of word features.
    """
    pass

  @abc.abstractmethod
  def evaluate(self, targets=None, predictions=None, probabilities=None, **kwargs):
    """Subclass must implement this.

    Evaluate results.

    Returns:
      Evaluator Object
    """
    pass

  @abc.abstractmethod
  def extract_preds_and_probs(self, scope=None, hparams=None, **kwargs):
    """ Submethod must implement this

    :return: predictions, probabilities
    """
    pass

  @abc.abstractmethod
  def _build_token_encoder(self, hparams, additional_embeddings=None):
    """Subclass must implement this.

    Build and run an RNN encoder.

    Args:
      hparams: Hyperparameters configurations.

    Returns:
      A tuple of encoder_outputs and encoder_state.
    """
    pass

  @abc.abstractmethod
  def _build_char_encoder(self, hparams):
    """Subclass must implement this.

    Build and run an RNN encoder.

    Args:
      hparams: Hyperparameters configurations.

    Returns:
      A tuple of encoder_outputs and encoder_state.
    """
    pass

  @abc.abstractmethod
  def _build_softmax_with_loss(self, x, hparams, scope):
    """Subclass must implement this.

        Build a softmax classifier with loss computation and evaluation metrics.

        Args:
          x: input tensor
          hparams: Hyperparameters configurations.
          scope: Name of model. Str.

        Returns:
          softmax classification with num_classes neurons, loss, evaluator
        """
    pass

  @abc.abstractmethod
  def _build_char_cnn_pipeline(self, x, hparams):
    """Subclass must implement this.

        Build convolution pipeline on fixed size chars or tokens.

        Args:
          x: Input.
          hparams: Hyperparameters configuration.

        Returns:
          Flattened to [batch_size, n_tokens] or similar.
        """

  @abc.abstractmethod
  def _build_token_cnn_pipeline(self, x, hparams):
    """Subclass must implement this.

        Build convolution pipeline on fixed size chars or tokens.

        Args:
          x: Input.
          hparams: Hyperparameters configuration.

        Returns:
          Flattened to [batch_size, n_tokens] or similar.
        """

  @abc.abstractmethod
  def _build_classifier(self, x=None, x2=None, hparams=None):
    """Subclass must implement this.

    Build dense layer from RNN output state.

    Args:
      x: Input, i.e. unpacked encoder h state.
      x2: Second input, i.e. the ensemble softmaxes
      hparams: Hyperparameters configuration.

    Returns:
      Logits
    """


class EnsembleBinaryStatistics(collections.namedtuple("EnsembleBinaryStatistics",
                                                      ("model_name",
                                                       "loss",
                                                       "accuracy",
                                                       "f1",
                                                       "AUC",
                                                       "precision",
                                                       "recall",
                                                       "specificity",
                                                       ))):
  pass
