"""S2C: Sequence-to-classification model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.python.layers import core as layers_core

from tfDeepNLP.convolution.CNN2D import CNN2D
from tfDeepNLP.convolution.CNN3D import CNN3D
from tfDeepNLP.models.classification.seq2class.s2c_base_model import S2CBaseModel
from tfDeepNLP.utils.evaluate_metrics_binary_classification import EvaluateMetricsBinaryClassification
from tfDeepNLP.utils.misc_utils import cond_scope

__all__ = ["BinaryClassificationModel"]


class BinaryClassificationModel(S2CBaseModel):
  """Sequence-to-classification dynamic model.

  This class implements a multi-layer recurrent neural network as encoder.
  This class also allows to use GRU cells in addition to LSTM cells with
  support for dropout.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_token_vocab_table,
               source_char_vocab_table,
               augment_embeddings=False,
               noise_amount_mean=None,
               noise_amount_stddev=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None
               ):
    super(BinaryClassificationModel, self).__init__(
      hparams=hparams,
      mode=mode,
      iterator=iterator,
      source_token_vocab_table=source_token_vocab_table,
      source_char_vocab_table=source_char_vocab_table,
      augment_embeddings=augment_embeddings,
      noise_amount_mean=noise_amount_mean,
      noise_amount_stddev=noise_amount_stddev,
      scope=scope,
      single_cell_fn=single_cell_fn,
      model_device_fn=model_device_fn,
      optimizer_op=optimizer_op,
      learning_rate=learning_rate,
      first_gpu_id=first_gpu_id
    )

  def _build_classifier(self, x=None, x2=None, hparams=None):
    with tf.variable_scope("build_classifier"):

      input_ = self._prepare_classifier_input(x=None, x2=None)

      print("Begin building dense layers")

      if hparams.use_ensemble:
        act_fn = tf.nn.tanh
      else:
        act_fn = tf.nn.relu

      fully_connected_layer_1 = layers_core.dense(
        inputs=input_,
        units=hparams.classifier_num_units,
        activation=act_fn,
        use_bias=True,
        name="fully_connected_layer_1",
      )

      logits = layers_core.dense(
        inputs=fully_connected_layer_1,
        units=hparams.num_classes,
        activation=None,
        use_bias=True,
        name="logits",
      )

      return logits

  def _build_softmax_with_loss(self, x, hparams, scope=None):
    with cond_scope(scope):

      logits = layers_core.dense(
        inputs=x,
        units=hparams.num_classes,
        activation=None,
        use_bias=True,
        name="logits",
      )

      predictions, probabilities = self.extract_preds_and_probs(logits)

      if hparams.num_classes == 2:

        # Get only probs for class 2
        probabilities = tf.identity(probabilities[:, 1:], name="ensemble_probability")

        if self.mode != tf.contrib.learn.ModeKeys.INFER:
          ensemble_evaluator = EvaluateMetricsBinaryClassification(self.targets, predictions,
                                                                   probabilities)
      else:
        raise ValueError("This model only works with binary classification.")

      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        loss = self.setup_loss_computation(hparams, logits, self.targets, multilabel=False)

        return probabilities, logits, loss, ensemble_evaluator
      else:
        return probabilities, logits, 0.0, None

  def _build_token_cnn_pipeline(self, x, hparams):
    """
        Convolution pipeline.

        :param x: input tensor with shape - [batch, in_height, in_width, in_channels].
          e.g. [batch_size, src_token_max_len, embedding_size_token, 1]
        :param hparams: hyperparameters
        :return: Flattened tensor [batch_size, token_size]
        """

    cnn2d = CNN2D(hparams.verbose_tensors)

    preset_fn = cnn2d.get_preset_fn(hparams.token_cnn_preset)

    output = preset_fn(x, num_token_level_layers=hparams.token_cnn_num_token_level_layers,
                       token_level_skip_connections=hparams.token_cnn_token_level_skip_connections)

    return cnn2d.flatten(output)

  def _build_char_cnn_pipeline(self, x, hparams):
    """
    Convolution pipeline.

    :param x: input tensor with shape - [batch, in_depth, in_height, in_width, in_channels].
      e.g. [batch_size, src_token_max_len, chars_per_token, chars_embedding_size, 1]
    :param hparams: hyperparameters
    :return: Flattened tensor [batch_size, token_size]
    """

    cnn3d = CNN3D(hparams.verbose_tensors)

    preset_fn = cnn3d.get_preset_fn(hparams.char_cnn_preset)

    output = preset_fn(x, num_token_level_layers=hparams.char_cnn_num_token_level_layers,
                       token_level_skip_connections=hparams.char_cnn_token_level_skip_connections)

    return cnn3d.flatten(output)

  def evaluate(self, targets=None, predictions=None, probabilites=None, **kwargs):

    return EvaluateMetricsBinaryClassification(self.targets, self.predictions,
                                               self.probabilities)
