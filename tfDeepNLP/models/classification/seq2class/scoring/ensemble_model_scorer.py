import tensorflow as tf

from tfDeepNLP.models.classification.classification_base_model import EnsembleBinaryStatistics

__all__ = ["EnsembleModelScorer", "add_up_ensemble_scores"]


class EnsembleModelScorer(object):
  def __init__(self, model_name):
    self.model_name_ = model_name
    self.losses = []
    self.accuracies = []
    self.f1_scores = []
    self.auc_scores = []
    self.precisions = []
    self.recalls = []
    self.specificities = []

    self.tf_summary_ = None
    self.avg_loss_ = None
    self.avg_accuracy_ = None
    self.avg_f1_ = None
    self.avg_auc_ = None
    self.avg_precision_ = None
    self.avg_recall_ = None
    self.avg_specificity_ = None

  def add_new_scores(self, ensemble_stats):
    if ensemble_stats.model_name != self.model_name_:
      raise ValueError("Model names are not equal.")
    self.losses.append(ensemble_stats.loss)
    self.accuracies.append(ensemble_stats.accuracy)
    self.f1_scores.append(ensemble_stats.f1)
    self.auc_scores.append(ensemble_stats.AUC)
    self.precisions.append(ensemble_stats.precision)
    self.recalls.append(ensemble_stats.recall)
    self.specificities.append(ensemble_stats.specificity)

  def average_scores(self):
    self.avg_loss_ = tf.reduce_mean(self.losses)
    self.avg_accuracy_ = tf.reduce_mean(self.accuracies)
    self.avg_f1_ = tf.reduce_mean(self.f1_scores)
    self.avg_auc_ = tf.reduce_mean(self.auc_scores)
    self.avg_precision_ = tf.reduce_mean(self.precisions)
    self.avg_recall_ = tf.reduce_mean(self.recalls)
    self.avg_specificity_ = tf.reduce_mean(self.specificities)

  def create_summary(self, summary_prefix=""):
    self.tf_summary_ = tf.summary.merge([
      tf.summary.scalar("{}_{}_loss".format(summary_prefix, self.model_name), self.avg_loss),
      tf.summary.scalar("{}_{}_accuracy".format(summary_prefix, self.model_name), self.avg_accuracy),
      tf.summary.scalar("{}_{}_f1".format(summary_prefix, self.model_name), self.avg_f1),
      tf.summary.scalar("{}_{}_auc".format(summary_prefix, self.model_name), self.avg_auc),
      tf.summary.scalar("{}_{}_precision".format(summary_prefix, self.model_name), self.avg_precision),
      tf.summary.scalar("{}_{}_recall".format(summary_prefix, self.model_name), self.avg_recall),
      tf.summary.scalar("{}_{}_specificity".format(summary_prefix, self.model_name), self.avg_specificity),
    ])

  @property
  def model_name(self):
    return self.model_name_

  @property
  def tf_summary(self):
    if self.tf_summary_ is None:
      raise ValueError("tf_summary must be created before it is available.")
    return self.tf_summary_

  @property
  def avg_loss(self):
    if self.avg_loss_ is None:
      raise ValueError("avg_loss must be computed before it is available.")
    else:
      return self.avg_loss_

  @property
  def avg_accuracy(self):
    if self.avg_accuracy_ is None:
      raise ValueError("avg_accuracy must be computed before it is available.")
    else:
      return self.avg_accuracy_

  @property
  def avg_f1(self):
    if self.avg_f1_ is None:
      raise ValueError("avg_f1 must be computed before it is available.")
    else:
      return self.avg_f1_

  @property
  def avg_auc(self):
    if self.avg_auc_ is None:
      raise ValueError("avg_auc must be computed before it is available.")
    else:
      return self.avg_auc_

  @property
  def avg_precision(self):
    if self.avg_precision_ is None:
      raise ValueError("avg_precision must be computed before it is available.")
    else:
      return self.avg_precision_

  @property
  def avg_recall(self):
    if self.avg_recall_ is None:
      raise ValueError("avg_recall must be computed before it is available.")
    else:
      return self.avg_recall_

  @property
  def avg_specificity(self):
    if self.avg_specificity_ is None:
      raise ValueError("avg_specificity must be computed before it is available.")
    else:
      return self.avg_specificity_

  @property
  def avg_EnsembleStatistics(self):
    """
    For recursive averaging.
    :return: EnsembleStatistics object with averaged values
    """
    return EnsembleBinaryStatistics(model_name=self.model_name,
                                    loss=self.avg_loss,
                                    accuracy=self.avg_accuracy,
                                    f1=self.avg_f1,
                                    AUC=self.avg_auc,
                                    precision=self.avg_precision,
                                    recall=self.avg_recall,
                                    specificity=self.avg_specificity
                                    )


def add_up_ensemble_scores(step_results, create_summary=True, summary_prefix=""):
  """

  :param step_results: list of lists of tuples
  :return:
  """

  model_object_dict = {}
  summaries = []

  # Find all the (unique) model names
  model_names = list(set([step_result.model_name for step_result in step_results]))

  # Map each model name to a EnsembleModelScorer
  for m_name in model_names:
    model_object_dict[m_name] = EnsembleModelScorer(m_name)

  # Append all scores to their respective models EnsembleModelScorer
  for step_result in step_results:
    model_object_dict[step_result.model_name].add_new_scores(step_result)

  # Average scores in all models' EnsembleModelScorers
  for m_name in model_names:
    model_object_dict[m_name].average_scores()
    if create_summary:
      model_object_dict[m_name].create_summary(summary_prefix)
      summaries.append(model_object_dict[m_name].tf_summary)

  if create_summary:
    # merge summaries to one
    summary_merged = tf.summary.merge(summaries,
                                      name="ensemble_stats_summary_merger")

  return model_names, model_object_dict, summary_merged if create_summary else None
