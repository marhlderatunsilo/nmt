import tensorflow as tf

__all__ = ["eval_running_stats"]


def eval_running_stats(stats_tuple):
  (eval_loss_,
   eval_acc_,
   eval_f1_,
   eval_auc_,
   eval_prec_,
   eval_recall_,
   eval_specificity_,
   eval_class_ratio_,
   # eval_step_predict_count_,
   eval_step_word_count_,
   eval_batch_size_,
   ) = stats_tuple

  # Eval store for running stats - must be reset after evaluation
  with tf.variable_scope("eval_running_stats"):
    eval_n_batches = tf.get_variable("eval_n_batches", initializer=0.0)
    eval_stats_step_loss = tf.get_variable("eval_stats_step_loss", initializer=0.0)
    eval_stats_accuracy = tf.get_variable("eval_stats_accuracy", initializer=0.0)
    eval_stats_f1 = tf.get_variable("eval_stats_f1", initializer=0.0)
    eval_stats_auc = tf.get_variable("eval_stats_auc", initializer=0.0)
    eval_stats_precision = tf.get_variable("eval_stats_precision", initializer=0.0)
    eval_stats_recall = tf.get_variable("eval_stats_recall", initializer=0.0)
    eval_stats_specificity = tf.get_variable("eval_stats_specificity", initializer=0.0)
    eval_stats_class_ratio = tf.get_variable("eval_stats_class_ratio", initializer=0.0)
    eval_stats_step_word_count = tf.get_variable("eval_stats_step_word_count", initializer=0.0)
    eval_stats_batch_size = tf.get_variable("eval_stats_batch_size", initializer=0)

    update_n_batches = tf.assign_add(eval_n_batches, 1.0)
    update_eval_stats_step_loss = tf.assign_add(eval_stats_step_loss, eval_loss_)
    update_eval_stats_accuracy = tf.assign_add(eval_stats_accuracy, eval_acc_)
    update_eval_stats_f1 = tf.assign_add(eval_stats_f1, eval_f1_)
    update_eval_stats_auc = tf.assign_add(eval_stats_auc, eval_auc_)
    update_eval_stats_precision = tf.assign_add(eval_stats_precision, eval_prec_)
    update_eval_stats_recall = tf.assign_add(eval_stats_recall, eval_recall_)
    update_eval_stats_specificity = tf.assign_add(eval_stats_specificity, eval_specificity_)
    update_eval_stats_class_ratio = tf.assign_add(eval_stats_class_ratio, eval_class_ratio_)
    update_eval_stats_step_word_count = tf.assign_add(eval_stats_step_word_count, eval_step_word_count_)
    update_eval_stats_batch_size = tf.assign_add(eval_stats_batch_size, eval_batch_size_)

    mean_eval_stats_step_loss = tf.div(update_eval_stats_step_loss, update_n_batches, name="mean_eval_stats_step_loss")
    mean_eval_stats_accuracy = tf.div(update_eval_stats_accuracy, update_n_batches, name="mean_eval_stats_accuracy")
    mean_eval_stats_f1 = tf.div(update_eval_stats_f1, update_n_batches, name="mean_eval_stats_f1")
    mean_eval_stats_auc = tf.div(update_eval_stats_auc, update_n_batches, name="mean_eval_stats_auc")
    mean_eval_stats_precision = tf.div(update_eval_stats_precision, update_n_batches, name="mean_eval_stats_precision")
    mean_eval_stats_recall = tf.div(update_eval_stats_recall, update_n_batches, name="mean_eval_stats_recall")
    mean_eval_stats_specificity = tf.div(update_eval_stats_specificity, update_n_batches,
                                         name="mean_eval_stats_specificity")
    mean_eval_stats_class_ratio = tf.div(update_eval_stats_class_ratio, update_n_batches,
                                         name="mean_eval_stats_class_ratio")
    # mean_eval_stats_step_word_count = tf.div(update_eval_stats_step_word_count, update_n_batches, # step_word_count is the sum, which is already what the updated version represents.
    #                                          name="mean_eval_stats_step_word_count")
    mean_eval_stats_batch_size = tf.div(update_eval_stats_batch_size, tf.cast(update_n_batches, dtype=tf.int32),
                                        name="mean_eval_stats_batch_size", )

    running_initializer = tf.variables_initializer(
      [eval_n_batches, eval_stats_step_loss, eval_stats_accuracy,
       eval_stats_f1, eval_stats_auc, eval_stats_precision, eval_stats_recall, eval_stats_specificity,
       eval_stats_class_ratio, eval_stats_step_word_count, eval_stats_batch_size,
       ],
      name='initialize_running_vars'
    )

    return tuple([
      mean_eval_stats_step_loss, mean_eval_stats_accuracy, mean_eval_stats_f1, mean_eval_stats_auc,
      mean_eval_stats_precision, mean_eval_stats_recall, mean_eval_stats_specificity,
      mean_eval_stats_class_ratio, update_eval_stats_step_word_count, mean_eval_stats_batch_size
    ]), running_initializer
