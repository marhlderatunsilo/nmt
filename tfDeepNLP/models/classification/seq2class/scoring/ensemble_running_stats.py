import tensorflow as tf

from tfDeepNLP.models.classification.classification_base_model import EnsembleBinaryStatistics

__all__ = ["EnsembleRunningScorer", "create_ensemble_running_scorers",
           "get_ensemble_running_scorers_initializers", "create_ensemble_running_scorers_summary"]


class EnsembleRunningScorer(object):
  def __init__(self, model_name, avg_ensemble_stats):
    self.__model_name = model_name
    self.__avg_ensemble_stats = avg_ensemble_stats
    assert self.__model_name == self.__avg_ensemble_stats.model_name, \
      "model name must match model name in avg_ensemble_stats"

    self.scope = "{}/eval_ensemble_running_stats".format(self.__model_name)

    with tf.variable_scope(self.scope):
      self.__n_batches = tf.get_variable("n_batches", initializer=0.0)
      self.__loss = tf.get_variable("loss", initializer=0.0)
      self.__accuracy = tf.get_variable("accuracy", initializer=0.0)
      self.__f1 = tf.get_variable("f1", initializer=0.0)
      self.__auc = tf.get_variable("auc", initializer=0.0)
      self.__precision = tf.get_variable("precision", initializer=0.0)
      self.__recall = tf.get_variable("recall", initializer=0.0)
      self.__specificity = tf.get_variable("specificity", initializer=0.0)

      self.__update_n_batches = tf.assign_add(self.__n_batches, 1.0)
      self.__update_loss = tf.assign_add(self.__loss, self.__avg_ensemble_stats.loss)
      self.__update_accuracy = tf.assign_add(self.__accuracy, self.__avg_ensemble_stats.accuracy)
      self.__update_f1 = tf.assign_add(self.__f1, self.__avg_ensemble_stats.f1)
      self.__update_auc = tf.assign_add(self.__auc, self.__avg_ensemble_stats.AUC)
      self.__update_precision = tf.assign_add(self.__precision, self.__avg_ensemble_stats.precision)
      self.__update_recall = tf.assign_add(self.__recall, self.__avg_ensemble_stats.recall)
      self.__update_specificity = tf.assign_add(self.__specificity, self.__avg_ensemble_stats.specificity)

  def update_average(self):
    with tf.variable_scope(self.scope):
      self.__avg_loss = tf.div(self.__update_loss, self.__update_n_batches, name="avg_loss")
      self.__avg_accuracy = tf.div(self.__update_accuracy, self.__update_n_batches,
                                   name="avg_accuracy")
      self.__avg_f1 = tf.div(self.__update_f1, self.__update_n_batches, name="avg_f1")
      self.__avg_auc = tf.div(self.__update_auc, self.__update_n_batches, name="avg_auc")
      self.__avg_precision = tf.div(self.__update_precision, self.__update_n_batches,
                                    name="avg_precision")
      self.__avg_recall = tf.div(self.__update_recall, self.__update_n_batches, name="avg_recall")
      self.__avg_specificity = tf.div(self.__update_specificity, self.__update_n_batches,
                                      name="avg_specificity")

  def create_summary(self, summary_prefix=""):
    self.update_average()
    self.tf_summary_ = tf.summary.merge([
      tf.summary.scalar("{}_{}_loss".format(summary_prefix, self.model_name), self.avg_loss),
      tf.summary.scalar("{}_{}_accuracy".format(summary_prefix, self.model_name), self.avg_accuracy),
      tf.summary.scalar("{}_{}_f1".format(summary_prefix, self.model_name), self.avg_f1),
      tf.summary.scalar("{}_{}_auc".format(summary_prefix, self.model_name), self.avg_auc),
      tf.summary.scalar("{}_{}_precision".format(summary_prefix, self.model_name), self.avg_precision),
      tf.summary.scalar("{}_{}_recall".format(summary_prefix, self.model_name), self.avg_recall),
      tf.summary.scalar("{}_{}_specificity".format(summary_prefix, self.model_name), self.avg_specificity),
    ])

  @property
  def model_name(self):
    return self.__model_name

  @property
  def initializer(self):
    return tf.variables_initializer(
      [self.__n_batches, self.__loss, self.__accuracy,
       self.__f1, self.__auc, self.__precision, self.__recall,
       self.__specificity,
       ],
      name='initialize_running_vars'
    )

  @property
  def tf_summary(self):
    if self.tf_summary_ is None:
      raise ValueError("tf_summary must be created before it is available.")
    return self.tf_summary_

  @property
  def avg_scores(self):
    return tuple([
      self.avg_loss, self.avg_accuracy, self.avg_f1, self.avg_auc,
      self.avg_precision, self.avg_recall, self.avg_specificity
    ])

  @property
  def avg_loss(self):
    if self.__avg_loss is None:
      raise ValueError("avg_loss must be computed before it is available.")
    else:
      return self.__avg_loss

  @property
  def avg_accuracy(self):
    if self.__avg_accuracy is None:
      raise ValueError("avg_accuracy must be computed before it is available.")
    else:
      return self.__avg_accuracy

  @property
  def avg_f1(self):
    if self.__avg_f1 is None:
      raise ValueError("avg_f1 must be computed before it is available.")
    else:
      return self.__avg_f1

  @property
  def avg_auc(self):
    if self.__avg_auc is None:
      raise ValueError("avg_auc must be computed before it is available.")
    else:
      return self.__avg_auc

  @property
  def avg_precision(self):
    if self.__avg_precision is None:
      raise ValueError("avg_precision must be computed before it is available.")
    else:
      return self.__avg_precision

  @property
  def avg_recall(self):
    if self.__avg_recall is None:
      raise ValueError("avg_recall must be computed before it is available.")
    else:
      return self.__avg_recall

  @property
  def avg_specificity(self):
    if self.__avg_specificity is None:
      raise ValueError("avg_specificity must be computed before it is available.")
    else:
      return self.__avg_specificity

  @property
  def avg_EnsembleStatistics(self):
    """
        For recursive averaging.
        :return: EnsembleStatistics object with averaged values
        """
    return EnsembleBinaryStatistics(model_name=self.model_name,
                                    loss=self.avg_loss,
                                    accuracy=self.avg_accuracy,
                                    f1=self.avg_f1,
                                    AUC=self.avg_auc,
                                    precision=self.avg_precision,
                                    recall=self.avg_recall,
                                    specificity=self.avg_specificity)


def create_ensemble_running_scorers(model_names, model_object_dict):
  """

  :param models: list of at-step-averaged models
  :return: model_object_dict for running scorers
  """

  model_object_runner_dict = {}

  # Map each model name to a EnsembleRunningScorer
  for m_name in model_names:
    model_object_runner_dict[m_name] = EnsembleRunningScorer(m_name, model_object_dict[m_name].avg_EnsembleStatistics)

  return model_object_runner_dict


def get_ensemble_running_scorers_initializers(model_names, model_object_runner_dict):
  initializers = []
  for m_name in model_names:
    initializers.append(model_object_runner_dict[m_name].initializer)
  return initializers


def create_ensemble_running_scorers_summary(model_names, model_object_runner_dict, summary_prefix=""):
  summaries = []
  # Average scores in all models' EnsembleModelScorers
  for m_name in model_names:
    model_object_runner_dict[m_name].create_summary(summary_prefix)
    summaries.append(model_object_runner_dict[m_name].tf_summary)
  return tf.summary.merge(
    summaries,
    name="ensemble_stats_runner_summary_merger"
  )
