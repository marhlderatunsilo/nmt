import numpy as np
import tensorflow as tf

__all__ = ["add_up_scores", "add_summary_from_stats"]


def add_up_scores(step_results, py_or_tf="py"):
  """

  Note: Output order must be the same as input order, so
  we can run it repeatedly.

  :param step_results: list of tuples
  :param py_or_tf: whether to use numpy or tensorflow for finding averages, etc.
  :return: Tuple similar to input tuples, but averaged across tuples.
  """
  # Init arrays for model stats for this step
  step_losses = []
  accuracies = []
  f1_scores = []
  auc_scores = []
  precisions = []
  recalls = []
  specificities = []
  class_ratios = []
  # step_predict_counts = []
  step_word_counts = []
  batch_sizes = []

  # Get stats from each train model
  for step_result in step_results:
    (step_loss_,
     accuracy_,
     f1_,
     auc_,
     prec_,
     recall_,
     specificity_,
     class_ratio_,
     step_word_count_,
     batch_size_,
     ) = step_result

    step_losses.append(step_loss_)
    accuracies.append(accuracy_)
    f1_scores.append(f1_)
    auc_scores.append(auc_)
    precisions.append(prec_)
    recalls.append(recall_)
    specificities.append(specificity_)
    class_ratios.append(class_ratio_)
    step_word_counts.append(step_word_count_)
    batch_sizes.append(batch_size_)

  # Reduce stats arrays to scalars

  if py_or_tf == "py":
    avg_step_loss_py = np.mean(step_losses)
    avg_accuracy_py = np.mean(accuracies)
    avg_f1_py = np.mean(f1_scores)
    avg_auc_py = np.mean(auc_scores)
    avg_precision_py = np.mean(precisions)
    avg_recall_py = np.mean(recalls)
    avg_specificity_py = np.mean(specificities)
    avg_class_ratio_py = np.mean(class_ratios)
    # avg_step_predict_count_py = np.mean(step_predict_counts)
    avg_batch_size_py = np.mean(batch_sizes)
    # avg_step_word_count_py = np.mean(step_word_counts) * 1.0
    sum_step_word_count_py = sum(step_word_counts) * 1.0
    # min_step_word_count_py = min(step_word_counts)
    # max_step_word_count_py = max(step_word_counts)

    return (
      avg_step_loss_py, avg_accuracy_py, avg_f1_py, avg_auc_py, avg_precision_py, avg_recall_py, avg_specificity_py,
      avg_class_ratio_py, sum_step_word_count_py, avg_batch_size_py
    )

  else:
    avg_step_loss_tf = tf.reduce_mean(step_losses)
    avg_accuracy_tf = tf.reduce_mean(accuracies)
    avg_f1_tf = tf.reduce_mean(f1_scores)
    avg_auc_tf = tf.reduce_mean(auc_scores)
    avg_precision_tf = tf.reduce_mean(precisions)
    avg_recall_tf = tf.reduce_mean(recalls)
    avg_specificity_tf = tf.reduce_mean(specificities)
    avg_class_ratio_tf = tf.reduce_mean(class_ratios)
    # avg_step_predict_count_tf = tf.reduce_mean(step_predict_counts)
    avg_batch_size_tf = tf.reduce_mean(batch_sizes)
    # avg_step_word_count_tf = tf.reduce_mean(step_word_counts) * 1.0
    sum_step_word_count_tf = tf.cast(tf.reduce_sum(step_word_counts), dtype=tf.float32)
    # min_step_word_count_tf = tf.reduce_min(step_word_counts)
    # max_step_word_count_tf = tf.reduce_max(step_word_counts)

    return (
      avg_step_loss_tf, avg_accuracy_tf, avg_f1_tf, avg_auc_tf, avg_precision_tf, avg_recall_tf, avg_specificity_tf,
      avg_class_ratio_tf, sum_step_word_count_tf, avg_batch_size_tf,
    )


def add_summary_from_stats(stats_tuple, summary_prefix=""):
  (step_loss_,
   accuracy_,
   f1_,
   auc_,
   prec_,
   recall_,
   specificity_,
   class_ratio_,
   step_word_count_,
   batch_size_,
   ) = stats_tuple

  tf_summary = tf.summary.merge([
    tf.summary.scalar("{}_loss".format(summary_prefix), step_loss_),
    tf.summary.scalar("{}_accuracy".format(summary_prefix), accuracy_),
    tf.summary.scalar("{}_f1".format(summary_prefix), f1_),
    tf.summary.scalar("{}_auc".format(summary_prefix), auc_),
    tf.summary.scalar("{}_precision".format(summary_prefix), prec_),
    tf.summary.scalar("{}_recall".format(summary_prefix), recall_),
    tf.summary.scalar("{}_specificity".format(summary_prefix), specificity_),
    tf.summary.scalar("{}_class_ratio".format(summary_prefix), class_ratio_),
    tf.summary.scalar("{}_word_count_sum".format(summary_prefix), step_word_count_),
    tf.summary.scalar("{}_batch_size".format(summary_prefix), batch_size_),
  ])

  return tf_summary
