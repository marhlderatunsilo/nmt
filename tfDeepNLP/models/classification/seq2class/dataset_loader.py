"""
@author Ludvig Olsen
@date Dec. 12th 2017
Parts were copied from a personal project.
Parts were from the original iterator utils from the Google NMT code.

"""

import collections

import tensorflow as tf

from tfDeepNLP.input_pipes.dataset_loader import DatasetLoader

__all__ = ["BatchedInput", "S2CDatasetLoader"]


class S2CDatasetLoader(DatasetLoader):
  """Wrapper class around tf.data.Dataset pipeline.
  """

  def __init__(self, file_path, mode, batch_size, token_vocab_table=None, char_vocab_table=None, sos=None, eos=None,
               source_reverse=False, random_seed=None, num_buckets=None, fallback_bucket_width=None,
               num_splits=None, src_token_max_len=None, token_cnn_tokens_per_sentence=None,
               char_cnn_tokens_per_sentence=None, char_cnn_chars_per_token=None, shuffle=True,
               num_parallel_calls=32, buffer_size=10000):
    """
    :param file_path: Path to TFRecord file.
    :param mode: tf.contrib.learn.ModeKeys (TRAIN,EVAL,INFER) where inference is with no known labels (INFER not yet supported).
    :param batch_size: int.
    :param token_vocab_table: Vocab table for src tokens.
    :param char_vocab_table: Vocab table for src chars.
    :param sos: Start of Sentence symbol.
    :param eos: End of Sentence symbol.
    :param source_reverse: Reverse text.
    :param random_seed: Random seed to ensure reproducibility.
    :param num_buckets: Number of buckets.
    :param fallback_bucket_width: The bucket width used when src_token_max_len is None.
    :param num_splits: Number of splits, e.g. 1 split per GPU.
    :param src_token_max_len: Max sequence length of text.
    :param char_cnn_chars_per_token: Fixed number of chars per token. (Padded or reduced to fit).
    :param shuffle: bool.
    :param num_parallel_calls: Number of parallel processes used in input parsing.
    :param buffer_size: int.
    """

    super().__init__(file_path=file_path, mode=mode, batch_size=batch_size, src_token_vocab_table=token_vocab_table,
                     src_char_vocab_table=char_vocab_table, sos=sos, eos=eos, source_reverse=source_reverse,
                     random_seed=random_seed, num_buckets=num_buckets,
                     fallback_bucket_width=fallback_bucket_width, num_splits=num_splits,
                     src_token_max_len=src_token_max_len, token_cnn_tokens_per_sentence=token_cnn_tokens_per_sentence,
                     char_cnn_tokens_per_sentence=char_cnn_tokens_per_sentence,
                     char_cnn_chars_per_token=char_cnn_chars_per_token,
                     shuffle=shuffle, num_parallel_calls=num_parallel_calls, buffer_size=buffer_size)

    print("token_cnn_tokens_per_sentence: ", self.token_cnn_tokens_per_sentence)

    if self.mode == tf.contrib.learn.ModeKeys.INFER:
      """
      For inference without true labels.
      We probably want to feed the data differently,
      and so, this will work totally different than the train and eval modes.
      """
      pass
    else:

      dataset = tf.data.TFRecordDataset(self.file_path)

      # Currently, the training and eval parsers do the same, i.e. simply load the data from TFRecords.
      # But we might want to have some processing in them separately at some point, so we keep this structure.
      if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
        dataset = dataset.map(self._parse_function_train,
                              num_parallel_calls=self.num_parallel_calls)

      elif self.mode == tf.contrib.learn.ModeKeys.EVAL:
        dataset = dataset.map(self._parse_function_eval,
                              num_parallel_calls=self.num_parallel_calls)

      else:
        raise ValueError("Invalid mode '%s'." % mode)

      # shuffle the first `buffer_size` elements of the dataset
      if shuffle:
        dataset = dataset.shuffle(buffer_size=self.buffer_size, seed=self.random_seed)

      dataset = self.preprocess_dataset(dataset)

      if self.num_buckets > 1:
        batched_dataset = self.bucket_batching(dataset)
      else:
        print("NOT USING BUCKETING!!! ..... --- !!!")
        batched_dataset = self.batching_func(dataset)
        batched_dataset = batched_dataset.prefetch(self.buffer_size)

      self.__iterator = batched_dataset.make_initializable_iterator(shared_name="training_iterator")
      (token_ids, token_fixed_ids, char_ids, char_fixed_ids, target, seq_len, word_length, title_cased, all_upper_case,
       all_lower_case, mixed_case, punctuation, numeric) = self.__iterator.get_next()

      print("\nShapes of parsed inputs")
      print(tf.shape(token_ids),
            tf.shape(token_fixed_ids),
            tf.shape(char_ids),
            tf.shape(char_fixed_ids),
            tf.shape(target),
            tf.shape(seq_len),
            tf.shape(word_length),
            tf.shape(title_cased),
            tf.shape(all_upper_case),
            tf.shape(all_lower_case),
            tf.shape(mixed_case),
            tf.shape(punctuation),
            tf.shape(numeric)
            )

      print("Iterator: Got next")
      print(token_ids, token_fixed_ids, char_ids, char_fixed_ids, target, seq_len, word_length, title_cased,
            all_upper_case, all_lower_case, mixed_case, punctuation, numeric)
      print()
      self.__batched_data = BatchedInput(
        source_tokens=token_ids,
        source_tokens_fixed=token_fixed_ids,
        source_chars=char_ids,
        source_chars_fixed=char_fixed_ids,
        target=target,  # TODO lacks one-hot encoding of targets
        source_sequence_length=seq_len,
        word_length=word_length,
        title_cased=title_cased,
        all_upper_case=all_upper_case,
        all_lower_case=all_lower_case,
        mixed_case=mixed_case,
        punctuation=punctuation,
        numeric=numeric
      )

    if self.mode != tf.contrib.learn.ModeKeys.INFER:
      # This is the one to use in training
      # It splits each batch to the GPU's, so
      # all batches running at the same time is from the same bucket
      print("About to split batches from: ", self.__batched_data)
      self.__split_batches = self.split_batched_input()

  @property
  def split_batches(self):
    print('Getting subclass split_batches')
    # Update super's __split_batches before returning
    super(S2CDatasetLoader, S2CDatasetLoader).split_batches.__set__(self, self.__split_batches)
    return super().split_batches

  @property
  def batched_data(self):
    print('Getting subclass batched_data')
    # Update super's __batched_data before returning
    super(S2CDatasetLoader, S2CDatasetLoader).batched_data.__set__(self, self.__batched_data)
    return super().batched_data

  @property
  def iterator(self):
    print('Getting subclass iterator')
    # Update super's __iterator before returning
    super(S2CDatasetLoader, S2CDatasetLoader).iterator.__set__(self, self.__iterator)
    return super().iterator

  def _parse_function_train(self, content):
    """Input parser for samples of the training set."""

    print("Parsing")
    (text_lower, text_upper, target, seq_len, title_cased, all_upper_case, all_lower_case, mixed_case, punctuation,
     numeric) = self._parse_function(content)

    return text_lower, text_upper, target, seq_len, title_cased, all_upper_case, \
           all_lower_case, mixed_case, punctuation, numeric

  def _parse_function_eval(self, content):
    """Input parser for samples of the dev/test set."""
    return self._parse_function_train(content)

  def _parse_function_inference(self, content):
    """Input parser for inference without true labels, e.g. in Java."""
    pass

  def _parse_function(self, content):
    keys_to_features = {'text_lower': tf.FixedLenFeature((), tf.string),
                        'text_upper': tf.FixedLenFeature((), tf.string),
                        'target': tf.FixedLenFeature((), tf.int64),
                        'sequence_length': tf.FixedLenFeature((), tf.int64),
                        'title_cased': tf.VarLenFeature(tf.int64),
                        'all_upper_case': tf.VarLenFeature(tf.int64),
                        'all_lower_case': tf.VarLenFeature(tf.int64),
                        'mixed_case': tf.VarLenFeature(tf.int64),
                        'punctuation': tf.VarLenFeature(tf.int64),
                        'numeric': tf.VarLenFeature(tf.int64)}
    parsed_features = tf.parse_single_example(content, keys_to_features)

    target = tf.cast(parsed_features['target'], tf.int32)
    sequence_length = tf.cast(parsed_features['sequence_length'], tf.int32)
    title_cased = tf.cast(tf.sparse_tensor_to_dense(parsed_features['title_cased']), tf.float32)
    all_upper_case = tf.cast(tf.sparse_tensor_to_dense(parsed_features['all_upper_case']), tf.float32)
    all_lower_case = tf.cast(tf.sparse_tensor_to_dense(parsed_features['all_lower_case']), tf.float32)
    mixed_case = tf.cast(tf.sparse_tensor_to_dense(parsed_features['mixed_case']), tf.float32)
    punctuation = tf.cast(tf.sparse_tensor_to_dense(parsed_features['punctuation']), tf.float32)
    numeric = tf.cast(tf.sparse_tensor_to_dense(parsed_features['numeric']), tf.float32)

    return parsed_features['text_lower'], parsed_features['text_upper'], target, sequence_length, title_cased, \
           all_upper_case, all_lower_case, mixed_case, punctuation, numeric

  def preprocess_dataset(self, dataset):

    # Split text to tokens
    dataset = dataset.map(
      lambda text_lower, text_upper, *args: (
        tf.string_split([text_lower], skip_empty=True).values,
        tf.string_split([text_upper], skip_empty=True).values,
        *args),
      num_parallel_calls=self.num_parallel_calls)

    # Filter zero length input sequences.
    dataset = dataset.filter(
      lambda tokens_lower, tokens_upper, target, *args: tf.logical_and(tf.size(tokens_lower) > 0, tf.size(target) > 0))

    # Remove input paddings
    dataset = dataset.map(
      lambda tokens_lower, tokens_upper, target, seq_len, title_cased, all_upper_case, all_lower_case, mixed_case,
             punctuation, numeric: (
        tf.slice(tokens_lower, [0], [seq_len], name='slice_text_lower_padding'),
        tf.slice(tokens_upper, [0], [seq_len], name='slice_text_upper_padding'),
        target,
        seq_len,
        tf.slice(title_cased, [0], [seq_len], name='slice_title_cased_padding'),
        tf.slice(all_upper_case, [0], [seq_len], name='slice_all_upper_case_padding'),
        tf.slice(all_lower_case, [0], [seq_len], name='slice_all_lower_case_padding'),
        tf.slice(mixed_case, [0], [seq_len], name='slice_mixed_case_padding'),
        tf.slice(punctuation, [0], [seq_len], name='slice_punctuation_case_padding'),
        tf.slice(numeric, [0], [seq_len], name='slice_numeric_case_padding')),
      num_parallel_calls=self.num_parallel_calls)

    # Split text_upper to character tensors
    # We do it here, so padding (token level) is already cut away
    # We also cut and pad to ensure fixed number of chars per token
    dataset = dataset.map(
      lambda tokens_lower, tokens_upper, target, seq_len, *args:
      (tokens_lower,
       tf.map_fn(lambda txt: self.cut_to_character(txt, self.char_cnn_chars_per_token, self.eos),
                 tokens_upper,
                 name='chars_extraction'
                 ),
       target, seq_len, *args), num_parallel_calls=self.num_parallel_calls)

    # Find word lengths
    dataset = dataset.map(
      lambda tokens, chars, target, seq_len, *args:
      (tokens, chars, target, seq_len,

       tf.map_fn(lambda txt:  # word_length
                 tf.cast(
                   tf.size(
                     tf.string_split([txt], delimiter='', skip_empty=True).values
                   ),
                   tf.float32),
                 tokens, dtype=tf.float32, back_prop=False, name='calculate_word_length'),

       *args), num_parallel_calls=self.num_parallel_calls)

    # Cut to src_token_max_len
    if self.src_token_max_len:
      dataset = dataset.map(
        lambda tokens, chars, target, seq_len, word_length, title_cased, all_upper_case, all_lower_case, mixed_case,
               punctuation, numeric: (
          tokens[:self.src_token_max_len],
          chars[:self.src_token_max_len, :],
          target,
          tf.reduce_min([seq_len, self.src_token_max_len]),
          word_length[:self.src_token_max_len],
          title_cased[:self.src_token_max_len],
          all_upper_case[:self.src_token_max_len],
          all_lower_case[:self.src_token_max_len],
          mixed_case[:self.src_token_max_len],
          punctuation[:self.src_token_max_len],
          numeric[:self.src_token_max_len]
        ),
        num_parallel_calls=self.num_parallel_calls)

    if self.source_reverse:
      dataset = dataset.map(
        lambda tokens, chars, target, seq_len, word_length, title_cased, all_upper_case, all_lower_case, mixed_case,
               punctuation, numeric: (
          tf.reverse(tokens, axis=[0]),
          tf.reverse(tf.reverse(chars, axis=[1]), axis=[0]),  # first reverse chars per token, then the order of tokens
          target,
          seq_len,
          tf.reverse(word_length, axis=[0]),
          tf.reverse(title_cased, axis=[0]),
          tf.reverse(all_upper_case, axis=[0]),
          tf.reverse(all_lower_case, axis=[0]),
          tf.reverse(mixed_case, axis=[0]),
          tf.reverse(punctuation, axis=[0]),
          tf.reverse(numeric, axis=[0])),
        num_parallel_calls=self.num_parallel_calls
      )

    # Create fixed length tokens for 2d convolution and fixed length chars for 3d convolution
    dataset = dataset.map(
      lambda tokens, chars, *args: (tokens,
                                    self.make_fixed_length_1d(tokens, self.token_cnn_tokens_per_sentence,
                                                              self.eos),
                                    chars,
                                    self.make_fixed_length_2d(chars, [self.char_cnn_tokens_per_sentence,
                                                                      self.char_cnn_chars_per_token],
                                                              self.eos, fix_inner=False),
                                    *args),
      num_parallel_calls=self.num_parallel_calls
    )

    # Convert the word strings to ids.  Word strings that are not in the
    # vocab get the lookup table's default_value integer.
    if self.src_token_vocab_table and self.src_char_vocab_table:
      dataset = dataset.map(
        lambda tokens, tokens_fixed, chars, chars_fixed, *args: (
          tf.cast(self.src_token_vocab_table.lookup(tokens), tf.int32),
          tf.cast(self.src_token_vocab_table.lookup(tokens_fixed), tf.int32),
          tf.cast(self.src_char_vocab_table.lookup(chars), tf.int32),
          tf.cast(self.src_char_vocab_table.lookup(chars_fixed), tf.int32),
          *args),
        num_parallel_calls=self.num_parallel_calls)
    elif self.src_token_vocab_table:
      dataset = dataset.map(
        lambda tokens, tokens_fixed, chars, chars_fixed, *args: (
          tf.cast(self.src_token_vocab_table.lookup(tokens), tf.int32),
          tf.cast(self.src_token_vocab_table.lookup(tokens_fixed), tf.int32),
          chars, chars_fixed,
          *args),
        num_parallel_calls=self.num_parallel_calls)
    elif self.src_char_vocab_table:
      dataset = dataset.map(
        lambda tokens, tokens_fixed, chars, chars_fixed, *args: (
          tokens, tokens_fixed,
          tf.cast(self.src_char_vocab_table.lookup(chars), tf.int32),
          tf.cast(self.src_char_vocab_table.lookup(chars_fixed), tf.int32),
          *args),
        num_parallel_calls=self.num_parallel_calls)
    else:
      raise ValueError("At least one vocab table must be provided.")

    return dataset

  # Bucket by source sequence length (buckets for lengths 0-9, 10-19, ...)
  def batching_func(self, dataset):
    return dataset.padded_batch(
      self.batch_size,
      # Entries that are 1d vectors have shape [None]
      # Entries that are scalars have shape []
      padded_shapes=(
        tf.TensorShape([None]),  # tokens
        tf.TensorShape([None]),  # tokens_fixed
        tf.TensorShape([None, None]),  # chars # TODO Does it pad this? It shouldn't.
        tf.TensorShape([None, None]),  # chars_fixed # TODO Does it pad this? It shouldn't.
        tf.TensorShape([]),  # target
        tf.TensorShape([]),  # seq_len
        tf.TensorShape([None]),  # word length
        tf.TensorShape([None]),  # title cased
        tf.TensorShape([None]),  # all_upper_case
        tf.TensorShape([None]),  # all_lower_case
        tf.TensorShape([None]),  # mixed_case
        tf.TensorShape([None]),  # punctuation
        tf.TensorShape([None])  # numeric
      ),
      # Pad the text sequence with eos tokens.
      # Pad ints with -1
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        self.src_token_eos_id if self.src_token_vocab_table else self.eos,  # tokens
        self.src_token_eos_id if self.src_token_vocab_table else self.eos,  # tokens_fixed
        self.src_char_eos_id if self.src_char_vocab_table else self.eos,  # chars
        self.src_char_eos_id if self.src_char_vocab_table else self.eos,  # chars
        0,  # target
        0,  # seq_len
        1.0,  # word length
        1.0,  # title cased
        1.0,  # all_upper_case
        1.0,  # all_lower_case
        1.0,  # mixed_case
        1.0,  # punctuation
        1.0  # numeric
      ))

  def split_batching_func(self, dataset):
    return dataset.padded_batch(
      self.num_splits,  # TODO batch size // num splits ??
      # Entries with unkown-length vectors have shape [None, None]
      # Entries that are scalars have shape [None]
      padded_shapes=(
        tf.TensorShape([None, None]),  # tokens
        tf.TensorShape([None, None]),  # tokens_fixed
        tf.TensorShape([None, None, None]),  # chars # TODO Does it pad this? It shouldn't.
        tf.TensorShape([None, None, None]),  # chars_fixed # This shouldn't do anything, as it is fixed length
        tf.TensorShape([None]),  # target
        tf.TensorShape([None]),  # seq_len
        tf.TensorShape([None, None]),  # word length
        tf.TensorShape([None, None]),  # title cased
        tf.TensorShape([None, None]),  # all_upper_case
        tf.TensorShape([None, None]),  # all_lower_case
        tf.TensorShape([None, None]),  # mixed_case
        tf.TensorShape([None, None]),  # punctuation
        tf.TensorShape([None, None])  # numeric
      ),
      # Pad the text sequence with eos tokens.
      # Pad ints with -1
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        self.src_token_eos_id if self.src_token_vocab_table else self.eos,  # tokens
        self.src_token_eos_id if self.src_token_vocab_table else self.eos,  # tokens_fixed
        self.src_char_eos_id if self.src_char_vocab_table else self.eos,  # chars
        self.src_char_eos_id if self.src_char_vocab_table else self.eos,  # chars
        0,  # target
        0,  # seq_len
        1.0,  # word length
        1.0,  # title cased
        1.0,  # all_upper_case
        1.0,  # all_lower_case
        1.0,  # mixed_case
        1.0,  # punctuation
        1.0  # numeric
      ))

  def bucket_batching(self, dataset, *args):
    def key_func(unused_1, unused_2, unused_3, unused_4, unused_5, seq_len, unused_6, unused_7, unused_8, unused_9,
                 unused_10, unused_11, unused_12):
      """
      Calculate bucket_width by maximum source sequence length.
      Sentences with length [0, bucket_width) go to bucket 0
                     length [bucket_width, 2 * bucket_width) go to bucket 1
                     ...
                     length over ((num_bucket-1) * bucket_width) words all go into the last bucket.

      """
      if self.src_token_max_len:
        bucket_width = (self.src_token_max_len + self.num_buckets - 1) // self.num_buckets
      else:
        bucket_width = self.fallback_bucket_width

      # Bucket sentence pairs by the length of their source sentence.
      bucket_id = seq_len // bucket_width
      return tf.to_int64(tf.minimum(self.num_buckets, bucket_id))

    def reduce_func(unused_key, windowed_data):
      return self.batching_func(windowed_data)

    def key_func_split(unused_1, unused_2, unused_3, unused_4, unused_5, seq_len, unused_6, unused_7, unused_8,
                       unused_9, unused_10, unused_11, unused_12):

      if self.src_token_max_len:
        bucket_width = (self.src_token_max_len * self.batch_size + self.num_buckets - 1) // self.num_buckets
      else:
        bucket_width = self.fallback_bucket_width * self.batch_size

      # Bucket sentence pairs by the length of their source sentence and target
      # sentence.
      bucket_id = tf.reduce_sum(seq_len) // bucket_width
      return tf.to_int64(tf.minimum(self.num_buckets, bucket_id))

    def reduce_func_split(unused_key, windowed_data):
      return self.split_batching_func(windowed_data)

    batched_dataset = dataset.apply(
      tf.contrib.data.group_by_window(
        key_func=key_func, reduce_func=reduce_func, window_size=self.batch_size))

    if self.num_splits:
      batched_dataset = batched_dataset.apply(
        tf.contrib.data.group_by_window(
          key_func=key_func_split, reduce_func=reduce_func_split, window_size=self.num_splits))

    return batched_dataset

  def split_batched_input(self):
    (source_tokens, source_tokens_fixed, source_characters, source_characters_fixed, targets, source_sequence_lengths,
     word_lengths, title_cases, all_upper_cases, all_lower_cases, mixed_cases, punctuations, numerics) = (
      self.__batched_data.source_tokens,
      self.__batched_data.source_tokens_fixed,
      self.__batched_data.source_chars,
      self.__batched_data.source_chars_fixed,
      self.__batched_data.target,
      self.__batched_data.source_sequence_length,
      self.__batched_data.word_length,
      self.__batched_data.title_cased,
      self.__batched_data.all_upper_case,
      self.__batched_data.all_lower_case,
      self.__batched_data.mixed_case,
      self.__batched_data.punctuation,
      self.__batched_data.numeric)

    # Remove rank from lists of scalar
    # ...

    split_batches = []

    # TODO: NOTICE!!
    # Some of the training ops expect the dimensionality of the time dimension of target_output, i.e. the labels, and
    # tf.reduce_max(target_sequence_length) to match.
    # This is not the case because of the padding created by the global tf.reduce_max(target_sequence_length)
    # (before the split)
    # We therefore clip/slice away the excess padding

    with tf.name_scope("split_batched_input"):
      def create_slicer(i):  # TODO why the nested functions?
        def create_slice():
          # Cut away excess padding
          source_sequence_length = source_sequence_lengths[i]
          max_source_length = tf.reduce_max(source_sequence_length)
          source_toks = source_tokens[i]
          source_toks = source_toks[:, :max_source_length]
          source_chars = source_characters[i]
          source_chars = source_chars[:, :max_source_length, :]
          word_length = word_lengths[i]
          word_length = word_length[:, :max_source_length]
          title_cased = title_cases[i]
          title_cased = title_cased[:, :max_source_length]
          all_upper_case = all_upper_cases[i]
          all_upper_case = all_upper_case[:, :max_source_length]
          all_lower_case = all_lower_cases[i]
          all_lower_case = all_lower_case[:, :max_source_length]
          mixed_case = mixed_cases[i]
          mixed_case = mixed_case[:, :max_source_length]
          punctuation = punctuations[i]
          punctuation = punctuation[:, :max_source_length]
          numeric = numerics[i]
          numeric = numeric[:, :max_source_length]

          split_batch = BatchedInput(
            source_tokens=source_toks,
            source_tokens_fixed=source_tokens_fixed[i],
            source_chars=source_chars,
            source_chars_fixed=source_characters_fixed[i],
            target=targets[i],
            source_sequence_length=source_sequence_length,
            word_length=word_length,
            title_cased=title_cased,
            all_upper_case=all_upper_case,
            all_lower_case=all_lower_case,
            mixed_case=mixed_case,
            punctuation=punctuation,
            numeric=numeric
          )

          return split_batch

        return create_slice

      first_slice = create_slicer(0)()
      print(first_slice)
      with tf.device(tf.DeviceSpec(device_type="GPU", device_index=0)):
        split_batch = BatchedInput(
          source_tokens=tf.identity(first_slice.source_tokens),
          source_tokens_fixed=tf.identity(first_slice.source_tokens_fixed),
          source_chars=tf.identity(first_slice.source_chars),
          source_chars_fixed=tf.identity(first_slice.source_chars_fixed),
          target=tf.identity(first_slice.target),
          source_sequence_length=tf.identity(first_slice.source_sequence_length),
          word_length=tf.identity(first_slice.word_length),
          title_cased=tf.identity(first_slice.title_cased),
          all_upper_case=tf.identity(first_slice.all_upper_case),
          all_lower_case=tf.identity(first_slice.all_lower_case),
          mixed_case=tf.identity(first_slice.mixed_case),
          punctuation=tf.identity(first_slice.punctuation),
          numeric=tf.identity(first_slice.numeric)
        )

        split_batches.append(split_batch)

      def zeros_slice():
        return BatchedInput(
          source_tokens=first_slice.source_tokens,
          source_tokens_fixed=first_slice.source_tokens_fixed,
          source_chars=first_slice.source_chars,
          source_chars_fixed=first_slice.source_chars_fixed,
          target=first_slice.target,
          source_sequence_length=first_slice.source_sequence_length,
          word_length=first_slice.word_length,
          title_cased=first_slice.title_cased,
          all_upper_case=first_slice.all_upper_case,
          all_lower_case=first_slice.all_lower_case,
          mixed_case=first_slice.mixed_case,
          punctuation=first_slice.punctuation,
          numeric=first_slice.numeric
        )

      available_batches_count = tf.shape(source_tokens)[0]

      for i in range(1, self.num_splits):
        split_batch = tf.cond(i < available_batches_count,
                              true_fn=create_slicer(i),
                              false_fn=zeros_slice)

        with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):
          split_batch = BatchedInput(
            source_tokens=tf.identity(split_batch.source_tokens),
            source_tokens_fixed=tf.identity(split_batch.source_tokens_fixed),
            source_chars=tf.identity(split_batch.source_chars),
            source_chars_fixed=tf.identity(split_batch.source_chars_fixed),
            target=tf.identity(split_batch.targets),
            source_sequence_length=tf.identity(split_batch.source_sequence_length),
            word_length=tf.identity(split_batch.word_length),
            title_cased=tf.identity(split_batch.title_cased),
            all_upper_case=tf.identity(split_batch.all_upper_case),
            all_lower_case=tf.identity(split_batch.all_lower_case),
            mixed_case=tf.identity(split_batch.mixed_case),
            punctuation=tf.identity(split_batch.punctuation),
            numeric=tf.identity(split_batch.numeric)
          )

          split_batches.append(split_batch)

      print()
      print("Split batches: ", split_batches)

      return split_batches


class BatchedInput(collections.namedtuple("BatchedInput",
                                          ("source_tokens",
                                           "source_tokens_fixed",
                                           "source_chars",
                                           "source_chars_fixed",
                                           "target",
                                           "source_sequence_length",
                                           "word_length",
                                           "title_cased",
                                           "all_upper_case",
                                           "all_lower_case",
                                           "mixed_case",
                                           "punctuation",
                                           "numeric"))):
  pass
