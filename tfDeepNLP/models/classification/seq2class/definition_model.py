"""S2C: Sequence-to-classification model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.python.layers import core as layers_core

from tfDeepNLP.models.classification.seq2class.binary_classification_model import BinaryClassificationModel
from tfDeepNLP.utils.misc_utils import tf_print_if

__all__ = ["DefinitionClassificationModel"]


class DefinitionClassificationModel(BinaryClassificationModel):
  """Sequence-to-classification dynamic model.

  This class implements a multi-layer recurrent neural network as encoder.
  This class also allows to use GRU cells in addition to LSTM cells with
  support for dropout.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_token_vocab_table,
               source_char_vocab_table,
               augment_embeddings=False,
               noise_amount_mean=None,
               noise_amount_stddev=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None
               ):
    super(DefinitionClassificationModel, self).__init__(
      hparams=hparams,
      mode=mode,
      iterator=iterator,
      source_token_vocab_table=source_token_vocab_table,
      source_char_vocab_table=source_char_vocab_table,
      augment_embeddings=augment_embeddings,
      noise_amount_mean=noise_amount_mean,
      noise_amount_stddev=noise_amount_stddev,
      scope=scope,
      single_cell_fn=single_cell_fn,
      model_device_fn=model_device_fn,
      optimizer_op=optimizer_op,
      learning_rate=learning_rate,
      first_gpu_id=first_gpu_id
    )

  def _build_classifier(self, x=None, x2=None, hparams=None):
    with tf.variable_scope("build_classifier"):

      input_ = self._prepare_classifier_input(x=None, x2=None)

      print("Begin building dense layers")

      if hparams.use_ensemble:
        act_fn = tf.nn.tanh
      else:
        act_fn = tf.nn.relu

      fully_connected_layer_1 = layers_core.dense(
        inputs=input_,
        units=hparams.classifier_num_units,
        activation=act_fn,
        use_bias=True,
        name="fully_connected_layer_1",
      )

      logits = layers_core.dense(
        inputs=fully_connected_layer_1,
        units=hparams.num_classes,
        activation=None,
        use_bias=True,
        name="logits",
      )

      return logits

  def _cnn1d(self, x, n_features, scope=None):
    with tf.variable_scope(scope + "/CNN1D" if scope else "CNN1D"):
      # Look at entire token embedding - collapses the embedding dimension (2) to 1 element
      filter = tf.get_variable('filter', [3, x.get_shape()[-1].value, n_features],
                               initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
                               dtype=tf.float32)
      conv = tf.nn.conv1d(x, filters=filter,
                          stride=3,
                          padding="VALID", name="convolution")
      conv = tf.nn.relu(conv, name="relu")

      return tf_print_if(conv, [tf.shape(conv)], summarize=10, message="CNN1D: ",
                         verbose=self.hparams.verbose_tensors)
