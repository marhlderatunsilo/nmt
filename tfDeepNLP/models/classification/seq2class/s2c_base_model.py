"""S2C: Basic sequence-to-classification model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf

from tfDeepNLP.models.classification.classification_base_model import ClassificationBaseModel
from tfDeepNLP.models.classification.seq2class.dataset_loader import BatchedInput
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.misc_utils import cond_scope

utils.check_tensorflow_version()

__all__ = ["S2CBaseModel"]


class S2CBaseModel(ClassificationBaseModel):
  """Concepts to Multilabel classification base class.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_token_vocab_table,
               source_char_vocab_table=None,
               augment_embeddings=False,
               noise_amount_mean=None,
               noise_amount_stddev=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None):

    # This should be set before super call as super uses this member
    self.__allowed_models = ["token_rnn", "token_cnn", "char_rnn", "char_cnn"]

    super(S2CBaseModel, self).__init__(
      hparams=hparams,
      mode=mode,
      iterator=iterator,
      source_token_vocab_table=source_token_vocab_table,
      source_char_vocab_table=source_char_vocab_table,
      augment_embeddings=augment_embeddings,
      noise_amount_mean=noise_amount_mean,
      noise_amount_stddev=noise_amount_stddev,
      scope=scope,
      single_cell_fn=single_cell_fn,
      model_device_fn=model_device_fn,
      optimizer_op=optimizer_op,
      learning_rate=learning_rate,
      first_gpu_id=first_gpu_id)

  @property
  def targets(self):
    """
    Gets target from iterator.

    Returns:
      Tensor with targets.
      """
    return self.iterator.targets

  @property
  def source_tokens(self):
    return self.iterator.source_tokens

  @property
  def source_chars(self):
    return self.iterator.source_chars

  @property
  def allowed_models(self):
    """
    Gets list of allowed models. Or None for all models.

    Currently available:
    """
    return self.__allowed_models

  @property
  def does_multilabel_classification(self):
    return False

  def extract_preds_and_probs(self, logits=None, hparams=None, scope=None):
    if logits is None:
      raise ValueError("logits cannot be None.")
    with cond_scope(scope):
      predictions = tf.argmax(logits, 1, output_type=tf.int32, name='predictions')
      probabilities = tf.nn.softmax(logits, name="probabilities")
      if hparams.num_classes == 2:
        # Get only probs for class 2
        probabilities = tf.identity(probabilities[:, 1:], name="infer_probability")

      return predictions, probabilities

  @property
  def batched_input_class(self):
    return BatchedInput

  def add_word_features_to_embeddings(self, embeddings):
    return tf.transpose(tf.concat([
      tf.transpose(embeddings),
      [self.iterator.word_length],
      [self.iterator.title_cased],
      [self.iterator.all_upper_case],
      [self.iterator.all_lower_case],
      [self.iterator.mixed_case],
      [self.iterator.punctuation],
      [self.iterator.numeric]],
      axis=0))
