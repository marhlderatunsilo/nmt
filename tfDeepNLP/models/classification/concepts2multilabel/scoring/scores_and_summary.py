import numpy as np
import tensorflow as tf

__all__ = ["add_up_scores", "add_summary_from_stats"]


def add_up_scores(step_results, py_or_tf="py"):
  """

  Note: Output order must be the same as input order, so
  we can run it repeatedly.

  :param step_results: list of tuples
  :param py_or_tf: whether to use numpy or tensorflow for finding averages, etc.
  :return: Tuple similar to input tuples, but averaged across tuples.
  """
  # Init arrays for model stats for this step
  step_losses = []
  f1_scores = []
  precisions = []
  recalls = []
  hamming_losses = []
  exact_matches = []
  step_word_counts = []
  batch_sizes = []
  gradient_norm_summaries = []

  # Get stats from each train model
  for step_result in step_results:
    (step_loss_,
     f1_,
     prec_,
     recall_,
     hamming_loss_,
     exact_match_,
     step_word_count_,
     batch_size_,
     gradient_norm_summary_) = step_result

    step_losses.append(step_loss_)
    f1_scores.append(f1_)
    precisions.append(prec_)
    recalls.append(recall_)
    hamming_losses.append(hamming_loss_)
    exact_matches.append(exact_match_)
    step_word_counts.append(step_word_count_)
    batch_sizes.append(batch_size_)
    if py_or_tf == "tf" and gradient_norm_summary_ is not None:
      gradient_norm_summaries.append(tf.summary.merge(gradient_norm_summary_))


  # Reduce stats arrays to scalars

  if py_or_tf == "py":
    avg_step_loss_py = np.mean(step_losses)

    avg_f1_py = np.mean(f1_scores)

    avg_precision_py = np.mean(precisions)
    avg_recall_py = np.mean(recalls)
    avg_hamming_loss_py = np.mean(hamming_losses)
    avg_exact_matches_py = np.mean(exact_matches)
    avg_batch_size_py = np.mean(batch_sizes)
    sum_step_word_count_py = sum(step_word_counts) * 1.0

    return (
      avg_step_loss_py, avg_f1_py, avg_precision_py, avg_recall_py, avg_hamming_loss_py,
      avg_exact_matches_py, sum_step_word_count_py, avg_batch_size_py
    )

  else:
    avg_step_loss_tf = tf.reduce_mean(step_losses)
    avg_f1_tf = tf.reduce_mean(f1_scores)
    avg_precision_tf = tf.reduce_mean(precisions)
    avg_recall_tf = tf.reduce_mean(recalls)
    avg_hamming_loss_tf = tf.reduce_mean(hamming_losses)
    avg_exact_matches_tf = tf.reduce_mean(exact_matches)

    avg_batch_size_tf = tf.reduce_mean(batch_sizes)
    sum_step_word_count_tf = tf.cast(tf.reduce_sum(step_word_counts), dtype=tf.float32)
    if len(gradient_norm_summaries) > 0:
      merged_gradient_norm_summaries = tf.summary.merge(gradient_norm_summaries)
    else:
      merged_gradient_norm_summaries = tf.convert_to_tensor(0)

    return (
      avg_step_loss_tf, avg_f1_tf, avg_precision_tf, avg_recall_tf, avg_hamming_loss_tf,
      avg_exact_matches_tf, sum_step_word_count_tf, avg_batch_size_tf, merged_gradient_norm_summaries
    )


def add_summary_from_stats(stats_tuple, summary_prefix=""):
  (step_loss_,
   f1_,
   prec_,
   recall_,
   hamming_loss_,
   exact_match_,
   step_word_count_,
   batch_size_,
   merged_gradient_norm_summaries_
   ) = stats_tuple

  summaries = [
    tf.summary.scalar("{}_loss".format(summary_prefix), step_loss_),
    tf.summary.scalar("{}_f1".format(summary_prefix), f1_),
    tf.summary.scalar("{}_precision".format(summary_prefix), prec_),
    tf.summary.scalar("{}_recall".format(summary_prefix), recall_),
    tf.summary.scalar("{}_hamming_loss".format(summary_prefix), hamming_loss_),
    tf.summary.scalar("{}_exact_match".format(summary_prefix), exact_match_),
    tf.summary.scalar("{}_word_count_sum".format(summary_prefix), step_word_count_),
    tf.summary.scalar("{}_batch_size".format(summary_prefix), batch_size_),
  ]

  if merged_gradient_norm_summaries_ is not None:
    summaries.append(merged_gradient_norm_summaries_)

  tf_summary = tf.summary.merge(summaries)

  return tf_summary
