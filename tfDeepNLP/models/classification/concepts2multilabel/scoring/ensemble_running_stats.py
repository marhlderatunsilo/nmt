import tensorflow as tf

from tfDeepNLP.models.classification.concepts2multilabel.c2ml_base_model import EnsembleMultilabelStatistics

__all__ = ["EnsembleRunningScorer", "create_ensemble_running_scorers",
           "get_ensemble_running_scorers_initializers", "create_ensemble_running_scorers_summary"]


class EnsembleRunningScorer(object):
  def __init__(self, model_name, avg_ensemble_stats):
    self.__model_name = model_name
    self.__avg_ensemble_stats = avg_ensemble_stats
    assert self.__model_name == self.__avg_ensemble_stats.model_name, \
      "model name must match model name in avg_ensemble_stats"

    self.scope = "{}/eval_ensemble_running_stats".format(self.__model_name)

    with tf.variable_scope(self.scope):
      self.__n_batches = tf.get_variable("n_batches", initializer=0.0)
      self.__loss = tf.get_variable("loss", initializer=0.0)
      self.__f1 = tf.get_variable("f1", initializer=0.0)
      self.__precision = tf.get_variable("precision", initializer=0.0)
      self.__recall = tf.get_variable("recall", initializer=0.0)
      self.__hamming_loss = tf.get_variable("hamming_loss", initializer=0.0)
      self.__exact_match = tf.get_variable("exact_match", initializer=0.0)

      self.__update_n_batches = tf.assign_add(self.__n_batches, 1.0)
      self.__update_loss = tf.assign_add(self.__loss, self.__avg_ensemble_stats.loss)
      self.__update_f1 = tf.assign_add(self.__f1, self.__avg_ensemble_stats.f1)
      self.__update_precision = tf.assign_add(self.__precision, self.__avg_ensemble_stats.precision)
      self.__update_recall = tf.assign_add(self.__recall, self.__avg_ensemble_stats.recall)
      self.__update_hamming_loss = tf.assign_add(self.__hamming_loss, self.__avg_ensemble_stats.hamming_loss)
      self.__update_exact_match = tf.assign_add(self.__exact_match, self.__avg_ensemble_stats.exact_match)

  def update_average(self):
    with tf.variable_scope(self.scope):
      self.__avg_loss = tf.div(self.__update_loss, self.__update_n_batches, name="avg_loss")
      self.__avg_f1 = tf.div(self.__update_f1, self.__update_n_batches, name="avg_f1")
      self.__avg_precision = tf.div(self.__update_precision, self.__update_n_batches,
                                    name="avg_precision")
      self.__avg_recall = tf.div(self.__update_recall, self.__update_n_batches, name="avg_recall")
      self.__avg_hamming_loss = tf.div(self.__update_hamming_loss, self.__update_n_batches,
                                       name="avg_hamming_loss")
      self.__avg_exact_match = tf.div(self.__update_exact_match, self.__update_n_batches,
                                      name="avg_exact_match")

  def create_summary(self, summary_prefix=""):
    self.update_average()
    self.tf_summary_ = tf.summary.merge([
      tf.summary.scalar("{}_{}_loss".format(summary_prefix, self.model_name), self.avg_loss),
      tf.summary.scalar("{}_{}_f1".format(summary_prefix, self.model_name), self.avg_f1),
      tf.summary.scalar("{}_{}_precision".format(summary_prefix, self.model_name), self.avg_precision),
      tf.summary.scalar("{}_{}_recall".format(summary_prefix, self.model_name), self.avg_recall),
      tf.summary.scalar("{}_{}_hamming_loss".format(summary_prefix, self.model_name), self.avg_hamming_loss),
      tf.summary.scalar("{}_{}_exact_match".format(summary_prefix, self.model_name), self.avg_exact_match),
    ])

  @property
  def model_name(self):
    return self.__model_name

  @property
  def initializer(self):
    return tf.variables_initializer(
      [self.__n_batches, self.__loss,
       self.__f1, self.__precision, self.__recall,
       self.__hamming_loss, self.__exact_match
       ],
      name='initialize_running_vars'
    )

  @property
  def tf_summary(self):
    if self.tf_summary_ is None:
      raise ValueError("tf_summary must be created before it is available.")
    return self.tf_summary_

  @property
  def avg_scores(self):
    return tuple([
      self.avg_loss, self.avg_f1,
      self.avg_precision, self.avg_recall,
      self.avg_hamming_loss, self.avg_exact_match
    ])

  @property
  def avg_loss(self):
    if self.__avg_loss is None:
      raise ValueError("avg_loss must be computed before it is available.")
    else:
      return self.__avg_loss

  @property
  def avg_f1(self):
    if self.__avg_f1 is None:
      raise ValueError("avg_f1 must be computed before it is available.")
    else:
      return self.__avg_f1

  @property
  def avg_precision(self):
    if self.__avg_precision is None:
      raise ValueError("avg_precision must be computed before it is available.")
    else:
      return self.__avg_precision

  @property
  def avg_recall(self):
    if self.__avg_recall is None:
      raise ValueError("avg_recall must be computed before it is available.")
    else:
      return self.__avg_recall

  @property
  def avg_hamming_loss(self):
    if self.__avg_hamming_loss is None:
      raise ValueError("avg_hamming_loss must be computed before it is available.")
    else:
      return self.__avg_hamming_loss

  @property
  def avg_exact_match(self):
    if self.__avg_exact_match is None:
      raise ValueError("avg_exact_match must be computed before it is available.")
    else:
      return self.__avg_exact_match

  @property
  def avg_EnsembleStatistics(self):
    """
        For recursive averaging.
        :return: EnsembleStatistics object with averaged values
        """
    return EnsembleMultilabelStatistics(model_name=self.model_name,
                                        loss=self.avg_loss,
                                        f1=self.avg_f1,
                                        precision=self.avg_precision,
                                        recall=self.avg_recall,
                                        hamming_loss=self.avg_hamming_loss,
                                        exact_match=self.avg_exact_match)


def create_ensemble_running_scorers(model_names, model_object_dict):
  """

  :param models: list of at-step-averaged models
  :return: model_object_dict for running scorers
  """

  model_object_runner_dict = {}

  # Map each model name to a EnsembleRunningScorer
  for m_name in model_names:
    model_object_runner_dict[m_name] = EnsembleRunningScorer(m_name, model_object_dict[m_name].avg_EnsembleStatistics)

  return model_object_runner_dict


def get_ensemble_running_scorers_initializers(model_names, model_object_runner_dict):
  initializers = []
  for m_name in model_names:
    initializers.append(model_object_runner_dict[m_name].initializer)
  return initializers


def create_ensemble_running_scorers_summary(model_names, model_object_runner_dict, summary_prefix=""):
  summaries = []
  # Average scores in all models' EnsembleModelScorers
  for m_name in model_names:
    model_object_runner_dict[m_name].create_summary(summary_prefix)
    summaries.append(model_object_runner_dict[m_name].tf_summary)
  return tf.summary.merge(
    summaries,
    name="ensemble_stats_runner_summary_merger"
  )
