"""S2C: Basic sequence-to-classification model with dynamic RNN support."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import collections

import tensorflow as tf

from tfDeepNLP.initializers.conditional_initializer import CondInitializer
from tfDeepNLP.models.classification.classification_base_model import ClassificationBaseModel
from tfDeepNLP.models.classification.concepts2multilabel.dataset_loader import BatchedInput
from tfDeepNLP.multi_cells.stack_bidirectional_dynamic_rnn import stack_bidirectional_dynamic_rnn
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.evaluate_metrics_multilabel_classification import EvaluateMetricsMultilabelClassification
from tfDeepNLP.utils.misc_utils import cond_scope
from tfDeepNLP.utils.sparse_utils import dense_to_sparse

utils.check_tensorflow_version()

__all__ = ["C2MLBaseModel", "EnsembleMultilabelStatistics"]


class C2MLBaseModel(ClassificationBaseModel):
  """Concepts to Multilabel classification base class.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_token_vocab_table,
               target_token_vocab_table=None,
               augment_embeddings=False,
               noise_amount_mean=None,
               noise_amount_stddev=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None):

    # This should be set before super call as super uses this member
    self.__allowed_models = ["token_rnn", "token_cnn"]
    self.__dense_targets = None

    super(C2MLBaseModel, self).__init__(
      hparams=hparams,
      mode=mode,
      iterator=iterator,
      source_token_vocab_table=source_token_vocab_table,
      target_token_vocab_table=target_token_vocab_table,
      augment_embeddings=augment_embeddings,
      noise_amount_mean=noise_amount_mean,
      noise_amount_stddev=noise_amount_stddev,
      scope=scope,
      single_cell_fn=single_cell_fn,
      model_device_fn=model_device_fn,
      optimizer_op=optimizer_op,
      learning_rate=learning_rate,
      first_gpu_id=first_gpu_id)

  @property
  def targets(self):
    """
    Gets target from iterator.

    Returns:
      Tensor with targets.
      """

    if self.__dense_targets is None:
      tgt_ids = self.iterator.target_concepts
      print("tgt_ids:", tgt_ids)

      # tgt_ids = tf.Print(tgt_ids, [tgt_ids], message="tgt_ids: ", summarize=100)

      # tgt_ids = dense_to_sparse(tgt_ids, eos_token=tgt_eos_id)

      label_counts = self.iterator.target_sequence_length

      def create_dense_target(targets_label_counts):
        targets, label_counts = targets_label_counts

        sparse_target = dense_to_sparse(targets[:label_counts])

        def create_indicator():
          dense_target = tf.sparse_to_indicator(sparse_target, vocab_size=self.hparams.tgt_token_vocab_size)
          dense_target = tf.to_float(dense_target)
          return dense_target

        dense_target = create_indicator()

        return dense_target

      self.__dense_targets = tf.map_fn(fn=create_dense_target, elems=(tgt_ids, label_counts), dtype=tf.float32,
                                       parallel_iterations=32)

    return self.__dense_targets

  @property
  def source_tokens(self):
    return self.iterator.source_concepts

  @property
  def source_chars(self):
    raise ValueError("Model works on concept keys. Character level is not meaningful.")

  @property
  def allowed_models(self):
    """
    Gets list of allowed models. Or None for all models.
    """
    return self.__allowed_models

  @property
  def does_multilabel_classification(self):
    return True

  def extract_preds_and_probs(self, logits=None, hparams=None, scope=None):
    """
    :return: predictions as 2d tensor with values 0 or 1, probabilities as 2d tensor with values in [0,1].
    """
    if logits is None:
      raise ValueError("logits cannot be None.")
    with cond_scope(scope):

      if self.hparams.loss_type == "softmax_cross_entropy":
        probabilities = tf.nn.softmax(logits, name="probabilities")
      elif self.hparams.loss_type == "sigmoid_cross_entropy":
        probabilities = tf.nn.sigmoid(logits, name="probabilities")
      else:
        raise ValueError("Unknown loss_type in hparams")

      # if self.hparams.eval_at_k is not None:
      #   target_sequence_lengths = tf.fill([tf.shape(logits)[0]], self.hparams.eval_at_k)
      # else:
      #   target_sequence_lengths = self.iterator.target_sequence_length
      #
      # logits_depth = logits.get_shape()[-1].value

      # def extract_predictions(probabilities_num_labels):
      #   probabilities, num_labels = probabilities_num_labels
      #   values, indices = tf.nn.top_k(probabilities, k=num_labels, sorted=True)
      #   prediction = tf.reduce_sum(tf.one_hot(indices, depth=logits_depth), 0)
      #   return prediction
      #
      # predictions = tf.map_fn(fn=extract_predictions, elems=(probabilities, target_sequence_lengths), dtype=tf.float32,
      #                         parallel_iterations=self.hparams.batch_size)

      cut_off = 0.8 * (1 / self.hparams.eval_at_k)

      if self.hparams.label_smoothing:
        num_classes = tf.cast(
          tf.shape(logits)[-1], logits.dtype)

        cut_off = cut_off - (self.hparams.label_smoothing / num_classes)

      predictions = tf.to_float(probabilities > cut_off) #

      return predictions, probabilities

  @property
  def batched_input_class(self):
    return BatchedInput

  def add_word_features_to_embeddings(self, embeddings):
    return tf.transpose(tf.concat([
      tf.transpose(embeddings),
      [self.iterator.pre_sentence_tag]
    ], axis=0))

  def _build_token_encoder(self, hparams, additional_embeddings=None):
    """Build an encoder."""
    num_layers = hparams.token_rnn_num_layers
    num_residual_layers = hparams.token_rnn_num_residual_layers

    with tf.variable_scope("token_encoder") as scope:
      dtype = scope.dtype
      # Look up embedding, emp_inp: [batch_size, max_time, num_units]
      encoder_emb_inp = tf.nn.embedding_lookup(
        self.token_embedding_encoder, self.source_tokens)

      # encoder_emb_inp = tf.Print(encoder_emb_inp, [self.source_tokens], message="self.source_tokens: ", summarize=300)

      # if self.augment_embeddings:
      #   encoder_emb_inp = embedding_augmentation(encoder_emb_inp,
      #                                            noise_amount_mean=self.noise_amount_mean,
      #                                            noise_amount_stddev=self.noise_amount_stddev)

      if additional_embeddings is not None:
        encoder_emb_inp = tf.concat([encoder_emb_inp, additional_embeddings], axis=-1)

      if self.time_major:
        encoder_emb_inp = tf.transpose(encoder_emb_inp, perm=[1, 0, 2])

      # if hparams.use_word_features:
      #   # Concatenate embeddings and word features
      #   encoder_emb_inp = self.add_word_features_to_embeddings(encoder_emb_inp)

      # Encoder_outpus: [max_time, batch_size, num_units]
      if hparams.token_rnn_encoder_type == "uni":
        utils.print_out("  num_layers = %d, num_residual_layers=%d" %
                        (num_layers, num_residual_layers))
        cell = self._build_encoder_cell_token(
          hparams, num_layers, num_residual_layers)

        encoder_outputs, encoder_state = tf.nn.dynamic_rnn(
          cell,
          encoder_emb_inp,
          dtype=dtype,
          parallel_iterations=min(hparams.batch_size, 32),
          swap_memory=True,
          sequence_length=self.iterator.source_sequence_length,
          time_major=self.time_major)

      elif hparams.token_rnn_encoder_type == "bi":
        num_bi_layers = int(num_layers / 2)
        num_bi_residual_layers = int(num_residual_layers / 2)
        utils.print_out("  num_bi_layers = %d, num_bi_residual_layers=%d" %
                        (num_bi_layers, num_bi_residual_layers))

        encoder_outputs, bi_encoder_state = (
          self._build_bidirectional_rnn(
            inputs=encoder_emb_inp,
            sequence_length=self.iterator.source_sequence_length,
            dtype=dtype,
            hparams=hparams,
            num_bi_layers=num_bi_layers,
            num_bi_residual_layers=num_bi_residual_layers))

        if num_bi_layers == 1:
          encoder_state = bi_encoder_state
        else:
          # alternatively concat forward and backward states
          encoder_state = []
          for layer_id in range(num_bi_layers):
            encoder_state.append(bi_encoder_state[0][layer_id])  # forward
            encoder_state.append(bi_encoder_state[1][layer_id])  # backward
          encoder_state = tuple(encoder_state)
      elif hparams.token_rnn_encoder_type == "bi_all":

        num_bi_layers = int(num_layers / 2)
        num_bi_residual_layers = int(num_residual_layers / 2)

        # Construct forward and backward cells
        with tf.variable_scope("fw_cell", initializer=CondInitializer(lambda shape, dtype: len(shape) == 2,
                                                                      tf.orthogonal_initializer(),
                                                                      tf.glorot_normal_initializer())):
          fw_cells = self._build_encoder_cell_token(hparams,
                                                    num_bi_layers,
                                                    0)
        with tf.variable_scope("bw_cell", initializer=CondInitializer(lambda shape, dtype: len(shape) == 2,
                                                                      tf.orthogonal_initializer(),
                                                                      tf.glorot_normal_initializer())):
          bw_cells = self._build_encoder_cell_token(hparams,
                                                    num_bi_layers,
                                                    0)

        encoder_outputs, output_state_fw, output_state_bw = stack_bidirectional_dynamic_rnn(fw_cells,
                                                                                            bw_cells,
                                                                                            encoder_emb_inp,
                                                                                            sequence_length=self.iterator.source_sequence_length,
                                                                                            dtype=dtype,
                                                                                            swap_memory=True,
                                                                                            num_bi_residual_layers=num_bi_residual_layers,
                                                                                            time_major=self.time_major)
        if num_bi_layers == 1:
          encoder_state = (output_state_fw, output_state_bw)
        else:

          print("output_state_fw: ", output_state_fw)

          encoder_state = (output_state_fw[-1], output_state_bw[-1])


      else:
        raise ValueError("Unknown encoder_type %s" % hparams.token_rnn_encoder_type)

    return encoder_outputs, encoder_state

  def _build_char_encoder(self, hparams):
    """Build a CNN encoder."""

    raise NotImplemented()

  def _build_token_cnn_pipeline(self, x, hparams):
    """
        Convolution pipeline.
    """

    raise NotImplemented()

  def _build_char_cnn_pipeline(self, x, hparams):
    """
    Convolution pipeline.
    """
    raise NotImplemented()

  def evaluate(self, targets=None, predictions=None, probabilities=None, **kwargs):

    # dense_targets = tf.Print(dense_targets, [tf.reduce_max(predictions), tf.reduce_sum(predictions, -1), dense_targets], message="POOP: ", summarize=200)

    # dense_targets = tf.Print(dense_targets, [targets, dense_targets], message="eval debug: ", summarize=40003)

    return EvaluateMetricsMultilabelClassification(targets, predictions, probabilities)

  def statistics(self):
    if self.mode != tf.contrib.learn.ModeKeys.INFER:
      return (
        self.loss,
        self.evaluator.f1,
        self.evaluator.precision,
        self.evaluator.recall,
        self.evaluator.hamming_loss,
        self.evaluator.exact_match,
        self.word_count,
        self.batch_size,
        self.gradient_norm_summary
      )
    else:
      print("Statistics does nothing in infer mode.")
      return tf.no_op()

  def extract_ensemble_statistics(self, model_name, evaluator, loss):

    return EnsembleMultilabelStatistics(model_name=model_name,
                                        loss=loss,
                                        f1=evaluator.f1,
                                        precision=evaluator.precision,
                                        recall=evaluator.recall,
                                        hamming_loss=evaluator.hamming_loss,
                                        exact_match=evaluator.exact_match
                                        )


class EnsembleMultilabelStatistics(collections.namedtuple("EnsembleMultilabelStatistics",
                                                          ("model_name",
                                                           "loss",
                                                           "f1",
                                                           "precision",
                                                           "recall",
                                                           "hamming_loss",
                                                           "exact_match"
                                                           ))):
  pass
