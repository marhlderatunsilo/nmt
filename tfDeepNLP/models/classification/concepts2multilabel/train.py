# Copyright 2017 Google Inc. All Rights Reserved. # TODO this is not really Google code anymore, perhaps revise copyright text
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""S2C: For training concepts2multilabel models on single machine multi-gpu setup."""
from __future__ import print_function

import os
import shutil
import time
import json

import numpy as np
import tensorflow as tf
from tensorflow.python import debug as tf_debug

from tfDeepNLP.create_optimizer import create_optimizer
from tfDeepNLP.data_parallelization_utils import average_gradients
from tfDeepNLP.models.classification.concepts2multilabel.create_models import create_model
from tfDeepNLP.models.classification.concepts2multilabel.dataset_loader import C2MLDatasetLoader
from tfDeepNLP.models.classification.concepts2multilabel.editor_keywords_model import EditorKeywordsClassificationModel
from tfDeepNLP.models.classification.concepts2multilabel.scoring.scores_and_summary import add_up_scores, \
  add_summary_from_stats
from tfDeepNLP.models.classification.concepts2multilabel.scoring.ensemble_model_scorer import add_up_ensemble_scores
from tfDeepNLP.models.classification.concepts2multilabel.scoring.ensemble_running_stats import \
  create_ensemble_running_scorers, \
  get_ensemble_running_scorers_initializers, create_ensemble_running_scorers_summary
from tfDeepNLP.models.classification.concepts2multilabel.scoring.running_stats import eval_running_stats
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils import vocab_utils
from tfDeepNLP.utils.misc_utils import get_session

utils.check_tensorflow_version()

__all__ = [
  "c2ml_train"
]


def c2ml_train(hparams,
               mode,
               scope=None,
               single_cell_fn=None):
  """
  Train a multilabel classification model!

  This is the single machine, multi-GPU version!
  """

  """
  Model settings
  """

  # Logging and output
  log_device_placement = hparams.log_device_placement
  out_dir = hparams.out_dir
  model_dir = hparams.out_dir
  log_file = os.path.join(out_dir, "log_%d" % time.time())
  log_f = tf.gfile.GFile(log_file, mode="a")
  utils.print_out("# log_file=%s" % log_file, log_f)
  summary_name = "train_log_%d" % hparams.task_index

  # Mode
  mode = "train" if mode in ["train", "training"] else mode
  mode = "eval" if mode in ["eval", "evaluation"] else mode
  mode = "infer" if mode in ["infer", "inference"] else mode
  mode = "save" if mode in ["save", "saver", "saving", "save_model"] else mode

  assert hparams.char_cnn_tokens_per_sentence <= hparams.src_token_max_len, \
    "hparams: tokens_per_sentence must be less than or equal to src_token_max_len."

  print("Mode:", mode)

  # Training
  num_train_steps = hparams.num_train_steps
  num_train_epochs = hparams.num_train_epochs
  steps_per_stats = hparams.steps_per_stats
  steps_per_external_eval = hparams.steps_per_external_eval
  steps_per_eval = steps_per_stats * 10
  # if not steps_per_external_eval:
  #   steps_per_external_eval = 5 * steps_per_eval
  config_proto = utils.get_config_proto(
    log_device_placement=log_device_placement)

  model_creator_dict = {"editor_keywords": EditorKeywordsClassificationModel}

  model_creator = model_creator_dict[hparams.model]

  """
    Create training models, session and graph.

    Single machine multi-gpu:
      We create a shared dataset iterator.
      We create a model per gpu - prefixed "tower_%d".
      We calculate loss and gradients for each model.
      We average gradients.
        On gpu:0 by default but the best performing gpu can be specified.
      We apply averaged gradients with the shared optimizer.

      Each model is created in the same graph and run from the same session.
  """

  with tf.device('/cpu:0'):

    global_step = tf.train.get_or_create_global_step()
    epoch_counter = tf.get_variable("epoch_counter", initializer=0, trainable=False)
    epoch_summary = tf.summary.merge([tf.summary.scalar("epoch", epoch_counter)])
    increment_epoch_counter = tf.assign_add(epoch_counter, 1)

    create_model_template = tf.make_template('create_model_template',
                                             create_model,
                                             create_scope_now_=True,
                                             model_creator=model_creator,
                                             hparams=hparams,
                                             scope=scope,
                                             single_cell_fn=single_cell_fn,
                                             )

    """
    Create vocab tables
    """
    if hparams.use_tokens:
      src_vocab_file = hparams.src_token_vocab_file
      src_vocab_table = vocab_utils.create_vocab_table(src_vocab_file)

      src_vocab_table = vocab_utils.check_vocab_table(src_vocab_table, src_vocab_file, 3)
    else:
      src_vocab_table = None

    tgt_vocab_file = hparams.tgt_token_vocab_file
    tgt_vocab_table = vocab_utils.create_vocab_table(tgt_vocab_file)
    tgt_vocab_table = vocab_utils.check_vocab_table(tgt_vocab_table, tgt_vocab_file, 3)

    opt, learning_rate = create_optimizer(hparams, global_step)

    """
    BUILD TRAIN
    """
    if mode == "train":
      with tf.name_scope("training"):
        data_file = "%s.%s" % (hparams.train_prefix, hparams.file_suffix)

        print("train_file: ", data_file)

        buffer_size = hparams.batch_size * hparams.num_gpus * 500

        input_pipe_train = C2MLDatasetLoader(
          file_path=data_file,
          mode=tf.contrib.learn.ModeKeys.TRAIN,
          batch_size=hparams.batch_size,
          src_vocab_table=src_vocab_table,
          tgt_vocab_table=tgt_vocab_table,
          sos=hparams.sos,
          eos=hparams.eos,
          source_reverse=hparams.source_reverse,
          random_seed=None,
          num_buckets=hparams.num_buckets,
          fallback_bucket_width=hparams.fallback_bucket_width,
          num_splits=hparams.num_gpus,
          src_token_max_len=hparams.src_token_max_len,
          src_cnn_tokens_per_sentence=hparams.token_cnn_tokens_per_sentence,
          shuffle=True,
          num_parallel_calls=hparams.num_parallel_calls,
          buffer_size=buffer_size
        )

        batches = input_pipe_train.split_batches

        print("batches: ", batches)

        # WARNING/HACK This is created to cheat and put the variables on the CPU
        unused_train_model = create_model_template(
          iterator=batches[0],
          source_token_vocab_table=src_vocab_table,
          target_token_vocab_table=tgt_vocab_table,
          augment_embeddings=hparams.augment_embeddings,
          noise_amount_mean=hparams.embedding_noise_amount_mean,
          noise_amount_stddev=hparams.embedding_noise_amount_stddev,
          optimizer_op=opt,
          learning_rate=learning_rate,
          first_gpu_id=None,
          mode=tf.contrib.learn.ModeKeys.TRAIN
        )

        tower_grads = []
        train_models = []
        with tf.variable_scope(tf.get_variable_scope()):
          for i in range(hparams.num_gpus):
            with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):
              with tf.name_scope('%s_%d' % ('tower', i)):
                print("\nTRAIN NAMESCOPE: " + str(tf.contrib.framework.get_name_scope()))

                train_model = create_model_template(
                  iterator=batches[i],
                  source_token_vocab_table=src_vocab_table,
                  target_token_vocab_table=tgt_vocab_table,
                  augment_embeddings=hparams.augment_embeddings,
                  noise_amount_mean=hparams.embedding_noise_amount_mean,
                  noise_amount_stddev=hparams.embedding_noise_amount_stddev,
                  optimizer_op=opt,
                  learning_rate=learning_rate,  # TODO Should it just be opt? Or should it be learning_rate?
                  first_gpu_id=i,
                  mode=tf.contrib.learn.ModeKeys.TRAIN
                )

                train_models.append(train_model)
                tower_grads.append(train_model.model.gradients)

        # Calculate the mean of each gradient
        # This is the synchronization point across all towers
        with tf.device(tf.DeviceSpec(device_type="GPU", device_index=0)):
          grads = average_gradients(tower_grads)
          update = opt.apply_gradients(grads, global_step=global_step)

      """
      BUILD EVAL
      """

      with tf.name_scope("evaluation"):
        data_file_path_eval = tf.placeholder(tf.string, name="data_file_path_eval")

        print("eval_file: ", data_file_path_eval)  # TODO Probably can't cause path is not static

        input_pipe_eval = C2MLDatasetLoader(
          file_path=data_file_path_eval,
          mode=tf.contrib.learn.ModeKeys.EVAL,
          batch_size=hparams.batch_size,
          src_vocab_table=src_vocab_table,
          tgt_vocab_table=tgt_vocab_table,
          sos=hparams.sos,
          eos=hparams.eos,
          source_reverse=hparams.source_reverse,
          random_seed=hparams.random_seed,
          num_buckets=hparams.num_buckets,
          fallback_bucket_width=hparams.fallback_bucket_width,
          num_splits=hparams.num_gpus,
          src_token_max_len=hparams.src_token_max_len,
          src_cnn_tokens_per_sentence=hparams.token_cnn_tokens_per_sentence,
          shuffle=False,
          num_parallel_calls=hparams.num_parallel_calls,
          buffer_size=buffer_size
        )

        eval_batches = input_pipe_eval.split_batches
        eval_models = []

        with tf.variable_scope(tf.get_variable_scope()):
          for i in range(hparams.num_gpus):
            with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):
              with tf.name_scope('%s_%d' % ('tower', i)):
                print("\nEVAL NAMESCOPE: " + str(tf.contrib.framework.get_name_scope()))

                eval_model = create_model_template(
                  iterator=eval_batches[i],
                  source_token_vocab_table=src_vocab_table,
                  target_token_vocab_table=tgt_vocab_table,
                  augment_embeddings=False,
                  noise_amount_mean=0.0,
                  noise_amount_stddev=0.0,
                  optimizer_op=opt,
                  learning_rate=learning_rate,
                  first_gpu_id=i,
                  mode=tf.contrib.learn.ModeKeys.EVAL
                )

                eval_models.append(eval_model)

      """
      Statistics ops
      """

      # Train
      train_statistics_ops = tuple([t_model.model.statistics() for t_model in train_models])
      train_statistics_ops_avg = add_up_scores(train_statistics_ops, py_or_tf="tf")
      train_summary = add_summary_from_stats(train_statistics_ops_avg, "train")
      if hparams.use_ensemble:
        train_ensemble_statistics_from_towers = [t_model.model.ensemble_statistics for t_model in train_models]
        train_ensemble_statistics_ops = [e_model for e_model_list in train_ensemble_statistics_from_towers
                                         for e_model
                                         in e_model_list]
        train_ensemble_model_names, train_ensemble_model_scorers, train_ensemble_summary = add_up_ensemble_scores(
          train_ensemble_statistics_ops,
          summary_prefix="train_ensemble")

      # Eval
      statistics_eval_ops = tuple([e_model.model.statistics() for e_model in eval_models])
      statistics_eval_ops_avg = add_up_scores(statistics_eval_ops, py_or_tf="tf")

      if hparams.use_ensemble:
        eval_ensemble_statistics_from_towers = [t_model.model.ensemble_statistics for t_model in eval_models]
        eval_ensemble_statistics_ops = [e_model for e_model_list in eval_ensemble_statistics_from_towers
                                        for e_model
                                        in e_model_list]
        eval_ensemble_model_names, eval_ensemble_model_scorers_dict, _ = add_up_ensemble_scores(
          eval_ensemble_statistics_ops, create_summary=False)

      # Eval running stats
      eval_stats_means, eval_stats_initializer = eval_running_stats(statistics_eval_ops_avg)
      eval_stats_means_summary = add_summary_from_stats(eval_stats_means, summary_prefix="eval")

      if hparams.use_ensemble:
        eval_ensemble_running_scorers_dict = create_ensemble_running_scorers(eval_ensemble_model_names,
                                                                             eval_ensemble_model_scorers_dict)
        eval_ensemble_stats_initializers = get_ensemble_running_scorers_initializers(eval_ensemble_model_names,
                                                                                     eval_ensemble_running_scorers_dict)
        eval_ensemble_stats_summary = create_ensemble_running_scorers_summary(eval_ensemble_model_names,
                                                                              eval_ensemble_running_scorers_dict,
                                                                              summary_prefix="eval_ensemble")

    elif mode in ["infer", "save"]:
      """
      BUILD INFERENCE
      """

      with tf.name_scope("inference"):
        data_file_path_infer = tf.placeholder(tf.string, name="data_file_path_infer")

        print("infer_file: ", data_file_path_infer)  # TODO Probably can't cause path is not static

        input_pipe_eval = C2MLDatasetLoader(
          file_path=data_file_path_infer,
          mode=tf.contrib.learn.ModeKeys.INFER,
          batch_size=hparams.batch_size,
          src_vocab_table=src_vocab_table,
          tgt_vocab_table=tgt_vocab_table,
          sos=hparams.sos,
          eos=hparams.eos,
          source_reverse=hparams.source_reverse,
          random_seed=hparams.random_seed,
          num_buckets=hparams.num_buckets,
          fallback_bucket_width=hparams.fallback_bucket_width,
          num_splits=hparams.num_gpus,
          src_token_max_len=hparams.src_token_max_len,
          src_cnn_tokens_per_sentence=hparams.token_cnn_tokens_per_sentence,
          shuffle=False,
          num_parallel_calls=hparams.num_parallel_calls,
        )

        infer_batches = input_pipe_eval.split_batches
        infer_models = []

        with tf.variable_scope(tf.get_variable_scope()):
          for i in range(hparams.num_gpus):
            with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):
              with tf.name_scope('%s_%d' % ('tower', i)):
                print("\nEVAL NAMESCOPE: " + str(tf.contrib.framework.get_name_scope()))

                infer_model = create_model_template(
                  iterator=infer_batches[i],
                  source_token_vocab_table=src_vocab_table,
                  target_token_vocab_table=tgt_vocab_table,
                  augment_embeddings=False,
                  noise_amount_mean=0.0,
                  noise_amount_stddev=0.0,
                  optimizer_op=opt,
                  learning_rate=learning_rate,
                  first_gpu_id=i,
                  mode=tf.contrib.learn.ModeKeys.INFER
                )

                infer_models.append(infer_model)

      raise NotImplementedError("Infer mode has not been implemented yet.")
      # with tf.name_scope("inference"):
      #   # Input placeholders
      #   infer_sentence_lower_ph = tf.placeholder(tf.string, name="infer_sentence_lower_ph")
      #   infer_sentence_upper_ph = tf.placeholder(tf.string, name="infer_sentence_upper_ph")
      #   infer_seq_len_ph = tf.placeholder(tf.int32, name="infer_seq_len_ph")
      #   infer_word_length_ph = tf.placeholder(tf.float32, name="infer_word_length_ph")
      #   infer_title_cased_ph = tf.placeholder(tf.float32, name="infer_title_cased_ph")
      #   infer_all_upper_case_ph = tf.placeholder(tf.float32, name="infer_all_upper_case_ph")
      #   infer_all_lower_case_ph = tf.placeholder(tf.float32, name="infer_all_lower_case_ph")
      #   infer_mixed_case_ph = tf.placeholder(tf.float32, name="infer_mixed_case_ph")
      #   infer_punctuation_ph = tf.placeholder(tf.float32, name="infer_punctuation_ph")
      #   infer_numeric_ph = tf.placeholder(tf.float32, name="infer_numeric_ph")
      #
      #   ### Preprocessing text
      #
      #   # Split in tokens
      #   infer_sentence_lower = tf.string_split([infer_sentence_lower_ph], skip_empty=True).values
      #   infer_sentence_upper = tf.string_split([infer_sentence_upper_ph], skip_empty=True).values
      #
      #   # Cut to max length
      #   if hparams.src_token_max_len_infer:
      #     infer_sentence_lower = tf.slice(infer_sentence_lower, [0], [hparams.src_token_max_len_infer],
      #                                     name='slice_text_lower_max_len_infer')
      #     infer_sentence_upper = tf.slice(infer_sentence_upper, [0], [hparams.src_token_max_len_infer],
      #                                     name='slice_text_upper_max_len_infer')
      #
      #   # Reverse
      #   if hparams.source_reverse:
      #     infer_sentence_lower = tf.reverse(infer_sentence_lower, axis=[0])
      #     infer_sentence_upper = tf.reverse(infer_sentence_upper, axis=[0])
      #
      #   # Cut to chars
      #   infer_chars = tf.map_fn(
      #     lambda txt: S2MLDatasetLoader.cut_to_character(txt, hparams.char_cnn_chars_per_token, hparams.eos),
      #     infer_sentence_upper,
      #     name='infer_chars_extraction')
      #
      #   # Look up word ids in vocab table
      #   infer_sentence_lower = src_vocab_table.lookup(infer_sentence_lower)
      #   infer_chars = src_char_vocab_table.lookup(infer_chars)
      #
      #   infer_sentence = tf.reshape(infer_sentence_lower, shape=[1, -1])
      #   infer_chars = tf.reshape(infer_chars, shape=[1, -1])
      #   infer_seq_len = tf.reshape(infer_seq_len_ph, shape=[-1])
      #   infer_word_length = tf.reshape(infer_word_length_ph, shape=[1, -1])
      #   infer_title_cased = tf.reshape(infer_title_cased_ph, shape=[1, -1])
      #   infer_all_upper_case = tf.reshape(infer_all_upper_case_ph, shape=[1, -1])
      #   infer_all_lower_case = tf.reshape(infer_all_lower_case_ph, shape=[1, -1])
      #   infer_mixed_case = tf.reshape(infer_mixed_case_ph, shape=[1, -1])
      #   infer_punctuation = tf.reshape(infer_punctuation_ph, shape=[1, -1])
      #   infer_numeric = tf.reshape(infer_numeric_ph, shape=[1, -1])
      #
      #   it = BatchedInput(source_tokens=infer_sentence,
      #                     source_chars=infer_chars,
      #                     target=None,
      #                     source_sequence_length=infer_seq_len,
      #                     word_length=infer_word_length,
      #                     title_cased=infer_title_cased,
      #                     all_upper_case=infer_all_upper_case,
      #                     all_lower_case=infer_all_lower_case,
      #                     mixed_case=infer_mixed_case,
      #                     punctuation=infer_punctuation,
      #                     numeric=infer_numeric)
      #
      #   with tf.variable_scope(tf.get_variable_scope()):
      #     infer_model = create_model_template(
      #       iterator=it,
      #       source_token_vocab_table=src_vocab_table,
      #       source_char_vocab_table=src_char_vocab_table,
      #       optimizer_op=opt,
      #       learning_rate=learning_rate,  # TODO Should it just be opt? Or should it be learning_rate?
      #       first_gpu_id=0,
      #       mode=tf.contrib.learn.ModeKeys.INFER
      #     )
      #
      #   infer_prediction, infer_probability = infer_model.model.get_classifications()

    with tf.name_scope("session"):
      # Create saver
      saver = tf.train.Saver(sharded=False, name='saver')
      epoch_saver = tf.train.Saver(sharded=False, name='epoch_saver', max_to_keep=hparams.ckpt_last_n_epochs)

      if mode == "train":
        embeddings_placeholder = tf.placeholder(dtype=tf.float32,
                                                shape=[hparams.src_token_vocab_size, hparams.token_embedding_size],
                                                name="embeddings_placeholder")
        assign_embeddings = tf.assign(ref=train_models[0].model.token_embedding_encoder, value=embeddings_placeholder)

      if mode != "save":
        # Create hooks for MonitoredTrainingSession
        train_hooks = [
          tf.train.StopAtStepHook(last_step=num_train_steps),
          tf.train.CheckpointSaverHook(
            save_steps=steps_per_stats,
            checkpoint_dir=model_dir,
            saver=saver,
            checkpoint_basename="classify.ckpt"
          ),
          # tf.train.NanTensorHook(train_statistics_ops_avg[0])
        ]

        # Create training session
        # MonitoredTrainingSession handles loading/saving of checkpoints, variables initialization, with more
        sess = tf.train.MonitoredTrainingSession(
          is_chief=True,
          checkpoint_dir=model_dir,
          hooks=train_hooks,
          save_summaries_secs=None,
          save_summaries_steps=None,
          save_checkpoint_secs=None,
          config=config_proto,
        )

      else:
        # Create model builder
        builder = tf.saved_model.builder.SavedModelBuilder(model_dir + "saved_model")
        sess = tf.Session()

        # Init tables - Needed in java
        init_all_tables = tf.group(tf.tables_initializer(name="init_all_tables"))
        sess.run(init_all_tables)

        # Restore variables from disk.
        latest_ckpt = tf.train.latest_checkpoint(model_dir)
        saver.restore(sess, latest_ckpt)

  if hparams.debug:
    sess = tf_debug.LocalCLIDebugWrapperSession(sess)

  # Init counters
  last_stats_step = 0
  last_eval_step = 0
  last_external_eval_step = 0

  """
  Running session
  """

  if mode == "train":

    """
    Get ready for training! 
    """

    # Create summary writer for tensorboard
    summary_writer = tf.summary.FileWriter(
      os.path.join(out_dir, summary_name), sess.graph)

    # Get the evaluated version of global step (Can't use a tensor for comparison in python!)
    global_step_python = tf.train.global_step(sess, global_step)
    epoch_python = sess.run(epoch_counter)

    # Initialize evaluation metrics
    step_time, checkpoint_loss, checkpoint_predict_count = 0.0, 0.0, 0.0
    checkpoint_total_count = 0.0
    speed = 0.0
    avg_step_time = 0

    # Assign embeddings first time if needed:
    if not global_step_python > 0 and mode == "train":
      print("Assigning Embeddings")
      # Assign embeddings
      # We only need to do this for the first model because of the way they share parameters

      with np.load(hparams.embeddings_path) as data:
        pretrained_embeddings = data["embeddings"]

      print("shape: ", pretrained_embeddings.shape)

      sess.run(assign_embeddings, {
        embeddings_placeholder: pretrained_embeddings})

    """
    Pre-evaluation
    """
    # if global_step_python == 0:
    #   utils.print_out("# Perform external pre-evaluation")
    #
    #   print("data_file_path_eval:", data_file_path_eval )
    #   print("hparams.dev_prefix:", hparams.dev_prefix )
    #   print("hparams.file_suffix:", hparams.file_suffix)
    #   # Initialize the eval iterator and stats initializer
    #   sess.run([input_pipe_eval.iterator.initializer, eval_stats_initializer],
    #            feed_dict={data_file_path_eval: hparams.dev_prefix + "." + hparams.file_suffix})
    #
    #   if hparams.use_ensemble:
    #     # Initialize the eval ensemble initilizer
    #     sess.run([eval_ensemble_stats_initializers],
    #              feed_dict={data_file_path_eval: hparams.dev_prefix + "." + hparams.file_suffix})
    #
    #   step_results_eval_collection = []
    #
    #   while True:
    #     try:
    #       pre_step_results_eval, pre_eval_summary_py = sess.run(
    #         [statistics_eval_ops_avg, eval_stats_means_summary],
    #         feed_dict={data_file_path_eval: hparams.dev_prefix + "." + hparams.file_suffix})
    #
    #       step_results_eval_collection.append(pre_step_results_eval)
    #
    #       if hparams.use_ensemble:
    #         # sess.run([eval_ensemble_stats_updater])
    #         eval_ensemble_stats_summary_py = sess.run(eval_ensemble_stats_summary)
    #
    #     except tf.errors.OutOfRangeError:
    #       # Only add summary on last step
    #       summary_writer.add_summary(pre_eval_summary_py, epoch_python)
    #
    #       if hparams.use_ensemble:
    #         summary_writer.add_summary(eval_ensemble_stats_summary_py, epoch_python)
    #
    #       (avg_step_loss_eval, avg_eval_f1, avg_eval_precision,
    #        avg_eval_recall, avg_eval_hamming_loss, avg_eval_exact_match, sum_step_word_count_eval,
    #        avg_batch_size_eval) = add_up_scores(step_results_eval_collection, py_or_tf="py")
    #
    #       # Print evaluation stats
    #       utils.print_out(
    #         "  \n----------------------"
    #         "Evaluation on Dev set\n"
    #         "loss {:.3f} hamming {:.2f} f1 {:.2f} prec {:.2f} "
    #         "rec {:.2f} exact_match {:.2f}\n".format(avg_step_loss_eval, avg_eval_hamming_loss,
    #                                                  avg_eval_f1, avg_eval_precision,
    #                                                  avg_eval_recall, avg_eval_exact_match),
    #         log_f)
    #       break

    """
    Start training
    """

    # Initialize iterators
    sess.run(input_pipe_train.iterator.initializer)

    print("Start training loop")

    while global_step_python < num_train_steps:
      global_step_python = sess.run(global_step)
      epoch_python = sess.run(epoch_counter)

      start_time = time.time()

      try:

        # Run a step
        # Magic happens
        _, step_results, train_summary_py = sess.run(
          [update, train_statistics_ops, train_summary])
        (avg_train_step_loss, avg_train_f1, avg_train_precision, avg_train_recall,
         avg_train_hamming_loss, avg_train_exact_match, sum_step_word_count_train,
         avg_batch_size_train) = add_up_scores(
          step_results, py_or_tf="py")

        if hparams.use_ensemble:
          train_ensemble_summary_py = sess.run(train_ensemble_summary)

        summary_writer.add_summary(train_summary_py, global_step_python)

        if hparams.use_ensemble:
          summary_writer.add_summary(train_ensemble_summary_py, global_step_python)

        # Update statistics
        step_time += (time.time() - start_time)

      except tf.errors.OutOfRangeError:

        epoch_python, epoch_summary_python = sess.run([increment_epoch_counter, epoch_summary])
        summary_writer.add_summary(epoch_summary_python, global_step_python)

        """
        We run external evaluation to inspect the current ability of the model.
        There is no more data, so we initialize the model iterators again 
          (to load more data) and go to next epoch.
        """

        print("OutOfRangeError!! Restarting iterators!")

        utils.print_out(
          "# Finished epoch {}, step {}. Perform external evaluation".format(
            epoch_python, global_step_python))

        # Initialize the eval iterator
        sess.run([input_pipe_eval.iterator.initializer, eval_stats_initializer],
                 feed_dict={data_file_path_eval: hparams.dev_prefix + "." + hparams.file_suffix})

        if hparams.use_ensemble:
          # Initialize the eval ensemble initilizer
          sess.run([eval_ensemble_stats_initializers],
                   feed_dict={data_file_path_eval: hparams.dev_prefix + "." + hparams.file_suffix})

        step_results_eval_collection = []

        utils.print_out("Starting evaluation")

        while True:
          try:
            step_results_eval, eval_summary_py = sess.run(
              [statistics_eval_ops_avg, eval_stats_means_summary],
              feed_dict={data_file_path_eval: hparams.dev_prefix + "." + hparams.file_suffix})

            step_results_eval_collection.append(step_results_eval)

            if hparams.use_ensemble:
              eval_ensemble_stats_summary_py = sess.run(eval_ensemble_stats_summary)

          except tf.errors.OutOfRangeError:
            # Only add summary on last step
            summary_writer.add_summary(eval_summary_py, epoch_python)

            if hparams.use_ensemble:
              summary_writer.add_summary(eval_ensemble_stats_summary_py, epoch_python)

            (avg_step_loss_eval, avg_eval_f1, avg_eval_precision,
             avg_eval_recall, avg_eval_hamming_loss, avg_eval_exact_match, sum_step_word_count_eval,
             avg_batch_size_eval) = add_up_scores(step_results_eval_collection, py_or_tf="py")

            # Print evaluation stats
            utils.print_out(
              "  \n----------------------"
              "Evaluation on Dev set\n"
              "loss {:.3f} hamming {:.2f} f1 {:.2f} prec {:.2f} "
              "rec {:.2f} exact match {:.2f} \n".format(avg_step_loss_eval, avg_eval_hamming_loss,
                                                        avg_eval_f1, avg_eval_precision,
                                                        avg_eval_recall, avg_eval_exact_match),
              log_f)
            break

        if not (sess.should_stop() or epoch_python >= num_train_epochs):

          utils.print_out("# Save epoch #{} model ckpt".format(epoch_python))
          epoch_saver.save(get_session(sess), model_dir + 'epoch_ckpts/classify_epoch_{}.ckpt'.format(epoch_python))

          # Reinitialize the iterator
          sess.run(input_pipe_train.iterator.initializer)
          continue
        else:
          break

      # Update statistics
      checkpoint_loss += (avg_train_step_loss * avg_batch_size_train)
      # checkpoint_predict_count += avg_step_predict_count
      checkpoint_total_count += sum_step_word_count_train  # TODO Is it correct?

      if not (sess.should_stop() or epoch_python >= num_train_epochs):

        # Write step summaries. TODO Is this the best way to add all summaries?
        # for gpu_id, summary in enumerate(step_summaries):
        #   summary_writer.add_summary(summary, global_step_python)

        # Once in a while, we print statistics.
        if global_step_python - last_stats_step >= steps_per_stats:
          last_stats_step = global_step_python

          # Print statistics for the previous epoch.
          avg_step_time = step_time / steps_per_stats
          # train_ppl = #utils.safe_exp(checkpoint_loss / checkpoint_predict_count)

          speed = checkpoint_total_count / (1000 * step_time)
          utils.print_out(
            "  global step {} lr {:.4f} "
            "step-time {:.2f} wps {:.2f}k loss {:.3f}\n"
            "    hamming {:.2f} f1 {:.2f} prec {:.2f} "
            "rec {:.2f} exact match {:.2f}".format(global_step_python, learning_rate.eval(session=sess),
                                                   avg_step_time, speed, avg_train_step_loss, avg_train_hamming_loss,
                                                   avg_train_f1, avg_train_precision,
                                                   avg_train_recall, avg_train_exact_match),
            log_f)

          # Reset timer and loss.
          step_time, checkpoint_loss, checkpoint_predict_count = 0.0, 0.0, 0.0
          checkpoint_total_count = 0.0

    """
    Test set evaluation
    """

    if sess.should_stop() or epoch_python >= num_train_epochs:
      utils.print_out("# Save final model ckpt")

      saver.save(get_session(sess), model_dir + 'classify_final.ckpt')

      utils.print_out("# Perform external test set evaluation")

      # Initialize the eval iterator
      sess.run([input_pipe_eval.iterator.initializer, eval_stats_initializer],
               feed_dict={data_file_path_eval: hparams.test_prefix + "." + hparams.file_suffix})

      if hparams.use_ensemble:
        # Initialize the eval ensemble initilizer
        sess.run([eval_ensemble_stats_initializers],
                 feed_dict={data_file_path_eval: hparams.dev_prefix + "." + hparams.file_suffix})

      step_results_eval_collection = []

      while True:
        try:
          post_step_results_eval, post_eval_summary_py = sess.run(
            [statistics_eval_ops_avg, eval_stats_means_summary],
            feed_dict={data_file_path_eval: hparams.test_prefix + "." + hparams.file_suffix})

          step_results_eval_collection.append(post_step_results_eval)

          #
          if hparams.use_ensemble:
            post_eval_ensemble_summary_py = sess.run(eval_ensemble_stats_summary)



        except tf.errors.OutOfRangeError:
          # Only add summary on last step
          summary_writer.add_summary(post_eval_summary_py, epoch_python)

          if hparams.use_ensemble:
            summary_writer.add_summary(post_eval_ensemble_summary_py, epoch_python)

          (avg_step_loss_eval, avg_eval_f1, avg_eval_precision,
           avg_eval_recall, avg_eval_hamming_loss, avg_eval_exact_match, sum_step_word_count_eval,
           avg_batch_size_eval) = add_up_scores(step_results_eval_collection, py_or_tf="py")

          # Print evaluation stats
          utils.print_out(
            "  \n----------------------"
            "Evaluation on Test set\n"
            "loss {:.3f} hamming {:.2f} f1 {:.2f} prec {:.2f} "
            "rec {:.2f} exact match {:.2f}\n".format(avg_step_loss_eval, avg_eval_hamming_loss,
                                                     avg_eval_f1, avg_eval_precision,
                                                     avg_eval_recall, avg_eval_exact_match),
            log_f)
          break

    summary_writer.close()

  elif mode == "infer":

    with open(hparams.out_dir + 'infer_probability_distributions.json', mode='w', encoding="utf-8") as json_output_f:

      probabilities = [infer_model.model.probabilities for infer_model in infer_models]
      np_probabilities, input_batch = sess.run((probabilities, infer_batches))

      data_to_dump = {}

      for tower_index, (tower_probabilities, tower_input) in enumerate(zip(np_probabilities, input_batch)):
       for element_input, element_prob in zip(tower_probabilities, tower_input):
         article_id = element_input[3]
         if article_id:
          data_to_dump[article_id] = element_prob.tolist()

      json.dump(data_to_dump, json_output_f)

    #raise NotImplementedError("Infer mode has not been implemented yet.")

    # # TODO Should be able to pass the final model checkpoint instead of the last one saved by monitoredTrainingSession?
    #
    # pred, prob = sess.run([infer_prediction, infer_probability],
    #                       feed_dict={infer_sentence_lower_ph: 'a cat is a large animal .',  # Must be lowercase!
    #                                  infer_sentence_upper_ph: 'A cat is a large animal .',  # Must be lowercase!
    #                                  infer_seq_len_ph: 7,
    #                                  infer_word_length_ph: np.asarray([1, 3, 2, 1, 5, 6, 1]).astype(np.float32),
    #                                  infer_title_cased_ph: np.asarray([1, 0, 0, 0, 0, 0, 0]).astype(np.float32),
    #                                  infer_all_upper_case_ph: np.asarray([1, 0, 0, 0, 0, 0, 0]).astype(np.float32),
    #                                  infer_all_lower_case_ph: np.asarray([0, 1, 1, 1, 1, 1, 1]).astype(np.float32),
    #                                  infer_mixed_case_ph: np.asarray([0, 0, 0, 0, 0, 0, 0]).astype(np.float32),
    #                                  infer_punctuation_ph: np.asarray([0, 0, 0, 0, 0, 0, 1]).astype(np.float32),
    #                                  infer_numeric_ph: np.asarray([0, 0, 0, 0, 0, 0, 0]).astype(np.float32)})
    #
    # print("Prediction", pred)
    # print("Probability", prob)
    #
    # pred, prob = sess.run([infer_prediction, infer_probability],
    #                       feed_dict={
    #                         infer_sentence_lower_ph: 'the universe is all of space and time ( spacetime ) and its contents , which includes planets , moons , stars , galaxies , the contents of intergalactic space and all matter and energy',
    #                         infer_sentence_lower_ph: 'The Universe is all of Space and time ( spacetime ) and its contents , which includes planets , moons , stars , galaxies , the contents of intergalactic Space and all matter and energy',
    #                         infer_seq_len_ph: 35,
    #                         infer_word_length_ph: np.asarray(
    #                           [3, 8, 2, 3, 2, 5, 3, 4, 1, 9, 1, 3, 3, 8, 1, 5, 8, 7, 1, 5, 1, 5, 1, 8, 1, 3, 8, 2, 13,
    #                            5, 3, 3, 6, 3, 6]).astype(np.float32),
    #                         infer_title_cased_ph: np.asarray(
    #                           [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                            0, 0, 0, 0, 0]).astype(np.float32),
    #                         infer_all_upper_case_ph: np.asarray(
    #                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                            0, 0, 0, 0, 0]).astype(np.float32),
    #                         infer_all_lower_case_ph: np.asarray(
    #                           [0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1,
    #                            1, 1, 1, 1, 1]).astype(np.float32),
    #                         infer_mixed_case_ph: np.asarray(
    #                           [1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                            0, 0, 0, 0, 0]).astype(np.float32),
    #                         infer_punctuation_ph: np.asarray(
    #                           [0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0,
    #                            0, 0, 0, 0, 0]).astype(np.float32),
    #                         infer_numeric_ph: np.asarray(
    #                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    #                            0, 0, 0, 0, 0]).astype(np.float32)})
    #
    # print("Prediction", pred)
    # print("Probability", prob)

    """
    Done training
    """

  elif mode == "save":

    # Save model bundle
    tags = ["SERVING"]
    builder.add_meta_graph_and_variables(sess, tags, clear_devices=False)
    builder.save()
    print("Saved inference model! Congratulations!")

    # Move vocab to saved model bundle
    shutil.copy2(src_vocab_file, model_dir + "saved_model/src_vocab.txt")
    shutil.copy2(tgt_vocab_file, model_dir + "saved_model/tgt_vocab.txt")
    print("Moved vocab files to saved_model folder.")

  return 0, 0, 0, 0, 0
