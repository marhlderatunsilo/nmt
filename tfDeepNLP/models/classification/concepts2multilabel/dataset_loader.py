"""
@author Ludvig Olsen
@date Dec. 12th 2017
Parts were copied from a personal project.
Parts were from the original iterator utils from the Google NMT code.


This was originally created for the Springer Nature hackday project.

It works on concept keys instead of raw text. Because of this, we do not care about character level information.


"""

import collections

import tensorflow as tf

from tfDeepNLP.input_pipes.dataset_loader import DatasetLoader

__all__ = ["BatchedInput", "C2MLDatasetLoader"]


class C2MLDatasetLoader(DatasetLoader):
  """Wrapper class around tf.data.Dataset pipeline.
  """

  def __init__(self, file_path, mode, batch_size, src_vocab_table=None, tgt_vocab_table=None, sos=None, eos=None,
               source_reverse=False, random_seed=None, num_buckets=None, fallback_bucket_width=None,
               num_splits=None, src_token_max_len=None, tgt_token_max_len=None, src_cnn_tokens_per_sentence=None,
               shuffle=True,
               num_parallel_calls=32, buffer_size=10000):
    """
    :param file_path: Path to TFRecord file.
    :param mode: tf.contrib.learn.ModeKeys (TRAIN,EVAL,INFER) where inference is with no known labels (INFER not yet supported).
    :param batch_size: int.
    :param src_vocab_table: Vocab table for src concepts.
    :param tgt_vocab_table: Vocab table for tgt concepts.
    :param sos: Start of Sentence symbol.
    :param eos: End of Sentence symbol.
    :param source_reverse: Reverse text.
    :param random_seed: Random seed to ensure reproducibility.
    :param num_buckets: Number of buckets.
    :param fallback_bucket_width: The bucket width used when src_token_max_len is None.
    :param num_splits: Number of splits, e.g. 1 split per GPU.
    :param src_token_max_len: Max sequence length of text.
    :param tgt_token_max_len: Max sequence length of tgt.
    :param src_cnn_tokens_per_sentence: Fixed number of tokens in source for CNN.
    :param shuffle: bool.
    :param num_parallel_calls: Number of parallel processes used in input parsing.
    :param buffer_size: int.
    """

    super().__init__(file_path=file_path, mode=mode, batch_size=batch_size, src_token_vocab_table=src_vocab_table,
                     tgt_token_vocab_table=tgt_vocab_table, sos=sos, eos=eos, source_reverse=source_reverse,
                     random_seed=random_seed, num_buckets=num_buckets,
                     fallback_bucket_width=fallback_bucket_width, num_splits=num_splits,
                     src_token_max_len=src_token_max_len, tgt_token_max_len=tgt_token_max_len,
                     token_cnn_tokens_per_sentence=src_cnn_tokens_per_sentence,
                     shuffle=shuffle, num_parallel_calls=num_parallel_calls, buffer_size=buffer_size)

    print("token_cnn_tokens_per_sentence: ", self.token_cnn_tokens_per_sentence)

    if self.mode == tf.contrib.learn.ModeKeys.INFER:
      """
      For inference without true labels.
      We probably want to feed the data differently,
      and so, this will work totally different than the train and eval modes.
      """
      pass
    else:

      dataset = tf.data.TFRecordDataset(self.file_path)

      # dataset = dataset.take(2000)
      # dataset = dataset.repeat()

      # Currently, the training and eval parsers do the same, i.e. simply load the data from TFRecords.
      # But we might want to have some processing in them separately at some point, so we keep this structure.
      if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
        dataset = dataset.map(self._parse_function_train,
                              num_parallel_calls=self.num_parallel_calls)

      elif self.mode == tf.contrib.learn.ModeKeys.EVAL:
        dataset = dataset.map(self._parse_function_eval,
                              num_parallel_calls=self.num_parallel_calls)
      elif self.mode == tf.contrib.learn.ModeKeys.INFER:
        dataset = dataset.map(self._parse_function_inference,
                              num_parallel_calls=self.num_parallel_calls)
      else:
        raise ValueError("Invalid mode '%s'." % mode)

      # shuffle the first `buffer_size` elements of the dataset
      if shuffle:
        dataset = dataset.shuffle(buffer_size=self.buffer_size, seed=self.random_seed)

      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        dataset = self.preprocess_dataset_train(dataset)
      else:
        dataset = self.preprocess_dataset_infer(dataset)

      if self.num_buckets > 1:
        batched_dataset = self.bucket_batching(dataset)
      else:
        print("NOT USING BUCKETING!!! ..... --- !!!")
        if self.mode != tf.contrib.learn.ModeKeys.INFER:
          batched_dataset = self.batching_func_train(dataset)
        else:
          batched_dataset = self.batching_func_infer(dataset)
        batched_dataset = batched_dataset.prefetch(self.buffer_size)

      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        self.__iterator = batched_dataset.make_initializable_iterator(shared_name="training_iterator")
        (src_ids, src_fixed_ids, tgt_ids, src_seq_len, tgt_seq_len, pre_sentence) = self.__iterator.get_next()

        # print("\nShapes of parsed inputs")
        # print(src_ids.get_shape(),
        #       src_fixed_ids.get_shape(),
        #       tgt_ids.get_shape(),
        #       src_seq_len.get_shape(),
        #       tgt_seq_len.get_shape(),
        #       pre_sentence.get_shape()
        #       )

        self.__batched_data = BatchedInput(
          source_concepts=src_ids,
          source_concepts_fixed=src_fixed_ids,
          target_concepts=tgt_ids,
          source_sequence_length=src_seq_len,
          target_sequence_length=tgt_seq_len,
          pre_sentence_tag=pre_sentence
        )

        # if self.mode != tf.contrib.learn.ModeKeys.INFER:
        # This is the one to use in training
        # It splits each batch to the GPU's, so
        # all batches running at the same time is from the same bucket
        print("About to split batches from: ", self.__batched_data)
        self.__split_batches = self.split_batched_input()
      else:
        self.__iterator = batched_dataset.make_initializable_iterator(shared_name="infer_iterator")
        (src_ids, src_fixed_ids, src_seq_len, pre_sentence, article_id) = self.__iterator.get_next()

        # print("\nShapes of parsed inputs")
        # print(src_ids.get_shape(),
        #       src_fixed_ids.get_shape(),
        #       tgt_ids.get_shape(),
        #       src_seq_len.get_shape(),
        #       tgt_seq_len.get_shape(),
        #       pre_sentence.get_shape()
        #       )

        self.__batched_data = InferBatchedInput(
          source_concepts=src_ids,
          source_concepts_fixed=src_fixed_ids,
          source_sequence_length=src_seq_len,
          pre_sentence_tag=pre_sentence,
          article_id=article_id,
        )

        # if self.mode != tf.contrib.learn.ModeKeys.INFER:
        # This is the one to use in training
        # It splits each batch to the GPU's, so
        # all batches running at the same time is from the same bucket
        print("About to split batches from: ", self.__batched_data)
        self.__split_batches = self.split_batched_input()

  @property
  def split_batches(self):
    print('Getting subclass split_batches')
    # Update super's __split_batches before returning
    super(C2MLDatasetLoader, C2MLDatasetLoader).split_batches.__set__(self, self.__split_batches)
    return super().split_batches

  @property
  def batched_data(self):
    print('Getting subclass batched_data')
    # Update super's __batched_data before returning
    super(C2MLDatasetLoader, C2MLDatasetLoader).batched_data.__set__(self, self.__batched_data)
    return super().batched_data

  @property
  def iterator(self):
    print('Getting subclass iterator')
    # Update super's __iterator before returning
    super(C2MLDatasetLoader, C2MLDatasetLoader).iterator.__set__(self, self.__iterator)
    return super().iterator

  def _parse_function_train(self, content):
    """Input parser for samples of the training set."""

    keys_to_features = {'source': tf.FixedLenFeature((), tf.string),
                        'target': tf.FixedLenFeature((), tf.string),
                        'src_seq_len': tf.FixedLenFeature((), tf.int64),
                        'tgt_seq_len': tf.FixedLenFeature((), tf.int64),
                        # 'is_skipped_tag': tf.VarLenFeature(tf.int64),
                        'is_pre_sentence_tag': tf.VarLenFeature(tf.int64),
                        # 'is_post_sentence_tag': tf.VarLenFeature(tf.int64)
                        # 'article_id': tf.FixedLenFeature((), tf.string),
                        }
    parsed_features = tf.parse_single_example(content, keys_to_features)

    source_sequence_length = tf.cast(parsed_features['src_seq_len'], tf.int32)
    target_sequence_length = tf.cast(parsed_features['tgt_seq_len'], tf.int32)
    pre_sentence = tf.cast(tf.sparse_tensor_to_dense(parsed_features['is_pre_sentence_tag']), tf.float32)

    return parsed_features['source'], parsed_features['target'], source_sequence_length, target_sequence_length, \
           pre_sentence  # , parsed_features["article_id"]

  def _parse_function_eval(self, content):
    """Input parser for samples of the dev/test set."""
    return self._parse_function_train(content)

  def _parse_function_inference(self, content):
    """Input parser for inference without true labels, e.g. in Java."""
    """Input parser for samples of the training set."""

    keys_to_features = {'source': tf.FixedLenFeature((), tf.string),
                        'src_seq_len': tf.FixedLenFeature((), tf.int64),
                        'is_pre_sentence_tag': tf.VarLenFeature(tf.int64),
                        'article_id': tf.FixedLenFeature((), tf.string),
                        }
    parsed_features = tf.parse_single_example(content, keys_to_features)
    source_sequence_length = tf.cast(parsed_features['src_seq_len'], tf.int32)
    pre_sentence = tf.cast(tf.sparse_tensor_to_dense(parsed_features['is_pre_sentence_tag']), tf.float32)

    return parsed_features['source'], source_sequence_length, pre_sentence, parsed_features["article_id"]

  # def _parse_function(self, content):

  def preprocess_dataset_train(self, dataset):

    # dataset = dataset.map(
    #   lambda src, tgt, src_seq_len, tgt_seq_len, *args: (tf.Print(src, [tgt_seq_len, tgt], message="tgt: "), tgt, src_seq_len, tgt_seq_len, *args),
    #   num_parallel_calls=self.num_parallel_calls)

    # Split text to tokens
    dataset = dataset.map(
      lambda src, tgt, *args: (
        tf.string_split([src], skip_empty=True).values,
        tf.string_split([tgt], delimiter=";", skip_empty=True).values,
        *args),
      num_parallel_calls=self.num_parallel_calls)

    # Filter zero length input sequences.
    dataset = dataset.filter(
      lambda src, tgt, src_seq_len, tgt_seq_len, *args: tf.logical_and(
        tf.logical_and(tf.size(src) > 0, tf.size(tgt) > 0), tf.logical_and(src_seq_len > 0, tgt_seq_len > 0)))

    # Remove input paddings
    dataset = dataset.map(
      lambda src, tgt, src_seq_len, tgt_seq_len, pre_sentence: (
        tf.slice(src, [0], [tf.minimum(src_seq_len, tf.size(src))], name='slice_source_padding'),
        tf.slice(tgt, [0], [tf.minimum(tgt_seq_len, tf.size(tgt))], name='slice_target_padding'),
        tf.minimum(src_seq_len, tf.size(src)),
        tf.minimum(tgt_seq_len, tf.size(tgt)),
        tf.slice(pre_sentence, [0], [tf.minimum(src_seq_len, tf.size(src))], name='slice_pre_sentence_tag_padding'),
      ),
      num_parallel_calls=self.num_parallel_calls)

    # Filter zero length input sequences.
    dataset = dataset.filter(
      lambda src, tgt, src_seq_len, tgt_seq_len, *args: tf.logical_and(
        tf.logical_and(tf.size(src) > 0, tf.size(tgt) > 0), tf.logical_and(src_seq_len > 0, tgt_seq_len > 0)))

    # Cut to src_token_max_len
    if self.src_token_max_len:
      dataset = dataset.map(
        lambda src, tgt, src_seq_len, tgt_seq_len, is_pre_sentence: (
          src[:self.src_token_max_len],
          tgt,
          tf.minimum(src_seq_len, self.src_token_max_len),
          tgt_seq_len,
          is_pre_sentence[:self.src_token_max_len]
        ),
        num_parallel_calls=self.num_parallel_calls)

    # Cut to tgt_token_max_len
    if self.tgt_token_max_len:
      dataset = dataset.map(
        lambda src, tgt, src_seq_len, tgt_seq_len, *args: (
          src,
          tgt[:self.tgt_token_max_len],
          src_seq_len,
          tf.minimum(tgt_seq_len, self.tgt_token_max_len),
          *args
        ),
        num_parallel_calls=self.num_parallel_calls)

    if self.source_reverse:
      dataset = dataset.map(
        lambda src, tgt, src_seq_len, tgt_seq_len, is_pre_sentence: (
          tf.reverse(src, axis=[0]),
          tgt,
          src_seq_len,
          tgt_seq_len,
          tf.reverse(is_pre_sentence, axis=[0])),
        num_parallel_calls=self.num_parallel_calls
      )

    # Create fixed length token sequences for 2d convolution
    dataset = dataset.map(
      lambda src, *args: (src,
                          self.make_fixed_length_1d(src, self.token_cnn_tokens_per_sentence, self.eos),
                          *args),
      num_parallel_calls=self.num_parallel_calls
    )

    # Convert the word strings to ids. Word strings that are not in the
    # vocab get the lookup table's default_value integer.

    dataset = dataset.map(
      lambda src, src_fixed, tgt, *args: (
        tf.cast(self.src_token_vocab_table.lookup(src), tf.int32),
        tf.cast(self.src_token_vocab_table.lookup(src_fixed), tf.int32),
        tf.cast(self.tgt_token_vocab_table.lookup(tgt), tf.int32),
        *args),
      num_parallel_calls=self.num_parallel_calls)

    # There should at least be one not unknown token
    dataset = dataset.filter(lambda src, src_fixed, tgt, *args: tf.reduce_sum(tgt) > 0)

    return dataset

  def preprocess_dataset_infer(self, dataset):

    # Split text to tokens
    dataset = dataset.map(
      lambda src, *args: (
        tf.string_split([src], skip_empty=True).values,
        *args),
      num_parallel_calls=self.num_parallel_calls)

    # Remove input paddings
    dataset = dataset.map(
      lambda src, src_seq_len, pre_sentence, *args: (
        tf.slice(src, [0], [tf.minimum(src_seq_len, tf.size(src))], name='slice_source_padding'),
        tf.minimum(src_seq_len, tf.size(src)),
        tf.slice(pre_sentence, [0], [tf.minimum(src_seq_len, tf.size(src))], name='slice_pre_sentence_tag_padding'),
        *args
      ),
      num_parallel_calls=self.num_parallel_calls)

    # Cut to src_token_max_len
    if self.src_token_max_len:
      dataset = dataset.map(
        lambda src, tgt, src_seq_len, tgt_seq_len, is_pre_sentence: (
          src[:self.src_token_max_len],
          tgt,
          tf.minimum(src_seq_len, self.src_token_max_len),
          tgt_seq_len,
          is_pre_sentence[:self.src_token_max_len]
        ),
        num_parallel_calls=self.num_parallel_calls)

    if self.source_reverse:
      dataset = dataset.map(
        lambda src, tgt, src_seq_len, tgt_seq_len, is_pre_sentence: (
          tf.reverse(src, axis=[0]),
          src_seq_len,
          tf.reverse(is_pre_sentence, axis=[0])),
        num_parallel_calls=self.num_parallel_calls
      )

    # Create fixed length token sequences for 2d convolution
    dataset = dataset.map(
      lambda src, *args: (src,
                          self.make_fixed_length_1d(src, self.token_cnn_tokens_per_sentence, self.eos),
                          *args),
      num_parallel_calls=self.num_parallel_calls
    )

    # Convert the word strings to ids. Word strings that are not in the
    # vocab get the lookup table's default_value integer.
    dataset = dataset.map(
      lambda src, src_fixed, tgt, *args: (
        tf.cast(self.src_token_vocab_table.lookup(src), tf.int32),
        tf.cast(self.src_token_vocab_table.lookup(src_fixed), tf.int32),
        *args),
      num_parallel_calls=self.num_parallel_calls)

    return dataset

  # Bucket by source sequence length (buckets for lengths 0-9, 10-19, ...)
  def batching_func_train(self, dataset):
    return dataset.padded_batch(
      self.batch_size,
      # Entries that are 1d vectors have shape [None]
      # Entries that are scalars have shape []
      padded_shapes=(
        tf.TensorShape([None]),  # src
        tf.TensorShape([None]),  # src_fixed
        tf.TensorShape([None]),  # tgt
        tf.TensorShape([]),  # src_seq_len
        tf.TensorShape([]),  # tgt_seq_len
        tf.TensorShape([None]),  # is_pre_sentence_tag
      ),
      # Pad the text sequence with eos tokens.
      # Pad ints with -1
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        self.src_token_eos_id,  # src
        self.src_token_eos_id,  # src_fixed
        self.tgt_eos_id,  # tgt
        0,  # src_seq_len
        0,  # tgt_seq_len
        1.0,  # is_pre_sentence_tag
      ))

  def batching_func_infer(self, dataset):
    return dataset.padded_batch(
      self.batch_size,
      # Entries that are 1d vectors have shape [None]
      # Entries that are scalars have shape []
      padded_shapes=(
        tf.TensorShape([None]),  # src
        tf.TensorShape([None]),  # src_fixed
        tf.TensorShape([]),  # src_seq_len
        tf.TensorShape([None]),  # is_pre_sentence_tag
        tf.TensorShape([])  # article_id
      ),
      # Pad the text sequence with eos tokens.
      # Pad ints with -1
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        self.src_token_eos_id,  # src
        self.src_token_eos_id,  # src_fixed
        self.tgt_eos_id,  # tgt
        0,  # src_seq_len
        0,  # tgt_seq_len
        1.0,  # is_pre_sentence_tag
        ""  # article_id
      ))

  def split_batching_func_train(self, dataset):
    return dataset.padded_batch(
      self.num_splits,
      # Entries with unkown-length vectors have shape [None, None]
      # Entries that are scalars have shape [None]
      padded_shapes=(
        tf.TensorShape([None, None]),  # src
        tf.TensorShape([None, None]),  # src_fixed
        tf.TensorShape([None, None]),  # tgt
        tf.TensorShape([None]),  # src_seq_len
        tf.TensorShape([None]),  # tgt_seq_len
        tf.TensorShape([None, None]),  # is_pre_sentence_tag
      ),
      # Pad the text sequence with eos tokens.
      # Pad ints with -1
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        self.src_token_eos_id,  # src
        self.src_token_eos_id,  # src_fixed
        self.tgt_eos_id,  # tgt
        0,  # src_seq_len
        0,  # tgt_seq_len
        1.0,  # is_pre_sentence_tag
      ))

  def split_batching_func_infer(self, dataset):
    return dataset.padded_batch(
      self.num_splits,
      # Entries with unkown-length vectors have shape [None, None]
      # Entries that are scalars have shape [None]
      padded_shapes=(
        tf.TensorShape([None, None]),  # src
        tf.TensorShape([None, None]),  # src_fixed
        tf.TensorShape([None]),  # src_seq_len
        tf.TensorShape([None, None]),  # is_pre_sentence_tag
        tf.TensorShape([None]),  # article_id
      ),
      # Pad the text sequence with eos tokens.
      # Pad ints with -1
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=(
        self.src_token_eos_id,  # src
        self.src_token_eos_id,  # src_fixed
        0,  # src_seq_len
        1.0,  # is_pre_sentence_tag
        ""  # article_id
      ))

  def bucket_batching(self, dataset, *args):
    def key_func_train(unused_1, unused_2, unused_3, seq_len, unused_4, unused_5):
      """
      Calculate bucket_width by maximum source sequence length.
      Sentences with length [0, bucket_width) go to bucket 0
                     length [bucket_width, 2 * bucket_width) go to bucket 1
                     ...
                     length over ((num_bucket-1) * bucket_width) words all go into the last bucket.
      """
      if self.src_token_max_len:
        bucket_width = (self.src_token_max_len + self.num_buckets - 1) // self.num_buckets
      else:
        bucket_width = self.fallback_bucket_width

      # Bucket sentence pairs by the length of their source sentence.
      bucket_id = seq_len // bucket_width
      return tf.to_int64(tf.minimum(self.num_buckets, bucket_id))

    def key_func_infer(unused_1, unused_2, seq_len, *unused):
      """
      Calculate bucket_width by maximum source sequence length.
      Sentences with length [0, bucket_width) go to bucket 0
                     length [bucket_width, 2 * bucket_width) go to bucket 1
                     ...
                     length over ((num_bucket-1) * bucket_width) words all go into the last bucket.
      """
      if self.src_token_max_len:
        bucket_width = (self.src_token_max_len + self.num_buckets - 1) // self.num_buckets
      else:
        bucket_width = self.fallback_bucket_width

      # Bucket sentence pairs by the length of their source sentence.
      bucket_id = seq_len // bucket_width
      return tf.to_int64(tf.minimum(self.num_buckets, bucket_id))

    def reduce_func(unused_key, windowed_data):
      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        return self.batching_func_train(windowed_data)
      else:
        return self.batching_func_infer(windowed_data)

    def key_func_split_train(unused_1, unused_2, unused_3, seq_len, unused_4, unused_5):

      if self.src_token_max_len:
        bucket_width = (self.src_token_max_len * self.batch_size + self.num_buckets - 1) // self.num_buckets
      else:
        bucket_width = self.fallback_bucket_width * self.batch_size

      # Bucket sentence pairs by the length of their source sentence and target
      # sentence.
      bucket_id = tf.reduce_sum(seq_len) // bucket_width
      return tf.to_int64(tf.minimum(self.num_buckets, bucket_id))

    def key_func_split_infer(unused_1, unused_2, seq_len, *unused):

      if self.src_token_max_len:
        bucket_width = (self.src_token_max_len * self.batch_size + self.num_buckets - 1) // self.num_buckets
      else:
        bucket_width = self.fallback_bucket_width * self.batch_size

      # Bucket sentence pairs by the length of their source sentence and target
      # sentence.
      bucket_id = tf.reduce_sum(seq_len) // bucket_width
      return tf.to_int64(tf.minimum(self.num_buckets, bucket_id))

    def reduce_func_split(unused_key, windowed_data):

      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        return self.split_batching_func_train(windowed_data)
      else:
        return self.split_batching_func_infer(windowed_data)

    if self.mode != tf.contrib.learn.ModeKeys.INFER:
      key_func = key_func_train
    else:
      key_func = key_func_infer

    batched_dataset = dataset.apply(
      tf.contrib.data.group_by_window(
        key_func=key_func, reduce_func=reduce_func, window_size=self.batch_size))

    if self.num_splits:

      if self.mode != tf.contrib.learn.ModeKeys.INFER:
        key_func_split = key_func_split_train
      else:
        key_func_split = key_func_split_infer

      batched_dataset = batched_dataset.apply(
        tf.contrib.data.group_by_window(
          key_func=key_func_split, reduce_func=reduce_func_split, window_size=self.num_splits))

    return batched_dataset

  def split_batched_input(self):

    if isinstance(self.__batched_data, BatchedInput):
      (source_cpts, source_cpts_fixed, target_cpts, source_sequence_lengths, target_sequence_lengths, pre_sent_tags) = (
        self.__batched_data.source_concepts,
        self.__batched_data.source_concepts_fixed,
        self.__batched_data.target_concepts,
        self.__batched_data.source_sequence_length,
        self.__batched_data.target_sequence_length,
        self.__batched_data.pre_sentence_tag)
    elif isinstance(self.__batched_data, InferBatchedInput):
      (source_cpts, source_cpts_fixed, source_sequence_lengths, pre_sent_tags, article_ids) = (
        self.__batched_data.source_concepts,
        self.__batched_data.source_concepts_fixed,
        self.__batched_data.source_sequence_length,
        self.__batched_data.pre_sentence_tag,
        self.__batched_data.article_id)
    else:
      raise ValueError("Unsupported batched tuple format for split_batched_input")

    # Remove rank from lists of scalar
    # ...

    split_batches = []

    # TODO: NOTICE!!
    # Some of the training ops expect the dimensionality of the time dimension of target_output, i.e. the labels, and
    # tf.reduce_max(target_sequence_length) to match.
    # This is not the case because of the padding created by the global tf.reduce_max(target_sequence_length)
    # (before the split)
    # We therefore clip/slice away the excess padding

    with tf.name_scope("split_batched_input"):
      def create_slicer(
          i):  # TODO why the nested functions? @Ludvig Because it might not be executed, see the tf.cond - Marhlder
        def create_slice():
          # Cut away excess padding
          source_sequence_length = source_sequence_lengths[i]

          max_source_length = tf.reduce_max(source_sequence_length)
          source_concepts = source_cpts[i]
          source_concepts = source_concepts[:, :max_source_length]

          pre_sent_tag = pre_sent_tags[i]
          pre_sent_tag = pre_sent_tag[:, :max_source_length]

          if isinstance(self.__batched_data, BatchedInput):
            target_sequence_length = target_sequence_lengths[i]
            max_tgt_length = tf.reduce_max(target_sequence_length)
            target_cpt = target_cpts[i]
            target_cpt = target_cpt[:, :max_tgt_length]

            split_batch = BatchedInput(
              source_concepts=source_concepts,
              source_concepts_fixed=source_cpts_fixed[i],
              target_concepts=target_cpt,
              source_sequence_length=source_sequence_length,
              target_sequence_length=target_sequence_length,
              pre_sentence_tag=pre_sent_tag
            )
          else:

            article_id = article_ids[i]

            split_batch = InferBatchedInput(
              source_concepts=source_concepts,
              source_concepts_fixed=source_cpts_fixed[i],
              source_sequence_length=source_sequence_length,
              pre_sentence_tag=pre_sent_tag,
              article_id=article_id
            )

          return split_batch

        return create_slice

      first_slice = create_slicer(0)()
      print(first_slice)
      with tf.device(tf.DeviceSpec(device_type="GPU", device_index=0)):

        if isinstance(first_slice, BatchedInput):
          split_batch = BatchedInput(
            source_concepts=tf.identity(first_slice.source_concepts),
            source_concepts_fixed=tf.identity(first_slice.source_concepts_fixed),
            target_concepts=tf.identity(first_slice.target_concepts),
            source_sequence_length=tf.identity(first_slice.source_sequence_length),
            target_sequence_length=tf.identity(first_slice.target_sequence_length),
            pre_sentence_tag=tf.identity(first_slice.pre_sentence_tag)
          )
        elif isinstance(first_slice, InferBatchedInput):
          split_batch = InferBatchedInput(
            source_concepts=tf.identity(first_slice.source_concepts),
            source_concepts_fixed=tf.identity(first_slice.source_concepts_fixed),
            source_sequence_length=tf.identity(first_slice.source_sequence_length),
            pre_sentence_tag=tf.identity(first_slice.pre_sentence_tag),
            article_id=tf.identity(first_slice.article_id)
          )
        else:
          raise ValueError("Unsupported batched tuple format for split_batched_input")

        split_batches.append(split_batch)

      def zeros_slice():
        if isinstance(first_slice, BatchedInput):
          return BatchedInput(
            source_concepts=first_slice.source_concepts,
            source_concepts_fixed=first_slice.source_concepts_fixed,
            target_concepts=first_slice.target_concepts,
            source_sequence_length=first_slice.source_sequence_length,
            target_sequence_length=first_slice.target_sequence_length,
            pre_sentence_tag=first_slice.pre_sentence_tag
          )
        elif isinstance(first_slice, InferBatchedInput):
          return InferBatchedInput(
            source_concepts=first_slice.source_concepts,
            source_concepts_fixed=first_slice.source_concepts_fixed,
            source_sequence_length=first_slice.source_sequence_length,
            pre_sentence_tag=first_slice.pre_sentence_tag,
            article_id=first_slice.article_id
          )
        else:
          raise ValueError("Unsupported batched tuple format for split_batched_input")

      available_batches_count = tf.shape(source_cpts)[0]

      for i in range(1, self.num_splits):
        split_batch = tf.cond(i < available_batches_count,
                              true_fn=create_slicer(i),
                              false_fn=zeros_slice)

        with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):

          if isinstance(split_batch, BatchedInput):
            split_batch = BatchedInput(
              source_concepts=tf.identity(split_batch.source_concepts),
              source_concepts_fixed=tf.identity(split_batch.source_concepts_fixed),
              target_concepts=tf.identity(split_batch.target_concepts),
              source_sequence_length=tf.identity(split_batch.source_sequence_length),
              target_sequence_length=tf.identity(split_batch.target_sequence_length),
              pre_sentence_tag=tf.identity(split_batch.pre_sentence_tag)
            )
          elif isinstance(first_slice, InferBatchedInput):
            split_batch = InferBatchedInput(
              source_concepts=tf.identity(split_batch.source_concepts),
              source_concepts_fixed=tf.identity(split_batch.source_concepts_fixed),
              source_sequence_length=tf.identity(split_batch.source_sequence_length),
              pre_sentence_tag=tf.identity(split_batch.pre_sentence_tag),
              article_id=tf.identity(split_batch.article_id)
            )
          else:
            raise ValueError("Unsupported batched tuple format for split_batched_input")

          split_batches.append(split_batch)

      print()
      print("Split batches: ", split_batches)

      return split_batches

class BatchedInput(collections.namedtuple("BatchedInput",
                                          ("source_concepts",
                                           "source_concepts_fixed",
                                           "target_concepts",
                                           "source_sequence_length",
                                           "target_sequence_length",
                                           "pre_sentence_tag"))):
  pass


class InferBatchedInput(collections.namedtuple("InferBatchedInput",
                                               ("source_concepts",
                                                "source_concepts_fixed",
                                                "source_sequence_length",
                                                "pre_sentence_tag",
                                                "article_id"))):
  pass
