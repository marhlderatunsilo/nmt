"""C2ML: Concepts-to-Multilabel-Classification model."""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
from tensorflow.python.layers import core as layers_core

from tfDeepNLP.layers.dense_with_scaling_factor import dense_layer_with_scaling_factor
from tfDeepNLP.models.classification.concepts2multilabel.c2ml_base_model import C2MLBaseModel
from tfDeepNLP.utils.evaluate_metrics_multilabel_classification import EvaluateMetricsMultilabelClassification

from tfDeepNLP.utils.misc_utils import cond_scope

__all__ = ["EditorKeywordsClassificationModel"]


class EditorKeywordsClassificationModel(C2MLBaseModel):
  """Article-concepts-to-multiple-labels dynamic model.
  """

  def __init__(self,
               hparams,
               mode,
               iterator,
               source_token_vocab_table,
               target_token_vocab_table,
               augment_embeddings=False,
               noise_amount_mean=None,
               noise_amount_stddev=None,
               scope=None,
               single_cell_fn=None,
               model_device_fn=None,
               optimizer_op=None,
               learning_rate=None,
               first_gpu_id=None
               ):
    super(EditorKeywordsClassificationModel, self).__init__(
      hparams=hparams,
      mode=mode,
      iterator=iterator,
      source_token_vocab_table=source_token_vocab_table,
      target_token_vocab_table=target_token_vocab_table,
      augment_embeddings=augment_embeddings,
      noise_amount_mean=noise_amount_mean,
      noise_amount_stddev=noise_amount_stddev,
      scope=scope,
      single_cell_fn=single_cell_fn,
      model_device_fn=model_device_fn,
      optimizer_op=optimizer_op,
      learning_rate=learning_rate,
      first_gpu_id=first_gpu_id
    )

  def _build_classifier(self, x=None, x2=None, hparams=None):
    with tf.variable_scope("build_classifier"):

      print("x:", x)
      print("x2:", x2)

      input_ = self._prepare_classifier_input(x=x, x2=x2)

      print("Begin building dense layers")

      fully_connected_layer_1 = layers_core.dense(
        inputs=input_,
        units=hparams.classifier_num_units,
        activation=tf.nn.tanh,
        use_bias=True,
        #sf_min=0.2, sf_max=2,
        name="fully_connected_layer_1",
      )

      dropout_fully_connected_layer_1 = tf.layers.dropout(fully_connected_layer_1, hparams.classifier_dropout)

      logits = layers_core.dense(
        inputs=dropout_fully_connected_layer_1,
        units=hparams.tgt_token_vocab_size,
        activation=None,
        use_bias=True,
        name="logits",
      )

      return logits

  def _build_softmax_with_loss(self, x, hparams, scope=None):
    raise NotImplemented()
    # with cond_scope(scope):
    #
    #   print("x:", x)
    #
    #   logits = layers_core.dense(
    #     inputs=x,
    #     units=hparams.num_classes,
    #     activation=None,
    #     use_bias=True,
    #     name="logits",
    #   )
    #
    #   predictions, probabilities = self.extract_preds_and_probs(logits)
    #   ensemble_evaluator = EvaluateMetricsMultilabelClassification(self.targets, predictions,
    #                                                                probabilities)
    #
    #   if self.mode != tf.contrib.learn.ModeKeys.INFER:
    #     loss = self.setup_loss_computation(hparams, logits, self.targets, multilabel=True)
    #
    #     return probabilities, logits, loss, ensemble_evaluator
    #   else:
    #     return probabilities, logits, 0.0, None

