import tensorflow as tf

from tfDeepNLP.metrics import tf_like_running_metrics
from tfDeepNLP.model_base.base_model import BaseModel
from tfDeepNLP.utils.gradient_utils import gradient_clip
from tfDeepNLP.utils.initializer_utils import get_initializer
from tfDeepNLP.utils.vocab_utils import create_python_vocab_table

class Sen2ClassBaseModel(BaseModel):
  def __init__(self,
               hparams,
               single_cell_fn=None,
               ):
    super(Sen2ClassBaseModel, self).__init__(hparams,
                                       single_cell_fn,
                                       )
    self.tag2idx = create_python_vocab_table(self.hparams.tag_vocab_file)

  def __call__(self, features, labels, mode, params):

    hparams = params

    # Initializer
    initializer = get_initializer(
      hparams.init_op, hparams.random_seed, hparams.init_weight)
    tf.get_variable_scope().set_initializer(initializer)

    ## Create the graph, the actual mode parameter decides exactly how (e.g. no dropout for inference.. )
    train_loss, labels_pred, labels_logits = self.build_graph(hparams, features, labels, mode)

    labels_logits = tf.nn.softmax(labels_logits)

    if mode == tf.estimator.ModeKeys.PREDICT:
      return self._predict(mode, labels_pred, labels_logits)
    elif mode == tf.estimator.ModeKeys.TRAIN:
      return self._train(labels, mode, hparams, train_loss, labels_pred)
    elif mode == tf.estimator.ModeKeys.EVAL:
      return self._eval(labels, mode, hparams, train_loss, labels_pred)

  def _train(self, labels, mode, hparams, train_loss, labels_pred):

    labels = tf.one_hot(labels['labels'], depth=self.hparams.tags_vocab_size, dtype=tf.int32)
    labels_pred = tf.one_hot(labels_pred, depth=self.hparams.tags_vocab_size, dtype=tf.int32)

    optimizer_op, learning_rate = self.create_optimizer(hparams, mode, batch_size=tf.shape(labels)[0])

    gradients = self.setup_gradient_computation(hparams, train_loss, optimizer_op, learning_rate)
    _global_step = tf.train.get_or_create_global_step()
    train_op = optimizer_op.apply_gradients(gradients, global_step=_global_step)
    train_op = tf.group(train_op)

    train_cnt = {}
    for k in self.tag2idx.keys():
      train_cnt[k] = tf.get_variable('train_cnt_' + k, initializer=tf.zeros_initializer(), trainable=False, shape=[],
                                     dtype=tf.int64)

    summary_metrics = {}
    _s = len(self.tag2idx)
    prefix = "training_per_batch_"
    for k, v in self.tag2idx.items():
      # summary_metrics['train_cnt_%s' % k] = tf.assign(train_cnt[k],
      #                                                 train_cnt[k] + tf.count_nonzero(tf.reshape(labels, shape=[-1, _s])[:, v]))

      summary_metrics[prefix + 'precision_%s' % k] = self.create_reset_metric(tf.metrics.precision,
                                                                              labels=tf.reshape(labels, shape=[-1,_s])[:, v],
                                                                              predictions=tf.reshape(labels_pred, shape=[-1,_s])[:, v])
      summary_metrics[prefix + 'recall_%s' % k] = self.create_reset_metric(tf.metrics.recall,
                                                                           labels=tf.reshape(labels, shape=[-1, _s])[:, v],
                                                                           predictions=tf.reshape(labels_pred, shape=[-1, _s])[:, v])

    summary_metrics[prefix + 'accuracy'] = self.create_reset_metric(tf.metrics.accuracy, labels=labels, predictions=labels_pred)
    summary_metrics[prefix + 'precision'] = self.create_reset_metric(tf.metrics.precision, labels=labels, predictions=labels_pred)
    summary_metrics[prefix + 'recall'] = self.create_reset_metric(tf.metrics.recall, labels=labels, predictions=labels_pred)

    metric_vars = tf.contrib.framework.get_variables(
      None, collection=tf.GraphKeys.METRIC_VARIABLES)
    reset_metrics_op = tf.variables_initializer(metric_vars)

    with tf.control_dependencies([reset_metrics_op]):
      for metric_name, metric_value in summary_metrics.items():
        tf.summary.scalar(metric_name, metric_value)
      f1 = 2 * summary_metrics[prefix + 'precision'] * summary_metrics[prefix + 'recall'] / (summary_metrics[prefix + 'precision'] + summary_metrics[prefix + 'recall'])
      tf.summary.scalar(prefix + "f1", f1)


    training_hooks = []

    return tf.estimator.EstimatorSpec(mode,
                                      loss=train_loss,
                                      train_op=train_op,
                                      training_hooks=training_hooks
                                      )

  def _eval(self, labels, mode, hparams, train_loss, labels_pred):

    labels = tf.one_hot(labels['labels'], depth=self.hparams.tags_vocab_size, dtype=tf.int32)
    labels_pred = tf.one_hot(labels_pred, depth=self.hparams.tags_vocab_size, dtype=tf.int32)

    eval_metrics = self.get_metrics(labels, labels_pred, "eval_metrics", "dev_")
    return tf.estimator.EstimatorSpec(mode,
                                      loss=train_loss,
                                      eval_metric_ops=eval_metrics,
                                      )

  def _predict(self, mode, labels_pred, labels_logits):

    labels_pred = tf.one_hot(labels_pred, depth=self.hparams.tags_vocab_size, dtype=tf.int32)

    predictions = {
      'labels_pred': labels_pred,
      'labels_logits': labels_logits,
    }

    export_outputs = {
      tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: tf.estimator.export.PredictOutput(labels_pred),
      'labels_logits': tf.estimator.export.PredictOutput(labels_logits),
    }

    return tf.estimator.EstimatorSpec(mode, predictions=predictions, export_outputs=export_outputs)

  def setup_gradient_computation(self, hparams, train_loss, optimizer_op, learning_rate, **kwargs):

    # Gradients and update operation for training the model.
    # Arrange for the embedding vars to appear at the beginning.

    gradients = optimizer_op.compute_gradients(
      train_loss,
      colocate_gradients_with_ops=hparams.colocate_gradients_with_ops,
    )

    if self.clip_gradients:
      gradients, self.params = zip(*gradients)
      self.clipped_gradients, gradient_norm_summary = gradient_clip(
        gradients, max_gradient_norm=hparams.max_gradient_norm)
      gradients = zip(self.clipped_gradients, self.params)
      self.train_summary = tf.summary.merge([tf.summary.scalar("lr", learning_rate)] + gradient_norm_summary)
    else:
      self.train_summary = tf.summary.merge([
        tf.summary.scalar("lr", learning_rate),
      ])

    return gradients

  def statistics(self):
    raise NotImplemented()

  def get_metrics(self, labels, labels_pred, sp, prefix):

    with tf.variable_scope(sp):
      precision_metric = tf.metrics.precision(labels=labels, predictions=labels_pred)
      recall_metric = tf.metrics.recall(labels=labels, predictions=labels_pred)
      accuracy_metric = tf.metrics.accuracy(labels=labels, predictions=labels_pred)
      f1_metric = tf_like_running_metrics.f1(precision_metric, recall_metric)

      eval_metrics = {
        prefix + "precision": precision_metric,
        prefix + "accuracy": accuracy_metric,
        prefix + "recall": recall_metric,
        prefix + 'f1': f1_metric,
      }
      _s = len(self.tag2idx)
      for k, v in self.tag2idx.items():
          eval_metrics[prefix + 'precision_%s' % k] = tf.metrics.precision(
              labels=tf.reshape(labels, shape=[-1, _s])[:, v],
              predictions=tf.reshape(labels_pred, shape=[-1, _s])[:, v]
          )
          eval_metrics[prefix + 'recall_%s' % k] = tf.metrics.recall(
              labels=tf.reshape(labels, shape=[-1, _s])[:, v],
              predictions=tf.reshape(labels_pred, shape=[-1, _s])[:, v]
          )
      return eval_metrics

  def create_reset_metric(self, metric, scope='reset_metrics', **metric_args):
    with tf.variable_scope(scope) as scope:
      metric_op, update_op = metric(**metric_args)
      vars = tf.contrib.framework.get_variables(
        scope, collection=tf.GraphKeys.LOCAL_VARIABLES)

      for var in vars:
        tf.add_to_collection(tf.GraphKeys.METRIC_VARIABLES, var)

    return update_op
