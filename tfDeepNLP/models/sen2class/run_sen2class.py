"""S2T: TensorFlow NER model implementation."""
import argparse
import os
import random

import numpy as np
import tensorflow as tf
import yaml

from prepare_comparison_with_pipeline_scoring import prepare_comparison_with_pipeline_scoring
from tfDeepNLP.models.sen2class import train_using_estimator
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils import vocab_utils
from tfDeepNLP.utils.common import YParams

utils.check_tensorflow_version()


def main(argv):
  train_fn = train_using_estimator.sen2class_train
  random_seed = hparams.random_seed
  if hparams.random_seed >= 0:
    utils.print_out("# Set random seed to %d" % random_seed)
    random.seed(random_seed)
    np.random.seed(random_seed)

  if hparams.task == "prepare_comparison":
    prepare_comparison_with_pipeline_scoring(hparams)
  else:
    # Default - Train the model
    train_fn(hparams)


def extend_hparams(hparams):
  # We copy the vocabs to outdir (above) and save the new paths (here) to ensure consistency

  if hasattr(hparams, 'token_vocab_train_dev'):
    token_vocab_train_dev_size, token_vocab_train_dev_file = vocab_utils.check_and_copy_vocab(
      os.path.join(hparams.work_dir, hparams.token_vocab_train_dev),
      hparams.out_dir,
      sos=hparams.sos,
      eos=hparams.eos,
      unk=hparams.unk,
      check_special_chars=False)

    hparams.add_hparam("token_vocab_train_dev_file", token_vocab_train_dev_file)
    hparams.add_hparam("token_vocab_train_dev_size", token_vocab_train_dev_size)

  if hasattr(hparams, 'token_vocab_inference'):
    token_vocab_inference_size, token_vocab_inference_file = vocab_utils.check_and_copy_vocab(
      os.path.join(hparams.work_dir, hparams.token_vocab_inference),
      hparams.out_dir,
      sos=hparams.sos,
      eos=hparams.eos,
      unk=hparams.unk,
      check_special_chars=False)

    hparams.add_hparam("token_vocab_inference_file", token_vocab_inference_file)
    hparams.add_hparam("token_vocab_inference_size", token_vocab_inference_size)

  if hasattr(hparams, 'char_vocab'):
    char_vocab_size, char_vocab_file = vocab_utils.check_and_copy_vocab(
      os.path.join(hparams.work_dir, hparams.char_vocab),
      hparams.out_dir,
      sos=hparams.sos,
      eos=hparams.eos,
      unk=hparams.unk,
      check_special_chars=False)

    hparams.add_hparam("char_vocab_file", char_vocab_file)
    hparams.add_hparam("char_vocab_size", char_vocab_size)

  if hasattr(hparams, 'tag_vocab'):
    tags_vocab_size, tag_vocab_file = vocab_utils.check_and_copy_vocab(
      os.path.join(hparams.work_dir, hparams.tag_vocab),
      hparams.out_dir,
      unk=vocab_utils.UNK,
      check_special_chars=False)

    hparams.add_hparam("tag_vocab_file", tag_vocab_file)
    hparams.add_hparam("tags_vocab_size", tags_vocab_size)

  return hparams


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='my model')
  parser.add_argument('--yaml_config_file', type=str,
                      default=os.path.join(os.getcwd(),
                                           'tfDeepNLP/models/sen2class/standard_hparams/metadata_run_config.yaml'),
                      help="out_dir and work_dir should be given here!!")
  parser.add_argument('--yaml_config_name', type=str, default='base')

  parser.add_argument('--yaml_param_file', type=str,
                      default=os.path.join(os.getcwd(),
                                           'tfDeepNLP/models/sen2class/standard_hparams/metadata_hparam.yaml'))
  parser.add_argument('--yaml_param_name', type=str, default='base')

  parser.add_argument('--task', type=str, default='train',
                      choices=('train','prepare_comparison'))

  parser.add_argument("--xls_dir", default='D:/SME feedback sheets')
  parser.add_argument("--output_file_name", default="SME_feedback_sentences.txt")
  parser.add_argument("--mode", default="XML", choices=["XML", "JSON"])

  args = parser.parse_args()

  hparams = YParams(args.yaml_config_file, args.yaml_config_name)

  existing_hparams_path = os.path.join(hparams.out_dir, "hparam.yaml")
  if not tf.gfile.Exists(hparams.out_dir):
    tf.gfile.MakeDirs(hparams.out_dir)

  if tf.gfile.Exists(existing_hparams_path):
    print("load hparam from ", existing_hparams_path)
    hparams = YParams(existing_hparams_path)
  else:
    hparams.update_param(YParams(args.yaml_param_file, args.yaml_param_name))
    print("save hparam to ", existing_hparams_path)
    with open(existing_hparams_path, 'w') as fout:
      yaml.dump({k: v for k, v in vars(hparams).items() if not k.startswith('_')}, fout, default_flow_style=False)

  hparams.update_param(args, ignore_keywords=['yaml'])
  hparams = extend_hparams(hparams)

  tf.app.run()
