import re


class IOBDataset(object):
  def __init__(self, filename, token_vocab, tags_vocab, collapse_tag_vocab=False):
    self.filename = filename
    self.token_vocab = token_vocab
    self.tags_vocab = tags_vocab

    self.collapse = collapse_tag_vocab
    self.tokens_and_tags_samples = list(self.load_iob_samples(self.filename))

  def __iter__(self):
    for tokens_and_tags in self.tokens_and_tags_samples:
      tokens, tags = zip(*tokens_and_tags)
      yield tags, tokens

  def __len__(self):
    """
    Iterates once over the corpus to set and store length
    """
    if self.length is None:
      self.length = len(self.tokens_and_tags_samples)
    return self.length

  def load_iob_samples(self, filename):
    """
    Load sentences. A line must contain at least a word and its tag.
    """
    start_sample_regex = re.compile("\\s*###\\s*\d{8}[a-zA-Z]")

    tokens_and_tags_sample = None
    with open(filename, encoding='utf-8') as f_in:
      for line in f_in:
        if len(line) == 0 or not line.strip():
          continue
        if start_sample_regex.match(line):
          if tokens_and_tags_sample and len(tokens_and_tags_sample) > 0:
            yield tokens_and_tags_sample
          tokens_and_tags_sample = []
        else:
          line_components = line.strip().split("\t")

          if len(line_components) == 3 and line_components[0] == line_components[1]:
            continue

          assert len(line_components) == 4

          token = line_components[0]
          tag = line_components[3][1:]

          if self.collapse:
            if tag == "O" or tag == "NO CLASS":
              tag = self.tags_vocab["O"]
            else:
              tag = self.tags_vocab["CNER"]
          else:
            tag = self.tags_vocab[tag]
          tokens_and_tags_sample.append( (token,tag) )

    if len(tokens_and_tags_sample) > 0:
      yield tokens_and_tags_sample