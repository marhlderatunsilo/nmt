import tensorflow as tf

from tfDeepNLP.gazetteers.gazetteers_lookup import lookup_gazetteers

__all__ = ["get_dataset"]


def bucket_batch_dataset(hparams, src_tgt_dataset, batch_size, num_buckets, src_max_len, padding_token_id,
                         padding_char_id,
                         padding_tag_id):
  # Bucket by source sequence length (buckets for lengths 0-9, 10-19, ...)
  def batching_func(x):

    # @formatter:off

    padded_shapes_list = [
      tf.TensorShape([None]),                       # tokens
      tf.TensorShape([]),                           # tokens_length
      tf.TensorShape([None]),                       # token_ids
      tf.TensorShape([None, None]),                 # char_ids
      tf.TensorShape([None]),                       # chars_sequence_length
      tf.TensorShape([None])                        # labels
    ]

    padding_values_list = [
        "",                             # tokens
        0,                              # tokens_length
        padding_token_id,               # token_ids
        padding_char_id,                # char_ids
        0,                              # chars_sequence_length
        padding_tag_id                  # Labels
    ]

    if hparams.gazetteers:
      gazetteers_shape = x.output_shapes[6]

      # print("gazetteers_shape: ", gazetteers_shape)

      padded_shapes_list.append(tf.TensorShape([None, gazetteers_shape[1]])) # gazetteers
      padding_values_list.append(0.0) # gazetteers

    return x.padded_batch(
      batch_size,
      padded_shapes=tuple(padded_shapes_list),
      # Pad the source and target sequences.
      # (Though notice we don't generally need to do this since
      # later on we will be masking out calculations past the true sequence.
      padding_values=tuple(padding_values_list)
    )
  # @formatter:on

  def key_func(unused_1, tokens_length, *unused):
    # Calculate bucket_width by maximum source sequence length.
    # Pairs with length [0, bucket_width) go to bucket 0, length
    # [bucket_width, 2 * bucket_width) go to bucket 1, etc.  Pairs with length
    # over ((num_bucket-1) * bucket_width) words all go into the last bucket.
    if src_max_len:
      bucket_width = (src_max_len + num_buckets - 1) // num_buckets
    else:
      bucket_width = 10

    # Bucket sentence pairs by the length of their source sentence and target
    # sentence.

    bucket_id = tokens_length // bucket_width
    return tf.to_int64(tf.minimum(num_buckets, bucket_id))

  def reduce_func(unused_key, windowed_data):
    return batching_func(windowed_data)

  if num_buckets > 1:

    batched_dataset = src_tgt_dataset.apply(
      tf.contrib.data.group_by_window(
        key_func=key_func, reduce_func=reduce_func, window_size=batch_size))

  else:
    batched_dataset = batching_func(src_tgt_dataset)

  # if num_splits:
  #   batched_dataset = batched_dataset.apply(
  #     tf.contrib.data.group_by_window(
  #       key_func=key_func_split, reduce_func=reduce_func_split, window_size=num_splits))

  return batched_dataset


def create_unpack_tf_record_fn(hparams):
  # We need the gazetteers tensor to know its static shape
  # gazetteer_feature_files = [os.path.join(hparams.work_dir, i) for i in hparams.gazetteer_feature_files]

  if hparams.gazetteers:
    look_up_mock = lookup_gazetteers(hparams.gazetteer_feature_files, ["mock_token"])

  # print("look_up_mock: ", look_up_mock)

  def unpack_tf_record(content):
    # Write to records

    keys_to_features_mapping = {
      'words': tf.VarLenFeature(tf.string),
      'word_ids_len': tf.FixedLenFeature((), tf.int64),
      'word_ids': tf.VarLenFeature(tf.int64),
      'char_ids': tf.VarLenFeature(tf.int64),
      'char_ids_shape': tf.FixedLenFeature((2), tf.int64),
      'char_ids_len': tf.VarLenFeature(tf.int64),
      'labels': tf.VarLenFeature(tf.int64),
    }

    if hparams.gazetteers:
      keys_to_features_mapping['gazetteers'] = tf.VarLenFeature(dtype=tf.float32)
      keys_to_features_mapping['gazetteers_shape'] = tf.FixedLenFeature((2), tf.int64)

    parsed_features = tf.parse_single_example(content, keys_to_features_mapping)

    tokens = parsed_features['words'].values
    tokens_length = tf.cast(parsed_features['word_ids_len'], tf.int32)
    token_ids = tf.cast(parsed_features['word_ids'], tf.int32).values
    char_ids = parsed_features['char_ids'].values
    char_ids = tf.cast(tf.reshape(char_ids, shape=parsed_features['char_ids_shape']), tf.int32)
    chars_sequence_length = tf.cast(parsed_features['char_ids_len'].values, tf.int32)

    labels = tf.cast(parsed_features['labels'].values, tf.int32)

    if hparams.gazetteers:
      gazetteers = tf.reshape(parsed_features['gazetteers'].values,
                              shape=parsed_features['gazetteers_shape'])  # [-1, 18]
      gazetteers.set_shape([None, look_up_mock.get_shape()[-1]])
      return tokens, tokens_length, token_ids, char_ids, chars_sequence_length, labels, gazetteers
    else:
      return tokens, tokens_length, token_ids, char_ids, chars_sequence_length, labels

  return unpack_tf_record


def get_dataset(hparams,
                dataset,
                py_token_vocab_table,
                py_char_vocab_table,
                py_tag_vocab_table,
                batch_size,
                unk,
                num_buckets,
                shuffle=True,
                repeat=True,
                random_seed=None,
                src_max_len=None,
                num_threads=32,
                output_buffer_size=None,
                limit_sample_count_to=-1):
  if not output_buffer_size:
    output_buffer_size = batch_size * 1000
  padding_token_id = py_token_vocab_table[unk]  # tf.cast(src_vocab_table.lookup(tf.constant(eos)), tf.int32)
  padding_char_id = py_char_vocab_table[unk]  # tf.cast(tgt_vocab_table.lookup(tf.constant(eos)), tf.int32)
  padding_tag_id = py_tag_vocab_table["O"]

  if limit_sample_count_to > 0:
    dataset = dataset.take(limit_sample_count_to)

  if shuffle and repeat:
    dataset = dataset.apply(tf.contrib.data.shuffle_and_repeat(output_buffer_size, seed=random_seed))
  elif shuffle:
    dataset = dataset.shuffle(output_buffer_size, seed=random_seed)
  elif repeat:
    dataset = dataset.repeat()

  unpack_tf_record = create_unpack_tf_record_fn(hparams)
  dataset = dataset.map(unpack_tf_record, num_parallel_calls=num_threads)

  batched_dataset = bucket_batch_dataset(hparams, dataset, batch_size, num_buckets, src_max_len,
                                         padding_token_id,
                                         padding_char_id,
                                         padding_tag_id)

  # def test_batching(tokens, tokens_length, token_ids, char_ids, chars_sequence_length, gazetteers, labels):
  #
  #   tokens_length = tf.Print(tokens_length, [tf.shape(token_ids), tf.shape(labels), tokens_length], message="Dyld test: ", summarize=2000)
  #
  #   return tokens, tokens_length, token_ids, char_ids, chars_sequence_length, gazetteers, labels
  #
  # batched_dataset = batched_dataset.map(test_batching)

  # Make the tuple shape fit requirements of estimator
  def map_tuple_shape(tokens, tokens_length, token_ids, char_ids, chars_sequence_length, labels, gazetteers=None):

    features_mapping = {
      "tokens": tokens,
      "tokens_length": tokens_length,
      "token_ids": token_ids,
      "char_ids": char_ids,
      "chars_sequence_length": chars_sequence_length,
    }

    if hparams.gazetteers and gazetteers is not None:
      features_mapping["gazetteers"] = gazetteers

    labels_mapping = {
      "labels": labels
    }

    return (features_mapping, labels_mapping)

  batched_dataset = batched_dataset.map(map_tuple_shape, num_parallel_calls=num_threads)

  return batched_dataset
