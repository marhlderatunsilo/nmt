from __future__ import print_function

import os
import time

import numpy as np
import tensorflow as tf

from csv_dataset import csv_dataset
from tfDeepNLP.models.sen2class.universal_sentence_encoder_linear_model import \
  UnivervalsSentenceEncoderSentenceClassifier
from tfDeepNLP.training.estimator import train_estimator
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.vocab_utils import create_python_vocab_table, create_vocab_table

utils.check_tensorflow_version()

__all__ = [
  "sen2class_train"
]


def sen2class_train(hparams, log_utils=False):
  """
  Model settings
  """
  # deprecated
  if log_utils:
    log_file = os.path.join(hparams.out_dir, "log_%d" % time.time())
    log_f = tf.gfile.GFile(log_file, mode="a")
    utils.print_out("# log_file=%s" % log_file, log_f)

  # Select model
  model_creator = UnivervalsSentenceEncoderSentenceClassifier

  """
  Get ready for training! 
  """

  tag_vocab_file = hparams.tag_vocab_file

  py_tags_vocab = create_python_vocab_table(tag_vocab_file)

  model = model_creator(
    hparams,
    ntags=len(py_tags_vocab))

  def create_embeddings_loader(embeddings_path):
    def load_embeddings(hook_obj, session, coord):
      print("Assigning Embeddings")
      # Assign embeddings
      # We only need to do this for the first model because of the way they share parameters

      with np.load(embeddings_path) as data:
        pretrained_embeddings = data["embeddings"]

      print("shape: ", pretrained_embeddings.shape)
      print("model.embeddings_placeholder: ", hook_obj.embeddings_placeholder)

      session.run(hook_obj.assign_embeddings, {
        hook_obj.embeddings_placeholder: pretrained_embeddings})

    return load_embeddings

  # Hooks for the estimator to call during training
  train_hooks = []

  # Hooks for the estimator to use during evalutation of the development data set
  dev_hooks = []

  # Get file names of tfrecords files
  train_records_names = [os.path.join(hparams.work_dir, prefix + '.txt') for prefix in
                         hparams.train_prefix.split(",")]
  dev_records_names = [os.path.join(hparams.work_dir, hparams.dev_prefix + '.txt')]

  def map_tags_to_indexes(csv_dataset):
    tags_vocab = create_vocab_table(tag_vocab_file)

    def mapping_fn(features, labels):
      labels["labels"] = tags_vocab.lookup(labels["labels"])
      return (features, labels)

    return csv_dataset.map(mapping_fn)

  def train_input_fn():
    with tf.device(tf.DeviceSpec(device_type="CPU", device_index=0)):
      dataset = csv_dataset(train_records_names,
                            ["sentence", "labels"],
                            {"sentence"}, {"labels"}, hparams.batch_size)

      dataset = map_tags_to_indexes(dataset)

    return dataset

  def dev_input_fn():
    dataset = csv_dataset(dev_records_names,
                          ["sentence", "labels"],
                          {"sentence"}, {"labels"}, hparams.batch_size, shuffle=False)

    dataset = map_tags_to_indexes(dataset)

    return dataset

  def serving_input_receiver_fn():
    """An input receiver that expects a serialized tf.Example."""
    input_sentences_placeholder = tf.placeholder(dtype=tf.string,
                                                 shape=[None],
                                                 name='input_sentences_placeholder')
    features = {
      "sentences": input_sentences_placeholder,
    }

    return tf.estimator.export.ServingInputReceiver(features, features.copy())

  train_estimator(hparams, train_input_fn=train_input_fn,
                  train_hooks=train_hooks,
                  dev_input_fn=dev_input_fn,
                  dev_hooks=dev_hooks,
                  infer_input_fn=dev_input_fn,
                  infer_hooks=dev_hooks,
                  model_fn=model,
                  serving_input_receiver_fn=serving_input_receiver_fn)
