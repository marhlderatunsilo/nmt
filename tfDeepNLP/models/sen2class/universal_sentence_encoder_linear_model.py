"""lstm model for NER"""
from __future__ import absolute_import

import tensorflow as tf
import tensorflow_hub as hub
from tensorflow.python.layers import core as layers_core

from tfDeepNLP.models.sen2class.sen2class_base_model import Sen2ClassBaseModel


class UnivervalsSentenceEncoderSentenceClassifier(Sen2ClassBaseModel):

  def __init__(self, hparams,
               ntags,
               dataset=None,
               is_inference=False):

    self.ntags = ntags
    self.debug = False
    self.train_loss = None
    self.dataset = dataset
    self.is_inference = is_inference

    super(UnivervalsSentenceEncoderSentenceClassifier, self).__init__(hparams)

  def add_sentence_representations(self, mode, sentences):

    """
    Adds word embeddings to self
    """

    embed = hub.Module("https://tfhub.dev/google/universal-sentence-encoder-large/3")
    embeddings = embed(sentences)

    return embeddings

  def add_logits(self, hparams, mode, sentence_representations):

    for layer_index in range(0, hparams.num_layers):
      with tf.variable_scope("layers", reuse=tf.AUTO_REUSE):
        layer = layers_core.Dense(
          sentence_representations.get_shape()[-1], use_bias=True, name="layer", activation=tf.nn.tanh)

      sentence_representations = layer(sentence_representations) + sentence_representations
      sentence_representations = tf.contrib.layers.layer_norm(sentence_representations)

    with tf.variable_scope("proj", reuse=tf.AUTO_REUSE):
      output_layer = layers_core.Dense(
        self.ntags, use_bias=True, name="output_projection", activation=None
      )

      logits = output_layer(sentence_representations)

    return logits

  def add_inference_op(self, logits):
    with tf.variable_scope("inference", reuse=tf.AUTO_REUSE):
      labels_pred = tf.cast(tf.argmax(logits, axis=-1), tf.int32, name="predicted_labels")

      return labels_pred, logits

  def setup_loss_computation(self, logits, labels, reuse=tf.AUTO_REUSE, **kwargs):

    with tf.variable_scope("loss", reuse=reuse):
      losses = tf.losses.softmax_cross_entropy(logits=logits, onehot_labels=tf.one_hot(labels, depth=self.ntags),
                                               label_smoothing=0.1,
                                               reduction=tf.losses.Reduction.MEAN)

    return losses

  def add_summary(self, sess):
    self.statistics = tf.summary.merge_all()

  def build_graph(self, hparams, features, labels, mode, **kwargs):

    sentences = features["sentence"]

    if labels:
      labels = labels["labels"]

    sentence_representations = self.add_sentence_representations(mode, sentences)  # gazetteers

    logits = self.add_logits(hparams, mode, sentence_representations)

    labels_pred, labels_logits = self.add_inference_op(logits)

    if not mode == tf.estimator.ModeKeys.PREDICT:
      train_loss = self.setup_loss_computation(logits, labels)

      if hparams.get('l2_scale', 0) > 0 and "super_convergence" not in hparams.optimizer:
        l2_loss = hparams.l2_scale * sum(tf.nn.l2_loss(tf_var) for tf_var in
                                         tf.trainable_variables())

        train_loss = train_loss + l2_loss

    else:
      train_loss = None

    return train_loss, labels_pred, labels_logits
