from __future__ import print_function

import os
import time

import numpy as np
import tensorflow as tf

from tfDeepNLP.gazetteers.gazetteers_lookup import lookup_gazetteers
from tfDeepNLP.models.seq2tag.dataset_iterator.ner_iterator import get_iterator
from tfDeepNLP.models.seq2tag.dataset_iterator.tf_records_dataset import get_dataset
from tfDeepNLP.models.seq2tag.sequence_tagging_model import SequenceTaggingModel
from tfDeepNLP.training.estimator import train_estimator
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.embeddings_utils.init_embeddings_hook import AssignEmbeddingsFnHook
from tfDeepNLP.utils.vocab_utils import create_python_vocab_table, create_vocab_table

utils.check_tensorflow_version()

__all__ = [
  "s2t_train"
]

def s2t_train(hparams,
              single_cell_fn=None, log_utils=False):
  """
  Model settings
  """
  # deprecated
  if log_utils:
    log_file = os.path.join(hparams.out_dir, "log_%d" % time.time())
    log_f = tf.gfile.GFile(log_file, mode="a")
    utils.print_out("# log_file=%s" % log_file, log_f)

  # Select model
  model_creator = SequenceTaggingModel

  """
  Get ready for training! 
  """

  token_vocab_train_dev_file = hparams.token_vocab_train_dev_file
  token_vocab_inference_file = hparams.token_vocab_inference_file
  character_level_vocab_file = hparams.char_vocab_file
  tag_vocab_file = hparams.tag_vocab_file

  py_train_dev_token_vocab = create_python_vocab_table(token_vocab_train_dev_file)
  py_inference_token_vocab = create_python_vocab_table(token_vocab_inference_file)
  py_character_level_vocab = create_python_vocab_table(character_level_vocab_file)
  py_tags_vocab = create_python_vocab_table(tag_vocab_file)

  model = model_creator(
    hparams,
    py_train_dev_token_vocab,
    py_inference_token_vocab,
    py_tags_vocab,
    py_character_level_vocab,
    unk_word_idx=py_train_dev_token_vocab["$UNK$"],
    unk_char_idx=py_character_level_vocab["$UNK$"],
    ntags=len(py_tags_vocab),
    single_cell_fn=single_cell_fn)

  def create_embeddings_loader(embeddings_path):
    def load_embeddings(hook_obj, session, coord):
      print("Assigning Embeddings")
      # Assign embeddings
      # We only need to do this for the first model because of the way they share parameters

      with np.load(embeddings_path) as data:
        pretrained_embeddings = data["embeddings"]

      print("shape: ", pretrained_embeddings.shape)
      print("model.embeddings_placeholder: ", hook_obj.embeddings_placeholder)

      session.run(hook_obj.assign_embeddings, {
        hook_obj.embeddings_placeholder: pretrained_embeddings})

    return load_embeddings

  # Hooks for the estimator to call during training
  train_hooks = []

  # Add embeddings assigment hook if an embeddings file was given in hparams
  if hparams.embeddings_filename:
    train_hooks.append(
      AssignEmbeddingsFnHook(hparams, assign_embeddings_fn=create_embeddings_loader(hparams.embeddings_filename),
                             vocab_size=len(py_train_dev_token_vocab),
                             target_embeddings_variable_getter=lambda: model._token_embeddings))

  # Hooks for the estimator to use during evalutation of the development data set
  dev_hooks = []

  # Get file names of tfrecords files
  train_records_names = [os.path.join(hparams.work_dir, prefix + '_seq2tag.tfrecords') for prefix in
                         hparams.train_prefix.split(",")]
  dev_records_names = [os.path.join(hparams.work_dir, hparams.dev_prefix + '_seq2tag.tfrecords')]

  def train_input_fn():
    with tf.device(tf.DeviceSpec(device_type="CPU", device_index=0)):
      dataset = tf.data.TFRecordDataset(train_records_names)

      dataset = get_dataset(hparams,
                            dataset,  # feature_dataset, label_dataset
                            py_train_dev_token_vocab,
                            py_character_level_vocab,
                            py_tags_vocab,
                            hparams.batch_size,
                            hparams.unk,
                            hparams.num_buckets,
                            shuffle=hparams.shuffle_training,
                            repeat=False,
                            random_seed=None,
                            src_max_len=None,
                            num_threads=hparams.parallel_iterations,
                            limit_sample_count_to=hparams.train_set_size_limit)
    print("input_fn dataset: ", dataset)

    return dataset

  def dev_input_fn():

    with tf.device(tf.DeviceSpec(device_type="CPU", device_index=0)):
      dataset = tf.data.TFRecordDataset(dev_records_names)
      dataset = get_dataset(hparams,
                            dataset,
                            py_train_dev_token_vocab,
                            py_character_level_vocab,
                            py_tags_vocab,
                            hparams.infer_batch_size,
                            hparams.unk,
                            hparams.num_buckets,
                            shuffle=False,
                            repeat=False,
                            random_seed=None,
                            src_max_len=None,
                            num_threads=hparams.parallel_iterations,
                            limit_sample_count_to=hparams.dev_set_size_limit)

    return dataset

  def serving_input_receiver_fn():
    """An input receiver that expects a serialized tf.Example."""
    input_sentences_placeholder = tf.placeholder(dtype=tf.string,
                                                 shape=[None],
                                                 name='input_sentences_placeholder')

    input_dataset = tf.data.Dataset.from_tensors(input_sentences_placeholder)

    inference_token_vocab = create_vocab_table(token_vocab_inference_file)
    character_level_vocab = create_vocab_table(character_level_vocab_file)
    dataset_iterator = get_iterator(
      dataset=input_dataset,
      input_batch_size=tf.shape(input_sentences_placeholder, out_type=tf.int64)[0],
      word_table=inference_token_vocab,
      char_table=character_level_vocab,
      unk_word_idx=py_train_dev_token_vocab[hparams.unk],
      char_level_representation_max_len=hparams.char_level_representation_max_len,
      num_buckets=None,
      shuffle=False,
      label_dataset=False,
      split_words=True,
      repeat=False,
      prefetch=False,
      perform_batching=True,
    )

    initializer, (tokens, tokens_length, token_ids, char_ids, chars_sequence_length, _) = dataset_iterator

    features = {
      "tokens": tokens,
      "tokens_length": tokens_length,
      "token_ids": token_ids,
      "char_ids": char_ids,
      "chars_sequence_length": chars_sequence_length,
    }

    if hparams.gazetteers:
      gazetteer_feature_files = hparams.gazetteer_feature_files
      features["gazetteers"] = lookup_gazetteers(gazetteer_feature_files, tokens)

    return tf.estimator.export.ServingInputReceiver(features, features.copy())

  train_estimator(hparams, train_input_fn=train_input_fn,
                  train_hooks=train_hooks,
                  dev_input_fn=dev_input_fn,
                  dev_hooks=dev_hooks,
                  infer_input_fn=dev_input_fn,
                  infer_hooks=dev_hooks,
                  model_fn=model,
                  serving_input_receiver_fn=serving_input_receiver_fn)
