__all__ = ["CommonArgumentsParser"]


class CommonArgumentsParser(object):
  DEFAULT_EMPTY_LIST_VALUE = [None]

  def __init__(self):
    pass

  def __call__(self, common_parser):
    # common_parser = argparse.ArgumentParser()

    """Build Argumentparent_parser."""
    common_parser.register("type", "bool", lambda v: v.lower() == "true")

    common_parser.add_argument("--mode", type=str, default="train", choices=('train', 'infer', "save", "predict_debug", "evaluate_python"),
                               help="Mode to run in: train, eval, infer or save. "
                                    "Training also includes evaluation, so eval is for new evaluation sets that includes targets."
                                    "Inference is for data with no known targets."
                                    "Save is for saving model bundle")

    """ Network """

    common_parser.add_argument("--num_classes", type=int, default=None,
                               help="Number of target classes to classify.")

    """ RNN """

    common_parser.add_argument("--time_major", type="bool", nargs="?", const=True,
                               default=True,
                               help="Whether to use time-major mode for dynamic RNN.")
    common_parser.add_argument("--num_embeddings_partitions", type=int, default=0,
                               help="Number of partitions for embedding vars.")
    common_parser.add_argument("--multi_rnn_cell", type=str, default="stacked",
                               help="Method of combining layers, default is (stacked)")

    """ attention mechanisms """

    common_parser.add_argument("--attention", type=str, default="", help="""\
            luong | scaled_luong | bahdanau | normed_bahdanau | normed_monotonic_bahdanau | coverage_normed_bahdanau | monotonic_luong or set to "" for no
            attention\
            """)

    common_parser.add_argument("--pass_hidden_state", type="bool", nargs="?", const=True, default=True,
                               help="""Whether to pass encoder's hidden state to decoder when using an attention based model.""")

    """ Optimizer """

    common_parser.add_argument("--optimizer", type=str, default="adam", help="sgd | adam")
    common_parser.add_argument("--learning_rate", type=float, default=1.0,
                               help="Learning rate. Adam: 0.001 | 0.0001")
    common_parser.add_argument("--start_decay_step", type=int, default=0,
                               # TODO Not sure lr decay is actually functional
                               help="When we start to decay")
    common_parser.add_argument("--decay_steps", type=int, default=10000,
                               help="How frequent we decay")
    common_parser.add_argument("--decay_factor", type=float, default=1.0,
                               help="How much we decay.")

    common_parser.add_argument("--num_training_samples", type=int, default=None,
                               help="Num training example per epoch. ")
    common_parser.add_argument("--num_train_steps", type=int, default=12000,
                               help="Num global steps to train. "
                                    "If both num_train_steps and num_train_epochs are set, the one"
                                    "that is met first, will cause the model to stop training.")
    common_parser.add_argument("--num_train_epochs", type=int, default=12000,
                               help="Num epochs to train. "
                                    "If both num_train_steps and num_train_epochs are set, the one"
                                    "that is met first, will cause the model to stop training.")
    common_parser.add_argument("--colocate_gradients_with_ops", type="bool", nargs="?",
                               const=True, default=False,
                               help=("Whether to try colocating gradients with "
                                     "corresponding op"))
    common_parser.add_argument("--add_coverage_loss", type="bool", default=False,
                               help="(Only works with attention!) Whether to add a coverage term to the loss function.")
    common_parser.add_argument("--cov_loss_wt", type=float, default=1.0, help="How much to weigh the add_coverage_loss")
    common_parser.add_argument("--use_sampled_softmax", type=bool, default=False,
                               help="Whether to use sampled softmax during training")
    common_parser.add_argument("--sampled_shortlist_size", type=int, default=-1,
                               help="The amount of classes to sample for \"sampled softmax\"")
    common_parser.add_argument("--clip_gradients", type=bool, default=True,
                               help="Whether to clip gradients or not.")
    common_parser.add_argument("--max_gradient_norm", type=float, default=5.0,
                               help="Clip gradients to this norm.")

    """ Initializer """

    common_parser.add_argument("--init_op", type=str, default="uniform",
                               help="orthogonal | uniform | glorot_normal | glorot_uniform")
    common_parser.add_argument("--init_weight", type=float, default=0.1,
                               help=("for uniform init_op, initialize weights "
                                     "between [-this, this]."))

    """ Data """

    common_parser.add_argument("--file_suffix", type=str, default=None,
                               help="input file extension suffix, e.g., tfrecords.")
    common_parser.add_argument("--src", type=str, default=None,
                               help="Source suffix, e.g., en.")
    common_parser.add_argument("--tgt", type=str, default=None,
                               help="Target suffix, e.g., de.")
    common_parser.add_argument("--train_prefix", type=str, default=None,
                               help="Train prefix, expect file with file/src/tgt suffix.")
    common_parser.add_argument("--dev_prefix", type=str, default=None,
                               help="Dev prefix, expect file with file/src/tgt suffix.")
    common_parser.add_argument("--test_prefix", type=str, default=None,
                               help="Test prefix, expect file with file/src/tgt suffix.")
    common_parser.add_argument("--out_dir", type=str, default=None,
                               help="Store log/model files.")

    """ Vocab """
    common_parser.add_argument("--check_special_tokens_src_vocab", type="bool", default=True,
                               help="Whether to check for and add (if not present) special model tokens (UNK, SOS, EOS).")
    common_parser.add_argument("--check_special_tokens_tgt_vocab", type="bool", default=True,
                               help="Whether to check for and add (if not present) special model tokens (UNK, SOS, EOS).")

    common_parser.add_argument("--unk", type=str, default="<unk>",
                               help="Start-of-sentence symbol.")
    common_parser.add_argument("--sos", type=str, default="<s>",
                               help="Start-of-sentence symbol.")
    common_parser.add_argument("--eos", type=str, default="</s>",
                               help="End-of-sentence symbol.")
    common_parser.add_argument("--share_vocab", type="bool", nargs="?", const=True, default=False,
                               help="""Whether to use the source vocab and embeddings for both source and target.""")
    common_parser.add_argument("--vocab_prefix", type=str, default=None, help="""\
                Vocab prefix, expect file with file/src/tgt suffix. If None, extract from train files.""")

    """ Embeddings """
    common_parser.add_argument("--token_embedding_size", type=int, default=None,
                               help="Dimensionality of token embeddings.")
    common_parser.add_argument("--char_embedding_size", type=int, default=None,
                               help="Dimensionality of character level embeddings.")
    common_parser.add_argument("--token_embedding_dropout", type=float, default=None,
                               help="Dropout rate for token embeddings.")
    common_parser.add_argument("--char_embedding_dropout", type=float, default=None,
                               help="Dropout rate for character level embeddings.")
    common_parser.add_argument("--train_embeddings", type="bool", default=True, help="Whether to train embeddings.")

    """ Other input"""

    common_parser.add_argument("--gazetteer_feature_files", type=str, action='append', default=self.DEFAULT_EMPTY_LIST_VALUE,
                               help="A list of files containing newline seperated gazetteer values. Each file will correspond to a new gazetteer feature.")

    """ Text processing """
    common_parser.add_argument("--source_reverse", type="bool", nargs="?", const=True,
                               default=False, help="Reverse source sequence.")

    """ Training """
    common_parser.add_argument("--batch_size", type=int, default=None, help="Batch size when training.")
    common_parser.add_argument("--infer_batch_size", type=int, default=None, help="Batch size when not training.")
    common_parser.add_argument("--parallel_iterations", type=int, default=32,
                               help="Number of parallel iterations / threads / actual batch size on GPU.")
    common_parser.add_argument("--steps_per_stats", type=int, default=100,
                               help=("How many training steps to do per stats logging."
                                     "Save checkpoint every 10x steps_per_stats"))
    common_parser.add_argument("--max_train", type=int, default=0,
                               help="Limit on the size of training data (0: no limit).")
    common_parser.add_argument("--num_buckets", type=int, default=5,
                               help="Put data into similar-length buckets.")
    common_parser.add_argument("--fallback_bucket_width", type=int, default=10,
                               help="Default bucket width. Used when src_max_len is None.")
    common_parser.add_argument("--dev_set_size_limit", type=int, default=None,
                               help="Option to only use the first n elements of dev set.")


    """ BPE """
    common_parser.add_argument("--bpe_delimiter", type=str, default=None,
                               help="Set to @@ to activate BPE")

    """ Running """

    common_parser.add_argument("--num_gpus", type=int, default=1,
                               help="Number of gpus in each worker.")
    common_parser.add_argument("--first_gpu", type=int, default=0,
                               help="GPU index to use as the first GPU within the worker.")
    common_parser.add_argument("--single_task_gpu", type=int, default=0,
                               help="GPU index to use for single tasks. Assign to best performing GPU.")
    common_parser.add_argument("--log_device_placement", type="bool", nargs="?",
                               const=True, default=False, help="Debug GPU allocation.")
    common_parser.add_argument("--metrics", type=str, default="accuracy",
                               help=("Comma-separated list of evaluations "
                                     "metrics (bleu,rouge,accuracy)"))  # TODO Update once more has been *correctly* added
    common_parser.add_argument("--steps_per_external_eval", type=int, default=None,
                               help="""How many training steps to do per external evaluation. Automatically set
                                    based on data if None.""")
    common_parser.add_argument("--scope", type=str, default=None,
                               help="scope to put variables under")
    common_parser.add_argument("--hparams_path", type=str, default='standard_hparams/metadata_tagging.json',
                               help=("Path to standard hparams json file that overrides"
                                     "hparams values from FLAGS."))
    common_parser.add_argument("--random_seed", type=int, default=None,
                               help="Random seed (>0, set a specific seed).")
    common_parser.add_argument("--debug", type=bool, default=False,
                               help="Use tfdbg to debug training session.")  # TODO Add to other sessions so it depends on which you choose to run

    """ Inference """
    common_parser.add_argument("--ckpt", type=str, default="",
                               help="Checkpoint file to load a model for inference.")
    common_parser.add_argument("--length_penalty_weight", type=float, default=0.0,
                               help="Length penalty for beam search.")

    """ Job info """
    common_parser.add_argument("--jobid", type=int, default=0,
                               help="Task id of the worker.")
    common_parser.add_argument("--num_workers", type=int, default=1,
                               help="Number of workers (inference only).")
    common_parser.add_argument("--num_parallel_calls", type=int, default=32,
                               help="Number of parallel calls used for data input parsing.")
    common_parser.add_argument("--ps_hosts", type=str, default="",
                               help="Comma-separated list of hostname:port pairs")
    common_parser.add_argument("--worker_hosts", type=str, default="",
                               help="Comma-separated list of hostname:port pairs")
    common_parser.add_argument("--job_name", type=str, default=None,
                               help="One of 'ps', 'worker'")
    common_parser.add_argument("--verbose_tensors", type=bool, default=False,
                               help="Turn on tf.Print statements.")
    # Defining tf.train.Server
    common_parser.add_argument("--task_index", type=int, default=0,
                               help="Index of task within the job")

    self.common_parser = common_parser

    return common_parser
