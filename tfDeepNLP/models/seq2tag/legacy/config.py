import json
import os

from tfDeepNLP.models.seq2tag.constants import *


class Config(object):
  # train_dev for big matrix, train_only for small matrix
  _word_opt = {-1: "no_word_emb",
               0: "update_emb_train_dev",
               1: "update_emb_train_only",
               2: "no_update_emb_train_dev",
               3: "no_update_emb_train_only",
               4: "no_update_emb_all"}

  _tag_scheme = [IO, IOB2]

  def __init__(self, path, fn_json='hparams/both.json', logger=None):
    self.logger = logger
    self.idx_filename = os.path.join(path, "idx.json")

    hparam = json.load(open(fn_json))
    for k, v in hparam.items():
      setattr(self, k, v)

    if hasattr(self, "hidden_size"):  # LSTM model
      self.output_path = path
      self.min_token_freq_to_train = 5
      self.lstm_suffix = "." + fn_json.split('/')[-1].replace('.json', '')
      self.fn_lstm = os.path.join(path, "model_%s/" % self.lstm_suffix[1:])
      self.fn_lstm_bundle = os.path.join(path, "model_%s_bundle/" % self.lstm_suffix[1:])
      self.fn_lstm_train_bundle = os.path.join(path, "model_%s_train_bundle/" % self.lstm_suffix[1:])
      assert self.tag_scheme in self._tag_scheme, "input format must be from " + str(self._tag_scheme)
      assert self.word_opt in self._word_opt, "input format must be from " + str(self._word_opt)
      self.words = False if self.word_opt < 0 else True
      self.train_embeddings = True if self.word_opt in [0, 1] else False
      if hasattr(self, 'dim_word'):
        self.embeddings_filename = os.path.join(path, "glove.840B.{}d.trimmed.npz".format(self.dim_word))

    else:  # CRFSuite model
      if self.postag_idx == -1:
        self.fn_crf = os.path.join(path, 'crfsuite.%s' % self.tag_scheme)
      else:
        self.fn_crf = os.path.join(path, 'crfsuite.pos.%s' % self.tag_scheme)
