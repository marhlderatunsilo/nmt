from tfDeepNLP.models.seq2tag.legacy.common_arguments_parser import CommonArgumentsParser


class S2TArgumentsParser(CommonArgumentsParser):
  def __call__(self, parser):
    parser = super(S2TArgumentsParser, self).__call__(parser)

    # Vocab
    parser.add_argument("--token_vocab_train_dev", type=str, default=None, help="""\
              Token vocab file to use during training (subset of inference) 
              """)

    parser.add_argument("--token_vocab_inference", type=str, default=None, help="""\
                  Token vocab file to use during inference (full input vocab)
                  """)

    parser.add_argument("--char_vocab", type=str, default=None, help="""\
                Char vocab prefix, expect file with file suffix.
                """)
    parser.add_argument("--tag_vocab", type=str, default=None, help="""\
                    Tags vocab prefix, expect file with file suffix.
                    """)
    parser.add_argument("--infer_embeddings_filename", type=str, default=None, help="""\
                        Embeddings file to use during inference.
                        """)

    # Additional data sources
    parser.add_argument("--person_names_file", type=str, default=None,
                        help="Path to a file which includes person names for the gazetteers")

    parser.add_argument("--gazetteers", type="bool", default=True, help="Whether to include gazetteers.")
    parser.add_argument("--crf", type="bool", default=True, help="Whether to use a conditional random field.")

    parser.add_argument("--use_uppercase_chars", type="bool", default=True,
                        help="Whether to include case information about chars.")
    parser.add_argument("--lstm_suffix", type=str, default="lstm",
                        help="model name suffix.")
    parser.add_argument("--embeddings_filename", type=str, default=None,
                        help="Name for the numpy file containing the embeddings.")
    parser.add_argument("--idx_filename", type=str, default=None,
                        help="Filename of word embedding indexes.")
    parser.add_argument("--tag_scheme", type=str, default=None,
                        help="The tag_scheme.")

    # Token level hyperparameters
    parser.add_argument("--tokens", type="bool", default=True, help="Whether to include token level features/embeddings.")

    parser.add_argument("--token_rnn_num_encoder_layers", type=int, default=None,
                        help="Token encoder sub-network depth.")
    parser.add_argument("--token_rnn_num_decoder_layers", type=int, default=None,
                        help="Token decoder sub-network depth.")

    parser.add_argument("--token_rnn_encoder_num_residual_layers", type=int, default=0,
                        help="The number of residual layers for the token encoder sub-network.")
    parser.add_argument("--token_rnn_decoder_num_residual_layers", type=int, default=0,
                        help="The number of residual layers for the token decoder sub-network.")

    parser.add_argument("--token_rnn_dropout", type=float, default=0.0,
                        help="Dropout rate (not keep_prob) for token sub-network")
    parser.add_argument("--token_rnn_zoneout", type=float, default=0.0,
                        help="Zoneout rate (not keep_prob) for token level sub-network")
    parser.add_argument("--token_rnn_forget_bias", type=float, default=1.0,
                        help="Forget bias for LSTM cells.")
    parser.add_argument("--token_rnn_unit_type", type=str, default="lstm",
                        help="lstm | gru | layer_norm_lstm | nlstm | layer_norm_nlstm")
    parser.add_argument("--token_rnn_norm_gain", type=float, default=1.0,
                        help="Norm gain for layer norm LSTM types. For token RNN.")
    parser.add_argument("--token_rnn_num_units", type=int, default=None,
                        help="Number of units for the token model layers.")
    #parser.add_argument("--token_embedding_size", type=int, default=None,
    #                    help="Dimensionality of token embeddings.")
    parser.add_argument("--token_representation_dropout", type=float, default=0.1,
                        help="Dropout rate of the entire token representation, including: Token embeddings, character level embedding, and gazetteers")

    parser.add_argument("--transformer_num_heads", type=int, default=None,
                        help="Number of attention heads used by transformer layers")
    parser.add_argument("--token_level_architecture", type=str, default="RNN",
                        help="What type of network to use for the token level part of the model")


    # Character level hyperparameters
    parser.add_argument("--chars", type="bool", default=True, help="Whether to include char level features/embeddings.")
    parser.add_argument("--char_rnn_num_layers", type=int, default=None,
                        help="Character sub-network depth.")
    parser.add_argument("--char_rnn_residual", type="bool", nargs="?", const=True,
                        default=False,
                        help="Whether to add residual connections to the character level sub-network.")
    parser.add_argument("--char_rnn_num_residual_layers", type=int, default=0, help="The number of residual layers for the character level sub-network.")
    parser.add_argument("--char_rnn_dropout", type=float, default=0.0,
                        help="Dropout rate (not keep_prob) for character level sub-network")
    parser.add_argument("--char_rnn_zoneout", type=float, default=0.0,
                        help="Zoneout rate (not keep_prob) for character level sub-network")
    parser.add_argument("--char_rnn_forget_bias", type=float, default=1.0,
                        help="Forget bias for LSTM cells.")
    parser.add_argument("--char_rnn_unit_type", type=str, default="lstm",
                        help="lstm | gru | layer_norm_lstm | nlstm | layer_norm_nlstm")
    parser.add_argument("--char_rnn_norm_gain", type=float, default=1.0,
                        help="Norm gain for layer norm LSTM types. For char RNN.")
    parser.add_argument("--char_rnn_num_units", type=int, default=None, help="Number of units for the character level model layers.")
    parser.add_argument("--char_level_representation_max_len", type=int, default=None,
                        help="Maximum number of characters in each token character level representation")


    #parser.add_argument("--char_embedding_size", type=int, default=None,
    #                    help="Dimensionality of character embeddings.")

    # From common hparams, TODO delete unnecessary ones
    parser.add_argument("--inference_input_file", type=str, default=None,
                        help="Set to the text to decode.")
    parser.add_argument("--inference_list", type=str, default=None,
                        help=("A comma-separated list of sentence indices "
                              "(0-based) to decode."))
    parser.add_argument("--inference_pred_output_file", type=str, default=None,
                        help="Output file to store classification results.")
    parser.add_argument("--inference_prob_output_file", type=str, default=None,
                        help="Output file to store classification probability results.")
    parser.add_argument("--inference_ref_file", type=str, default=None,
                        help="""Reference file to compute evaluation scores (if provided).""")

    parser.add_argument("--src_max_len", type=int, default=50,
                        help="Max length of src sequences during training.")
    parser.add_argument("--tgt_max_len", type=int, default=50,
                        help="Max length of tgt sequences during training.")
    parser.add_argument("--src_max_len_infer", type=int, default=None,
                        help="Max length of src sequences during inference.")
    parser.add_argument("--tgt_max_len_infer", type=int, default=None,
                        help="""Max length of tgt sequences during inference.
                                Also used to restrict the maximum decoding length.""")

    parser.add_argument("--unit_type", type=str, default="lstm",
                        help="lstm | gru | layer_norm_lstm")
    parser.add_argument("--use_peepholes", type=bool, default=False,
                        help="Whether to use peepholes in the selected unit architecture. (Ignored if not supported)"),


    parser.add_argument("--encoder_type", type=str, default="uni", help="""\
                uni | bi | gnmt. For bi, we build num_layers/2 bi-directional layers.For
                gnmt, we build 1 bi-directional layer, and (num_layers - 1) uni-
                directional layers.\
                """)

    parser.add_argument("--zoneout", type=float, default=0.0,
                        help="Zoneout prob")

    parser.add_argument("--create_tf_records", type="bool", default=False,
                        help="Mode option: Whether to create Tensorflow records (Instead of training of inference)  ")

    parser.add_argument("--create_tf_records_cner", type="bool", default=False,
                        help="Mode option: Whether to create Tensorflow records (Instead of training of inference)  ")

    parser.add_argument("--create_tf_records_metadata_tagging", type="bool", default=False,
                        help="Mode option: Whether to create Tensorflow records (Instead of training of inference)  ")

    parser.add_argument("--collapse_tag_vocab", type="bool", default=False,
                        help="Whether to use a collapsed version of the tag vocab (Only applies to tf_records creation)")

    parser.add_argument("--rnn_init_op", type=int,
                        default=None,
                        help="The init up used for RNN layers, options are : orthogonal | uniform | glorot_normal | glorot_uniform | orthogonal+glorot_normal")

    parser.add_argument("--l2_scale", type=float,
                        default=None,
                        help="How much to scale l2 loss")

    return parser
