import os
import tensorflow as tf

from tfDeepNLP.utils import vocab_utils, misc_utils

class CommonHyperParamsCreator:
  def __call__(self, flags):
    """Create training hparams."""

    return tf.contrib.training.HParams(
      # Data
      src=flags.src,
      tgt=flags.tgt,
      file_suffix=flags.file_suffix,
      train_prefix=flags.train_prefix,
      dev_prefix=flags.dev_prefix,
      test_prefix=flags.test_prefix,
      out_dir=flags.out_dir,

      # Networks
      time_major=flags.time_major,
      num_embeddings_partitions=flags.num_embeddings_partitions,
      multi_rnn_cell=flags.multi_rnn_cell,
      num_classes=flags.num_classes,
      scope=flags.scope,

      # Attention mechanisms
      attention=flags.attention,

      pass_hidden_state=flags.pass_hidden_state,

      # Train
      optimizer=flags.optimizer,
      num_training_samples=flags.num_training_samples,
      num_train_steps=flags.num_train_steps,
      num_train_epochs=flags.num_train_epochs,
      batch_size=flags.batch_size,
      infer_batch_size=flags.infer_batch_size,
      parallel_iterations=flags.parallel_iterations,
      init_op=flags.init_op,
      init_weight=flags.init_weight,
      max_gradient_norm=flags.max_gradient_norm,
      learning_rate=flags.learning_rate,
      start_decay_step=flags.start_decay_step,
      decay_factor=flags.decay_factor,
      decay_steps=flags.decay_steps,
      colocate_gradients_with_ops=flags.colocate_gradients_with_ops,
      clip_gradients=flags.clip_gradients,
      add_coverage_loss=flags.add_coverage_loss,
      cov_loss_wt=float(flags.cov_loss_wt),
      use_sampled_softmax=flags.use_sampled_softmax,
      sampled_shortlist_size=flags.sampled_shortlist_size,
      debug=flags.debug,
      dev_set_size_limit=flags.dev_set_size_limit,

      # Data constraints
      num_buckets=flags.num_buckets,
      fallback_bucket_width=flags.fallback_bucket_width,
      max_train=flags.max_train,
      source_reverse=flags.source_reverse,

      # Inference
      length_penalty_weight=flags.length_penalty_weight,

      # Vocab
      unk=flags.unk if flags.unk else vocab_utils.UNK,
      sos=flags.sos if flags.sos else vocab_utils.SOS,
      eos=flags.eos if flags.eos else vocab_utils.EOS,
      bpe_delimiter=flags.bpe_delimiter,
      check_special_tokens_src_vocab=flags.check_special_tokens_src_vocab,
      check_special_tokens_tgt_vocab=flags.check_special_tokens_tgt_vocab,

      # Embeddings
      token_embedding_size=flags.token_embedding_size,
      char_embedding_size=flags.char_embedding_size,
      token_embedding_dropout=flags.token_embedding_dropout,
      char_embedding_dropout=flags.char_embedding_dropout,
      train_embeddings=flags.train_embeddings,

      # Other input
      gazetteer_feature_files=flags.gazetteer_feature_files,

      # Misc
      mode=flags.mode,
      num_gpus=flags.num_gpus,
      epoch_step=0,  # record where we were within an epoch.
      steps_per_stats=flags.steps_per_stats,
      steps_per_external_eval=flags.steps_per_external_eval,
      num_parallel_calls=flags.num_parallel_calls,
      share_vocab=flags.share_vocab,
      metrics=flags.metrics.split(","),
      log_device_placement=flags.log_device_placement,
      verbose_tensors=flags.verbose_tensors,
      random_seed=flags.random_seed,
      task_index=None,  # We reserve this name to set it later
      first_gpu=None,  # We reserve this name to set it later
      single_task_gpu=None,  # We reserve this name to set it later

    )

  def extend_hparams(self, hparams, flags):
    """Extend training hparams."""

    # Flags
    misc_utils.print_out("# hparams:")
    misc_utils.print_out("  src=%s" % hparams.src)
    misc_utils.print_out("  tgt=%s" % hparams.tgt)
    misc_utils.print_out("  train_prefix=%s" % hparams.train_prefix)
    misc_utils.print_out("  dev_prefix=%s" % hparams.dev_prefix)
    misc_utils.print_out("  test_prefix=%s" % hparams.test_prefix)
    misc_utils.print_out("  out_dir=%s" % hparams.out_dir)

    # Set num_residual_layers

    # Check out_dir
    if not tf.gfile.Exists(hparams.out_dir):
      misc_utils.print_out("# Creating output directory %s ..." % hparams.out_dir)
      tf.gfile.MakeDirs(hparams.out_dir)

    # Evaluation
    for metric in hparams.metrics:
      hparams.add_hparam("best_" + metric, 0)  # larger is better
      best_metric_dir = os.path.join(hparams.out_dir, "best_" + metric)
      hparams.add_hparam("best_" + metric + "_dir", best_metric_dir)
      tf.gfile.MakeDirs(best_metric_dir)

    # Distributed Convenience
    hparams.add_hparam("num_ps_hosts", len(flags.ps_hosts.split(",")))
    hparams.add_hparam("num_worker_hosts", len(flags.worker_hosts.split(",")))

    return hparams

  def ensure_compatible_hparams(self, hparams, default_hparams, hparams_path):
    """Make sure the loaded hparams is compatible with new changes."""
    default_hparams = misc_utils.maybe_parse_standard_hparams(
      default_hparams, hparams_path)

    # For compatible reason, if there are new fields in default_hparams,
    #   we add them to the current hparams
    default_config = default_hparams.values()
    config = hparams.values()
    for key in default_config:
      if key not in config:
        hparams.add_hparam(key, default_config[key])

    # Make sure that the loaded model has latest values for the below keys
    updated_keys = [
      "out_dir", "num_gpus", "test_prefix", "beam_width",
      "length_penalty_weight", "num_train_steps"
    ]
    for key in updated_keys:
      if key in default_config and getattr(hparams, key) != default_config[key]:
        misc_utils.print_out("# Updating hparams.%s: %s -> %s" %
                             (key, str(getattr(hparams, key)),
                              str(default_config[key])))
        setattr(hparams, key, default_config[key])
    return hparams

  def create_or_load_hparams(self, out_dir, default_hparams, hparams_path, flags):
    """Create hparams or load hparams from out_dir."""
    hparams = misc_utils.load_hparams(out_dir)
    if not hparams:
      hparams = default_hparams
      hparams = misc_utils.maybe_parse_standard_hparams(
        hparams, hparams_path)
      hparams = self.extend_hparams(hparams, flags)
    else:
      hparams = self.ensure_compatible_hparams(hparams, default_hparams, hparams_path)

    # Override the local hparams task index with what was given in flags
    hparams.task_index = flags.task_index
    hparams.first_gpu = flags.first_gpu
    hparams.single_task_gpu = flags.single_task_gpu
    hparams.mode = flags.mode

    # Save HParams
    misc_utils.save_hparams(out_dir, hparams)

    for metric in hparams.metrics:
      misc_utils.save_hparams(getattr(hparams, "best_" + metric + "_dir"), hparams)

    # Print HParams
    misc_utils.print_hparams(hparams)
    return hparams
