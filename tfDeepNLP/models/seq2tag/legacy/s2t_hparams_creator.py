
from tfDeepNLP.models.seq2tag.constants import *
from tfDeepNLP.utils import vocab_utils
from tfDeepNLP.utils.common_hparams_creator import CommonHyperParamsCreator


class Seq2TagHyperParamsCreator(CommonHyperParamsCreator):
  _word_opt = [
    "update_emb_train_dev",
    "update_emb_train_only",
    "no_update_emb_train_dev",
    "no_update_emb_train_only",
    "no_update_emb_all"]

  _tag_scheme = [IO, IOB2]

  def __call__(self, flags):
    self.hparams = super(Seq2TagHyperParamsCreator, self).__call__(flags)

    # Pre-trained embeddings
    self.hparams.add_hparam("infer_embeddings_filename", flags.infer_embeddings_filename)
    self.hparams.add_hparam("embeddings_filename", flags.embeddings_filename)

    self.hparams.add_hparam("person_names_file", flags.person_names_file)
    self.hparams.add_hparam("gazetteers", flags.gazetteers)
    self.hparams.add_hparam("crf", flags.crf)

    self.hparams.add_hparam("use_uppercase_chars", flags.use_uppercase_chars)
    self.hparams.add_hparam("lstm_suffix", flags.lstm_suffix)
    self.hparams.add_hparam("idx_filename", flags.idx_filename)
    self.hparams.add_hparam("tag_scheme", flags.tag_scheme)
    #self.hparams.add_hparam("vocab_prefix", flags.vocab_prefix)

    #self.hparams.add_hparam("__token_vocab_train_dev", flags.token_vocab_train_dev)
    #self.hparams.add_hparam("__token_vocab_inference", flags.token_vocab_inference)
    #self.hparams.add_hparam("__char_vocab", flags.char_vocab)
    #self.hparams.add_hparam("__tags_vocab", flags.tags_vocab)

    # Token level hyperparameters
    self.hparams.add_hparam("tokens", flags.tokens)
    self.hparams.add_hparam("token_rnn_num_encoder_layers", flags.token_rnn_num_encoder_layers)
    self.hparams.add_hparam("token_rnn_num_decoder_layers", flags.token_rnn_num_decoder_layers)
    self.hparams.add_hparam("token_rnn_encoder_num_residual_layers", flags.token_rnn_encoder_num_residual_layers)
    self.hparams.add_hparam("token_rnn_decoder_num_residual_layers", flags.token_rnn_decoder_num_residual_layers)
    self.hparams.add_hparam("token_rnn_dropout", flags.token_rnn_dropout)
    self.hparams.add_hparam("token_rnn_zoneout", flags.token_rnn_zoneout)
    self.hparams.add_hparam("token_rnn_forget_bias", flags.token_rnn_forget_bias)
    self.hparams.add_hparam("token_rnn_unit_type", flags.token_rnn_unit_type)
    self.hparams.add_hparam("token_rnn_norm_gain", flags.token_rnn_norm_gain)
    self.hparams.add_hparam("token_rnn_num_units", flags.token_rnn_num_units)
    #self.hparams.add_hparam("token_embedding_size", flags.token_embedding_size)
    self.hparams.add_hparam("token_representation_dropout", flags.token_representation_dropout)
    self.hparams.add_hparam("transformer_num_heads", flags.transformer_num_heads)
    self.hparams.add_hparam("token_level_architecture", flags.token_level_architecture)

    # Character level hyperparameters
    self.hparams.add_hparam("chars", flags.chars)
    self.hparams.add_hparam("char_rnn_num_layers", flags.char_rnn_num_layers)
    self.hparams.add_hparam("char_rnn_residual", flags.char_rnn_residual)
    self.hparams.add_hparam("char_rnn_num_residual_layers", flags.char_rnn_num_residual_layers)
    self.hparams.add_hparam("char_rnn_dropout", flags.char_rnn_dropout)
    self.hparams.add_hparam("char_rnn_zoneout", flags.char_rnn_zoneout)
    self.hparams.add_hparam("char_rnn_forget_bias", flags.char_rnn_forget_bias)
    self.hparams.add_hparam("char_rnn_unit_type", flags.char_rnn_unit_type)
    self.hparams.add_hparam("char_rnn_norm_gain", flags.char_rnn_norm_gain)
    self.hparams.add_hparam("char_rnn_num_units", flags.char_rnn_num_units)
    self.hparams.add_hparam("char_level_representation_max_len", flags.char_level_representation_max_len)

    #self.hparams.add_hparam("char_embedding_size", flags.char_embedding_size)

    self.hparams.add_hparam("unit_type", flags.unit_type)
    self.hparams.add_hparam("use_peepholes", flags.use_peepholes)
    self.hparams.add_hparam("encoder_type", flags.encoder_type)
    self.hparams.add_hparam("src_max_len", flags.src_max_len)
    self.hparams.add_hparam("tgt_max_len", flags.tgt_max_len)
    self.hparams.add_hparam("src_max_len_infer", flags.src_max_len_infer)
    self.hparams.add_hparam("tgt_max_len_infer", flags.tgt_max_len_infer)

    self.hparams.add_hparam("rnn_init_op", flags.rnn_init_op)

    self.hparams.add_hparam("l2_scale", flags.l2_scale)

    self.hparams.add_hparam("collapse_tag_vocab", flags.collapse_tag_vocab)

    return self.hparams

  def extend_hparams(self, hparams, flags):
    self.hparams = super(Seq2TagHyperParamsCreator, self).extend_hparams(hparams, flags)

    #if not self.hparams.idx_filename:
    #  raise ValueError("hparams.idx_filename must be provided.")

    # self.hparams.add_hparam("mapping_idx", self.load_idx())
    #self.hparams.add_hparam("_word_opt", self._word_opt)
    #self.hparams.add_hparam("_tag_scheme", self._tag_scheme)

    token_vocab_train_dev_size, token_vocab_train_dev_file = vocab_utils.check_and_copy_vocab(
      flags.token_vocab_train_dev,
      self.hparams.out_dir,
      sos=self.hparams.sos,
      eos=self.hparams.eos,
      unk=hparams.unk,
      check_special_chars=False)

    token_vocab_inference_size, token_vocab_inference_file = vocab_utils.check_and_copy_vocab(
      flags.token_vocab_inference,
      self.hparams.out_dir,
      sos=self.hparams.sos,
      eos=self.hparams.eos,
      unk=hparams.unk,
      check_special_chars=False)

    char_vocab_size, char_vocab_file = vocab_utils.check_and_copy_vocab(
      flags.char_vocab,
      self.hparams.out_dir,
      sos=self.hparams.sos,
      eos=self.hparams.eos,
      unk=hparams.unk,
      check_special_chars=False)

    tags_vocab_size, tag_vocab_file = vocab_utils.check_and_copy_vocab(
      flags.tag_vocab,
      self.hparams.out_dir,
      sos=self.hparams.sos,
      eos=self.hparams.eos,
      unk=vocab_utils.UNK,
      check_special_chars=False)

    # We copy the vocabs to outdir (above) and save the new paths (here) to ensure consistency
    self.hparams.add_hparam("token_vocab_train_dev_file", token_vocab_train_dev_file)
    self.hparams.add_hparam("token_vocab_inference_file", token_vocab_inference_file)
    self.hparams.add_hparam("char_vocab_file", char_vocab_file)
    self.hparams.add_hparam("tag_vocab_file", tag_vocab_file)

    self.hparams.add_hparam("token_vocab_train_dev_size", token_vocab_train_dev_size)
    self.hparams.add_hparam("token_vocab_inference_size", token_vocab_inference_size)
    self.hparams.add_hparam("char_vocab_size", char_vocab_size)
    self.hparams.add_hparam("tags_vocab_size", tags_vocab_size)

    return self.hparams
