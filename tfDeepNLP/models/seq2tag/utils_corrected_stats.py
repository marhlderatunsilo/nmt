from tfDeepNLP.models.seq2tag.utils_correct_input import correct_tag, TAG_TITLE
from tfDeepNLP.models.seq2tag.utils import get_precision_recall_f1, exact_match_stat, get_segments
from collections import defaultdict


def parse_file(filename):
  tokens, tags, predicted = [], [], []
  with open(filename) as fin:
    fin.readline()
    fin.readline()
    for line in fin:
      if not line.strip():
        yield tokens, tags, predicted
        tokens, tags, predicted = [], [], []
      else:
        a, b, c = line.strip().split()
        tokens.append(a)
        tags.append(b)
        predicted.append(c)
  return tokens, tags, predicted


def get_title_stat(filename):
  doc_cnt, tp, fp, fn = 0, 0, 0, 0
  no_title_cnt = 0
  for tokens, tags, predicted in parse_file(filename):
    if (len(set(tags)) < 3) or (TAG_TITLE not in tags):
      no_title_cnt += 1
      continue
    _, new_tags, _ = correct_tag(tokens, tags)
    if new_tags:
      _tp, _fp, _fn = exact_match_stat(new_tags, predicted, 'I-TITLE', 'I-TITLE')[:3]
      doc_cnt += 1
      tp += _tp
      fp += _fp
      fn += _fn
      if _fn > 0:
        print(tokens[:10])
  print('title', doc_cnt, no_title_cnt, tp, fp, fn)  # 756 317 28 589227 728
  precision, recall, f1 = get_precision_recall_f1(tp, fp, fn)
  print(precision, recall, f1)


def get_author_stat(filename):
  tp, fp, fn = 0, 0, 0
  for tokens, tags, predicted in parse_file(filename):
    _tp, _fp, _fn = exact_match_stat(tags, predicted, 'I-AUTHOR', 'E-AUTHOR')[:3]
    tp += _tp
    fp += _fp
    fn += _fn
  precision, recall, f1 = get_precision_recall_f1(tp, fp, fn)
  print('author', tp, fp, fn)
  print(precision, recall, f1)


def print_true_pred(tokens, tags, predicted, pa, pb):
  for p in range(pa, pb):
    print(tokens[p] + '\t' + tags[p] + '\t' + predicted[p])
  print()


def compare_true_pred_title(filename, start_idx='I-TITLE', end_idx='I-TITLE'):
  cnts = defaultdict(lambda: 0)
  for tokens, tags, predicted in parse_file(filename):
    true_segs = get_segments(tags, start_idx, end_idx)
    pred_segs = get_segments(predicted, start_idx, end_idx)
    if true_segs == pred_segs:
      cnts['same'] += 1
    elif not true_segs:
      cnts['not_truth'] += 1
    elif not pred_segs:
      cnts['not_pred'] += 1
    else:
      cnts['diff'] += 1
      pa = min(true_segs[0][0], pred_segs[0][0])
      pb = max(true_segs[-1][1], pred_segs[-1][1])
      print_true_pred(tokens, tags, predicted, pa, pb)
  print(cnts)


if __name__ == '__main__':

  filename = '/Users/qianyuxx/Documents/unsilo/data/metadata/predictions_springer_corpus.txt'
  # get_title_stat(filename)
  # get_author_stat(filename)
  compare_true_pred_title(filename)