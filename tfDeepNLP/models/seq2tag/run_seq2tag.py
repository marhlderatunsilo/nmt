"""S2T: TensorFlow NER model implementation."""
import argparse
import random
import os
import pprint
import numpy as np
import tensorflow as tf
import yaml

from tfDeepNLP.models.seq2tag.evalute_using_estimator_with_python_script import s2t_evaluate_python
from tensorflow.python.platform import tf_logging
from tfDeepNLP.utils.common import YParams, add_file_handler, create_empty_folder
from tfDeepNLP.models.seq2tag.predict_debugging_using_estimator import s2t_predict
from tfDeepNLP.models.seq2tag import train_using_estimator
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils import vocab_utils

utils.check_tensorflow_version()


def main(argv):
  train_fn = train_using_estimator.s2t_train
  random_seed = hparams.random_seed
  if hparams.random_seed >= 0:
    utils.print_out("# Set random seed to %d" % random_seed)
    random.seed(random_seed)
    np.random.seed(random_seed)
  if hparams.task == "predict_debug":
    s2t_predict(hparams)
  elif hparams.task == "evaluate_python":
    s2t_evaluate_python(hparams)
  elif hparams.task == 'create_tf_records':
    from models.NER_tagging.code.conll2k.create_tf_records_from_dataset import create_tf_records
    create_tf_records(hparams)
  elif hparams.task == 'create_tf_records_cner':
    from models.NER_tagging.code.chemical_ner.create_tf_records_from_dataset_files import \
      create_tf_records as create_tf_records_cner
    create_tf_records_cner(hparams)
  elif hparams.task == 'create_tf_records_metadata_tagging':
    from models.metadata_tagging.code.springer_metadata_tagging.create_tf_records_from_dataset_files import \
      create_tf_records as create_tf_records_metadata_tagging
    create_tf_records_metadata_tagging(hparams)
  else:
    # Default - Train the model
    train_fn(hparams)


def extend_hparams(hparams):
  token_vocab_train_dev_size, token_vocab_train_dev_file = vocab_utils.check_and_copy_vocab(
    os.path.join(hparams.work_dir, hparams.token_vocab_train_dev),
    hparams.out_dir,
    sos=hparams.sos,
    eos=hparams.eos,
    unk=hparams.unk,
    check_special_chars=False)

  token_vocab_inference_size, token_vocab_inference_file = vocab_utils.check_and_copy_vocab(
    os.path.join(hparams.work_dir, hparams.token_vocab_inference),
    hparams.out_dir,
    sos=hparams.sos,
    eos=hparams.eos,
    unk=hparams.unk,
    check_special_chars=False)

  char_vocab_size, char_vocab_file = vocab_utils.check_and_copy_vocab(
    os.path.join(hparams.work_dir, hparams.char_vocab),
    hparams.out_dir,
    sos=hparams.sos,
    eos=hparams.eos,
    unk=hparams.unk,
    check_special_chars=False)

  tags_vocab_size, tag_vocab_file = vocab_utils.check_and_copy_vocab(
    os.path.join(hparams.work_dir, hparams.tag_vocab),
    hparams.out_dir,
    sos=hparams.sos,
    eos=hparams.eos,
    unk=vocab_utils.UNK,
    check_special_chars=False)

  # We copy the vocabs to outdir (above) and save the new paths (here) to ensure consistency
  hparams.add_hparam("token_vocab_train_dev_file", token_vocab_train_dev_file)
  hparams.add_hparam("token_vocab_inference_file", token_vocab_inference_file)
  hparams.add_hparam("char_vocab_file", char_vocab_file)
  hparams.add_hparam("tag_vocab_file", tag_vocab_file)

  hparams.add_hparam("token_vocab_train_dev_size", token_vocab_train_dev_size)
  hparams.add_hparam("token_vocab_inference_size", token_vocab_inference_size)
  hparams.add_hparam("char_vocab_size", char_vocab_size)
  hparams.add_hparam("tags_vocab_size", tags_vocab_size)

  return hparams


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='my model')
  parser.add_argument('--yaml_config_file', type=str,
                      default=os.path.join(os.getcwd(), 'tfDeepNLP/models/seq2tag/standard_hparams/metadata_run_config.yaml'),
                      help="out_dir and work_dir should be given here!!")
  parser.add_argument('--yaml_config_name', type=str, default='base')

  parser.add_argument('--yaml_param_file', type=str,
                      default=os.path.join(os.getcwd(), 'tfDeepNLP/models/seq2tag/standard_hparams/metadata_hparam.yaml'))
  parser.add_argument('--yaml_param_name', type=str, default='base')

  parser.add_argument('--task', type=str, default='train',
                      choices=('train', 'infer', "save", 'create_tf_records', 'create_tf_records_cner', 'create_tf_records_metadata_tagging', 'evaluate_python', 'predict_debug'))

  args = parser.parse_args()

  hparams = YParams(args.yaml_config_file, args.yaml_config_name)

  existing_hparams_path = os.path.join(hparams.out_dir, "hparam.yaml")
  if not tf.gfile.Exists(hparams.out_dir):
    tf.gfile.MakeDirs(hparams.out_dir)

  if tf.gfile.Exists(existing_hparams_path):
    print("load hparam from ", existing_hparams_path)
    hparams = YParams(existing_hparams_path)
  else:
    hparams.update_param(YParams(args.yaml_param_file, args.yaml_param_name))
    print("save hparam to ", existing_hparams_path)
    with open(existing_hparams_path, 'w') as fout:
      yaml.dump({k: v for k, v in vars(hparams).items() if not k.startswith('_')}, fout, default_flow_style=False)

  hparams.update_param(args, ignore_keywords=['yaml'])
  hparams = extend_hparams(hparams)

  if hparams.num_gpus <= 1:
    tf.logging.set_verbosity(tf.logging.INFO)
  tf_logger = tf_logging._get_logger()

  if 'lrfinder' in args.yaml_param_name:
    hparams.set_hparam('out_dir', os.path.join(hparams.out_dir, args.yaml_param_name))
    create_empty_folder(hparams.out_dir)
    log_file = os.path.join(hparams.out_dir, "log_lrfinder")
    add_file_handler(tf_logger, log_file)

  tf_logger.info(pprint.pformat({k: v for k, v in vars(hparams).items() if not k.startswith('_')}))

  tf.app.run()
