from __future__ import print_function

import os
import time

import numpy as np
import tensorflow as tf

from models.metadata_tagging.code.springer_metadata_tagging.metadata_dataset_utils import MetadataDataset
from tfDeepNLP.gazetteers.gazetteers_lookup import lookup_gazetteers
from tfDeepNLP.hooks.initializer_hook import ExtraInitializationHook
from tfDeepNLP.models.seq2tag.dataset_iterator.ner_iterator import get_iterator
from tfDeepNLP.models.seq2tag.sequence_tagging_model import SequenceTaggingModel
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.dataset_utils import DatasetPartitioner
from tfDeepNLP.utils.vocab_utils import create_python_vocab_table, create_vocab_table, create_reverse_python_vocab_table
from tfDeepNLP.models.seq2tag.utils import get_precision_recall_f1, exact_match_stat

utils.check_tensorflow_version()

__all__ = [
  "s2t_evaluate_python"
]


def s2t_evaluate_python(hparams,
                        scope=None,
                        single_cell_fn=None):
  # Logging and output
  out_dir = hparams.out_dir
  log_file = os.path.join(out_dir, "log_%d" % time.time())
  log_f = tf.gfile.GFile(log_file, mode="a")
  utils.print_out("# log_file=%s" % log_file, log_f)

  print("token_level_num_mixedin_transformer_layers:", hparams.token_level_num_mixedin_transformer_layers)

  # Select model
  model_creator = SequenceTaggingModel

  """
  Get ready for training!
  """

  token_vocab_train_dev_file = hparams.token_vocab_train_dev_file
  token_vocab_inference_file = hparams.token_vocab_inference_file
  character_level_vocab_file = hparams.char_vocab_file
  tag_vocab_file = hparams.tag_vocab_file

  py_train_dev_token_vocab = create_python_vocab_table(token_vocab_train_dev_file)
  py_inference_token_vocab = create_python_vocab_table(token_vocab_inference_file)
  py_character_level_vocab = create_python_vocab_table(character_level_vocab_file)
  py_tags_vocab = create_python_vocab_table(tag_vocab_file)
  reverse_py_tags_vocab = create_reverse_python_vocab_table(tag_vocab_file)

  model_fn = model_creator(
    hparams,
    py_train_dev_token_vocab,
    py_inference_token_vocab,
    py_tags_vocab,
    py_character_level_vocab,
    unk_word_idx=py_train_dev_token_vocab["$UNK$"],
    unk_char_idx=py_character_level_vocab["$UNK$"],
    ntags=len(py_tags_vocab),
    single_cell_fn=single_cell_fn)

  initializer_hack = []

  def create_evaluation_input_fn(eval_dataset):
    def evaluation_input_fn():
      """An input receiver that expects a serialized tf.Example."""

      eval_dataset_tf = tf.data.Dataset.from_generator(eval_dataset.__iter__,
                                                       output_types=(tf.int32, tf.string),
                                                       output_shapes=(
                                                         tf.TensorShape([None]),
                                                         tf.TensorShape([None])))

      # input_dataset = tf.data.Dataset.from_tensors(input_sentences_placeholder)
      inference_token_vocab = create_vocab_table(token_vocab_inference_file)
      character_level_vocab = create_vocab_table(character_level_vocab_file)
      dataset_iterator = get_iterator(
        dataset=eval_dataset_tf,
        input_batch_size=120,
        word_table=inference_token_vocab,
        char_table=character_level_vocab,
        unk_word_idx=py_train_dev_token_vocab[hparams.unk],
        char_level_representation_max_len=hparams.char_level_representation_max_len,
        num_buckets=None,
        shuffle=False,
        label_dataset=True,
        split_words=False,
        repeat=False,
        prefetch=False,
        perform_batching=True,
      )

      initializer, (tokens, tokens_length, token_ids, char_ids, chars_sequence_length, _) = dataset_iterator
      initializer_hack.clear()
      initializer_hack.append(initializer)

      # token_ids = tf.Print(token_ids, [token_ids], message="token_ids: ", summarize=1000)

      with tf.control_dependencies([initializer]):
        gazetteers = lookup_gazetteers(hparams.gazetteer_feature_files, tokens)

        features = {"tokens": tokens,
                    "tokens_length": tokens_length,
                    "token_ids": token_ids,
                    "char_ids": char_ids,
                    "chars_sequence_length": chars_sequence_length,
                    "gazetteers": gazetteers
                    }

      return (features, None)

    return evaluation_input_fn

  distribution_strategy = tf.contrib.distribute.OneDeviceStrategy(tf.DeviceSpec(device_type="GPU", device_index=0))

  config_proto = utils.get_config_proto(
    log_device_placement=hparams.log_device_placement)

  estimator_config = tf.estimator.RunConfig(
    save_checkpoints_secs=20 * 60,  # Save checkpoints every 20 minutes.
    keep_checkpoint_max=5,  # Retain the 10 most recent checkpoints.
    session_config=config_proto,
    model_dir=hparams.out_dir,
    save_summary_steps=10,
    train_distribute=distribution_strategy
  )

  estimator = tf.estimator.Estimator(
    model_fn=model_fn,
    model_dir=hparams.out_dir,
    params=hparams,
    config=estimator_config)

  train_filename = "%s.%s" % ("models/metadata_tagging/data/org_data/arXiv_metadata_corpus_clean", hparams.src)

  metadata_dataset = MetadataDataset(train_filename, py_train_dev_token_vocab, py_tags_vocab)

  # train_dataset = DatasetPartitioner(metadata_dataset, 10, [0, 1, 2, 3, 4, 5, 6, 7])
  # dev_dataset = DatasetPartitioner(metadata_dataset, 10, [8])
  eval_dataset = DatasetPartitioner(metadata_dataset, 100, [98, 99])
  #eval_dataset = DatasetPartitioner(metadata_dataset, 200, [198, 199])

  eval_dataset = list(eval_dataset)
  metadata_dataset = None
  eval_dataset_size = 0#len(eval_dataset)

  print("Done loading dataset with ", len(eval_dataset), " elements")

  title_statistics = {"tp": 0, "fp": 0, "fn": 0}
  author_statistics = {"tp": 0, "fp": 0, "fn": 0}

  with open(hparams.out_dir + "predictions_springer_corpus.txt", mode="w", encoding="utf8") as output_file:
    output_file.write("Header: Token Tag Predicted_Token\n\n")

    results_generator = estimator.predict(create_evaluation_input_fn(eval_dataset),
                                          hooks=[ExtraInitializationHook(lambda: initializer_hack[0])],
                                          yield_single_examples=False)

    #print("Remaining elements: ", len(eval_dataset))

    #print("Predictions are done !")

    #batch = eval_dataset[:120]
    #eval_dataset = eval_dataset[120:]


    dataset_offset = 0
    for idx, result in enumerate(results_generator):
      batch_result_sequence_ids = result["output_sequence_ids"]

      number_of_predicted_items = len(batch_result_sequence_ids)

      batch_tags, batch_tokens = zip(*eval_dataset[dataset_offset:dataset_offset+number_of_predicted_items]) #*batch)
      #batch_tokens = batch_tags
      dataset_offset = dataset_offset + number_of_predicted_items

      for batch_index, result_sequence_ids in enumerate(batch_result_sequence_ids):

        tokens = list(batch_tokens[batch_index])
        tags = list(batch_tags[batch_index])
        predicted_tags = np.argmax(result_sequence_ids, -1)

        #predicted_tags = batch_tags

        #print("tags: ", tags)
        #print("predicted_tags: ", predicted_tags)

        tp_title, fp_title, fn_title, _, __, ___ = exact_match_stat(tags, predicted_tags,
                                                                               py_tags_vocab["I-TITLE"],
                                                                               py_tags_vocab["I-TITLE"])
        title_statistics["tp"] += tp_title
        title_statistics["fp"] += fp_title
        title_statistics["fn"] += fn_title

        #print("title f1: ", f1_title)

        # Author names
        tp_author, fp_author, fn_author, _, __, ___ = exact_match_stat(tags, predicted_tags,
                                                                                  py_tags_vocab["I-AUTHOR"],
                                                                                  py_tags_vocab["E-AUTHOR"])

        author_statistics["tp"] += tp_author
        author_statistics["fp"] += fp_author
        author_statistics["fn"] += fn_author

        #print("author f1: ", f1_author)

        sb = []
        for token, tag, predicted_tag in zip(tokens, tags, predicted_tags):
          sb.append(token + "\t" + reverse_py_tags_vocab[tag] + "\t" + reverse_py_tags_vocab[predicted_tag])
        sb.append("\n")

        output_file.write("\n".join(sb))
        eval_dataset_size = eval_dataset_size + 1

  title_statistics["precision"], title_statistics["recall"], title_statistics["f1"] = get_precision_recall_f1(title_statistics["tp"], title_statistics["fp"], title_statistics["fn"])
  author_statistics["precision"], author_statistics["recall"] , author_statistics["f1"] = get_precision_recall_f1(author_statistics["tp"], author_statistics["fp"], author_statistics["fn"])

  print("title precision: ", title_statistics["precision"])
  print("title recall: ", title_statistics["recall"])
  print("title f1: ", title_statistics["f1"])

  print("author precision: ", author_statistics["precision"])
  print("author recall: ", author_statistics["recall"])
  print("author f1: ", author_statistics["f1"])

  print("eval_dataset_size: ", eval_dataset_size)