from __future__ import print_function

import os
import time

import numpy as np
import tensorflow as tf

from tfDeepNLP.gazetteers.gazetteers_lookup import lookup_gazetteers
from tfDeepNLP.models.seq2tag.dataset_iterator.ner_iterator import get_iterator
from tfDeepNLP.models.seq2tag.sequence_tagging_model import SequenceTaggingModel
from tfDeepNLP.utils import misc_utils as utils
from tfDeepNLP.utils.vocab_utils import create_python_vocab_table, create_vocab_table

utils.check_tensorflow_version()

__all__ = [
  "s2t_predict"
]


def s2t_predict(hparams,
                scope=None,
                single_cell_fn=None):
  # Logging and output
  log_file = os.path.join(hparams.out_dir, "log_%d" % time.time())
  log_f = tf.gfile.GFile(log_file, mode="a")
  utils.print_out("# log_file=%s" % log_file, log_f)

  # Select model
  model_creator = SequenceTaggingModel

  """
  Get ready for training! 
  """

  token_vocab_train_dev_file = hparams.token_vocab_train_dev_file
  token_vocab_inference_file = hparams.token_vocab_inference_file  # posixpath.join(hparams.work_dir, hparams.token_vocab_inference_file)
  character_level_vocab_file = hparams.char_vocab_file
  tag_vocab_file = hparams.tag_vocab_file

  py_train_dev_token_vocab = create_python_vocab_table(token_vocab_train_dev_file)
  py_inference_token_vocab = create_python_vocab_table(token_vocab_inference_file)
  py_character_level_vocab = create_python_vocab_table(character_level_vocab_file)
  py_tags_vocab = create_python_vocab_table(tag_vocab_file)

  model_fn = model_creator(
    hparams,
    py_train_dev_token_vocab,
    py_inference_token_vocab,
    py_tags_vocab,
    py_character_level_vocab,
    unk_word_idx=py_train_dev_token_vocab["$UNK$"],
    unk_char_idx=py_character_level_vocab["$UNK$"],
    ntags=len(py_tags_vocab),
    single_cell_fn=single_cell_fn)

  def serving_input_receiver_fn():
    """An input receiver that expects a serialized tf.Example."""
    input_sentences_placeholder = [
      "Poster Presentation Discovery and validation of immunological biomarkers using an advanced flow cytometry platform <\\n> <\\n> <\\n> Villanova F , Di Meglio P , Inokuma M , Maino V C , Nestle F O <\n> <\n> Introduction Immune mediated inflammatory diseases ( IMIDs ) , such as rheumatoid arthritis , psoriasis and Crohn ’s disease are a group of chronic conditions sharing common inflammatory pathways . <eos> Inflammatory cytokine imbalance is central in IMID pathophysiology suggesting a key role for CD4 + T cells in shaping the immune response via cytokine production . <eos> Given the phenotypic and functional complexity of the T-cell compartment , multiparameter flow cytometry represents one of the best experimental tools to study T cell biology . <eos> Aim The aim of our immuno - monitoring project is to develop a flow cytometry - platform to define and validate immune cell signatures in inflammatory diseases . <eos> In particular we want to simultaneously analyze the frequency and functional state ( cytokine production , STAT protein phosphorylation ) of T helper cell subsets relevant for IMIDs . <eos> Patients and methods Multicolour flow cytometry panels were developed for the simultaneous detection of up to 10 parameters both at surface and intracellular levels . <eos> PBMC are stimulated with PMA / Ionomycin and then stained in 96 well plates containing lyophilized reagents ( lyoplates ) to ensure consistency and reproducibility between different primary samples . <eos> Cellular activation and signalling was assessed by phosphoflow cytometry after ex vivo stimulation"]

    input_dataset = tf.data.Dataset.from_tensors(input_sentences_placeholder)
    inference_token_vocab = create_vocab_table(token_vocab_inference_file)
    character_level_vocab = create_vocab_table(character_level_vocab_file)
    dataset_iterator = get_iterator(
      dataset=input_dataset,
      input_batch_size=tf.shape(input_sentences_placeholder, out_type=tf.int64)[0],
      word_table=inference_token_vocab,
      char_table=character_level_vocab,
      unk_word_idx=py_train_dev_token_vocab[hparams.unk],
      char_level_representation_max_len=hparams.char_level_representation_max_len,
      num_buckets=None,
      shuffle=False,
      label_dataset=False,
      split_words=True,
      repeat=False,
      prefetch=False,
      perform_batching=True,
    )

    initializer, (tokens, tokens_length, token_ids, char_ids, chars_sequence_length, _) = dataset_iterator

    token_ids = tf.Print(token_ids, [token_ids], message="token_ids: ", summarize=1000)

    with tf.control_dependencies([initializer]):
      gazetteer_feature_files = [i for i in hparams.gazetteer_feature_files]
      gazetteers = lookup_gazetteers(gazetteer_feature_files, tokens)

      features = {"tokens": tokens,
                  "tokens_length": tokens_length,
                  "token_ids": token_ids,
                  "char_ids": char_ids,
                  "chars_sequence_length": chars_sequence_length,
                  "gazetteers": gazetteers
                  }

    return (features, None)

  distribution_strategy = tf.contrib.distribute.OneDeviceStrategy(tf.DeviceSpec(device_type="GPU", device_index=0))

  config_proto = utils.get_config_proto(
    log_device_placement=hparams.log_device_placement)

  estimator_config = tf.estimator.RunConfig(
    save_checkpoints_secs=20 * 60,  # Save checkpoints every 20 minutes.
    keep_checkpoint_max=5,  # Retain the 10 most recent checkpoints.
    session_config=config_proto,
    model_dir=hparams.out_dir,
    save_summary_steps=1,
    train_distribute=distribution_strategy
  )

  estimator = tf.estimator.Estimator(
    model_fn=model_fn,
    model_dir=hparams.out_dir,
    params=hparams,
    config=estimator_config)

  results = estimator.predict(serving_input_receiver_fn, yield_single_examples=False)

  for result in results:
    print("result:", np.argmax(result["output_sequence_ids"], axis=2))
    print("result:", np.max(result["output_sequence_logits"], axis=2))
    break
