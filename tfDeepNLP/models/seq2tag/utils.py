
def get_segments(seq, start_idx, end_idx):
  # consider pattern 1112, 1222
  i = 0
  segments = []
  current_start, current_end = -1, -1
  while i < len(seq):
    if seq[i] == start_idx:
      # check previous position
      if start_idx != end_idx and i > 0 and seq[i - 1] == end_idx:
        segments.append([current_start, i])
        current_start, current_end = -1, -1
      if current_start < 0:
        current_start = i
      current_end = i + 1
    if seq[i] == end_idx:
      current_end = i + 1
    if seq[i] not in [start_idx, end_idx]:
      if current_start >= 0 and seq[i - 1] == end_idx:
        segments.append([current_start, i])
        current_start, current_end = -1, -1
    i += 1
  if current_start >= 0 and seq[-1] == end_idx:
    segments.append([current_start, current_end])
  # print([''.join(seq[a:b]) for a, b in segments])
  return segments


def get_precision_recall_f1(tp, fp, fn):
  precision = tp / float((tp + fp)) if tp + fp > 0 else 0
  recall = tp / float((tp + fn)) if tp + fn > 0 else 0
  f1 = 2 * precision * recall / (precision + recall) if precision + recall > 0 else 0
  return precision, recall, f1


def exact_match_stat(labels, labels_pred, start_idx, end_idx):
  # need tp, fp, fn
  true_segs = get_segments(labels, start_idx, end_idx)
  pred_segs = get_segments(labels_pred, start_idx, end_idx)
  tp = sum([1 for i in pred_segs if i in true_segs])
  fp = len(pred_segs) - tp
  fn = sum([1 for i in true_segs if i not in pred_segs])
  precision, recall, f1 = get_precision_recall_f1(tp, fp, fn)
  return tp, fp, fn, precision, recall, f1
