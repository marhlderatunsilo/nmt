import tensorflow as tf

from tfDeepNLP.models.seq2tag.dataset_iterator.iterator_utils import BatchedInput


def get_iterator(dataset,
                 input_batch_size,
                 word_table,
                 char_table,
                 unk_word_idx,
                 char_level_representation_max_len,
                 num_buckets=1,
                 shuffle=False,
                 label_dataset=None,
                 num_splits=None,
                 output_buffer_size=1000,
                 num_parallel_calls=36,
                 split_words=True,
                 repeat=False,
                 prefetch=True,
                 perform_batching=True):
  # Create tensor constant with uppercase letters
  ucons = [chr(i) for i in range(65, 91)]
  upchars = tf.constant(ucons, dtype=tf.string)

  # Create lookup table for uppercase letters
  upcharslut = tf.contrib.lookup.index_table_from_tensor(mapping=upchars, num_oov_buckets=1,
                                                         default_value=-1, name="upcharslut")

  if repeat:
    dataset = dataset.repeat()

  if shuffle:
    dataset = dataset.shuffle(
      output_buffer_size,
      seed=None
    )

  if label_dataset:
    # assert tags_idx is not None

    def reorder_data(tags, words):
      return (words, tags)

    dataset = dataset.map(reorder_data, num_parallel_calls=num_parallel_calls)

  # Splits a sentence into words seperated by a " " (space)
  if (not label_dataset) and split_words:
    def split_words(sentence, *unused):
      print("sentence: ", sentence)
      split_words = tf.string_split(sentence, delimiter=" ").values
      return (split_words, *unused)

    dataset = dataset.map(split_words, num_parallel_calls=num_parallel_calls)

  if word_table:
    def initial_lookup(words, *unused):
      word_ids = tf.cast(word_table.lookup(words), tf.int32)

      # For debugging
      # word_ids = tf.Print(word_ids, [word_ids, words], message="word_and_ids!?:",
      #                    summarize=100)

      # word_and_ids = tuple([, words])

      return (word_ids, words, *unused)

    dataset = dataset.map(initial_lookup, num_parallel_calls=num_parallel_calls)

    def oov_word_lookup_fn(word_id_word_upcharinds_splitchars_wordlength):
      # Create tensor with lower case chars
      lcons = [chr(i) for i in range(97, 123)]
      lchars = tf.constant(lcons, dtype=tf.string)

      # Manually unpack tuple (tf_while / tf_map does not seem to support this )
      word_id = word_id_word_upcharinds_splitchars_wordlength[0]
      word = word_id_word_upcharinds_splitchars_wordlength[1]
      upcharinds = word_id_word_upcharinds_splitchars_wordlength[2]
      splitchars = word_id_word_upcharinds_splitchars_wordlength[3]
      word_length = word_id_word_upcharinds_splitchars_wordlength[4]

      # Cut out the padding of the word
      upcharinds = tf.slice(upcharinds, [0], [word_length])
      splitchars = tf.slice(splitchars, [0], [word_length])

      # For debugging
      # word_id = tf.Print(word_id, [word_id, word], message="word_ids and word!?:",
      #                      summarize=10)

      # Create new word if word was of the vocab, otherwise return original word
      word = tf.cond(tf.equal(word_id, tf.cast(unk_word_idx, tf.int32)),
                     lambda: tf.reduce_join(
                       tf.map_fn(lambda x: tf.cond(x[0] > 25, lambda: x[1], lambda: lchars[x[0]]),
                                 (upcharinds, splitchars),
                                 dtype=tf.string)),
                     lambda: word)

      return word

  # Splits a word into its constituent chars
  # This function also pads the char sequences to the length with the longest word
  def chars_splitter_fn(word_and_max_length):
    word = word_and_max_length[0]
    max_length = word_and_max_length[1]

    unicode_word_length = tf.strings.length(word, unit="UTF8_CHAR")
    ascii_word_length = tf.strings.length(word, unit="BYTE")

    def split_unicode_string_to_characters():

      # Checks two bytes at a time and reports if the bytes correspond to one unicode character or two ascii chars
      # We check two bytes at a time, but we cant check two bytes at last index
      def check_substring(index):
        def check_two_bytes():
          two_bytes = tf.strings.substr(word, index, 2)
          utf_word_length = tf.strings.length(two_bytes, unit="UTF8_CHAR")
          is_unicode = tf.equal(utf_word_length, 1)

          return tf.cond(is_unicode, true_fn=lambda: two_bytes,
                         false_fn=lambda: tf.strings.substr(word, index, 1)), tf.logical_not(is_unicode)

        def check_last_byte():
          def is_previous_unicode():
            previous_two_bytes = tf.strings.substr(word, index - 2, 2)
            utf_word_length = tf.strings.length(previous_two_bytes, unit="UTF8_CHAR")
            return tf.equal(utf_word_length, 1)

          return (tf.strings.substr(word, index, 1),
                  tf.cond(ascii_word_length > 2, true_fn=is_previous_unicode, false_fn=lambda: False))

        return tf.cond(tf.equal(index, ascii_word_length - 1),
                       true_fn=check_last_byte, false_fn=check_two_bytes)

      ascii_indexes = tf.range(0, ascii_word_length)

      unicode_characters_and_trash, is_not_unicode = tf.map_fn(check_substring, ascii_indexes,
                                                               dtype=(tf.string, tf.bool))
      is_not_unicode_ta = tf.TensorArray(dtype=bool, size=ascii_word_length, clear_after_read=False)

      def repair_is_not_unicode():
        counter0 = tf.constant(0)
        condition = lambda i, _: i < is_not_unicode_ta.size() - 1

        def body(i, is_not_unicode_ta):
          is_not_double_zeroes = tf.reduce_any([is_not_unicode[i], is_not_unicode[i+1]])

          is_not_unicode_ta = tf.cond(is_not_double_zeroes,
                                      true_fn=lambda: is_not_unicode_ta.write(i, is_not_unicode[i]),
                                      false_fn=lambda: is_not_unicode_ta.write(i, is_not_unicode[i]).write(i + 1, True)) #

          return tf.cond(is_not_double_zeroes, true_fn=lambda: i+1, false_fn=lambda: i+2), is_not_unicode_ta

        return tf.while_loop(condition, body, loop_vars=[counter0, is_not_unicode_ta], parallel_iterations=1)[1]

      is_not_unicode = repair_is_not_unicode().stack()

      is_not_trash = tf.concat([[True], is_not_unicode[:-1]], axis=0)

      unicode_character_indexes = tf.where(is_not_trash)

      # unicode_characters_and_trash = tf.Print(unicode_characters_and_trash,
      #                                         [unicode_characters_and_trash, is_not_trash,
      #                                          tf.shape(unicode_characters_and_trash)],
      #                                         message="unicode_characters_and_trash: ",
      #                                         summarize=15)

      chars_split = tf.gather_nd(unicode_characters_and_trash, unicode_character_indexes)

      # joined_with_separators = tf.strings.reduce_join([word, character_separator_joined], axis=[0, 1])

      # chars_split = tf.Print(chars_split, [chars_split, unicode_word_length, ascii_word_length, tf.shape(chars_split)],
      #                        message="CHARS: ",
      #                        summarize=15)

      return chars_split

    chars_split = tf.cond(tf.equal(ascii_word_length, unicode_word_length),
                          true_fn=lambda: tf.string_split([word], delimiter="").values,
                          false_fn=split_unicode_string_to_characters)

    chars_split = chars_split[:max_length]  # Cut sequence of chars to max length
    num_chars = tf.size(chars_split)
    # Pad split chars
    chars_split = tf.concat([chars_split, tf.fill([max_length - num_chars], value=tf.constant(""))], axis=0)

    # For debugging
    # chars_split = tf.Print(chars_split, [chars_split, num_chars, max_length], message="char_ids_lookup!?:", summarize=100)

    return chars_split

  # Calculates the length of the given word
  def chars_size_fn(word):

    # chars_split = tf.string_split([word], delimiter="").values
    # num_chars = tf.size(chars_split)
    num_chars = tf.strings.length(word, unit="UTF8_CHAR")

    return num_chars

  def map_word_char_idx(word_ids, words, *unused):

    # Count words
    word_count = tf.size(word_ids)

    # Calculate length of char ids
    char_ids_len = tf.map_fn(chars_size_fn, words,
                             dtype=(tf.int32), parallel_iterations=1, back_prop=False)

    char_ids_len = tf.minimum(char_ids_len, char_level_representation_max_len)

    # Find the max length
    max_length = tf.reduce_max(char_ids_len)

    # Create padded char sequences
    chars = tf.map_fn(chars_splitter_fn, (words, tf.fill([word_count], value=max_length)),
                      dtype=(tf.string), parallel_iterations=1, back_prop=False)

    # chars = tf.Print(chars, [tf.shape(chars),], message="chars shape!?:",
    #                        summarize=100)

    char_ids = tf.cast(char_table.lookup(chars), tf.int32)

    # splitchars = tf.string_split(tf.reshape(s, [-1]), delimiter="").values
    upcharinds = upcharslut.lookup(chars)

    words_with_subsitutes = tf.map_fn(oov_word_lookup_fn,
                                      tuple([word_ids, words, upcharinds, chars, char_ids_len]),
                                      dtype=tf.string, parallel_iterations=1, back_prop=False)

    word_ids = tf.cast(word_table.lookup(words_with_subsitutes), tf.int32)

    if char_table:
      return (words, word_count, word_ids, char_ids, char_ids_len, *unused)

    return (words, word_count, word_ids, *unused)

  dataset = dataset.map(map_word_char_idx, num_parallel_calls=num_parallel_calls)

  # If we have labels look them up here
  # if label_dataset:

  # Transform tags to list if dict
  # if isinstance(tags_idx, dict):
  #     tags_idx = sorted(tags_idx.items(), key=lambda k: k[1])
  #
  # labels_table = tf.contrib.lookup.index_table_from_tensor(mapping=tags_idx, num_oov_buckets=1,
  #                                                          default_value=-1)
  #
  # def look_up_labels(words, word_ids, word_count, labels):
  #
  #     labels = tf.cast(labels_table.lookup(labels), tf.int32)
  #
  #     return words, word_ids, word_count, labels
  #
  # dataset = dataset.map(look_up_labels)

  if char_table:
    padded_shapes = [
      tf.TensorShape([None]),  # raw string
      tf.TensorShape([]),  # word_ids_len
      tf.TensorShape([None]),  # word_ids
      tf.TensorShape([None, None]),  # char_ids
      tf.TensorShape([None]),  # char_ids_len
    ]
    padding_values = [
      "",
      -1,  # word_ids_len -- unused
      0,  # word_ids
      0,  # char_ids
      -1,  # char_ids_len -- unused
    ]
  else:
    padded_shapes = [
      tf.TensorShape([None]),
      tf.TensorShape([]),  # word_ids_len
      tf.TensorShape([None]),  # word_ids
    ]
    padding_values = [
      "",
      0,  # word_ids_len -- unused
      0,  # word_ids
    ]

  # If label_dataset: We will have to pad the labels as well
  if label_dataset:
    padded_shapes.append(tf.TensorShape([None]))
    padding_values.append(tf.constant(0, dtype=tf.int32))

  if perform_batching:
    batched_dataset = dataset.padded_batch(
      input_batch_size,
      # The 2 entries that needs to be padded are the word_ids and char_ids;
      # these have unknown-length vectors.
      padded_shapes=tuple(padded_shapes),
      padding_values=tuple(padding_values))
  else:
    batched_dataset = dataset

  def split_batching_func(x):
    if char_table:
      padded_shapes = [
        tf.TensorShape([None, None]),  # raw string
        tf.TensorShape([None]),  # word_ids_len
        tf.TensorShape([None, None]),  # word_ids
        tf.TensorShape([None, None, None]),  # char_ids
        tf.TensorShape([None, None]),  # char_ids_len
      ]
      padding_values = [
        "",
        -1,  # word_ids_len -- unused
        0,  # word_ids
        0,  # char_ids
        -1,  # char_ids_len -- unused
      ]
    else:
      padded_shapes = [
        tf.TensorShape([None, None]),
        tf.TensorShape([None]),  # word_ids_len
        tf.TensorShape([None, None]),  # word_ids
      ]
      padding_values = [
        "",
        0,  # word_ids_len -- unused
        0,  # word_ids
      ]

    # If label_dataset: We will have to pad the labels as well
    if label_dataset:
      padded_shapes.append(tf.TensorShape([None, None]))
      padding_values.append(tf.constant(0, dtype=tf.int32))

    return x.padded_batch(
      num_splits,
      padded_shapes=tuple(padded_shapes),
      padding_values=tuple(padding_values))

  def key_func_split(unused_1, word_ids_len, unused_2, *unsued):
    bucket_width = 5 * input_batch_size

    # Bucket sentence pairs by the length of their source sentence and target
    # sentence.
    bucket_id = tf.reduce_sum(word_ids_len) // bucket_width
    return tf.to_int64(tf.minimum(num_buckets, bucket_id))

  def reduce_func_split(unused_key, windowed_data):
    return split_batching_func(windowed_data)

  if num_splits and perform_batching:
    print("!! SPLITTING !!")
    if num_buckets > 1:
      batched_dataset = batched_dataset.apply(
        tf.contrib.data.group_by_window(
          key_func=key_func_split, reduce_func=reduce_func_split, window_size=num_splits))
    else:
      batched_dataset = split_batching_func(batched_dataset)

  if prefetch:
    batched_dataset = batched_dataset.prefetch(output_buffer_size)

  batched_iter = batched_dataset.make_initializable_iterator()

  initializer = tf.group(batched_iter.initializer, name="inference_dataset_initializer")

  if label_dataset and char_table:
    (words, word_ids_len, word_ids, char_ids, char_ids_len, labels) = (batched_iter.get_next())
  elif char_table:
    (words, word_ids_len, word_ids, char_ids, char_ids_len) = (batched_iter.get_next())
    labels = None
  else:
    (words, word_ids_len, word_ids) = (batched_iter.get_next())
    char_ids = None
    char_ids_len = None
    labels = None

  return initializer, BatchedInput(words, word_ids_len, word_ids, char_ids, char_ids_len, labels)
