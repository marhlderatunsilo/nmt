import collections

import tensorflow as tf
from tfDeepNLP.models.seq2tag.constants import *


class BatchedInput(collections.namedtuple("BatchedInput",
                                          ("words",
                                           "word_ids_len",
                                           "word_ids",
                                           "char_ids",
                                           "char_ids_len", "labels"))):
  pass


class SplitBatchedInput(collections.namedtuple("SplitBatchedInput",
                                               ("words",
                                                "word_ids_len",
                                                "word_ids",
                                                "char_ids",
                                                "char_ids_len", "labels"))):
  pass


def split_batched_input(batched_input, num_splits):
  _words, _word_ids_len, _word_ids, _char_ids, _char_ids_len, _labels = (
    batched_input.words, batched_input.word_ids_len, batched_input.word_ids,
    batched_input.char_ids, batched_input.char_ids_len, batched_input.labels)

  split_batches = []

  # TODO: NOTICE!!
  # Some of the training ops expect the dimensionality of the time dimension of target_output, i.e. the labels, and
  # tf.reduce_max(target_sequence_length) to match.
  # This is not the case because of the padding created by the global tf.reduce_max(target_sequence_length)
  # (before the split)
  # We therefore clip/slice away the excess padding

  with tf.name_scope("split_batched_input"):
    def create_slicer(i):
      def create_slice():
        words_slice = _words[i]
        word_ids_len_slice = _word_ids_len[i]
        word_ids_slice = _word_ids[i]
        char_ids_slice = _char_ids[i]
        char_ids_len_slice = _char_ids_len[i]
        labels_slice = _labels[i]

        # Cut away excess padding
        words_slice = words_slice[:, :tf.reduce_max(word_ids_len_slice)]
        char_ids_slice = char_ids_slice[:, :tf.reduce_max(word_ids_len_slice), :tf.reduce_max(char_ids_len_slice)]
        labels_slice = labels_slice[:, :tf.reduce_max(word_ids_len_slice)]

        # target_output = tf.Print(target_output, [i, target_output], message="CALCULATED TARGET OUTPUT", f)

        split_batch = SplitBatchedInput(
          words=words_slice,
          word_ids_len=word_ids_len_slice,
          word_ids=word_ids_slice,
          char_ids=char_ids_slice,
          char_ids_len=char_ids_len_slice,
          labels=labels_slice)

        return split_batch

      return create_slice

    first_slice = create_slicer(0)()
    with tf.device(tf.DeviceSpec(device_type="GPU", device_index=0)):
      split_batch = SplitBatchedInput(
        words=tf.identity(first_slice.words),
        word_ids_len=tf.identity(first_slice.word_ids_len),
        word_ids=tf.identity(first_slice.word_ids),
        char_ids=tf.identity(first_slice.char_ids),
        char_ids_len=tf.identity(first_slice.char_ids_len),
        labels=tf.identity(first_slice.labels))

    split_batches.append(split_batch)

    def zeros_slice():
      return SplitBatchedInput(
        words=first_slice.words,
        word_ids_len=first_slice.word_ids_len,
        word_ids=first_slice.word_ids,
        char_ids=first_slice.char_ids,
        char_ids_len=first_slice.char_ids_len,
        labels=first_slice.labels)

    available_batches_count = tf.shape(_words)[0]

    for i in range(1, num_splits):
      split_batch = tf.cond(i < available_batches_count,
                            true_fn=create_slicer(i),
                            false_fn=zeros_slice)

      with tf.device(tf.DeviceSpec(device_type="GPU", device_index=i)):
        split_batch = SplitBatchedInput(
          words=tf.identity(split_batch.words),
          word_ids_len=tf.identity(split_batch.word_ids_len),
          word_ids=tf.identity(split_batch.word_ids),
          char_ids=tf.identity(split_batch.char_ids),
          char_ids_len=tf.identity(split_batch.char_ids_len),
          labels=tf.identity(split_batch.labels))

        split_batches.append(split_batch)

    return split_batches


def get_resources(hparams, mapping_idx, load_pretrained=True):
  assert hparams.tokens or hparams.chars, "should use at least words or/and chars embeddings"

  vocab_tags = mapping_idx[hparams.tag_scheme]
  vocab_chars = mapping_idx[VOCAB_CHAR] if hparams.chars else {}
  vocab_words = mapping_idx[hparams._word_opt[4]] if hparams.tokens else {}

  if hparams.tokens and load_pretrained:
    pretrained_embeddings = load_trimmed_vectors(hparams.embeddings_filename)
    # assert pretrained_embeddings.shape[0] == mapping_idx[CNT_TRAIN] + mapping_idx[CNT_DEV]
    # if hparams.word_opt in [1, 3]:
    #  pretrained_embeddings = pretrained_embeddings[:mapping_idx[CNT_TRAIN]]
    assert len(vocab_words) - 1 - pretrained_embeddings.shape[0] in [0, mapping_idx[CNT_LEARN]]
  else:
    pretrained_embeddings = None

  return pretrained_embeddings, vocab_words, vocab_tags, vocab_chars


def get_resources_for_graph(hparams, mapping_idx):
  assert hparams.tokens or hparams.chars, "should use at least words or/and chars embeddings"

  vocab_words = None
  vocab_chars = None
  unk_word_idx = None
  unk_char_idx = None

  python_vocab_chars = [i[0] for i in sorted(mapping_idx[VOCAB_CHAR].items(), key=lambda x: x[1])]
  unk_char_idx = len(python_vocab_chars) - 1

  # Create tensorflow lookup table
  vocab_chars = tf.contrib.lookup.index_table_from_tensor(tf.constant(python_vocab_chars, tf.string),
                                                          default_value=unk_char_idx, name="vocab_chars")

  python_vocab_words = [i[0] for i in sorted(mapping_idx[hparams._word_opt[4]].items(), key=lambda x: x[1])]
  unk_word_idx = len(python_vocab_words) - 1

  # Create tensorflow lookup table
  vocab_words = tf.contrib.lookup.index_table_from_tensor(tf.constant(python_vocab_words, tf.string),
                                                          default_value=unk_word_idx, name="vocab_words")

  python_vocab_tags = [i[0] for i in sorted(mapping_idx[VOCAB_TAG].items(), key=lambda x: x[1])]

  # Create tensorflow lookup table
  vocab_tags = tf.contrib.lookup.index_table_from_tensor(tf.constant(python_vocab_tags, tf.string),
                                                         default_value=-1, name="vocab_tags")

  return vocab_words, vocab_chars, vocab_tags, unk_word_idx, unk_char_idx
