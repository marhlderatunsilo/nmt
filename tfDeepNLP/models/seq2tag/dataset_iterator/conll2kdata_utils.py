"""build vocab from the data and extract trimmed glove vectors"""
import argparse
import json
import os
import re
from collections import defaultdict
from itertools import chain
from random import randint, shuffle, uniform

import numpy as np
from sklearn.metrics import classification_report
from sklearn.preprocessing import LabelBinarizer
# from config import Config
from tfDeepNLP.models.seq2tag.constants import *

from tfDeepNLP.models.seq2tag.legacy.config import Config
from tfDeepNLP.utils.common import get_logger


def reverse_dict(d):
  return {v: k for k, v in d.items()}


def is_a_number(w):
  pat = re.compile(r'[-./,\s]')
  return re.sub(pat, '', w).isdigit()


def load_text_input(fn_text):
  sentences = []
  with open(fn_text) as fin:
    sentence = ''
    while True:
      line = fin.readline().strip()
      if line:
        sentence += ' ' + line
        if line[-1] == '.':
          if len(sentence) > 20:
            sentences.append(sentence)
          sentence = ''
      else:
        break
  return sentences


def iob2io(tag):
  if '-' in tag:
    return tag.split('-')[1]
  return tag


def iob2(tags):
  """
  Check that tags have a valid IOB format.
  Tags in IOB1 format are converted to IOB2.
  """
  for i, tag in enumerate(tags):
    if tag == 'O':
      continue
    split = tag.split('-')
    if len(split) != 2 or split[0] not in ['I', 'B']:
      return False
    if split[0] == 'B':
      continue
    elif i == 0 or tags[i - 1] == 'O':  # conversion IOB1 to IOB2
      tags[i] = 'B' + tag[1:]
    elif tags[i - 1][1:] == tag[1:]:
      continue
    else:  # conversion IOB1 to IOB2
      tags[i] = 'B' + tag[1:]
  return True


def load_sentences_conll2003(filename):
  """
  Load sentences. A line must contain at least a word and its tag.
  Sentences are separated by empty lines.
  :return list of sentence, where each sentence is a list of tuples (word, ..., tag)
  """
  sentence = []
  with open(filename, encoding='utf-8') as fin:
    for line in fin:
      if line.startswith('-DOCSTART-'):
        continue
      if not line.strip():
        if len(sentence) > 0:
          yield sentence
        sentence = []
      else:
        word = line.strip().split()
        assert len(word) >= 2
        if is_a_number(word[0]):
          word[0] = NUM
        sentence.append(word)
  if len(sentence) > 0:
    yield sentence


def load_sentences_conll2002(filename):
  """
  Load sentences. A line must contain at least a word and its tag.
  Sentences are separated by empty lines.
  :return list of sentence, where each sentence is a list of tuples (word, ..., tag)
  """
  sentence = []
  with open(filename) as fin:
    for line in fin:
      if not line.strip():
        if len(sentence) > 0:
          yield sentence
        sentence = []
      else:
        word = line.replace(",", " ").strip().split()
        assert len(word) >= 2
        if is_a_number(word[0]):
          word[0] = NUM

        word[-1] = word[-1].upper()

        tags_to_ignore = ["B-GPE", "I-GPE", "B-TIM", "I-TIM", "B-ART", "I-ART", "B-EVE",
                          "I-EVE", "B-NAT", "I-NAT"]

        geo_tags = ["B-GEO", "I-GEO"]

        if word[-1] in tags_to_ignore:
          word[-1] = "O"
        elif word[-1] in geo_tags:
          word[-1] = word[-1].replace("GEO", "LOC")

        sentence.append(word)
  if len(sentence) > 0:
    yield sentence


class Dataset(object):
  def __init__(self, filename=None, words_idx={}, chars_idx={}, tags_idx={}, tag_scheme=None, use_raw_input=False,
               augument=False, pos_idx={}, extra_data_filename=None, use_uppercase_chars=True, look_up_ids=True):
    self.filename = filename
    self.extra_data_filename = extra_data_filename
    self.words_idx = words_idx
    self.unk_word_idx = len(words_idx) - 1
    self.chars_idx = chars_idx
    self.unk_char_idx = len(chars_idx) - 1
    self.tags_idx = tags_idx
    self.pos_idx = pos_idx
    self.tag_scheme = tag_scheme
    self.length = None
    self.use_raw_input = use_raw_input
    self.augument = augument
    self.use_uppercase_chars = use_uppercase_chars
    self.look_up_ids = look_up_ids

    if filename:
      self.sentences = list(load_sentences_conll2003(self.filename))

    if self.extra_data_filename and not self.use_raw_input:
      self.sentences += list(load_sentences_conll2002(self.extra_data_filename))

  @staticmethod
  def update_tag_scheme(sentence, tag_scheme):
    """
    Check and update sentence tagging scheme to IOB2.
    Only IO and IOB2 schemes are accepted.
    """
    if tag_scheme == IO:
      tags = [iob2io(w[-1]) for w in sentence]
    elif tag_scheme == IOB2:
      tags = [w[-1] for w in sentence]
      if not iob2(tags):
        s_str = '\n'.join(' '.join(w) for w in sentence)
        raise Exception('Sentences should be given in IOB format!\ncheck: ' + s_str)
    else:
      raise Exception('Unknown tagging scheme!')
    for word, new_tag in zip(sentence, tags):
      word[-1] = new_tag

  def get_word_char_id(self, w):
    if not self.words_idx:
      word_id = w
    else:
      word_id = self.words_idx.get(w, self.words_idx.get(w.lower(), self.unk_word_idx))
    if self.chars_idx:
      char_id = [self.chars_idx.get(c, self.unk_char_idx) for c in w]
      return word_id, char_id
    else:
      return word_id

  def process_raw_input(self, sentence):
    output_idx = []
    for w in sentence:
      word_id = self.words_idx.get(w, self.words_idx.get(w.lower(), self.unk_word_idx))
      if self.chars_idx:
        char_id = [self.chars_idx.get(c, self.unk_char_idx) for c in w]
        output_idx.append((word_id, char_id))
      else:
        output_idx.append(word_id)
    return output_idx

  def __iter__(self):
    """iterator, return index of word, chars etc"""
    si = 0

    for sentence in self.sentences:
      si += 1
      if DEBUG and si > 500:
        raise StopIteration
      self.update_tag_scheme(sentence, self.tag_scheme)
      if self.use_raw_input:
        yield sentence
      else:

        tags = [self.tags_idx[s[-1]] for s in sentence]

        if self.use_uppercase_chars:
          words = [s[0] for s in sentence]
        else:
          words = [s[0].lower() for s in sentence]

        if self.augument and self.use_uppercase_chars:

          def first_lower(s):
            if not s:  # Added to handle case where s == None
              return
            else:
              return s[0].lower() + s[1:]

          enumerated_words_and_tags = list(zip(enumerate(words), tags))
          filtered_word_tags = list(filter(lambda a: a[1] != self.tags_idx["O"], enumerated_words_and_tags))
          number_of_ners = len(filtered_word_tags)

          should_alter = uniform(0, 1)
          if should_alter > 0.7:
            index = randint(0, len(words) - 1)
            words[index] = words[index].capitalize()

          if number_of_ners > 0:
            index = randint(0, number_of_ners - 1)
            index = filtered_word_tags[index][0][0]

            should_alter = uniform(0, 1)

            if should_alter > 0.7:
              words[index] = first_lower(words[index])

        if self.look_up_ids:
          ids = [self.get_word_char_id(word) for word in words]

          yield ids, tags, words
        else:
          # Bug in Tensorflow 1.4
          #words = list(map(lambda x: x.encode('utf-8'), words))
          yield tags, words

  def __len__(self):
    """
    Iterates once over the corpus to set and store length
    """
    if self.length is None:
      self.length = len(self.sentences)
    return self.length


def get_glove_vocab(filename):
  vocab_emb = dict()
  with open(filename, encoding='utf-8') as f:
    for _line in f:
      line = _line.strip().split(' ')
      if not is_a_number(line[0]):
        vocab_emb[line[0]] = np.asarray([float(x) for x in line[1:]])
  return vocab_emb


def load_trimmed_vectors(filename):
  try:
    with np.load(filename) as data:
      return data["embeddings"]

  except:
    raise Exception("{} not exists".format(filename))


def get_input_filename(path, conll=True):
  if not conll:
    train_filename = os.path.join(path, 'train.txt')
    dev_filename = os.path.join(path, 'dev.txt')
    test_filename = os.path.join(path, 'test.txt')
  else:
    train_filename = os.path.join(path, "train.en")
    dev_filename = os.path.join(path, "dev.en")
    test_filename = os.path.join(path, "test.en")
  return train_filename, dev_filename, test_filename


def build_data_conll(config, path_conll2003, glove_filename, path_conll2002=None):
  """
  consider only token and NER tags, ignore other things such as POS tag features
  creates a npz embedding file from trimmed glove vectors and a json file with token/char to index mapping
  """
  mapping_idx = {}
  train_filename, dev_filename, test_filename = get_input_filename(path_conll2003)
  vocab_emb = get_glove_vocab(glove_filename)

  # load train
  char_count = defaultdict(lambda: 0)
  tags_set = set()
  word_count = defaultdict(lambda: 0)
  for sentence in load_sentences_conll2003(train_filename):
    for word in sentence:
      word_count[(word[0])] += 1
      tags_set.add(word[-1])
      for c in word[0]:
        char_count[c] += 1

  # update char mapping
  vocab_chars = [c for c, cnt in char_count.items() if cnt >= config.min_token_freq_to_train]
  mapping_idx[VOCAB_CHAR] = {v: i for i, v in enumerate(vocab_chars + [UNK])}

  # update tag mapping
  tags_io, tags_iob2 = extend_tag_vocab(tags_set)
  mapping_idx[VOCAB_TAG] = {v: i for i, v in enumerate(list(tags_set))}
  mapping_idx[IO] = {v: i for i, v in enumerate(list(tags_io))}
  mapping_idx[IOB2] = {v: i for i, v in enumerate(list(tags_iob2))}

  vocab_word_train, vocab_word_learn = set(), set()
  for w, cnt in word_count.items():
    if w in vocab_emb:
      vocab_word_train.add(w)
    elif w.lower() in vocab_emb:
      vocab_word_train.add(w.lower())
    elif cnt >= config.min_token_freq_to_train:
      vocab_word_learn.add(w)
  mapping_idx[CNT_TRAIN] = len(vocab_word_train)
  mapping_idx[CNT_LEARN] = len(vocab_word_learn)

  # load dev/test
  vocab_word_dev = set()
  for fn in [dev_filename, test_filename]:
    for sentence in load_sentences_conll2003(fn):
      for word in sentence:
        for w in [word[0], word[0].lower()]:
          if (w not in vocab_word_train) and (w in vocab_emb):
            vocab_word_dev.add(w)
            break
  mapping_idx[CNT_DEV] = len(vocab_word_dev)

  assert UNK not in vocab_word_train
  assert UNK not in vocab_word_dev
  assert UNK not in vocab_word_learn

  # update word matrix
  mapping_idx[config._word_opt[0]] = {v: i for i, v in
                                      enumerate(
                                        list(vocab_word_train) + list(vocab_word_dev) + list(vocab_word_learn) + [
                                          UNK])}
  mapping_idx[config._word_opt[1]] = {v: i for i, v in
                                      enumerate(list(vocab_word_train) + list(vocab_word_learn) + [UNK])}
  mapping_idx[config._word_opt[2]] = {v: i for i, v in
                                      enumerate(list(vocab_word_train) + list(vocab_word_dev) + [UNK])}
  mapping_idx[config._word_opt[3]] = {v: i for i, v in
                                      enumerate(list(vocab_word_train) + [UNK])}

  extra_embeddings = set(vocab_emb.keys())
  extra_embeddings -= vocab_word_train
  extra_embeddings -= vocab_word_dev
  # extra_embeddings -= list(vocab_word_learn)
  extra_embeddings -= set([UNK])

  complete_vocabulary = list(vocab_word_train) + list(vocab_word_dev) + list(extra_embeddings)

  # update embeddings
  embeddings = []
  for v in complete_vocabulary:  # list(vocab_word_train) + list(vocab_word_dev):
    embeddings.append(vocab_emb[v])
  np.savez_compressed(config.embeddings_filename, embeddings=np.asarray(embeddings))

  # Add unk token to vocabulary
  complete_vocabulary += [UNK]

  mapping_idx[config._word_opt[4]] = {v: i for i, v in
                                      enumerate(complete_vocabulary)}

  with open(config.idx_filename, 'w', encoding='utf-8') as fin:
    json.dump(mapping_idx, fin)


def extend_tag_vocab(tags_set):
  tags_io = set([iob2io(t) for t in tags_set])
  tags_iob2 = set()
  for t in tags_io:
    if t == 'O':
      tags_iob2.add(t)
    else:
      tags_iob2.add('I-' + t)
      tags_iob2.add('B-' + t)
  return tags_io, tags_iob2


def _pad_sequences(sequences, pad_tok, max_length):
  sequence_padded, sequence_length = [], []
  for seq in sequences:
    seq = list(seq)
    seq_ = seq[:max_length] + [pad_tok] * max(max_length - len(seq), 0)
    sequence_padded += [seq_]
    sequence_length += [min(len(seq), max_length)]
  return sequence_padded, sequence_length


def pad_sequences(sequences, pad_tok, nlevels=1):
  """
  sequences: a generator of list or tuple
  pad_tok: the char to pad with
  :return
      a list of list where each sublist has same length
  """
  if nlevels == 1:
    max_length = max(map(lambda x: len(x), sequences))
    sequence_padded, sequence_length = _pad_sequences(sequences, pad_tok, max_length)

  elif nlevels == 2:
    max_length_word = max([max(map(lambda x: len(x), seq)) for seq in sequences])
    sequence_padded, sequence_length = [], []
    for seq in sequences:
      # all words are same length now
      sp, sl = _pad_sequences(seq, pad_tok, max_length_word)
      sequence_padded += [sp]
      sequence_length += [sl]

    max_length_sentence = max(map(lambda x: len(x), sequences))
    sequence_padded, _ = _pad_sequences(sequence_padded, [pad_tok] * max_length_word, max_length_sentence)
    sequence_length, _ = _pad_sequences(sequence_length, 0, max_length_sentence)
  return sequence_padded, sequence_length


def minibatches(data, minibatch_size):
  """
  Args:
      data: generator of (sentence, tags) tuples
      minibatch_size: (int)
  Returns:
      list of tuples
  """
  data = list(data)

  shuffle(data)

  x_batch, y_batch, z_batch = [], [], []
  for (x, y, z) in data:
    if len(x_batch) == minibatch_size:
      yield x_batch, y_batch, z_batch
      x_batch, y_batch, z_batch = [], [], []

    if type(x[0]) == tuple:
      x = zip(*x)
    x_batch += [x]
    y_batch += [y]
    z_batch += [z]

  if len(x_batch) != 0:
    yield x_batch, y_batch, z_batch


def combine_chunks(chunks):
  if not chunks:
    return chunks
  seq = [chunks[0]]
  for chunk in chunks[1:]:
    if chunk[0] == seq[-1][0] and chunk[1] == seq[-1][2]:
      seq[-1] = (seq[-1][0], seq[-1][1], chunk[2])
    else:
      seq.append(chunk)
  return seq


def get_chunks(seq, vocab_tags, tag_scheme, default_tag="O", combined=True):
  """
  :return list of (chunk_type, chunk_start, chunk_end), NB: can be an empty list!
  :example
      seq = [4, 5, 0, 3]
      vocab_tags = {"B-PER": 4, "I-PER": 5, "B-LOC": 3, "O": 0}
      result = [("PER", 0, 2), ("LOC", 3, 4)]
  """
  default = vocab_tags[default_tag]
  idx_to_tag = reverse_dict(vocab_tags)
  chunks = []
  chunk_type, chunk_start = None, None
  for i, tok in enumerate(seq):
    # End of a chunk
    if tok == default:
      if chunk_type is not None:
        chunks.append((chunk_type, chunk_start, i))
        chunk_type, chunk_start = None, None
    # End of a chunk + start of a chunk
    else:
      if tag_scheme == IOB2:
        tok_chunk_begin_end, tok_chunk_type = idx_to_tag[tok].split('-')
        if chunk_type is None:
          chunk_type, chunk_start = tok_chunk_type, i
        elif tok_chunk_type != chunk_type or tok_chunk_begin_end == "B":
          chunks.append((chunk_type, chunk_start, i))
          chunk_type, chunk_start = tok_chunk_type, i
      elif tag_scheme == IO:
        tok_chunk_type = idx_to_tag[tok]
        if chunk_type is None:
          chunk_type, chunk_start = tok_chunk_type, i
        elif tok_chunk_type != chunk_type:
          chunks.append((chunk_type, chunk_start, i))
          chunk_type, chunk_start = tok_chunk_type, i
  # end condition
  if chunk_type is not None:
    chunk = (chunk_type, chunk_start, len(seq))
    chunks.append(chunk)
  if combined:
    return combine_chunks(chunks)
  else:
    return chunks


def iob_classification_report(y_true, y_pred, default_tag='O'):
  """
  Classification report for a list of IOB-encoded sequences.
  """
  lb = LabelBinarizer()
  y_true_combined = lb.fit_transform(list(chain.from_iterable(y_true)))
  y_pred_combined = lb.transform(list(chain.from_iterable(y_pred)))

  tagset = set(lb.classes_) - {default_tag}
  tagset = sorted(tagset, key=lambda tag: tag.split('-', 1)[::-1])
  class_indices = {cls: idx for idx, cls in enumerate(lb.classes_)}

  return classification_report(
    y_true_combined,
    y_pred_combined,
    labels=[class_indices[cls] for cls in tagset],
    target_names=tagset,
  )


if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='NER related tasks')
  parser.add_argument('-j', '--json', default='hparams/both.json', help='json file for configurations')
  parser.add_argument('-c', '--conll', default="train/",
                      help='location of CoNLL')
  parser.add_argument('-g', '--glove', default=".",
                      help='location of glove file')
  parser.add_argument('-p', '--path', default='.',
                      help='location of working directory')
  ARGS = parser.parse_args()

  logger = get_logger(os.path.join(ARGS.path, '.preprocess'))
  config = Config(ARGS.path, ARGS.json, logger)

  glove_filename = os.path.join(ARGS.glove, "glove.840B.{}d.txt".format(config.dim_word))
  build_data_conll(config, ARGS.conll, glove_filename)
  # config.load_idx()
