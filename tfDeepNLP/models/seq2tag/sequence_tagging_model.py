"""lstm model for NER"""
from __future__ import absolute_import

import tensorflow as tf
import tensorflow_hub as hub
from tensor2tensor.models.research import universal_transformer
from tensorflow.python.layers import core as layers_core
from tensorflow.python.ops import array_ops

from tfDeepNLP.attention.attention import create_attention_mechanism
from tfDeepNLP.models.seq2tag.s2t_base_model import S2TBaseModel
from tfDeepNLP.multi_cells.stack_bidirectional_dynamic_rnn import stack_bidirectional_dynamic_rnn
from tfDeepNLP.rnn_utils import create_rnn_cell
from tfDeepNLP.utils.initializer_utils import get_initializer


class SequenceTaggingModel(S2TBaseModel):

  def __init__(self, hparams,
               train_token_table,
               inference_token_table,
               tags_table,
               chars_table,
               unk_word_idx,
               unk_char_idx,
               ntags,
               single_cell_fn,
               dataset=None,
               is_inference=False):

    self.train_token_table = train_token_table
    self.inference_token_table = inference_token_table
    self.vocab_tags = tags_table
    self.vocab_chars = chars_table
    self.nchars = len(chars_table)
    self.ntags = ntags
    self.unk_word_idx = unk_word_idx
    self.unk_char_idx = unk_char_idx
    self.debug = False
    self.train_loss = None
    self.dataset = dataset
    self.is_inference = is_inference

    super(SequenceTaggingModel, self).__init__(hparams, single_cell_fn)

  def add_token_representations(self, mode, tokens_length, tokens, token_ids, char_ids, char_sequence_length,
                                gazetteers=None):
    """
    Adds word embeddings to self
    """

    if not self.hparams.tokens and not self.hparams.chars:
      raise ValueError("Model should use either words/chars or both")
    if self.hparams.tokens:
      with tf.variable_scope("words"):

        # Some logging of token vocab sizes
        print("len(self.train_token_table): ", len(self.train_token_table))
        print("len(self.inference_token_table): ", len(self.inference_token_table))

        # We want to support multiple ways of handling embeddings, some times we train the embeddings ourselves
        # and sometimes we get the embeddings externally
        if mode != tf.estimator.ModeKeys.PREDICT or self.hparams.embeddings_filename == self.hparams.infer_embeddings_filename:
          self._token_embeddings_train = tf.get_variable("_token_embeddings_train_dev",
                                                         shape=[len(self.train_token_table),
                                                                self.hparams.token_embedding_size],
                                                         initializer=tf.random_uniform_initializer() if self.hparams.train_embeddings else tf.zeros_initializer(),
                                                         dtype=tf.float32,
                                                         trainable=self.hparams.train_embeddings)  #

        else:
          print("!!I went with _token_embeddings_inference!!")
          self._token_embeddings_inference = tf.get_variable("_token_embeddings_inference",
                                                             shape=[len(self.inference_token_table),
                                                                    self.hparams.token_embedding_size],
                                                             # We have to use tf.assign() elsewhere because of constant tensor size limitations
                                                             initializer=tf.zeros_initializer(),
                                                             dtype=tf.float32,
                                                             trainable=self.hparams.train_embeddings)

        if mode != tf.estimator.ModeKeys.PREDICT or self.hparams.embeddings_filename == self.hparams.infer_embeddings_filename:
          self._token_embeddings = self._token_embeddings_train
        else:
          self._token_embeddings = self._token_embeddings_inference

        pretrained_token_embeddings = tf.nn.embedding_lookup(self._token_embeddings, ids=token_ids,
                                                             name="token_embeddings")

    if self.hparams.chars:
      with tf.variable_scope("character_level"):
        # randomly initialised with default initializer
        _char_embeddings = tf.get_variable(name="_char_embeddings", dtype=tf.float32,
                                           shape=[self.nchars, self.hparams.char_embedding_size])
        s = tf.shape(char_ids)
        char_ids = tf.reshape(char_ids, shape=[-1, s[-1]])

        # shape = (batch size, max length of sentence, max length of word, dim_char)
        char_embeddings = tf.nn.embedding_lookup(_char_embeddings, char_ids, name="char_embeddings")

        # bi-lstm on chars, need 2 instances of cells
        with tf.variable_scope("fw_cell", initializer=get_initializer(
            self.hparams.rnn_init_op, self.hparams.random_seed, self.hparams.init_weight)):
          cells_fw = self.build_char_level_cell(self.hparams, mode)
        with tf.variable_scope("bw_cell", initializer=get_initializer(
            self.hparams.rnn_init_op, self.hparams.random_seed, self.hparams.init_weight)):
          cells_bw = self.build_char_level_cell(self.hparams, mode)

        # shape: (batch_size * max length of sentence)
        char_sequence_length = tf.reshape(char_sequence_length, shape=[-1])
        _, output_state_fw, output_state_bw = stack_bidirectional_dynamic_rnn(cells_fw, cells_bw,
                                                                              char_embeddings,
                                                                              sequence_length=char_sequence_length,
                                                                              dtype=tf.float32,
                                                                              parallel_iterations=self.hparams.parallel_iterations,
                                                                              swap_memory=True,
                                                                              num_bi_residual_layers=self.hparams.char_rnn_num_residual_layers)

        # axis = -1, concat at axis of shape[-1]
        char_level_representations = tf.concat([output_state_fw[-1].c, output_state_bw[-1].c], axis=-1)

        # shape = (batch, max length of sentence, 2 * char hidden size)
        char_level_representations = tf.reshape(char_level_representations,
                                                shape=[-1, s[1], 2 * self.hparams.char_rnn_num_units])

    # Combine (Concat different embeddings / features)
    if self.hparams.tokens and self.hparams.chars:
      token_representation_components = [pretrained_token_embeddings,
                                         char_level_representations]  # , identity_token_embeddings
    elif self.hparams.chars:
      token_representation_components = [char_level_representations]  # , identity_token_embeddings

    if self.hparams.gazetteers and gazetteers is not None:
      print("gazetteers addeed to token_representation_components")
      token_representation_components.append(gazetteers)


    if self.hparams.add_elmo_representations:

      if len(tf.get_collection(
          "SHARED_ELMO_INSTANCE_COLLECTION",
          scope=None
      )) == 0:

        elmo = hub.Module("https://tfhub.dev/google/elmo/2", name="ELMO", trainable=True)

        tf.add_to_collection(
          "SHARED_ELMO_INSTANCE_COLLECTION",
          elmo
        )

      elmo = tf.get_collection(
        "SHARED_ELMO_INSTANCE_COLLECTION",
        scope=None
      )[0]

      elmo_representations = elmo(
        inputs={
          "tokens": tokens,
          "sequence_len": tokens_length
        },
        signature="tokens",
        as_dict=True)["elmo"]

      token_representation_components.append(elmo_representations)

    print("token_representation_components:", token_representation_components)

    token_representations = tf.concat(token_representation_components, axis=-1)

    # Perform dropout on the entire input representation:
    # Dropout will here allow the model to see input with e.g. little character level representation,
    # no gazetteers, or little token level representation.
    # This will hopefully prevent overfitting to gazetteer features and character level features

    token_representations = tf.nn.dropout(token_representations,
                                          keep_prob=1 - self.hparams.token_representation_dropout if mode == tf.estimator.ModeKeys.TRAIN else 1)

    return token_representations

  def add_logits(self, hparams, mode, token_representations, sequence_lengths):
    with tf.variable_scope("token_bidirectional", reuse=tf.AUTO_REUSE):
      input_memory = token_representations

      print("memory shape: ", input_memory.get_shape()[-1].value)

      if hparams.token_level_architecture == "RNN":

        # Construct forward and backward cells
        with tf.variable_scope("fw_encoder_cell", initializer=get_initializer(
            hparams.rnn_init_op, hparams.random_seed, hparams.init_weight)):
          encoder_cells_fw = self._build_token_level_encoder_cell(hparams, mode)
        with tf.variable_scope("bw_encoder_cell", initializer=get_initializer(
            hparams.rnn_init_op, hparams.random_seed, hparams.init_weight)):
          encoder_cells_bw = self._build_token_level_encoder_cell(hparams, mode)

        with tf.variable_scope("encoder"):
          mid_output, _, __ = stack_bidirectional_dynamic_rnn(encoder_cells_fw, encoder_cells_bw,
                                                              token_representations,
                                                              sequence_length=sequence_lengths,
                                                              dtype=tf.float32,
                                                              swap_memory=True)

        with tf.variable_scope("decoder_cell", initializer=get_initializer(
            hparams.rnn_init_op, hparams.random_seed, hparams.init_weight)):
          decoder_cells = self._build_token_level_decoder_cell(hparams, mode)

        with tf.variable_scope("decoder_attention"):
          # attention_mechanism_input = create_attention_mechanism(
          #   hparams.attention, self.hparams.token_rnn_num_units, input_memory, sequence_lengths, name="input_attention")

          self_attention_mechanism = create_attention_mechanism(
            hparams.attention, self.hparams.token_rnn_num_units, mid_output, sequence_lengths, name="self_attention")

          decoder_cells[0] = tf.contrib.seq2seq.AttentionWrapper(
            decoder_cells[0],
            [self_attention_mechanism],
            # attention_layer_size=[self.hparams.token_rnn_num_units, self.hparams.token_rnn_num_units * 2],
            # output_attention=False,
            # alignment_history=False,
            name="decoder_attention_wrapper")

        # Output projection
        with tf.variable_scope("decoder"):
          decoded_output, _ = tf.nn.dynamic_rnn(tf.nn.rnn_cell.MultiRNNCell(decoder_cells), mid_output,
                                                sequence_length=sequence_lengths,
                                                dtype=tf.float32,
                                                swap_memory=True)

      elif hparams.token_level_architecture == "TRANSFORMER":

        # Pad token representation to expand the capacity of the model without expanding dimensionality of input embeddings
        # This is a side effect of the residual transformer architecture
        input_padding_size = hparams.token_rnn_num_units - input_memory.get_shape()[-1].value

        if input_padding_size < 0:
          raise ValueError("Token representation is larger than the desired hidden layer dimensionality / num_units")

        token_representations_shape = tf.shape(token_representations)
        padding_shape = [token_representations_shape[0], token_representations_shape[1], input_padding_size]

        padded_token_representations = tf.concat([token_representations, tf.zeros(padding_shape, dtype=tf.float32)],
                                                 axis=-1)

        # Inputs should be of shape[batch_size, timesteps, 1, hparams.hidden_dim]
        x = tf.expand_dims(padded_token_representations, 2)

        with tf.variable_scope("encoder"):

          # The universal transformer originates from the tensor2tensor library
          # which uses a different naming scheme for its hyperparameters,
          # we therefore have to "translate" some of our own hyperparameters to match
          def translate_hparams_to_encoder_transformer(hparams):
            new_params = universal_transformer.adaptive_universal_transformer_base()

            new_params.dropout = hparams.token_rnn_dropout
            new_params.batch_size = hparams.batch_size
            new_params.activation_dtype = 'float32'
            new_params.weight_dtype = 'float32'
            new_params.hidden_size = hparams.token_rnn_num_units  # input_memory.get_shape()[-1].value

            new_params.num_encoder_layers = hparams.token_rnn_num_encoder_layers
            new_params.num_hidden_layers = hparams.token_rnn_num_encoder_layers
            new_params.num_heads = hparams.transformer_num_heads
            new_params.attention_dropout = hparams.token_rnn_dropout
            new_params.self_attention_type = 'dot_product'
            new_params.max_length = hparams.src_max_len
            new_params.max_relative_position = 0
            new_params.initializer = 'uniform_unit_scaling'
            new_params.initializer_gain = 1.0
            new_params.proximity_bias = False
            new_params.layer_prepostprocess_dropout = hparams.token_rnn_dropout
            new_params.use_pad_remover = True
            new_params.norm_type = 'layer'
            new_params.ffn_layer = 'dense_relu_dense'
            new_params.filter_size = input_memory.get_shape()[-1].value * 4
            new_params.relu_dropout = hparams.token_rnn_dropout
            new_params.layer_prepostprocess_dropout = hparams.token_rnn_dropout

            # Universal transformer specific
            new_params.add_or_concat_timing_signal = "concat"
            # new_params.num_rec_steps = hparams.token_rnn_num_encoder_layers
            # new_params.act_max_steps = int(math.ceil(math.log2(hparams.token_rnn_num_encoder_layers) * 2))
            new_params.mix_with_transformer = hparams.token_level_mix_with_transformer
            new_params.num_mixedin_layers = hparams.token_level_num_mixedin_transformer_layers

            return new_params

          translated_hparams = translate_hparams_to_encoder_transformer(hparams)
          encoder = universal_transformer.UniversalTransformerEncoder(translated_hparams,
                                                                      mode=mode)  # tf.estimator.ModeKeys.PREDICT

          encoder_output_fw, _ = encoder({"inputs": x, "targets": 0, "target_space_id": tf.constant(1, dtype=tf.int32)})

          print("encoder_output_fw: ", encoder_output_fw)

          encoder_output_fw = tf.squeeze(encoder_output_fw, axis=2)

          if hparams.encoder_type == "bi":
            with tf.variable_scope("backwards"):

              def _reverse(input_, seq_lengths, seq_axis, batch_axis):
                if seq_lengths is not None:
                  return array_ops.reverse_sequence(
                    input=input_, seq_lengths=seq_lengths,
                    seq_axis=seq_axis, batch_axis=batch_axis)
                else:
                  return array_ops.reverse(input_, axis=[seq_axis])

              bw_padded_token_representations = _reverse(padded_token_representations, sequence_lengths, 1, 0)

              bw_x = tf.expand_dims(bw_padded_token_representations, 2)

              encoder_bw = universal_transformer.UniversalTransformerEncoder(translated_hparams,
                                                                          mode=mode)
              reverse_encoder_output_bw, _ = encoder_bw(
                {"inputs": bw_x, "targets": 0, "target_space_id": tf.constant(2, dtype=tf.int32)})

              reverse_output_bw = tf.squeeze(reverse_encoder_output_bw, axis=2)

              encoder_output_bw = _reverse(reverse_output_bw, sequence_lengths, 1, 0)

              encoder_output = tf.concat([encoder_output_fw, encoder_output_bw], axis=-1)

          else:
            encoder_output = encoder_output_fw

          print("encoder_output: ", encoder_output)

      else:
        raise ValueError("Unsupported token_level_architecture in hparams")

    with tf.variable_scope("proj", reuse=tf.AUTO_REUSE):
      output_layer = layers_core.Dense(
        self.ntags, use_bias=True, name="output_projection",
      )

      logits = output_layer(encoder_output)

    return logits

  def add_inference_op(self, logits, inference=False, labels=None, sequence_lengths=None):
    with tf.variable_scope("inference", reuse=tf.AUTO_REUSE):

      log_likelihood = None
      transition_params = None
      labels_pred = None

      if self.hparams.crf:
        if not inference:

          print("logits:", logits)
          print("labels:", labels)
          print("sequence_lengths:", sequence_lengths)

          log_likelihood, transition_params = tf.contrib.crf.crf_log_likelihood(
            logits, labels, sequence_lengths)

          # We also calculate labels_pred for training because we want to keep track of metrics
          labels_pred, best_score = tf.contrib.crf.crf_decode(
            logits,
            transition_params,
            sequence_lengths
          )

        else:
          transition_params = tf.get_variable("transitions", [self.ntags, self.ntags])
          transition_params = tf.identity(transition_params, name="transition_params")

          labels_pred, best_score = tf.contrib.crf.crf_decode(
            logits,
            transition_params,
            sequence_lengths
          )

        labels_pred = tf.identity(labels_pred, name="predicted_labels")
        print("labels_pred: ", labels_pred)

      else:
        labels_pred = tf.cast(tf.argmax(logits, axis=-1), tf.int32, name="predicted_labels")

      return log_likelihood, transition_params, labels_pred, logits

  def setup_loss_computation(self, logits, labels, log_likelihood=None, sequence_lengths=None, reuse=tf.AUTO_REUSE):

    with tf.variable_scope("loss", reuse=reuse):

      if self.hparams.crf:
        train_loss = tf.reduce_mean(-log_likelihood)  # negative log_likelihood
      else:

        losses = tf.losses.softmax_cross_entropy(logits=logits, onehot_labels=tf.one_hot(labels, depth=self.ntags),
                                                 label_smoothing=0.2,
                                                 reduction=tf.losses.Reduction.NONE)
        mask = tf.sequence_mask(sequence_lengths)
        losses = tf.boolean_mask(losses, mask)
        train_loss = tf.reduce_mean(losses)

    return train_loss

  def add_summary(self, sess):
    self.statistics = tf.summary.merge_all()

  def build_graph(self, hparams, features, labels, mode, **kwargs):

    tokens = features["tokens"]
    tokens_length = features["tokens_length"]
    token_ids = features["token_ids"]
    char_ids = features["char_ids"]
    char_sequence_length = features["chars_sequence_length"]
    gazetteers = features["gazetteers"] if hparams.gazetteers else None

    print("gazetteers:", gazetteers)

    if labels:
      labels = labels["labels"]

    word_count = tf.reduce_sum(tokens_length)

    token_representations = self.add_token_representations(mode, tokens_length, tokens, token_ids, char_ids,
                                                           char_sequence_length, gazetteers)  # gazetteers

    logits = self.add_logits(hparams, mode, token_representations, tokens_length)

    log_likelihood, transition_params, labels_pred, labels_logits = self.add_inference_op(logits, labels=labels,
                                                                                          sequence_lengths=tokens_length,
                                                                                          inference=mode == tf.estimator.ModeKeys.PREDICT)

    if not mode == tf.estimator.ModeKeys.PREDICT:
      train_loss = self.setup_loss_computation(logits, labels, log_likelihood=log_likelihood,
                                               sequence_lengths=tokens_length)

      if hparams.get('l2_scale', 0) > 0 and "super_convergence" not in hparams.optimizer:
        l2_loss = hparams.l2_scale * sum(tf.nn.l2_loss(tf_var) for tf_var in
                                         tf.trainable_variables())

        train_loss = train_loss + l2_loss

    else:
      train_loss = None

    return train_loss, word_count, labels_pred, transition_params, labels_logits

  def _build_token_level_encoder_cell(self, hparams, mode):
    """Build a multi-layer RNN cell that can be used by encoder."""

    return create_rnn_cell(
      unit_type=hparams.token_rnn_unit_type,
      num_units=hparams.token_rnn_num_units,
      num_layers=hparams.token_rnn_num_encoder_layers,
      num_residual_layers=0,
      forget_bias=hparams.token_rnn_forget_bias,
      dropout=hparams.token_rnn_dropout,
      zoneout=hparams.token_rnn_zoneout,
      norm_gain=hparams.token_rnn_norm_gain,
      mode=mode,
      single_cell_fn=self.single_cell_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes
    )

  def _build_token_level_decoder_cell(self, hparams, mode):
    """Build a multi-layer RNN cell or a list of RNN cells. """

    return create_rnn_cell(
      unit_type=hparams.token_rnn_unit_type,
      num_units=hparams.token_rnn_num_units,
      num_layers=hparams.token_rnn_num_decoder_layers,
      num_residual_layers=hparams.token_rnn_decoder_num_residual_layers,
      forget_bias=hparams.token_rnn_forget_bias,
      dropout=hparams.token_rnn_dropout,
      zoneout=hparams.token_rnn_zoneout,
      norm_gain=hparams.token_rnn_norm_gain,
      mode=mode,
      single_cell_fn=self.single_cell_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      use_peepholes=hparams.use_peepholes
    )

  def build_char_level_cell(self, hparams, mode):
    """Build a multi-layer RNN cell or a list of RNN cells that can be used to encode character level information."""

    return create_rnn_cell(
      unit_type=hparams.char_rnn_unit_type,
      num_units=hparams.char_rnn_num_units,
      num_layers=hparams.char_rnn_num_layers,
      num_residual_layers=0,
      forget_bias=hparams.char_rnn_forget_bias,
      dropout=hparams.char_rnn_dropout,
      zoneout=hparams.char_rnn_zoneout,
      norm_gain=hparams.char_rnn_norm_gain,
      mode=mode,
      single_cell_fn=self.single_cell_fn,
      multi_rnn_cell=hparams.multi_rnn_cell,
      # This parameter determines if it will return a multicell or a list of cells
      use_peepholes=hparams.use_peepholes
    )
