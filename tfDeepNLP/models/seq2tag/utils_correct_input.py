"""the training data might be wrong"""
import sys
from collections import defaultdict

LINE_BREAK = "<\\n>"
END_OF_TITLE = ['<eos>', LINE_BREAK]
TAG_OTHER = 'O'
TAG_TITLE = 'I-TITLE'
TAG_AUTHOR = 'I-AUTHOR'


def parse_file(filename):
  doi, tokens, tags = '', [], []
  with open(filename, encoding="utf8") as fin:
    for line in fin:
      if line.startswith('### Document DOI'):
        if tokens:
          yield doi, tokens, tags
        doi = line.strip()
        tokens, tags = [], []
      else:
        try:
          a, b = line.strip().split()
          tokens.append(a)
          tags.append(b)
        except:
          continue
  return doi, tokens, tags


def get_raw_title_segments(tags):
  raw_segs = []
  start_idx = - 1
  for i, t in enumerate(tags):
    if t == TAG_TITLE:
      if start_idx < 0:
        start_idx = i
    else:
      if start_idx >= 0: # end of title
        raw_segs.append([start_idx, i])
        start_idx = -1
  if start_idx >= 0:
    raw_segs.append([start_idx, len(tags)])
  return raw_segs


def get_first_author_idx(tags):
  raw_segs = []
  start_idx = - 1
  for i, t in enumerate(tags):
    if t == TAG_TITLE:
      if start_idx < 0:
        start_idx = i
    else:
      if start_idx >= 0:  # end of title
        raw_segs.append([start_idx, i])
        start_idx = -1
  if start_idx >= 0:
    raw_segs.append([start_idx, len(tags)])
  return raw_segs


def get_title_fixed_tags(tokens, tags, start_idx, end_idx):
  if (tokens[end_idx] not in END_OF_TITLE) and (tokens[end_idx+1] not in END_OF_TITLE):
    return [], []
  new_tags = list(map(lambda x: TAG_OTHER if x == TAG_TITLE else x, tags))
  for i in range(start_idx, end_idx):
    new_tags[i] = TAG_TITLE
  if tokens[end_idx] == '.':
    new_tags[end_idx] = TAG_TITLE
    end_idx += 1
  return tokens, new_tags


def correct_tag(tokens, tags, min_title_len=4, max_title_breaks=3):
  """return (new_tokens, new_tags) """
  raw_segment = get_raw_title_segments(tags)
  start_idx, end_idx = raw_segment[0]

  if len(raw_segment) == 1:
    if end_idx - start_idx >= min_title_len:
      _, new_tags = get_title_fixed_tags(tokens, tags, start_idx, end_idx)
      if new_tags:
        return tokens, new_tags, 'ok'
      else:
        return [], [], 'wrong: ending'
    else:
      return [], [], 'wrong: too short'

  i = 0
  title_breaks = 0
  while i <= len(raw_segment)-2:
    if set(tokens[raw_segment[i][1]: raw_segment[i+1][0]]) == set(LINE_BREAK):
      if title_breaks > max_title_breaks:
        return [], [], 'wrong: many title linebreak'
      title_breaks += 1
      end_idx = raw_segment[i+1][1]
    elif raw_segment[i+1][0] - raw_segment[i][1] <= 2:
      end_idx = raw_segment[i+1][1]
    else:
      new_tokens, new_tags = get_title_fixed_tags(tokens, tags, start_idx, end_idx)
      if new_tags:
        return new_tokens, new_tags, 'ok: fix'
      else:
        start_idx, end_idx = raw_segment[i+1]
    i += 1
  new_tokens, new_tags = get_title_fixed_tags(tokens, tags, start_idx, end_idx)
  if new_tags:
    return new_tokens, new_tags, 'ok: fix'
  return [], [], 'wrong: too broken'


def write_one_document(fout, doi, tokens, tags):
  fout.write(doi + '\n')
  fout.write('\n'.join([to + '\t' + ta for to, ta in zip(tokens, tags)]))
  fout.write('\n\n')


def run_correct(fn_input, fn_output):
  fout = open(fn_output, 'w', encoding="utf8")
  msg_cnt = defaultdict(lambda: [])
  for doi, tokens, tags in parse_file(fn_input):
    if (len(set(tags)) < 3) or (TAG_TITLE not in tags):  # need at least title/author/others
      msg_cnt['wrong: missing title/author'].append(doi)
    else:
      new_tokens, new_tags, msg = correct_tag(tokens, tags)
      msg_cnt[msg].append(doi)
      if new_tags:
        write_one_document(fout, doi, new_tokens, new_tags)
  fout.close()
  for k, v in msg_cnt.items():
    print(k, len(v), v[:3])


def title_first_cnt(filename):
  n1, n2 = 0, 0
  for doi, _, tags in parse_file(filename):
    if TAG_TITLE in tags and TAG_AUTHOR in tags:
      if tags.index(TAG_TITLE) < tags.index(TAG_AUTHOR):
        n1 += 1
      else:
        n2 += 1
  print(n1, n2)  # 10277, 209


if __name__ == '__main__':

  try:
    fn_input = sys.argv[1]
    fn_output = sys.argv[2]
  except:
    fn_input = "/Users/qianyuxx/Downloads/springer_corpus.txt"
    fn_output = "/Users/qianyuxx/Downloads/springer_corpus2.txt"

  # title_first_cnt(fn_input)
  run_correct(fn_input, fn_output)