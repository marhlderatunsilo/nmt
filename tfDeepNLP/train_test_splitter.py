input_path = 'C:/Users/marhl/Desktop/text-sum-sources-merged_processed.txt'
test_path = 'C:/Users/marhl/Desktop/textsum_training_data/test.articles'
dev_path = 'C:/Users/marhl/Desktop/textsum_training_data/dev.articles'
train_path = 'C:/Users/marhl/Desktop/textsum_training_data/train.articles'

# Every 5th goes to either test or dev
# So train is 80%, test is 10% and dev is 10% of the data
# Should be run on both data and targets to ensure they are still aligned

with open(input_path, encoding="utf8") as f_input, \
     open(test_path, 'w', encoding="utf8") as f_test, \
     open(dev_path, 'w', encoding="utf8") as f_dev, \
     open(train_path, 'w', encoding="utf8") as f_train:
  lines = f_input.readlines()
  counter = 0
  for i, line in enumerate(lines):
    if i % 5 == 0:
      if counter == 0:
        f_test.write(line)
        counter = 1
      else:
        f_dev.write(line)
        counter = 0
    else:
      f_train.write(line)
