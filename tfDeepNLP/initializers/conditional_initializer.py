from tensorflow.python.ops.init_ops import Initializer

class CondInitializer(Initializer):

  def __init__(self, cond_fn, true_initializer, false_initializer):
    self._cond_fn = cond_fn
    self._true_initializer = true_initializer
    self._false_initializer = false_initializer

  def __call__(self, shape, dtype=None, partition_info=None):

    if self._cond_fn(shape, dtype):
      weight_tensor = self._true_initializer(shape, dtype)
    else:
      weight_tensor = self._false_initializer(shape, dtype)

    return weight_tensor