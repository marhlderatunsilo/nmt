from tensorflow.python.ops import array_ops
from tensorflow.python.ops.init_ops import Initializer

class SplitInitializer(Initializer):

  def __init__(self, initializer1, initializer2, shape1, shape2, concat_axis):
    self._initializer1 = initializer1
    self._initializer2 = initializer2
    self._shape1 = shape1
    self._shape2 = shape2
    self._concat_axis = concat_axis

  def __call__(self, shape, dtype=None, partition_info=None):

    weight_tensor1 = self._initializer1(self._shape1, dtype=dtype)
    weight_tensor2 = self._initializer2(self._shape2, dtype=dtype)

    combined_weight_tensor = array_ops.concat(values=[weight_tensor1, weight_tensor2], axis=self._concat_axis)

    return combined_weight_tensor