
import tensorflow as tf
from tensorflow.python.training import session_run_hook

class ExtraInitializationHook(session_run_hook.SessionRunHook):
  """A ExtraInitializationHook handles running an extra op for initialization."""

  def __init__(self, extra_init_op):
    """Creates hook to handle an extra initialization op.

    Args:
      extra_init_op: An extra operation to run after the session is created.
    """

    self._extra_init_op = extra_init_op

  def after_create_session(self, session, coord):
    if hasattr( self._extra_init_op, '__call__'):
      self._extra_init_op = self._extra_init_op()

    if not isinstance(self._extra_init_op, tf.Operation):
      raise ValueError(
        "Argument extra_init_op should be an operation!")


    """Runs Extra Initialization op."""
    session.run(self._extra_init_op)
