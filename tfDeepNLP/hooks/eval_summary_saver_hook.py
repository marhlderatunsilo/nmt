import tensorflow as tf

class EvalSummarySaverHook(tf.train.SessionRunHook):
  """Saves summaries during eval loop."""

  def __init__(self,
               output_dir=None,
               #stop_after=1
               ):
    """Initializes a special `SummarySaverHook` to run during evaluations

    Args:
      output_dir: `string`, the directory to save the summaries to.
    """
    self._summary_op = None
    self._output_dir = output_dir
    self._global_step_tensor = None
    #self._stop_after = stop_after
    self._saves = None

  def begin(self):
    self._global_step_tensor = tf.train.get_or_create_global_step()
    if self._global_step_tensor is None:
      raise RuntimeError(
        "Global step should be created to use SummarySaverHook.")

  def before_run(self, run_context):  # pylint: disable=unused-argument
    requests = {"global_step": self._global_step_tensor}
    if self._saves is None:  # skip first time, because summaries only appear after first run
      self._saves = 0
    elif self._get_summary_op() is not None: #self._saves < self._stop_after and \
      requests["summary"] = self._get_summary_op()

    return tf.train.SessionRunArgs(requests)

  def after_run(self, run_context, run_values):
    _ = run_context
    if "summary" in run_values.results:
      print('Saving eval summaries')
      global_step = run_values.results["global_step"]
      #summary_writer = tf.summary.FileWriterCache.get(self._output_dir)

      self.last_global_step = global_step
      self.last_summary = run_values.results["summary"]



      self._saves += 1

  def end(self, session=None):
    _ = session

    summary_writer = tf.summary.FileWriterCache.get(self._output_dir)
    for summary in self.last_summary:
      summary_writer.add_summary(summary, self.last_global_step)

    summary_writer.flush()

  def _get_summary_op(self):
    """Fetches the summary op from collections
    """

    if self._summary_op is None:
      self._summary_op = tf.get_collection(tf.GraphKeys.SUMMARY_OP)

    return self._summary_op