from tfDeepNLP.utils import misc_utils as utils

import tensorflow as tf
from abc import ABC, abstractmethod
from tfDeepNLP.create_optimizer import create_optimizer

utils.check_tensorflow_version()

__all__ = ["BaseModel"]


class BaseModel(ABC):
  def __init__(self,
               hparams,
               single_cell_fn=None,
               ):
    """Create the model.

        Args:
          hparams: Hyperparameter configurations.
          mode: tf.contrib.learn.ModeKeys. (TRAIN | EVAL | INFER)
          iterator: Dataset Iterator that feeds data.
          source_vocab_table: Lookup table mapping source words to ids.
          scope: scope of the model.
          single_cell_fn: allow for adding customized cell. When not specified,
            we default to model_helper._single_cell
        """
    self.hparams = hparams
    self.time_major = hparams.time_major
    self.clip_gradients = hparams.clip_gradients

    # To make it flexible for external code to add other cell types
    # If not specified, we will later use model_helper._single_cell
    self.single_cell_fn = single_cell_fn

  def __call__(self, features, labels, mode, params):
    tf.train.get_or_create_global_step()

  def create_optimizer(self, hparams, mode, batch_size=None):
    if mode == tf.estimator.ModeKeys.TRAIN:
      return create_optimizer(hparams, tf.train.get_or_create_global_step(), batch_size=batch_size)
    else:
      return None

  @abstractmethod
  def build_graph(self, hparams, features, labels, mode, **kwargs):
    pass

  @abstractmethod
  def setup_loss_computation(self, hparams, logits, labels, **kwargs):
    pass

  @abstractmethod
  def setup_gradient_computation(self, hparams, train_loss, optimizer_op, learning_rate, **kwargs):
    pass

  @abstractmethod
  def statistics(self):
    pass
