# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""S2S: For training NMT models on single machine multi-gpu setup."""
from __future__ import print_function

import math

import numpy as np
import tensorflow as tf

#from tfDeepNLP.models.seq2seq.RNNMirroredStrategy import MirroredStrategy
from tensorflow.contrib.distribute import MirroredStrategy

from tfDeepNLP.utils import misc_utils as utils

utils.check_tensorflow_version()

__all__ = [
  "train_estimator"
]

"""
Generic code for training any model through an estimator
"""

def train_estimator(hparams,
                    *,
                    train_input_fn=None,
                    train_hooks=None,
                    dev_input_fn=None,
                    dev_hooks=None,
                    infer_input_fn=None,
                    infer_hooks=None,
                    model_fn=None,
                    serving_input_receiver_fn=None):


  if hparams.task == "train":
    if hparams.num_gpus > 1:
      distribution_strategy = MirroredStrategy(num_gpus=hparams.num_gpus)
    else:
      distribution_strategy = tf.contrib.distribute.OneDeviceStrategy(tf.DeviceSpec(device_type="GPU", device_index=0))
  else:
    distribution_strategy = tf.contrib.distribute.OneDeviceStrategy(tf.DeviceSpec(device_type="GPU", device_index=0))

  config_proto = utils.get_config_proto(
    log_device_placement=hparams.log_device_placement)

  estimator_config = tf.estimator.RunConfig(
    save_checkpoints_secs=20 * 60,  # Save checkpoints every 20 minutes.
    keep_checkpoint_max=5,  # Retain the 10 most recent checkpoints.
    session_config=config_proto,
    model_dir=hparams.out_dir,
    save_summary_steps=hparams.every_n_step,  # points on tensorbord
    train_distribute=distribution_strategy
  )

  estimator = tf.estimator.Estimator(
    model_fn=model_fn,
    model_dir=hparams.out_dir,
    params=hparams,
    config=estimator_config)

  dev_hooks += []

  if hparams.task == "infer":

    #estimator.predict(input_fn=infer_input_fn, hooks=infer_hooks)
    result = estimator.evaluate(infer_input_fn, hooks=infer_hooks)
    print('## inference result', result)


  # Export the model if the flag is set:
  elif hparams.task == "save":

    checkpoint = tf.train.get_checkpoint_state(estimator.model_dir)
    tf.reset_default_graph()
    print("checkpoint: ", checkpoint)

    save_checkpoint_path = checkpoint.model_checkpoint_path

    if hparams.infer_embeddings_filename:

      if "_infer_embeddings" not in checkpoint.model_checkpoint_path:
        save_checkpoint_path = checkpoint.model_checkpoint_path + "_infer_embeddings"

      with tf.Graph().as_default():
        config = tf.ConfigProto(allow_soft_placement=True)
        with tf.Session(config=config) as sess:
          saver = tf.train.import_meta_graph(checkpoint.model_checkpoint_path + '.meta')
          saver.restore(sess, checkpoint.model_checkpoint_path)

          #print("all variables before: ", tf.global_variables())

          if hparams.embeddings_filename != hparams.infer_embeddings_filename:

            # Get embedding variable
            with tf.variable_scope("words"):
              embeddings_variable = tf.get_variable("_token_embeddings_inference",
                                                    shape=[hparams.token_vocab_inference_size,
                                                           hparams.token_embedding_size],
                                                    initializer=tf.zeros_initializer(),
                                                    # We have to use tf.assign() elsewhere
                                                    # initializer=tf.constant_initializer(
                                                    # initialize_word_emb_from_numpy_array(self.hparams,
                                                    #                                      self.pretrained_embeddings,
                                                    #                                      self.hparams.dim_word,
                                                    #                                      self.nwords)),
                                                    dtype=tf.float32,
                                                    trainable=False)

            print("embeddings_variable:", embeddings_variable)

            with np.load(hparams.infer_embeddings_filename) as data:
              pretrained_embeddings = data["embeddings"]

            with tf.device('/cpu:0'):

              embeddings_placeholder = tf.placeholder(dtype=tf.float32,
                                                      shape=[hparams.token_vocab_inference_size,
                                                             hparams.token_embedding_size])

              # your update operation
              assing_embeddings_op = tf.assign(embeddings_variable, embeddings_placeholder)
              sess.run(assing_embeddings_op, {embeddings_placeholder: pretrained_embeddings})

              # Remove old embedding variable
              all_variables = tf.global_variables()
              all_variables = list(filter(lambda v: v.name != "words/_token_embeddings_train_dev", all_variables))

              print("all variables after: ", all_variables)
              # you can check that value is updated

              # this saves updated values to last checkpoint saved by estimator
              saver = tf.train.Saver()
              saver.save(sess, save_checkpoint_path)

              print("Loading from checkpoint:", save_checkpoint_path)

    export_dir_base = hparams.out_dir + "/" + "saved_model/"
    print("Saving model to " + export_dir_base)

    if not tf.gfile.Exists(export_dir_base):
      tf.gfile.MakeDirs(export_dir_base)

    estimator.export_savedmodel(export_dir_base=export_dir_base,
                                serving_input_receiver_fn=serving_input_receiver_fn,
                                strip_default_attrs=True,
                                checkpoint_path=save_checkpoint_path)

  elif hparams.task == "train":

    for epoch_counter in range(hparams.num_train_epochs):
      if hparams.get("max_steps", 0) > 0:
        estimator.train(input_fn=train_input_fn, hooks=train_hooks, max_steps=hparams.max_steps)
      else:
        estimator.train(input_fn=train_input_fn, hooks=train_hooks)
        estimator.evaluate(input_fn=dev_input_fn, hooks=dev_hooks)