#  Copyright 2016 The TensorFlow Authors. All Rights Reserved.
#
#  Licensed under the Apache License, Version 2.0 (the "License");
#  you may not use this file except in compliance with the License.
#  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
#  limitations under the License.
"""An Example of a DNNClassifier for the Iris dataset."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import tensorflow as tf

from tfDeepNLP.experimentation.basic_layer_expansion import normal_full_layer, leakage_wrapper, \
  dense_layer_with_scaling_factor, \
  dense_layer_with_random_scaling, dense_layer_with_scaling_and_random_scaling_factors


# tf.logging.set_verbosity(tf.logging.INFO)


def dense_model_fn(features, labels, mode, params):
  """Model function for Dense model."""
  # Input Layer
  # Reshape X to 2-D tensor: [batch_size, width*height*channels]
  # MNIST images are 28x28 pixels, and have one color channel
  input_layer = tf.reshape(features["x"], [-1, 28 * 28 * 1])

  dense_layer_fn = params["dense_layer_fn"]

  if not params["leakage"]:

    dlayer_1 = tf.nn.relu(dense_layer_fn(input_layer, name="dense_layer_1", units=params["num_units"], mode=mode))
    dlayer_2 = tf.nn.relu(dense_layer_fn(dlayer_1, name="dense_layer_2", units=params["num_units"], mode=mode))
    dlayer_3 = tf.nn.relu(dense_layer_fn(dlayer_2, name="dense_layer_3", units=params["num_units"], mode=mode))
    dlayer_4 = tf.nn.relu(dense_layer_fn(dlayer_3, name="dense_layer_4", units=params["num_units"], mode=mode))
    dlayer_5 = tf.nn.relu(dense_layer_fn(dlayer_4, name="dense_layer_5", units=params["num_units"], mode=mode))

  else:

    dlayer_1 = tf.nn.relu(
      leakage_wrapper(
        dense_layer_fn(
          input_layer,
          name="dense_layer_1",
          units=params["num_units"],
          mode=mode),
        name="dense_layer_1"))

    dlayer_2 = tf.nn.relu(
      leakage_wrapper(
        dense_layer_fn(
          dlayer_1,
          name="dense_layer_2",
          units=params["num_units"],
          mode=mode),
        name="dense_layer_2"))

    dlayer_3 = tf.nn.relu(
      leakage_wrapper(
        dense_layer_fn(
          dlayer_2,
          name="dense_layer_3",
          units=params["num_units"],
          mode=mode),
        name="dense_layer_3"))

    dlayer_4 = tf.nn.relu(
      leakage_wrapper(
        dense_layer_fn(
          dlayer_3,
          name="dense_layer_4",
          units=params["num_units"],
          mode=mode),
        name="dense_layer_4"))

    dlayer_5 = tf.nn.relu(
      leakage_wrapper(
        dense_layer_fn(
          dlayer_4,
          name="dense_layer_5",
          units=params["num_units"],
          mode=mode),
        name="dense_layer_5"))

  # Logits layer
  logits = tf.layers.dense(inputs=dlayer_5, units=10)

  predictions = {
    # Generate predictions (for PREDICT and EVAL mode)
    "classes": tf.argmax(input=logits, axis=1),
    # Add `softmax_tensor` to the graph. It is used for PREDICT and by the
    # `logging_hook`.
    "probabilities": tf.nn.softmax(logits, name="softmax_tensor")
  }
  if mode == tf.estimator.ModeKeys.PREDICT:
    return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

  # Calculate Loss (for both TRAIN and EVAL modes)
  loss = tf.losses.sparse_softmax_cross_entropy(labels=labels, logits=logits)

  equality = tf.equal(tf.argmax(logits, 1, output_type=tf.int32), labels)
  train_accuracy = tf.reduce_mean(tf.cast(equality, tf.float32))
  tf.summary.scalar("train_accuracy", train_accuracy)

  # Configure the Training Op (for TRAIN mode)
  if mode == tf.estimator.ModeKeys.TRAIN:
    optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001)
    train_op = optimizer.minimize(
      loss=loss,
      global_step=tf.train.get_global_step())
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

  # Add evaluation metrics (for EVAL mode)
  eval_metric_ops = {
    "accuracy": tf.metrics.accuracy(
      labels=labels, predictions=predictions["classes"])}
  return tf.estimator.EstimatorSpec(
    mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)


def main(unused_argv):
  # Load training and eval data
  mnist = tf.contrib.learn.datasets.load_dataset("mnist")
  train_data = mnist.train.images  # Returns np.array
  train_labels = np.asarray(mnist.train.labels, dtype=np.int32)
  eval_data = mnist.test.images  # Returns np.array
  eval_labels = np.asarray(mnist.test.labels, dtype=np.int32)

  dense_layer_functions = [dense_layer_with_scaling_factor, dense_layer_with_random_scaling,
                           dense_layer_with_scaling_and_random_scaling_factors, normal_full_layer]

  # Choose hparam
  model_no = 3
  dense_layer_fn = dense_layer_functions[1]
  print(dense_layer_fn.__name__)
  leakage = False
  print("leakage: ", leakage)

  # Create the Estimator
  mnist_classifier = tf.estimator.Estimator(
    model_fn=dense_model_fn, model_dir="mnist_model_{}".format(model_no),
    params={"dense_layer_fn": dense_layer_fn,
            "num_units": 32,
            "leakage": leakage})

  # Set up logging for predictions
  # Log the values in the "Softmax" tensor with label "probabilities"
  tensors_to_log = {"probabilities": "softmax_tensor"}
  logging_hook = tf.train.LoggingTensorHook(
    tensors=tensors_to_log, every_n_iter=50)

  # Train the model
  train_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": train_data},
    y=train_labels,
    batch_size=100,
    num_epochs=None,
    shuffle=True)
  mnist_classifier.train(
    input_fn=train_input_fn,
    steps=70000,
    hooks=[logging_hook])

  # Evaluate the model and print results
  eval_input_fn = tf.estimator.inputs.numpy_input_fn(
    x={"x": eval_data},
    y=eval_labels,
    num_epochs=1,
    shuffle=False)
  eval_results = mnist_classifier.evaluate(input_fn=eval_input_fn)
  print(eval_results)


if __name__ == "__main__":
  tf.app.run()
