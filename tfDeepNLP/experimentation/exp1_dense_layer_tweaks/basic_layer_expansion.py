"""
Experimentation with basic neural layer (W·A+b)

"""

import tensorflow as tf

"""
Add learned scaling factor.
A__i = (W · A__i-1 + b) * s
where l is truncated, e.g. [0.2, 2]

"""


def create_scaling_factor(name, sf_min=0.2, sf_max=2):
  with tf.variable_scope('scaling_factor'):
    s = tf.get_variable(name + "_scaling_factor", initializer=1.0)
    s_truncated = tf.clip_by_value(s, clip_value_min=sf_min, clip_value_max=sf_max,
                                   name=name + "_scaling_factor_truncated")
  return s_truncated


def dense_layer_with_scaling_factor(x, units, name, mode=None, sf_min=0.2, sf_max=2):
  normal_layer = normal_full_layer(x=x, units=units, name=name)

  sf = create_scaling_factor(name=name, sf_min=sf_min, sf_max=sf_max)

  return normal_layer * sf


"""
Add randomly sampled scaling factor. Random for each run.
A__i = (W · A__i-1 + b) * RandNorm(mean=1, sd=0.1, n=1)

"""


def create_random_scaling_factor(name, rs_mean=1.0, rs_std=0.1):
  with tf.variable_scope('random_scaling_factor'):
    r = tf.truncated_normal(shape=[1], mean=rs_mean, stddev=rs_std, name=name + "_random_scaling_factor")
  return r


def dense_layer_with_random_scaling(x, units, name, mode, rs_mean=1.0, rs_std=0.1):
  normal_layer = normal_full_layer(x=x, units=units, name=name)

  if mode != tf.estimator.ModeKeys.TRAIN:
    r = 1.0
  else:
    r = create_random_scaling_factor(name=name, rs_mean=rs_mean, rs_std=rs_std)

  return normal_layer * r


"""
Both scaling factors combined
A__i = (W · A__i-1 + b) * s * RandNorm(mean=1, sd=0.1, n=1)
where l is truncated, e.g. [0.2, 2]
"""


def dense_layer_with_scaling_and_random_scaling_factors(x, units, name, mode, sf_min=0.2, sf_max=2,
                                                        rs_mean=1.0, rs_std=0.1):
  normal_layer = normal_full_layer(x=x, units=units, name=name)

  if mode != tf.estimator.ModeKeys.TRAIN:
    r = 1.0
  else:
    r = create_random_scaling_factor(name=name, rs_mean=rs_mean, rs_std=rs_std)

  sf = create_scaling_factor(name=name, sf_min=sf_min, sf_max=sf_max)

  return normal_layer * r * sf


"""
Leaky neurons.

Idea 1:
Add the total output (sum or mean), gated with a learnable leakage factor, elementwise.
L__i = A__i + l * mean(A__i) 


Idea 2:
Randomly assign connections between neurons in layer and create a gate between them.
Probably should be fixed once assigned.
Look up leaky neurons. It might already exist?

"""


def _get_leakage(layer, name):
  # layer_size = int(layer.get_shape()[1])
  with tf.variable_scope('leakage_factor'):
    l = tf.get_variable(name + "_leakage_factor", initializer=0.05)
    l_truncated = tf.clip_by_value(l, clip_value_min=0.001, clip_value_max=0.2,
                                   name=name + "_leakage_factor_truncated")

  return tf.reduce_mean(tf.abs(layer)) * l_truncated


def leakage_wrapper(layer, name):
  leakage = _get_leakage(layer, name)
  return layer + leakage


def normal_full_layer(x, units, name, mode=None):
  input_size = int(x.get_shape()[1])

  with tf.variable_scope('weights'):
    W = tf.get_variable(name, shape=[input_size, units])

  with tf.variable_scope('biases'):
    b = tf.get_variable(name + "_bias", shape=[units], initializer=tf.zeros_initializer)

  with tf.variable_scope('pre_activate'):
    pre_activate = tf.matmul(x, W) + b

  return pre_activate
