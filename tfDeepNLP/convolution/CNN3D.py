import tensorflow as tf

from tfDeepNLP.utils.misc_utils import tf_print_if


class CNN3D(object):
  """
  Convolution blocks and presets for 3d text representations (tokens, chars, char embeddings)


  The input tensor should have shape - [batch, in_depth, in_height, in_width, in_channels].
      e.g. [batch_size, src_token_max_len, chars_per_token, chars_embedding_size, 1]

  filters have shape: [filter_depth, filter_height, filter_width, in_channels, out_channels]
  Must have strides[0] = strides[4] = 1
  Dilations - The dilation factor for each dimension of input
   if set to k > 1, there will be k-1 skipped cells between each filter element on that dimension
   Dilations in the batch and depth dimensions must be 1.

  """

  def __init__(self, verbose_tensors=False):
    self.__preset_fn_map = {"preset_1": self._cnn_preset_1}
    self.__verbose_tensors = verbose_tensors

  def get_preset_fn(self, preset="preset_1"):
    return self.__preset_fn_map[preset]

  def _cnn_preset_1(self, x, num_token_level_layers=1, token_level_skip_connections=False):
    """
    Collapses embeddings, chars and channels to dimension 1.
    Creates token level features in parallel with different dilations in each pipeline.
    Uses max pooling on output on token level (depth).
    """
    collapsed_embs = self._collapse_embeddings(x, 8)
    collapsed_chars = self._collapse_chars(collapsed_embs, 8)
    for ntll in range(num_token_level_layers):  # TODO does this work? Or do we need to append to a list?
      if ntll == 0:
        token_level_features = self._make_token_level_features(collapsed_chars, 16, scope=str(ntll))
      else:
        token_level_features_ = self._make_token_level_features(token_level_features, 16, scope=str(ntll))
        if token_level_skip_connections:
          token_level_features = self._make_skip_connection(token_level_features_, token_level_features)
        else:
          token_level_features = token_level_features_
    collapsed_channels = self._collapse_channels(token_level_features)
    max_pooled = self._max_pool3d(collapsed_channels, k_depth=3, padding="VALID")
    return max_pooled

  def flatten(self, x, scope=None):
    # flatten
    with tf.variable_scope("CNN3D_flatten/" + scope if scope else "CNN3D_flatten"):
      flattened = tf.reshape(x, shape=[-1, x.get_shape()[1].value * x.get_shape()[2].value * x.get_shape()[3].value *
                                       x.get_shape()[4].value], name="flattener")

      return tf_print_if(flattened, [tf.shape(flattened)], summarize=10, message="CNN3D_flatten: ",
                         verbose=self.__verbose_tensors)

  """
  Building blocks
  """

  def _collapse_embeddings(self, x, n_features, scope=None):
    with tf.variable_scope("CNN3D_collapse_embeddings/" + scope if scope else "CNN3D_collapse_embeddings"):
      # Look at entire char embedding - collapses that the embedding dimension (3) to 1 element
      filter = tf.get_variable('filter', [1, 1, x.get_shape()[3].value, x.get_shape()[-1].value, n_features],
                               initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
                               dtype=tf.float32)
      conv = tf.nn.conv3d(x, filter=filter,
                          strides=[1, 1, 1, 1, 1],
                          padding="VALID", name="convolution")
      conv = tf.nn.relu(conv, name="relu")

      return tf_print_if(conv, [tf.shape(conv)], summarize=10, message="CNN3D_collapse_embeddings: ",
                         verbose=self.__verbose_tensors)

  def _collapse_chars(self, x, n_features, scope=None):
    with tf.variable_scope("CNN3D_collapse_chars/" + scope if scope else "CNN3D_collapse_chars"):
      # Look at all chars in the word - collapses chars dimension.
      filter = tf.get_variable('filter', [1, x.get_shape()[2].value, 1, x.get_shape()[-1].value, n_features],
                               initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
                               dtype=tf.float32)
      conv = tf.nn.conv3d(x, filter=filter, strides=[1, 1, 1, 1, 1],
                          padding="VALID", name="convolution")
      conv = tf.nn.relu(conv, name="relu")

      return tf_print_if(conv, [tf.shape(conv)], summarize=10, message="CNN3D_collapse_chars: ",
                         verbose=self.__verbose_tensors)

  def _make_token_level_features(self, x, n_features, scope=None):
    assert n_features % 2 == 0, "n_features must be divisible with 2"
    n_features = int(n_features / 2)
    with tf.variable_scope("CNN3D_token_features/" + scope if scope else "CNN3D_token_features"):
      # Token level convolution with different levels of dilation

      filter_1 = tf.get_variable('filter_1', [3, 1, 1, x.get_shape()[-1].value, n_features],
                                 initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
                                 dtype=tf.float32)
      conv_1 = tf.nn.conv3d(x, filter=filter_1, strides=[1, 1, 1, 1, 1],
                            padding="SAME", name="convolution_1")

      filter_2 = tf.get_variable('filter_2', [3, 1, 1, x.get_shape()[-1].value, n_features],
                                 initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
                                 dtype=tf.float32)
      conv_2 = tf.nn.conv3d(x, filter=filter_2, strides=[1, 1, 1, 1, 1],
                            dilations=[1, 2, 1, 1, 1],  # "...<a> cat <is> an <animal>..."
                            padding="SAME", name="convolution_2")

      filter_3 = tf.get_variable('filter_3', [3, 1, 1, x.get_shape()[-1].value, n_features],
                                 initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
                                 dtype=tf.float32)
      conv_3 = tf.nn.conv3d(x, filter=filter_3, strides=[1, 1, 1, 1, 1],
                            dilations=[1, 3, 1, 1, 1],  # "...a <cat> is an <animal>..."
                            padding="SAME", name="convolution_3")

      conv_c = tf.concat([conv_1, conv_2, conv_3], axis=-1)
      conv_c = tf.nn.relu(conv_c, name="relu")

      return tf_print_if(conv_c, [tf.shape(conv_c)], summarize=10,
                         message="CNN3D_make_token_level_features: ", verbose=self.__verbose_tensors)

  def _collapse_channels(self, x, scope=None):
    with tf.variable_scope("CNN3D_collapse_channels/" + scope if scope else "CNN3D_collapse_channels"):
      filter = tf.get_variable('filter', [1, 1, 1, x.get_shape()[-1].value, 1],
                               initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
                               dtype=tf.float32)
      conv = tf.nn.conv3d(x, filter=filter,
                          strides=[1, 1, 1, 1, 1],
                          padding="VALID", name="convolution")
      conv = tf.nn.relu(conv, name="relu")

      return tf_print_if(conv, [tf.shape(conv)], summarize=10,
                         message="CNN_collapse_channels: ", verbose=self.__verbose_tensors)

  def _make_skip_connection(self, x, y, scope=None):
    with tf.variable_scope("CNN3D_skip_connection" + scope if scope else "CNN3D_skip_connection"):
      conv_c = tf.concat([x, y], axis=-1, name="skip_connection")

      return tf_print_if(conv_c, [tf.shape(conv_c)], summarize=10,
                         message="CNN3D_make_skip_connection: ", verbose=self.__verbose_tensors)

  def _max_pool3d(self, x, k_depth=1, k_height=1, k_width=1, padding="SAME", scope=None):
    with tf.variable_scope("CNN3D_max_pool3d/" + scope if scope else "CNN3D_max_pool3d"):
      pooled = tf.nn.max_pool3d(x, ksize=[1, k_depth, k_height, k_width, 1], strides=[1, 1, 1, 1, 1], padding=padding,
                                name="max_pool")

      return tf_print_if(pooled, [tf.shape(pooled)], summarize=10,
                         message="CNN3D_max_pool3d: ", verbose=self.__verbose_tensors)


"""
Extra blocks not currently in use. Needs some work.
"""
# def _block_1(self, x, hparams):
#   with tf.variable_scope("Conv_block_1"):
#     conv_1_max_pool = tf.nn.max_pool3d(x, ksize=[1, 1, 1, 2, 1], strides=[1, 1, 1, 2, 1], padding="VALID",
#                                        name="max_pool_1")
#
#     # First convolution goes through each character embedding individually 3 dims at a time. 16 out channels.
#     filter_1 = tf.get_variable('filter_1', [1, 1, 3, x.get_shape()[-1].value, 4],
#                                initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
#                                dtype=tf.float32)
#     conv_1 = tf.nn.conv3d(conv_1_max_pool, filter=filter_1,
#                           strides=[1, 1, 1, 1, 1],
#                           padding="VALID", name="convolution_1")
#     filter_1_reduce = tf.get_variable('filter_1_reduce', [1, 1, 1, 4, 1],
#                                       initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
#                                       dtype=tf.float32)
#     conv_1_reduce = tf.nn.conv3d(conv_1, filter=filter_1_reduce,
#                                  strides=[1, 1, 1, 1, 1],
#                                  padding="VALID", name="convolution_1_reduce")
#
#     conv_1_reduce = tf_print_if(conv_1_reduce, [tf.shape(conv_1_reduce)], summarize=10, message="\nConv_block_1: ",
#                                 verbose=verbose_tensors)
#     return conv_1_reduce
#
# def _block_2(self, x, hparams):
#   with tf.variable_scope("Conv_block_2"):
#     conv_2_max_pool = tf.nn.max_pool3d(x, ksize=[1, 1, 3, 1, 1], strides=[1, 1, 3, 1, 1], padding="VALID",
#                                        name="max_pool_2")
#
#     # Dilated on the char dimension.
#     filter_2_1 = tf.get_variable('filter_2_1', [1, 3, 1, x.get_shape()[-1].value, 8],
#                                  initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
#                                  dtype=tf.float32)
#     conv2_1 = tf.nn.conv3d(conv_2_max_pool, filter=filter_2_1, strides=[1, 1, 1, 1, 1],
#                            dilations=[1, 1, 3, 1, 1],
#                            padding="SAME", name="convolution_2_1")
#
#     # Dilated on the char dimension.
#     filter_2_2 = tf.get_variable('filter_2_2', [1, 3, 3, 1, 8],
#                                  initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
#                                  dtype=tf.float32)
#     conv2_2 = tf.nn.conv3d(conv_2_max_pool, filter=filter_2_2, strides=[1, 1, 1, 1, 1],
#                            dilations=[1, 1, 2, 1, 1],
#                            padding="SAME", name="convolution_2_2")
#     conv_2_c = tf.concat([conv2_1, conv2_2], axis=-1)
#
#     filter_2_reduce = tf.get_variable('filter_2_reduce', [1, 1, 1, 16, 1],
#                                       initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
#                                       dtype=tf.float32)
#     conv_2_reduce = tf.nn.conv3d(conv_2_c, filter=filter_2_reduce, strides=[1, 1, 1, 1, 1],
#                                  dilations=[1, 1, 1, 1, 1],
#                                  padding="VALID", name="convolution_2_reduce")
#
#     conv_2_reduce = tf_print_if(conv_2_reduce, [tf.shape(conv_2_reduce)], summarize=10, message="Conv_block_2: ",
#                                 verbose=verbose_tensors)
#
#     return conv_2_reduce
#
# def _block_6(self, x, hparams):
#   with tf.variable_scope("Conv_block_6"):
#     # We should end up with 1 value per token.
#     filter_6 = tf.get_variable('filter_6', [1, 1, 1, x.get_shape()[-1].value, 1],
#                                initializer=tf.truncated_normal_initializer(stddev=5e-2, dtype=tf.float32),
#                                dtype=tf.float32)
#     conv_6 = tf.nn.conv3d(x, filter=filter_6, strides=[1, 1, 1, 1, 1],
#                           padding="SAME", name="convolution_6")
#     conv_6 = tf_print_if(conv_6, [tf.shape(conv_6)], summarize=10, message="Conv_block_6: ",
#                          verbose=verbose_tensors)
#   return conv_6
