"""Utility functions for building models."""
from __future__ import print_function

import time

import tensorflow as tf

from .utils import misc_utils as utils

__all__ = [
  "get_device_str", "create_emb_for_encoder_and_decoder",
  "create_or_load_model",  # "load_model"
]


def get_device_str(first_device_id, device_id, num_gpus):
  """Return a device string for multi-GPU setup."""

  if num_gpus == 0:
    return "/cpu:0"
  device_str_output = "/gpu:%d" % (first_device_id + (device_id % num_gpus))
  return device_str_output


def create_emb_for_encoder_and_decoder(share_vocab,
                                       src_vocab_size,
                                       tgt_vocab_size,
                                       src_embed_size,
                                       tgt_embed_size,
                                       dtype=tf.float32,
                                       num_partitions=0,
                                       train_embeddings=True,
                                       scope=None,
                                       decode=True):
  """Create embedding matrix for both encoder and decoder.

  Args:
    share_vocab: A boolean. Whether to share embedding matrix for both
      encoder and decoder.
    src_vocab_size: An integer. The source vocab size.
    tgt_vocab_size: An integer. The target vocab size.
    src_embed_size: An integer. The embedding dimension for the encoder's
      embedding.
    tgt_embed_size: An integer. The embedding dimension for the decoder's
      embedding.
    dtype: dtype of the embedding matrix. Default to float32.
    num_partitions: number of partitions used for the embedding vars.
    scope: VariableScope for the created subgraph. Default to "embedding".
    decode: A bool. Whether to create embeddings for decoder

  Returns:
    embedding_encoder: Encoder's embedding matrix.
    embedding_decoder: Decoder's embedding matrix.

  Raises:
    ValueError: if use share_vocab but source and target have different vocab
      size.
  """

  if num_partitions <= 1:
    partitioner = None
  else:
    # Note: num_partitions > 1 is required for distributed training due to
    # embedding_lookup tries to colocate single partition-ed embedding variable
    # with lookup ops. This may cause embedding variables being placed on worker
    # jobs.
    partitioner = tf.fixed_size_partitioner(num_partitions if num_partitions >= 2 else 2)

  print("scope: ", scope)
  with utils.cond_scope(scope):  # only set variable scope if scope not None
    with tf.name_scope("embeddings") as scope:  # scope or # dtype=dtype, partitioner=partitioner,
      # Share embedding
      if share_vocab:
        if src_vocab_size != tgt_vocab_size:
          raise ValueError("Share embedding but different src/tgt vocab sizes"
                           " %d vs. %d" % (src_vocab_size, tgt_vocab_size))
        utils.print_out("# Use the same source embeddings for target")
        embedding = tf.get_variable(
          "embedding_share", [src_vocab_size, src_embed_size], dtype, trainable=train_embeddings, partitioner=partitioner)
        embedding_encoder = embedding
        if decode:
          embedding_decoder = embedding
      else:
        with tf.name_scope("encoder"):  #
          embedding_encoder = tf.get_variable(
            "embedding_encoder", [src_vocab_size, src_embed_size], dtype, trainable=train_embeddings, partitioner=partitioner)

        if decode:
          with tf.name_scope("decoder"):  # partitioner=partitioner
            embedding_decoder = tf.get_variable(
              "embedding_decoder", [tgt_vocab_size, tgt_embed_size], dtype, trainable=train_embeddings, partitioner=partitioner)

  if decode:
    return embedding_encoder, embedding_decoder
  else:
    return embedding_encoder


def load_model(model, ckpt, session, name):
  start_time = time.time()
  # model.saver.restore(session, ckpt)
  # session.run(tf.tables_initializer())
  utils.print_out(
    "  loaded %s model parameters from %s, time %.2fs" %
    (name, ckpt, time.time() - start_time))
  return model


def create_or_load_model(model, model_dir, session, name):
  """Create translation model and initialize or load parameters in session."""

  latest_ckpt = tf.train.latest_checkpoint(model_dir)
  if latest_ckpt:
    # model = load_model(model, latest_ckpt, session, name)
    print("Loaded model!!")
  else:
    start_time = time.time()
    # session.run(tf.global_variables_initializer())
    # session.run(tf.tables_initializer())
    utils.print_out("  created %s model with fresh parameters, time %.2fs" %
                    (name, time.time() - start_time))

  global_step = tf.train.global_step(session, model.global_step)
  return model, global_step
