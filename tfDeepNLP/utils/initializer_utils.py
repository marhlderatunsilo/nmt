"""Utility functions for dealing with initialization."""
from __future__ import print_function

import tensorflow as tf

from tfDeepNLP.initializers.conditional_initializer import CondInitializer

__all__ = [
  "get_initializer"
]


def get_initializer(init_op, seed=None, init_weight=None):
  """Create an initializer. init_weight is only for uniform."""
  if init_op == "orthogonal":
    return tf.orthogonal_initializer(seed=seed)
  elif init_op == "uniform":
    assert init_weight
    return tf.random_uniform_initializer(
      -init_weight, init_weight, seed=seed)
  elif init_op == "glorot_normal":
    return tf.contrib.keras.initializers.glorot_normal(
      seed=seed)
  elif init_op == "glorot_uniform":
    return tf.contrib.keras.initializers.glorot_uniform(
      seed=seed)
  elif init_op == "orthogonal+glorot_normal":
    return CondInitializer(lambda shape, dtype: len(shape) == 2,
                           tf.orthogonal_initializer(),
                           tf.glorot_normal_initializer())
  elif init_op == "orthogonal+glorot_uniform":
    return CondInitializer(lambda shape, dtype: len(shape) == 2,
                           tf.orthogonal_initializer(),
                           tf.glorot_uniform_initializer())
  else:
    raise ValueError("Unknown init_op %s" % init_op)
