import tensorflow as tf


class EvaluateMetricsMultilabelClassification(object):

  def __init__(self, targets, predictions, pred_probabilities):
    """
    Metrics for evaluating multilabel classification.

    Based on https://en.wikipedia.org/wiki/Multi-label_classification#Statistics_and_evaluation_metrics


    :param targets: target labels k-hot encoded
    :param predictions: predicted labels k-hot encoded
    :param pred_probabilities: predicted logits
    """

    self.targets = tf.squeeze(targets) if len(targets.get_shape()) > 2 else targets
    self.predictions = tf.squeeze(predictions) if len(predictions.get_shape()) > 2 else predictions
    self.pred_probabilities = tf.squeeze(pred_probabilities) if len(
      pred_probabilities.get_shape()) > 2 else pred_probabilities

    # For avoiding zero-division
    self.__epsilon = tf.constant(1.0e-6, name="epsilon")

    self.__dynamic_batch_size = tf.to_float(tf.shape(self.targets)[0])
    self.__num_classes = tf.to_float(tf.shape(self.targets)[-1])

    self.__num_correctly_classified_labels = tf.count_nonzero(self.targets * self.predictions, dtype=tf.float32)
    self.__num_differences = tf.count_nonzero(self.targets - self.predictions, dtype=tf.float32)
    # self.__num_false_negatives = tf.count_nonzero(tf.maximum(self.targets - self.predictions, 0), dtype=tf.float32)

    self.__num_positive_predictions = tf.count_nonzero(self.predictions, dtype=tf.float32)
    self.__num_positive_targets = tf.count_nonzero(self.targets, dtype=tf.float32)  # same as tp + fn

    self.__precision = tf.div(self.__num_correctly_classified_labels, self.__num_positive_predictions + self.__epsilon)
    self.__recall = tf.div(self.__num_correctly_classified_labels, self.__num_positive_targets + self.__epsilon)
    self.__f1 = tf.div(2 * self.__precision * self.__recall, self.__precision + self.__recall + self.__epsilon)

    # Get class indices for predicted and targets
    # def get_indices_of_positives(t):
    #   zero = tf.constant(0, dtype=tf.float32)
    #   return tf.where(tf.not_equal(t, zero))

    print("predictions: ", self.predictions)
    # predicted_labels = get_indices_of_positives(self.predictions)
    # true_labels = get_indices_of_positives(self.targets)

    # predicted_labels = tf.Print(predicted_labels, [tf.reduce_sum(self.predictions), tf.reduce_sum(self.targets)],
    #                             message="OSTEHAPS: ")

    # print("predicted_labels: ", predicted_labels)
    # print("true_labels: ", true_labels)

    # Exact match
    # percentage of samples that have all their labels classified correctly
    self.__num_exact_matches = tf.reduce_sum(tf.to_float(tf.reduce_all(tf.equal(predictions, targets), axis=-1)))
    self.__exact_match = tf.div(self.__num_exact_matches, self.__dynamic_batch_size)

    # Hamming loss
    # Based on scikit learn implementation. TODO Check that this is right.

    self.__hamming_loss = tf.div(self.__num_differences, tf.to_float(self.__dynamic_batch_size * self.__num_classes))

  @property
  def precision(self):
    """
    Gets precision.
    :return: Precision, tf.float32.
    """
    if self.__precision is None:
      raise ValueError("precision is None.")
    return self.__precision

  @property
  def recall(self):
    """
    Gets recall.
    :return: Recall, tf.float32.
    """
    if self.__recall is None:
      raise ValueError("recall is None.")
    return self.__recall

  @property
  def sensitivity(self):
    """
    Gets sensitivity.
    :return: Sensitivity, tf.float32.
    """
    if self.__recall is None:
      raise ValueError("sensitivity is None.")
    return self.__recall

  @property
  def f1(self):
    """
    Gets f1 score.
    :return: f1 score, tf.float32.
    """
    if self.__f1 is None:
      raise ValueError("f1 is None.")
    return self.__f1

  @property
  def exact_match(self):
    """
    Gets exact match, i.e. percentage of samples that have all their labels classified correctly
    :return: exact match percentage, tf.float32.
    """
    if self.__exact_match is None:
      raise ValueError("exact match is None.")
    return self.__exact_match

  @property
  def hamming_loss(self):
    """
    Gets hamming loss.
    Loss function, so lower is better.
    :return: hamming loss, tf.float32.
    """
    if self.__hamming_loss is None:
      raise ValueError("hamming loss is None.")
    return self.__hamming_loss

  @property
  def positive_predictions(self):
    """
    Gets number of positive predictions.
    :return: Number of positive predictions, tf.float32.
    """
    if self.__num_positive_predictions is None:
      raise ValueError("positive predictions is None.")
    return self.__num_positive_predictions

  @property
  def true_positives(self):
    """
    Gets number of positives in target.
    :return: Number of positives in target, tf.float32.
    """
    if self.__num_positive_targets is None:
      raise ValueError("true positives is None.")
    return self.__num_positive_targets
