""" Utility functions for distributed training"""
# https://medium.com/goodailab/how-to-write-distributed-tensorflow-code-with-an-example-on-tensorport-70bf3306adcb
# http://henning.kropponline.de/2017/03/19/distributing-tensorflow/
import tensorflow as tf
from ..utils import misc_utils as utils

def device_and_target(flags):
    """
    Defines the master, ClusterSpecs and device setters

    :param flags:
    :return: device function, server.target:
    """
    # If FLAGS.job_name is not set, we're running single-machine TensorFlow.
    # Don't set a device.
    if flags.job_name is None:
        print("Running single-machine training")
        return None, ""

    # Otherwise we're running distributed TensorFlow.
    print("Running distributed training")
    if flags.task_index is None or flags.task_index == "":
        raise ValueError("Must specify an explicit 'task_index'")
    if flags.ps_hosts is None or flags.ps_hosts == "":
        raise ValueError("Must specify an explicit 'ps_hosts'")
    if flags.worker_hosts is None or flags.worker_hosts == "":
        raise ValueError("Must specify an explicit 'worker_hosts'")

    cluster_spec = tf.train.ClusterSpec({
        "ps": flags.ps_hosts.split(","),
        "worker": flags.worker_hosts.split(","),
    })

    config_proto = utils.get_config_proto(job_name=flags.job_name)

    server = tf.train.Server(
        cluster_spec, job_name=flags.job_name, task_index=flags.task_index, config=config_proto,
        #protocol='grpc+mpi'
    )

    print("JOB NAME: ", flags.job_name)
    print("TASK INDEX: ", flags.task_index)

    if flags.job_name == "ps":
        server.join()

    worker_device = "/job:worker/task:{}".format(flags.task_index)
    print("worker_device: ", worker_device)
    # The device setter will automatically place Variables ops on separate
    # parameter servers (ps). The non-Variable ops will be placed on the workers.

    load_fn = tf.contrib.training.byte_size_load_fn
    greedy = tf.contrib.training.GreedyLoadBalancingStrategy(len(flags.ps_hosts.split(",")), load_fn)  # TODO Which params?

    device = tf.train.replica_device_setter(
        ps_device="/job:ps/cpu:0",
        worker_device=worker_device,
        cluster=cluster_spec,
        ps_strategy=greedy
        # the value of ps_tasks is derived from cluster
    )

    return device, server.target
