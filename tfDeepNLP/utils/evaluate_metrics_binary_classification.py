import tensorflow as tf
from tensorflow.python.framework import dtypes
from tensorflow.python.framework import ops
from tensorflow.python.ops import array_ops
from tensorflow.python.ops import check_ops
from tensorflow.python.ops import math_ops
from tensorflow.python.ops import state_ops
from tensorflow.python.ops import weights_broadcast_ops
from tensorflow.python.ops.metrics_impl import _remove_squeezable_dimensions, metric_variable


class EvaluateMetricsBinaryClassification(object):
  def __init__(self, targets, predictions, pred_probabilities, auc_curve='ROC', auc_summation_method='trapezoidal'):
    """
    Metrics for evaluating binary classification.
    :param targets: target classes
    :param predictions: binary predictions
    :param pred_probabilities: predicted logits
    """

    self.targets = tf.cond(tf.rank(targets) > 1,
                           lambda: tf.squeeze(targets),
                           lambda: targets)
    self.predictions = tf.cond(tf.rank(predictions) > 1,
                               lambda: tf.squeeze(predictions),
                               lambda: predictions)
    self.pred_probabilities = tf.cond(tf.rank(pred_probabilities) > 1,
                                      lambda: tf.squeeze(pred_probabilities),
                                      lambda: pred_probabilities)

    # For avoiding zero-division
    self.epsilon = tf.constant(1.0e-6, name="epsilon")

    # Batch stats
    # __class_ratio is the number of elements with class 2 in targets, divided by the total number of targets
    self.__class_ratio = tf.div(tf.count_nonzero(self.targets, dtype=tf.float32) + self.epsilon,
                                tf.cast(tf.size(self.targets), dtype=tf.float32))

    # Counts of true positives, true negatives, false positives and false negatives.
    self.__tp = tf.count_nonzero(self.predictions * self.targets, dtype=tf.float32)
    self.__tn = tf.count_nonzero((self.predictions - 1) * (self.targets - 1), dtype=tf.float32)
    self.__fp = tf.count_nonzero(self.predictions * (self.targets - 1), dtype=tf.float32)
    self.__fn = tf.count_nonzero((self.predictions - 1) * self.targets, dtype=tf.float32)
    self.__total_positives = self.__tp + self.__fp
    self.__total_negatives = self.__fp + self.__fn

    # Calculate accuracy, precision, recall and F1 score w/m.
    self.__accuracy = tf.div(self.__tp + self.__tn, self.__tp + self.__fp + self.__fn + self.__tn + self.epsilon)
    self.__correct = self.__tp + self.__tn
    self.__wrong = self.__fp + self.__fn
    self.__precision = tf.div(self.__tp, self.__tp + self.__fp + self.epsilon)
    self.__recall = tf.div(self.__tp, self.__tp + self.__fn + self.epsilon)
    self.__specificity = tf.div(self.__tn, self.__tn + self.__fp + self.epsilon)
    self.__false_positive_rate = tf.div(self.__fp, self.__fp + self.__tn + self.epsilon)
    self.__false_negative_rate = tf.div(self.__fn, self.__tp + self.__fn + self.epsilon)
    self.__f1 = (2 * self.__precision * self.__recall) / (self.__precision + self.__recall + self.epsilon)
    self.__auc = self._auc(curve=auc_curve, summation_method=auc_summation_method)

  def _auc(self, curve='ROC', summation_method='trapezoidal', num_thresholds=200, name="compute_AUC",
           weights=None):
    """
    Computes the roc-auc or pr-auc based on confusion counts.
    Based on https://github.com/tensorflow/tensorflow/blob/r1.4/tensorflow/python/ops/metrics_impl.py

    :param curve: Specifies the name of the curve to be computed, 'ROC' [default] or
      'PR' for the Precision-Recall-curve.
    :param summation_method:  Specifies the Riemann summation method used, 'trapezoidal'
      [default] that applies the trapezoidal rule, 'minoring' that applies
      left summation for increasing intervals and right summation for decreasing
      intervals or 'majoring' that applies the opposite.
    :param num_thresholds: The number of thresholds to use when discretizing the roc curve.
    :param name: An optional variable_scope name.
    :param weights: Optional `Tensor` whose rank is either 0, or the same rank as
      `labels`, and must be broadcastable to `labels` (i.e., all dimensions must
      be either `1`, or the same as the corresponding `labels` dimension).
    :return: AUC (Area under the Curve)
    """

    with tf.variable_scope(
        name, 'auc', (self.targets, self.pred_probabilities, weights)):
      if curve != 'ROC' and curve != 'PR':
        raise ValueError('curve must be either ROC or PR, %s unknown' % curve)
      kepsilon = 1e-7  # to account for floating point imprecisions
      thresholds = [(i + 1) * 1.0 / (num_thresholds - 1)
                    for i in range(num_thresholds - 2)]
      thresholds = [0.0 - kepsilon] + thresholds + [1.0 + kepsilon]

      values = self._confusion_matrix_at_thresholds_non_streaming(
        self.targets, self.pred_probabilities, thresholds, weights)

    def _compute_auc(tp, fn, tn, fp, name):
      """Computes the roc-auc or pr-auc based on confusion counts."""
      rec = math_ops.div(tp + self.epsilon, tp + fn + self.epsilon)
      if curve == 'ROC':
        fp_rate = math_ops.div(fp, fp + tn + self.epsilon)
        x = fp_rate
        y = rec
      else:  # curve == 'PR'.
        prec = math_ops.div(tp + self.epsilon, tp + fp + self.epsilon)
        x = rec
        y = prec
      if summation_method == 'trapezoidal':
        return math_ops.reduce_sum(
          math_ops.multiply(x[:num_thresholds - 1] - x[1:],
                            (y[:num_thresholds - 1] + y[1:]) / 2.),
          name=name)
      elif summation_method == 'minoring':
        return math_ops.reduce_sum(
          math_ops.multiply(x[:num_thresholds - 1] - x[1:],
                            math_ops.minimum(y[:num_thresholds - 1], y[1:])),
          name=name)
      elif summation_method == 'majoring':
        return math_ops.reduce_sum(
          math_ops.multiply(x[:num_thresholds - 1] - x[1:],
                            math_ops.maximum(y[:num_thresholds - 1], y[1:])),
          name=name)
      else:
        raise ValueError('Invalid summation_method: %s' % summation_method)

    # sum up the areas of all the trapeziums
    return _compute_auc(values['tp'], values['fn'], values['tn'], values['fp'], 'value')

  @staticmethod
  def _confusion_matrix_at_thresholds_non_streaming(
      labels, predictions, thresholds, weights=None, includes=None):
    """Computes true_positives, false_negatives, true_negatives, false_positives.

    ***
    None-streaming version of
    https://github.com/tensorflow/tensorflow/blob/r1.4/tensorflow/python/ops/metrics_impl.py
    ***

    This function creates up to four local variables, `true_positives`,
    `true_negatives`, `false_positives` and `false_negatives`.
    `true_positive[i]` is defined as the total weight of values in `predictions`
    above `thresholds[i]` whose corresponding entry in `labels` is `True`.
    `false_negatives[i]` is defined as the total weight of values in `predictions`
    at most `thresholds[i]` whose corresponding entry in `labels` is `True`.
    `true_negatives[i]` is defined as the total weight of values in `predictions`
    at most `thresholds[i]` whose corresponding entry in `labels` is `False`.
    `false_positives[i]` is defined as the total weight of values in `predictions`
    above `thresholds[i]` whose corresponding entry in `labels` is `False`.
    For estimation of these metrics over a stream of data, for each metric the
    function respectively creates an `update_op` operation that updates the
    variable and returns its value.
    If `weights` is `None`, weights default to 1. Use weights of 0 to mask values.
    Args:
      labels: A `Tensor` whose shape matches `predictions`. Will be cast to
        `bool`.
      predictions: A floating point `Tensor` of arbitrary shape and whose values
        are in the range `[0, 1]`.
      thresholds: A python list or tuple of float thresholds in `[0, 1]`.
      weights: Optional `Tensor` whose rank is either 0, or the same rank as
        `labels`, and must be broadcastable to `labels` (i.e., all dimensions must
        be either `1`, or the same as the corresponding `labels` dimension).
      includes: Tuple of keys to return, from 'tp', 'fn', 'tn', fp'. If `None`,
          default to all four.
    Returns:
      values: Dict of variables of shape `[len(thresholds)]`. Keys are from
          `includes`.
      update_ops: Dict of operations that increments the `values`. Keys are from
          `includes`.
    Raises:
      ValueError: If `predictions` and `labels` have mismatched shapes, or if
        `weights` is not `None` and its shape doesn't match `predictions`, or if
        `includes` contains invalid keys.
    """
    all_includes = ('tp', 'fn', 'tn', 'fp')
    if includes is None:
      includes = all_includes
    else:
      for include in includes:
        if include not in all_includes:
          raise ValueError('Invalid key: %s.' % include)

    with ops.control_dependencies([
      check_ops.assert_greater_equal(
        predictions,
        math_ops.cast(0.0, dtype=predictions.dtype),
        message='predictions must be in [0, 1]'),
      check_ops.assert_less_equal(
        predictions,
        math_ops.cast(1.0, dtype=predictions.dtype),
        message='predictions must be in [0, 1]')
    ]):
      predictions, labels, weights = _remove_squeezable_dimensions(
        predictions=math_ops.to_float(predictions),
        labels=math_ops.cast(labels, dtype=dtypes.bool),
        weights=weights)

    num_thresholds = len(thresholds)

    # Reshape predictions and labels.
    predictions_2d = array_ops.reshape(predictions, [-1, 1])
    labels_2d = array_ops.reshape(
      math_ops.cast(labels, dtype=dtypes.bool), [1, -1])

    # Use static shape if known.
    num_predictions = predictions_2d.get_shape().as_list()[0]

    # Otherwise use dynamic shape.
    if num_predictions is None:
      num_predictions = array_ops.shape(predictions_2d)[0]
    thresh_tiled = array_ops.tile(
      array_ops.expand_dims(array_ops.constant(thresholds), [1]),
      array_ops.stack([1, num_predictions]))

    # Tile the predictions after thresholding them across different thresholds.
    pred_is_pos = math_ops.greater(
      array_ops.tile(array_ops.transpose(predictions_2d), [num_thresholds, 1]),
      thresh_tiled)
    if ('fn' in includes) or ('tn' in includes):
      pred_is_neg = math_ops.logical_not(pred_is_pos)

    # Tile labels by number of thresholds
    label_is_pos = array_ops.tile(labels_2d, [num_thresholds, 1])
    if ('fp' in includes) or ('tn' in includes):
      label_is_neg = math_ops.logical_not(label_is_pos)

    if weights is not None:
      weights = weights_broadcast_ops.broadcast_weights(
        math_ops.to_float(weights), predictions)
      weights_tiled = array_ops.tile(array_ops.reshape(
        weights, [1, -1]), [num_thresholds, 1])
      thresh_tiled.get_shape().assert_is_compatible_with(
        weights_tiled.get_shape())
    else:
      weights_tiled = None

    values = {}

    if 'tp' in includes:
      true_p = metric_variable([num_thresholds], dtypes.float32, name='true_positives')
      is_true_positive = math_ops.to_float(
        math_ops.logical_and(label_is_pos, pred_is_pos))
      if weights_tiled is not None:
        is_true_positive *= weights_tiled
      values['tp'] = state_ops.assign(
        true_p, math_ops.reduce_sum(is_true_positive, 1))

    if 'fn' in includes:
      false_n = metric_variable([num_thresholds], dtypes.float32, name='false_negatives')
      is_false_negative = math_ops.to_float(
        math_ops.logical_and(label_is_pos, pred_is_neg))
      if weights_tiled is not None:
        is_false_negative *= weights_tiled
      values['fn'] = state_ops.assign(
        false_n, math_ops.reduce_sum(is_false_negative, 1))

    if 'tn' in includes:
      true_n = metric_variable([num_thresholds], dtypes.float32, name='true_negatives')
      is_true_negative = math_ops.to_float(
        math_ops.logical_and(label_is_neg, pred_is_neg))
      if weights_tiled is not None:
        is_true_negative *= weights_tiled
      values['tn'] = state_ops.assign(
        true_n, math_ops.reduce_sum(is_true_negative, 1))

    if 'fp' in includes:
      false_p = false_p = metric_variable([num_thresholds], dtypes.float32, name='false_positives')
      is_false_positive = math_ops.to_float(
        math_ops.logical_and(label_is_neg, pred_is_pos))
      if weights_tiled is not None:
        is_false_positive *= weights_tiled
      values['fp'] = state_ops.assign(
        false_p, math_ops.reduce_sum(is_false_positive, 1))

    return values

  @property
  def tp(self):
    """
    Gets count of true positives.
    :return: Count of true positives, tf.float32.
    """
    if self.__tp is None:
      raise ValueError("tp is None.")
    return self.__tp

  @property
  def fp(self):
    """
    Gets count of false positives.
    :return: Count of false positives, tf.float32.
    """
    if self.__fp is None:
      raise ValueError("fp is None.")
    return self.__fp

  @property
  def tn(self):
    """
    Gets count of true negatives.
    :return: Count of true negatives, tf.float32.
    """

    if self.__tn is None:
      raise ValueError("tn is None.")
    return self.__tn

  @property
  def fn(self):
    """
    Gets count of false negatives.
    :return: Count of false negatives, tf.float32.
    """

    if self.__fn is None:
      raise ValueError("fn is None.")
    return self.__fn

  @property
  def total_positives(self):
    """
    Gets count of total positives.
    :return: Count of total positives, tf.float32.
    """

    if self.__total_positives is None:
      raise ValueError("total_positives is None.")
    return self.__total_positives

  @property
  def total_negatives(self):
    """
    Gets count of total negatives.
    :return: Count of total negatives, tf.float32.
    """
    if self.__total_negatives is None:
      raise ValueError("total_negatives is None.")
    return self.__total_negatives

  @property
  def correct(self):
    """
    Gets count of correct predictions.
    :return: Count of correct predictions, tf.float32.
    """
    if self.__correct is None:
      raise ValueError("correct is None.")

    return self.__correct

  @property
  def wrong(self):
    """
    Gets count of wrong predictions.
    :return: Count of wrong predictions, tf.float32.
    """

    if self.__wrong is None:
      raise ValueError("wrong is None.")
    return self.__wrong

  @property
  def accuracy(self):
    """
    Gets accuracy.
    :return: Accuracy, tf.float32.
    """
    if self.__accuracy is None:
      raise ValueError("accuracy is None.")
    return self.__accuracy

  @property
  def precision(self):
    """
    Gets precision.
    :return: Precision, tf.float32.
    """
    if self.__precision is None:
      raise ValueError("precision is None.")
    return self.__precision

  @property
  def recall(self):
    """
    Gets recall.
    :return: Recall, tf.float32.
    """
    if self.__recall is None:
      raise ValueError("recall is None.")
    return self.__recall

  @property
  def sensitivity(self):
    """
    Gets sensitivity.
    :return: Sensitivity, tf.float32.
    """
    if self.__recall is None:
      raise ValueError("sensitivity is None.")
    return self.__recall

  @property
  def specificity(self):
    """
    Gets specificity.
    :return: specificity, tf.float32.
    """
    if self.__specificity is None:
      raise ValueError("specificity is None.")
    return self.__specificity

  @property
  def true_positive_rate(self):
    """
    Gets True Positive Rate.
    :return: True Positive Rate, tf.float32.
    """
    if self.__recall is None:
      raise ValueError("true_positive_rate is None.")
    return self.__recall

  @property
  def true_negative_rate(self):
    """
    Gets True Negative Rate.
    :return: True Negative Rate, tf.float32.
    """
    if self.__specificity is None:
      raise ValueError("specificity is None.")
    return self.__specificity

  @property
  def false_positive_rate(self):
    """
    Gets False Positive Rate.
    :return: False Positive Rate, tf.float32.
    """
    if self.__false_positive_rate is None:
      raise ValueError("false_positive_rate is None.")
    return self.__false_positive_rate

  @property
  def false_negative_rate(self):
    """
    Gets False Negative Rate.
    :return: False Negatuve Rate, tf.float32.
    """
    if self.__false_negative_rate is None:
      raise ValueError("false_negative_rate is None.")
    return self.__false_negative_rate

  @property
  def f1(self):
    """
    Gets f1 score.
    :return: f1 score, tf.float32.
    """
    if self.__f1 is None:
      raise ValueError("f1 is None.")
    return self.__f1

  @property
  def auc(self):
    """
    Gets Area under the Curve.
    :return: AUC (Area under the Curve), tf.float32.
    """
    if self.__auc is None:
      raise ValueError("auc is None.")
    return self.__auc

  @property
  def class_ratio(self):
    """
    Gets the ratio of class 2 elements.
    Calculated as number of class 2 targets / total number of targets.
    :return: Class 2 ratio, tf.float32.
    """
    if self.__class_ratio is None:
      raise ValueError("class_ratio is None.")
    return self.__class_ratio
