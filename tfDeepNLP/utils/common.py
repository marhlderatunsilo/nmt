import logging
import time
import sys
import os
import shutil
import yaml
import numpy as np
from tensorflow.contrib.training import HParams
import argparse


class YParams(HParams):
    def __init__(self, yaml_fn, config_name=''):
        super().__init__()
        with open(yaml_fn) as fp:
          if config_name:
            for k, v in yaml.load(fp)[config_name].items():
              self.add_hparam(k, v)
          else:
            for k, v in yaml.load(fp).items():
              self.add_hparam(k, v)

    def update_param(self, args, ignore_keywords=['yaml']):
        assert type(args) in [argparse.Namespace, type(self)]
        for k, v in vars(args).items():
          if k.startswith('_'):
            continue
          update_this_key = True
          for kw in ignore_keywords:
            if kw in k:  # filter out some cmd line params that are not arguments for program, eg: config files
              update_this_key = False
              continue
          if update_this_key and (v is not None):
            if self.get(k) is None:
              self.add_hparam(k, v)
            else:
              self.set_hparam(k, v)


def create_empty_folder(filepath):
    if os.path.exists(filepath):
      shutil.rmtree(filepath)
    os.mkdir(filepath)


def get_day_time():
    tm = str(time.localtime().tm_mon)
    td = str(time.localtime().tm_mday)
    if len(tm) < 2:
        tm = '0' + tm
    if len(td) < 2:
        td = '0' + td
    return tm + td


def add_file_handler(logger, filename):
    fh = logging.FileHandler(filename, mode='w')
    fh.setLevel(logging.INFO)
    fh.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(message)s'))
    logger.addHandler(fh)


def get_logger(filename='', mode='w', name='logger'):
    l = logging.getLogger(name)
    l.setLevel(logging.DEBUG)
    if filename:
      fh = logging.FileHandler(filename, mode=mode)
      fh.setLevel(logging.INFO)
      fh.setFormatter(logging.Formatter('%(asctime)s: %(message)s'))
      l.addHandler(fh)
    ch = logging.StreamHandler()
    ch.setLevel(logging.INFO)
    ch.setFormatter(logging.Formatter('%(asctime)s:%(levelname)s:%(message)s'))
    l.addHandler(ch)
    return l


def get_json_logger(filename, mode='w', name='json_logger'):
    l = logging.getLogger(name)
    l.setLevel(logging.INFO)
    fh = logging.FileHandler(filename, mode=mode)
    fh.setLevel(logging.INFO)
    fh.setFormatter(logging.Formatter('%(message)s'))
    l.addHandler(fh)
    return l


class Progbar(object):
    """Progbar class copied from keras (https://github.com/fchollet/keras/)

    Displays a progress bar.
    Small edit : added strict arg to update
    # Arguments
        target: Total number of steps expected.
        interval: Minimum visual progress update interval (in seconds).
    """

    def __init__(self, target, width=30, verbose=1):
        self.width = width
        self.target = target
        self.sum_values = {}
        self.unique_values = []
        self.start = time.time()
        self.total_width = 0
        self.seen_so_far = 0
        self.verbose = verbose

    def update(self, current, values=[], exact=[], strict=[]):
        """
        Updates the progress bar.
        # Arguments
            current: Index of current step.
            values: List of tuples (name, value_for_last_step).
                The progress bar will display averages for these values.
            exact: List of tuples (name, value_for_last_step).
                The progress bar will display these values directly.
        """

        for k, v in values:
            if k not in self.sum_values:
                self.sum_values[k] = [v * (current - self.seen_so_far), current - self.seen_so_far]
                self.unique_values.append(k)
            else:
                self.sum_values[k][0] += v * (current - self.seen_so_far)
                self.sum_values[k][1] += (current - self.seen_so_far)
        for k, v in exact:
            if k not in self.sum_values:
                self.unique_values.append(k)
            self.sum_values[k] = [v, 1]

        for k, v in strict:
            if k not in self.sum_values:
                self.unique_values.append(k)
            self.sum_values[k] = v

        self.seen_so_far = current

        now = time.time()
        if self.verbose == 1:
            prev_total_width = self.total_width
            sys.stdout.write("\b" * prev_total_width)
            sys.stdout.write("\r")

            numdigits = int(np.floor(np.log10(self.target))) + 1
            barstr = '%%%dd/%%%dd [' % (numdigits, numdigits)
            bar = barstr % (current, self.target)
            prog = float(current) / self.target
            prog_width = int(self.width * prog)
            if prog_width > 0:
                bar += ('=' * (prog_width - 1))
                if current < self.target:
                    bar += '>'
                else:
                    bar += '='
            bar += ('.' * (self.width - prog_width))
            bar += ']'
            sys.stdout.write(bar)
            self.total_width = len(bar)

            if current:
                time_per_unit = (now - self.start) / current
            else:
                time_per_unit = 0
            eta = time_per_unit * (self.target - current)
            info = ''
            if current < self.target:
                info += ' - ETA: %ds' % eta
            else:
                info += ' - %ds' % (now - self.start)
            for k in self.unique_values:
                if type(self.sum_values[k]) is list:
                    info += ' - %s: %.4f' % (k, self.sum_values[k][0] / max(1, self.sum_values[k][1]))
                else:
                    info += ' - %s: %s' % (k, self.sum_values[k])

            self.total_width += len(info)
            if prev_total_width > self.total_width:
                info += ((prev_total_width - self.total_width) * " ")

            sys.stdout.write(info)
            sys.stdout.flush()

            if current >= self.target:
                sys.stdout.write("\n")

        if self.verbose == 2:
            if current >= self.target:
                info = '%ds' % (now - self.start)
                for k in self.unique_values:
                    info += ' - %s: %.4f' % (k, self.sum_values[k][0] / max(1, self.sum_values[k][1]))
                sys.stdout.write(info + "\n")

    def add(self, n, values=[]):
        self.update(self.seen_so_far + n, values)