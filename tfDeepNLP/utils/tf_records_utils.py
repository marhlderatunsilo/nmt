import tensorflow as tf
import numpy as np

def create_tfr_writer(f_name, out_dir=None, options=None):
  """
  Creates TFRecordWriter.

  Note: Remember to close after use, with writer.close().

  Example
  -------

  writer = create_tfr_writer('my_data.tfrecords')
  for i in range(num_examples):
    example = tf.train.Example(features=tf.train.Features(feature={
      'feat_1': int64_array_feature(feature_1[i]),
      'label': int64_feature(labels[i])}))
    writer.write(example.SerializeToString())
  writer.close()

  :param f_name: Desired name of TFRecords output file. Is appended to self.out_dir.
                 Must have file extension ".tfrecords".
  :param out_dir: path to output directory. If None, file is written in working directory.
  :param options: A TFRecordOptions object (optional).
  :return: TFRecordWriter
  """
  assert f_name[-10:] == '.tfrecords', "file extension must be .tfrecords"
  if out_dir:
    assert isinstance(out_dir, str), "out_dir must be string or None."
    file_path = out_dir + f_name
  else:
    file_path = f_name
  return tf.python_io.TFRecordWriter(file_path, options=options)

def int64_feature(value):
  value = int(value)
  return tf.train.Feature(int64_list=tf.train.Int64List(value=[value]))

def int64_array_feature(array):
  array = list(np.int64(array))
  return tf.train.Feature(int64_list=tf.train.Int64List(value=array))

def int64_2darray_feature(array):
  array = list(np.int64(array).reshape([-1]))
  return tf.train.Feature(int64_list=tf.train.Int64List(value=array))

def float_feature(array):
  array = np.float32(array)
  return tf.train.Feature(float_list=tf.train.FloatList(value=array))

def float_2darray_feature(array):
  array = list(np.float32(array).reshape([-1]))
  return tf.train.Feature(float_list=tf.train.FloatList(value=array))

def bytes_feature(value):
  value = tf.compat.as_bytes(value)
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=[value]))

def bytes_array_feature(array):
  array = [tf.compat.as_bytes(elem) for elem in array]
  return tf.train.Feature(bytes_list=tf.train.BytesList(value=array))