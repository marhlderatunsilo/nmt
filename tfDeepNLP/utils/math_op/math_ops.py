from tensorflow.python.framework import dtypes, ops
from tensorflow.python.ops import gen_math_ops
from tensorflow.python.ops.math_ops import cast, reduce_sum, to_int64


def count_non_negative(input_tensor,
                  axis=None,
                  keep_dims=False,
                  dtype=dtypes.int64,
                  name=None,
                  reduction_indices=None):
  """Computes number of nonzero elements across dimensions of a tensor.
  Reduces `input_tensor` along the dimensions given in `axis`.
  Unless `keep_dims` is true, the rank of the tensor is reduced by 1 for each
  entry in `axis`. If `keep_dims` is true, the reduced dimensions
  are retained with length 1.
  If `axis` has no entries, all dimensions are reduced, and a
  tensor with a single element is returned.
  **NOTE** Floating point comparison to zero is done by exact floating point
  equality check.  Small values are **not** rounded to zero for purposes of
  the nonzero check.
  For example:
  ```python
  # 'x' is [[0, 1, 0]
  #         [1, 1, 0]]
  tf.count_nonzero(x) ==> 3
  tf.count_nonzero(x, 0) ==> [1, 2, 0]
  tf.count_nonzero(x, 1) ==> [1, 2]
  tf.count_nonzero(x, 1, keep_dims=True) ==> [[1], [2]]
  tf.count_nonzero(x, [0, 1]) ==> 3
  ```
  Args:
    input_tensor: The tensor to reduce. Should be of numeric type, or `bool`.
    axis: The dimensions to reduce. If `None` (the default),
      reduces all dimensions.
    keep_dims: If true, retains reduced dimensions with length 1.
    dtype: The output dtype; defaults to `tf.int64`.
    name: A name for the operation (optional).
    reduction_indices: The old (deprecated) name for axis.
  Returns:
    The reduced tensor (number of non-negative values).
  """
  with ops.name_scope(name, "count_non_negative", [input_tensor]):
    input_tensor = ops.convert_to_tensor(input_tensor, name="input_tensor")
    zero = input_tensor.dtype.as_numpy_dtype()
    return cast(
        reduce_sum(
            # int64 reduction happens on GPU
            to_int64(gen_math_ops.less_equal(zero, input_tensor)),
            axis=axis,
            keep_dims=keep_dims,
            reduction_indices=reduction_indices),
        dtype=dtype)