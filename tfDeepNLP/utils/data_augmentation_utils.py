import tensorflow as tf

from tfDeepNLP.utils.misc_utils import cond_scope

__ALL__ = ["create_noise_dataset", "embedding_augmentation"]


def create_noise_dataset(mean, stddev, vector_size, buffer_size, scope=None):
  with cond_scope(scope):
    return tf.data.Dataset.from_tensor_slices(
      (tf.random_normal([vector_size], mean=mean, stddev=stddev))).repeat().shuffle(
      buffer_size=buffer_size)


def embedding_augmentation(embeddings, noise_amount_mean=0.03, noise_amount_stddev=0.01, scope=None):
  """
  Adds noise to embeddings.
  Adapts the scale of the noise to each dimension.

  :param embeddings: tensor, float.
  :param noise_amount_mean: mean amount of noise to add, float scalar.
  :param noise_amount_stddev: standard deviation of the noise amount (truncated to 2 stddev.), float scalar.
  :param scope: scope.
  :return: embeddings with noise added/subtracted.
  """
  with cond_scope(scope):
    # Random noise percentages (-1.0 -> 1.0)
    noise = tf.truncated_normal([embeddings.get_shape()[-1].value], mean=0, stddev=0.5)
    # Random amount of noise to add (noise_amount_mean-2*noise_amount_stddev -> noise_amount_mean+2*noise_amount_stddev)
    amount_ = tf.truncated_normal([1], mean=noise_amount_mean, stddev=noise_amount_stddev)
    return embeddings + embeddings * noise * amount_
