"""plot learning rate finder"""
import os
import sys


def parse_log_file(filename):
    str_identifier = ':INFO:learning_rate = '
    kv = {}
    with open(filename) as fin:
      for line in fin:
        if str_identifier in line:
          for seg in line.split('INFO:')[1].split('(')[0].split(','):
            k, v = seg.strip().split('=')
            try:
              kv[k.strip()].append(float(v.strip()))
            except:
              kv[k.strip()] = [float(v.strip())]
    return kv


def collect_plot(path, pdf_name, in_one_folder, skip_start=4):
    import matplotlib.pyplot as plt
    plt.switch_backend('agg')
    from matplotlib.backends.backend_pdf import PdfPages
    if in_one_folder:
      config_filename = {p.replace('log_lrfinder_', ''): os.path.join(path, p) for p in os.listdir(path) if 'lrfinder' in p}
    else:
      config_filename = {p.replace('lrfinder_', ''): os.path.join(path, p, 'log_lrfinder') for p in os.listdir(path) if 'lrfinder' in p}
    pp = PdfPages(pdf_name)
    for config, filename in config_filename.items():
      kv = parse_log_file(filename)
      plt.figure()
      plt.subplot(211)
      plt.plot(kv["step"][skip_start:], kv["learning_rate"][skip_start:])
      plt.yscale('log')
      plt.xlabel("step")
      plt.ylabel("learning_rate")
      plt.title(config)
      plt.subplot(212)
      plt.plot(kv["learning_rate"][skip_start:], kv["loss"][skip_start:])
      plt.xscale('log')
      plt.xlabel("learning_rate")
      plt.ylabel("loss")
      pp.savefig()
    pp.close()


if __name__ == '__main__':
  path = sys.argv[1]
  pdf_name = os.path.join(path, sys.argv[2])
  collect_plot(path, pdf_name, False)