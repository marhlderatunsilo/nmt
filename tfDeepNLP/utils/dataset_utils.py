class DatasetPartitioner(object):
  def __init__(self, dataset, number_of_partitions, partitions_to_include):
    self.dataset = dataset
    self.number_of_partitions = number_of_partitions
    self.components_to_include = set(partitions_to_include)

  def __iter__(self):
    for component_idx, dataset_sample in enumerate(self.dataset):

      if (component_idx % self.number_of_partitions) not in self.components_to_include:
        continue

      yield dataset_sample


class DatasetCombiner(object):

  def __init__(self, datasets):
    self.datasets = datasets

  def __iter__(self):
    for dataset in self.datasets:
      for dataset_sample in dataset:
        yield dataset_sample
