"""Utility functions for calculating loss/cost."""
from __future__ import print_function

import time

import tensorflow as tf

from tfDeepNLP.utils import misc_utils as utils

__all__ = [
  "compute_perplexity", "compute_softmax_cross_entropy_loss"
]


def compute_softmax_cross_entropy_loss(targets, logits, batch_size, multilabel=False, label_smoothing=None,
                                       name='compute_softmax_cross_entropy', label_counts=None):
  """
  Compute cross entropy loss for logits, normalized by batch size.
  Notice: softmax is computed on logits.

  :param targets: targets with index of class starting from 0.
  :param logits: logits, float.
  :param batch_size: batch size, int.
  :param multilabel: bool.
    True => sigmoid cross-entropy
    False => softmax cross-entropy
  :param name: Name for cross entropy op.
  :return: Cross entropy loss.
  """
  if multilabel:

    # def create_dense_target(targets_label_counts):
    #   targets, label_counts = targets_label_counts
    #
    #   sparse_target = dense_to_sparse(targets[:label_counts])
    #
    #   dense_target = tf.sparse_to_indicator(sparse_target, vocab_size=logits.get_shape()[-1].value)
    #   dense_target = tf.to_float(dense_target)
    #
    #   return dense_target
    #
    # dense_targets = tf.map_fn(fn=create_dense_target, elems=(targets, label_counts), dtype=tf.float32,
    #                         parallel_iterations=32)

    print("dense_targets: ", targets)
    print("logits: ", logits)

    normalized_dense_targets = targets / tf.expand_dims(tf.to_float(label_counts), axis=1)
    normalized_dense_targets = tf.stop_gradient(normalized_dense_targets)

    if label_smoothing and label_smoothing > 0:
      label_smoothing = tf.constant(label_smoothing)
      num_classes = tf.cast(
        tf.shape(normalized_dense_targets)[-1], logits.dtype)
      smooth_positives = 1.0 - label_smoothing
      smooth_negatives = label_smoothing / num_classes
      normalized_dense_targets = normalized_dense_targets * smooth_positives + smooth_negatives

    crossent = tf.nn.softmax_cross_entropy_with_logits_v2(labels=normalized_dense_targets, logits=logits, name=name)

  else:
    crossent = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=targets, logits=logits, name=name)
  loss = tf.reduce_sum(crossent) / tf.to_float(batch_size)
  return loss


def compute_sigmoid_cross_entropy_loss(targets, logits, batch_size, name='compute_sigmoid_cross_entropy'):
  """
  Compute cross entropy loss for logits, normalized by batch size.
  Notice: softmax is computed on logits.

  :param targets: targets with index of class starting from 0.
  :param logits: logits, float.
  :param batch_size: batch size, int.
  :param name: Name for cross entropy op.
  :return: Cross entropy loss.
  """

  crossent = tf.nn.sigmoid_cross_entropy_with_logits(labels=targets, logits=logits, name=name)
  loss = tf.reduce_sum(crossent) / tf.to_float(batch_size)
  return loss


def compute_perplexity(models, sess, name, feed_dict=None, max_elements=None):
  """Compute perplexity of the output of the model.

  Args:
    model: model for compute perplexity.
    sess: tensorflow session to use.
    name: name of the batch.

  Returns:
    The perplexity of the eval outputs.
  """

  if not isinstance(models, list):
    models = [models]

  total_loss = 0
  total_predict_count = 0
  start_time = time.time()

  while True:
    try:

      # print("tuple(zip([model.eval_ops for model in models])): ", zip(*[model.eval_ops for model in models]))
      losses, predict_counts, batch_sizes = zip(*[model.eval_ops for model in models])

      if feed_dict:
        losses, predict_counts, batch_sizes = sess.run((losses, predict_counts, batch_sizes),
                                                       feed_dict=feed_dict)  # model.eval_ops  .eval(sess, feed_dict)
      else:
        losses, predict_counts, batch_sizes = sess.run((losses, predict_counts, batch_sizes))

      loss, predict_count, batch_size = sum(losses), sum(predict_counts), sum(batch_sizes)

      total_loss += loss * batch_size
      total_predict_count += predict_count

      if max_elements is not None and total_predict_count >= max_elements:
        break
    except tf.errors.OutOfRangeError:
      break

  perplexity = utils.safe_exp(total_loss / total_predict_count)
  utils.print_time("  eval %s: perplexity %.2f" % (name, perplexity),
                   start_time)
  return perplexity
