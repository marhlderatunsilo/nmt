from tensorflow.python.training import session_run_hook
import tensorflow as tf

class AssignEmbeddingsFnHook(session_run_hook.SessionRunHook):
  """Runs `feed_fn` and sets the `feed_dict` accordingly."""

  def __init__(self, hparams, assign_embeddings_fn, vocab_size, target_embeddings_variable_getter):
    """Initializes a `FeedFnHook`.
    Args:
      feed_fn: function that takes two arguments 'session', 'coord' and returns nothing
    """
    self.hparams = hparams
    self.assign_embeddings_fn = assign_embeddings_fn
    self.vocab_size = vocab_size
    self.global_step_tensor = None
    self.target_embeddings_variable_getter = target_embeddings_variable_getter

  def begin(self):

    #if self.global_step_tensor is None:
    distribution_strategy = tf.contrib.distribute.get_distribution_strategy()
    #with distribution_strategy.scope():
    self.global_step_tensor = distribution_strategy.read_var(tf.train.get_or_create_global_step())

    self.embeddings_placeholder = tf.placeholder(dtype=tf.float32,
                                                 shape=[self.vocab_size, self.hparams.token_embedding_size])

    self.assign_embeddings = distribution_strategy.broadcast(self.embeddings_placeholder, self.target_embeddings_variable_getter())
    self.assign_embeddings = distribution_strategy.unwrap(self.assign_embeddings)

  def after_create_session(self, session, coord):

    global_step = session.run(self.global_step_tensor)

    if global_step <= 0:
      self.assign_embeddings_fn(self, session, coord)