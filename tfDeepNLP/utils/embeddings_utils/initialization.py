import numpy as np

def initialize_word_emb_from_numpy_array(hparams, pretrained_numpy_embeddings, dim_word, nwords):
  embedding = np.zeros((nwords, dim_word), dtype=np.float32)
  if pretrained_numpy_embeddings is not None:
    nrow = pretrained_numpy_embeddings.shape[0]
    embedding[:nrow] = pretrained_numpy_embeddings
    for i in range(nrow, nwords):
      embedding[i] = np.random.normal(0, 0.1, dim_word)
  print("# init ", embedding.shape)
  print("# init ", embedding[0][:6])
  print("# init ", embedding[-1][:6])
  return embedding


