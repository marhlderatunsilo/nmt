import tensorflow as tf


def split_token_string_by_token(tokens_vector, split_token, consume_extra_token=False):
  tokens_ta = tf.TensorArray(dtype=tf.string, size=tf.size(tokens_vector), dynamic_size=False)
  tokens_ta = tokens_ta.unstack(tokens_vector)

  partitioning_ta = tf.TensorArray(dtype=tf.int32, size=0, dynamic_size=True, element_shape=[])
  substrings_ta = tf.TensorArray(dtype=tf.string, size=0, dynamic_size=True, element_shape=[None], infer_shape=False)

  i = tf.constant(0)
  cur_substring_number = tf.constant(0)
  loop_vars = (i, cur_substring_number, tokens_ta, partitioning_ta)

  cond = lambda i, _, __, ___: i < tokens_ta.size()

  def loop_fn(i, substring_number, tokens_ta, partitioning_ta):
    partitioning_ta = partitioning_ta.write(i, substring_number)
    token = tokens_ta.read(i)

    return i + 1, substring_number + tf.to_int32(tf.equal(token, split_token)), tokens_ta, partitioning_ta

  loop_vars = tf.while_loop(cond=cond, loop_vars=loop_vars, body=loop_fn, name="split_token_string_by_token_while_loop", back_prop=False)

  partitioning = loop_vars[3].stack()

  if consume_extra_token:
    org_partitioning = partitioning
    partitioning = partitioning - tf.one_hot(indices=tf.size(partitioning) - 1, depth=tf.size(partitioning),
                                                  dtype=tf.int32)

    # To fix cases where there is only one token
    partitioning = tf.maximum(partitioning, 0)

    #partitioning = tf.Print(partitioning, [partitioning, org_partitioning], summarize=2000,
    #                         message="initial partitioning: ")

  substring_lengths = tf.bincount(partitioning)
  substrings_ta = substrings_ta.split(tokens_vector, substring_lengths)

  return substrings_ta, substring_lengths


def pad_stack_token_ta(substrings_ta, substring_lengths):
  max_substring_length = tf.reduce_max(substring_lengths)
  substrings_padded_ta = tf.TensorArray(dtype=tf.string, size=0, dynamic_size=True)

  i = tf.constant(0)
  loop_vars = (i, substrings_ta, substrings_padded_ta)

  cond = lambda i, _, __: i < substrings_ta.size()

  def loop_fn(i, substrings_ta, substrings_padded_ta):
    substring = substrings_ta.read(i)
    substrings_padded_ta = substrings_padded_ta.write(i, tf.concat(
      [substring, tf.fill([max_substring_length - tf.size(substring)], " ")], axis=0))

    return i + 1, substrings_ta, substrings_padded_ta

  loop_vars = tf.while_loop(cond=cond, loop_vars=loop_vars, body=loop_fn)
  substrings_padded_ta = loop_vars[2]
  padded_substrings = substrings_padded_ta.stack()

  # result = tf.reduce_join(padded_substrings, axis=1, separator=" ")

  # padded_substrings = tf.Print(padded_substrings, [substring_lengths, tf.shape(padded_substrings)],
  #                              message="substring_lengths: ", summarize=100)

  return padded_substrings
