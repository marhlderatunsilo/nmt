# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Utility to handle vocabularies."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import codecs
import os

import tensorflow as tf
from tensorflow.python.ops import lookup_ops

from tfDeepNLP.utils import misc_utils as utils

UNK = "<unk>"
SOS = "<d>"
EOS = "</d>"
UNK_ID = 0


def check_and_copy_vocab(vocab_file, out_dir, check_special_chars=True, sos=None, eos=None, unk=None):
  """Check if vocab_file doesn't exist, create from corpus_file."""
  if tf.gfile.Exists(vocab_file):
    utils.print_out("# Vocab file %s exists" % vocab_file)
    vocab = []
    with codecs.getreader("utf-8")(tf.gfile.GFile(vocab_file, "rb")) as f:
      vocab_size = 0
      for word in f:
        vocab_size += 1
        vocab.append(word.strip())

    # Verify if the vocab starts with unk, sos, eos
    # If not, prepend those tokens & generate a new vocab file
    if not unk: unk = UNK
    if not sos: sos = SOS
    if not eos: eos = EOS

    if check_special_chars:
      assert len(vocab) >= 3
    if check_special_chars and (vocab[0] != unk or vocab[1] != sos or vocab[2] != eos):
      utils.print_out("The first 3 vocab words [%s, %s, %s]"
                      " are not [%s, %s, %s]" %
                      (vocab[0], vocab[1], vocab[2], unk, sos, eos))
      vocab = [unk, sos, eos] + vocab
      vocab_size += 3
      new_vocab_file = os.path.join(out_dir, os.path.basename(vocab_file))
      with codecs.getwriter("utf-8")(
          tf.gfile.GFile(new_vocab_file, "wb")) as f:
        for word in vocab:
          f.write("%s\n" % word)
      vocab_file = new_vocab_file
    else:
      new_vocab_file = os.path.join(out_dir, os.path.basename(vocab_file))
      with codecs.getwriter("utf-8")(
          tf.gfile.GFile(new_vocab_file, "wb")) as f:
        for word in vocab:
          f.write("%s\n" % word)
      vocab_file = new_vocab_file
  else:
    raise ValueError("vocab_file '%s' does not exist." % vocab_file)

  vocab_size = len(vocab)
  return vocab_size, vocab_file


def create_vocab_tables(src_vocab_file, tgt_vocab_file, share_vocab, create_python_vocabs=False):
  """Creates vocab tables for src_vocab_file and tgt_vocab_file."""
  src_vocab_table = lookup_ops.index_table_from_file(
    src_vocab_file, default_value=UNK_ID)
  if share_vocab:
    tgt_vocab_table = src_vocab_table
  else:
    tgt_vocab_table = lookup_ops.index_table_from_file(
      tgt_vocab_file, default_value=UNK_ID)

  if create_python_vocabs:

    py_src_vocab_table = create_python_vocab_table(src_vocab_file)

    if share_vocab:
      py_tgt_vocab_table = py_src_vocab_table
    else:
      py_tgt_vocab_table = create_python_vocab_table(tgt_vocab_table)

    return src_vocab_table, tgt_vocab_table, py_src_vocab_table, py_tgt_vocab_table
  else:
    return src_vocab_table, tgt_vocab_table


def create_python_vocab_table(vocab_file):
  py_vocab = {}
  with open(vocab_file, encoding="utf-8") as fileobj:
    for vocab_index, line in enumerate(fileobj):
      py_vocab[line.strip()] = vocab_index
  return py_vocab

def create_reverse_python_vocab_table(vocab_file):
  reverse_py_vocab = []
  with open(vocab_file, encoding="utf-8") as fileobj:
    for line in fileobj:
      reverse_py_vocab.append(line.strip())
  return reverse_py_vocab


def create_vocab_table(vocab_file):
  """Creates vocab table from a single vocab_file.
     The file is first loaded into a list -> tensor constant
     in order to make the vocab saved as a part of the graph
     and not as an op that requires the presence of a file.
  """

  vocab_as_list = []

  with open(vocab_file, encoding="utf-8") as fileobj:
    for vocab_index, line in enumerate(fileobj):
      vocab_as_list.append(line[:-1])

  return lookup_ops.index_table_from_tensor(tf.constant(vocab_as_list), default_value=UNK_ID,
                                            name="index_vocab_table_from_file")
  # index_table_from_file(vocab_file_path_op, default_value=UNK_ID, name="index_vocab_table_from_file")


def check_vocab_table(vocab_table, vocab_file, n_special_tags=3):
  utils.print_out("# Testing that all tokens in %s exist in vocab table" % vocab_file)
  if tf.gfile.Exists(vocab_file):
    vocab = []
    with codecs.getreader("utf-8")(tf.gfile.GFile(vocab_file, "rb")) as f:
      vocab_size = 0
      for word in f:
        vocab_size += 1
        vocab.append(word.strip())
  else:
    raise IOError("Could not find vocab file.")
  vocab = tf.convert_to_tensor(vocab[(n_special_tags - 1):])

  ids = tf.cast(vocab_table.lookup(vocab), tf.int32)
  special_ids_tf = tf.range(n_special_tags, dtype=tf.int32)

  intersection = tf.sets.set_intersection([ids], [special_ids_tf])

  def get_bad_indices(ids, n_special_tags):

    for i in range(n_special_tags):
      faulty_indices = tf.where(tf.equal(ids, tf.constant(i, name="special_id_{}".format(i))),
                                name="faulty_indices_{}".format(i))
      faulty_indices = tf.Print(faulty_indices, [faulty_indices],
                                # This should tell us the bad indices for the specific tag
                                message="Indices with id {}".format(i))
      if i == 0:
        faulty_indices_list = faulty_indices
      else:
        faulty_indices_list = tf.concat([faulty_indices_list, faulty_indices], axis=0)
    return faulty_indices_list

  with tf.control_dependencies([tf.Assert(tf.size(intersection) == 0,
                                          [get_bad_indices(ids, n_special_tags)],
                                          summarize=50)]):
    return vocab_table
