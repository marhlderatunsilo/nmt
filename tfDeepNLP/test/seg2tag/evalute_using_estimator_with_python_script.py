from tfDeepNLP.models.seq2tag.utils import get_segments, exact_match_stat
import unittest


class TestUtils(unittest.TestCase):

    def test_get_segments(self):
        self.assertEqual(get_segments(list('1100000000111'), '1', '1'), [[0, 2], [10, 13]])
        self.assertEqual(get_segments(list('001100000000111'), '1', '1'), [[2, 4], [12, 15]])
        self.assertEqual(get_segments(list('001100000000111'), '1', '2'), [])
        self.assertEqual(get_segments(list('0011200000000111'), '1', '2'), [[2, 5]])
        self.assertEqual(get_segments(list('00112000000001112'), '1', '2'), [[2, 5], [13, 17]])
        self.assertEqual(get_segments(list('1120000000011120'), '1', '2'), [[0, 3], [11, 15]])
        self.assertEqual(get_segments(list('1121200000011212'), '1', '2'), [[0, 3], [3, 5], [11, 14], [14, 16]])
        self.assertEqual(get_segments(list('12212200000012112'), '1', '2'), [[0, 3], [3, 6], [12, 14], [14, 17]])

    def test_exact_match_stat(self):
        self.assertEqual(exact_match_stat(list('12212200012112'), list('12012200012112'), '1', '2')[:3], (3, 1, 1))
        self.assertEqual(exact_match_stat(list('12012200012112'), list('12212200012112'), '1', '2')[:3], (3, 1, 1))


if __name__ == '__main__':
    unittest.main()