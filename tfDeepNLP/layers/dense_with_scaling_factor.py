import tensorflow as tf
from tensorflow.python.layers import core as layers_core

__all__ = ["dense_layer_with_scaling_factor", "create_scaling_factor"]


def dense_layer_with_scaling_factor(inputs, units, name, activation_fn=None, use_bias=True, sf_min=0.2, sf_max=2):
  """
  Creates dense layer multiplied by scaling factor.
  Scaling factor is a learned scalar between sf_min and sf_max, initialized to 1.0.

  Seems to improve training speed.

  :param inputs: Input
  :param units: Number of units in dense layer
  :param name: Name of layer. Needed to created unique scaling factor variable.
  :param activation_fn: Activation function.
  :param use_bias: Whether to add a bias to the dense layer.
  :param sf_min: Min value in truncation of scaling factor. Should be above 0.0.
  :param sf_max: Max value in truncation of scaling factor.
  :return:
  """

  assert sf_min > 0.0, "sf_min should be above 0.0."
  assert sf_min < sf_max, "sf_min should be less than sf_max."

  normal_dense_layer = layers_core.dense(
    inputs=inputs,
    units=units,
    activation=None,
    use_bias=use_bias,
    name=name + "_dense",
  )

  sf = create_scaling_factor(name=name, sf_min=sf_min, sf_max=sf_max)

  scaled_layer = normal_dense_layer * sf

  if activation_fn:
    return activation_fn(scaled_layer)
  else:
    return scaled_layer


def create_scaling_factor(name, sf_min=0.2, sf_max=2):
  with tf.variable_scope('scaling_factor'):
    s = tf.get_variable(name + "_scaling_factor", initializer=1.0)
    s_truncated = tf.clip_by_value(s, clip_value_min=sf_min, clip_value_max=sf_max,
                                   name=name + "_scaling_factor_truncated")
  return s_truncated
